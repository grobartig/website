<?php
Route::get('{locale}/home', 			            [ 'as' => 'home',			            'uses' => 'HomeController@index']);
Route::get('{locale}/property', 			        [ 'as' => 'property',					'uses' => 'PropertyController@index']);
Route::get('{locale}/property-detail/{slug}', 		[ 'as' => 'property-detail',			'uses' => 'PropertyController@detail']);
Route::get('{locale}/our-service', 					[ 'as' => 'our-service',				'uses' => 'OurServiceController@index']);
Route::get('{locale}/new', 							[ 'as' => 'new',						'uses' => 'NewController@index']);
Route::get('{locale}/about', 						[ 'as' => 'about',						'uses' => 'AboutController@index']);
Route::get('{locale}/our-team', 					[ 'as' => 'our-team',					'uses' => 'OurTeamController@index']);
Route::get('{locale}/contact', 						[ 'as' => 'contact',					'uses' => 'ContactController@index']);
// Route::get('{locale}/property-detail', 			    [ 'as' => 'property-details',			'uses' => 'PropertyDetailController@index']);
Route::put('{locale}/sendmessage', 					[ 'as' => 'sendmessage',			        'uses' => 'HomeController@sendMessage']);

//Get District And Commnue Data
Route::get('{locale}/get-districts-by-province-id', ['as' => 'property-districts', 	'uses' => 'PropertyController@districts']);
Route::get('{locale}/get-communes-by-province-id', 	['as' => 'communes', 			'uses' => 'PropertyController@communes']);
Route::get('{locale}/result-property', 		        [ 'as' => 'search-property',	'uses' => 'PropertyController@search']);

//404
Route::get('{locale}/404', 		[ 'as' => '404',			'uses' => 'NotFoundController@index']);