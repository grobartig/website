<?php

Route::get('/', 								['as' => 'index', 			'uses' => 'ContactsController@index']);
	Route::get('/{id}', 						['as' => 'edit', 			'uses' => 'ContactsController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'ContactsController@update']);
	Route::get('/create', 						['as' => 'create', 			'uses' => 'ContactsController@create']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'ContactsController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'ContactsController@trash']);
	Route::post('update-status', 				['as' => 'update-status', 	'uses' => 'ContactsController@updateStatus']);
	Route::post('update-delete-status', 		['as' => 'update-delete-status', 	'uses' => 'ContactsController@updateDeletedStatus']);
	Route::delete('/delete/{id}', 				['as' => 'delete', 			'uses' => 'ContactsController@delete']);
