<?php 
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> File
Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'FileController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'FileController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'FileController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'FileController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'FileController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'FileController@trash']);
});	