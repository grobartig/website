<?php

Route::get('/', 							['as' => 'index', 			'uses' => 'WelcomeController@index']);
	Route::get('/{id}', 						['as' => 'edit', 			'uses' => 'WelcomeController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'WelcomeController@update']);
	Route::get('/create', 						['as' => 'create', 			'uses' => 'WelcomeController@create']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'WelcomeController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'WelcomeController@trash']);
	Route::post('update-status', 				['as' => 'update-status', 	'uses' => 'WelcomeController@updateStatus']);
	Route::post('update-delete-status', 				['as' => 'update-delete-status', 	'uses' => 'WelcomeController@updateDeletedStatus']);
	Route::delete('/delete/{id}', 						['as' => 'delete', 			'uses' => 'WelcomeController@delete']);
