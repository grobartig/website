<?php

//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Accounts
Route::group(['as' => 'account.', 'prefix' => 'account'], function () {
	Route::get('/', 				['as' => 'index', 		'uses' => 'AccountController@index']);
	Route::get('/create', 			['as' => 'create', 		'uses' => 'AccountController@create']);
	Route::put('/', 				['as' => 'store', 		'uses' => 'AccountController@store']);
	Route::get('/{id}', 			['as' => 'edit', 		'uses' => 'AccountController@edit']);
	Route::post('/', 				['as' => 'update', 		'uses' => 'AccountController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 		'uses' => 'AccountController@trash']);
	
	Route::get('/{id}/groups', 						['as' => 'groups', 		'uses' => 'AccountController@groups']);
	Route::put('/create-account-group', 			['as' => 'create-account-group', 			'uses' => 'AccountController@createAccountGroup']);
	Route::post('/add-accounts-to-group', 			['as' => 'add-accounts-to-group', 			'uses' => 'AccountController@addAccountsToGruop']);
	Route::delete('{id}/remove-group/{group_id}', 	['as' => 'remove-group', 		'uses' => 'AccountController@removeGroup']);
	Route::get('search-groups', 					['as' => 'search-group', 		'uses' => 'AccountController@searchGroup']);
	Route::get('selected-groups', 					['as' => 'selected-group', 		'uses' => 'AccountController@selectedGroup']);
	Route::delete('remove-group-from-account', 		['as' => 'remove-group-from-account', 		'uses' => 'AccountController@removeGroupFromAccount']);
	Route::put('add-group-to-account', 				['as' => 'add-group-to-account', 		'uses' => 'AccountController@addGroupToAccount']);
	
	Route::get('/{id}/properties', 						['as' => 'properties', 		'uses' => 'AccountController@properties']);
});
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> List of Properties
Route::group(['as' => 'list.', 'prefix' => 'list'], function () {
	Route::get('/', 				['as' => 'index', 		'uses' => 'ListController@index']);
	Route::get('/create', 			['as' => 'create', 		'uses' => 'ListController@create']);
	Route::put('/', 				['as' => 'store', 		'uses' => 'ListController@store']);
	Route::get('/{id}', 			['as' => 'edit', 		'uses' => 'ListController@edit']);
	Route::post('/', 				['as' => 'update', 		'uses' => 'ListController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 		'uses' => 'ListController@trash']);
	Route::get('/{id}/properties', 	['as' => 'properties', 		'uses' => 'ListController@properties']);
	
	Route::delete('{id}/remove-property/{property_id}', 			['as' => 'remove-property', 		'uses' => 'ListController@removeProperty']);
	Route::get('search-properties', 			['as' => 'search-property', 		'uses' => 'ListController@searchProperty']);
	Route::get('selected-properties', 			['as' => 'selected-property', 		'uses' => 'ListController@selectedProperty']);
	Route::delete('remove-property-from-list', 	['as' => 'remove-property-from-list', 		'uses' => 'ListController@removePropertyFromList']);
	Route::put('add-property-to-list', 			['as' => 'add-property-to-list', 		'uses' => 'ListController@addPropertyToList']);
});
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Group
Route::group(['as' => 'group.', 'prefix' => 'group'], function () {
	Route::get('/', 				['as' => 'index', 		'uses' => 'GroupController@index']);
	Route::get('/create', 			['as' => 'create', 		'uses' => 'GroupController@create']);
	Route::put('/', 				['as' => 'store', 		'uses' => 'GroupController@store']);
	Route::get('/{id}', 			['as' => 'edit', 		'uses' => 'GroupController@edit']);
	Route::post('/', 				['as' => 'update', 		'uses' => 'GroupController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 		'uses' => 'GroupController@trash']);
	
	Route::get('/{id}/accounts', 	 					['as' => 'accounts', 					'uses' => 'GroupController@accounts']);
	Route::delete('{id}/remove-account/{account_group_id}', 	['as' => 'remove-account', 				'uses' => 'GroupController@removeAccount']);
	Route::get('search-groups', 						['as' => 'search-account', 				'uses' => 'GroupController@searchAccount']);
	Route::get('selected-groups', 						['as' => 'selected-account', 			'uses' => 'GroupController@selectedAccount']);
	Route::delete('remove-account-from-group', 			['as' => 'remove-account-from-group', 	'uses' => 'GroupController@removeAccountFromGroup']);
	Route::put('add-account-to-group', 					['as' => 'add-account-to-group', 		'uses' => 'GroupController@addAccountToGroup']);
});

//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Record
Route::group(['as' => 'record.', 'prefix' => 'record'], function () {
	Route::get('/', 				['as' => 'index', 		'uses' => 'RecordController@index']);
	Route::get('/create', 			['as' => 'create', 		'uses' => 'RecordController@create']);
	Route::put('/', 				['as' => 'store', 		'uses' => 'RecordController@store']);
	Route::get('/{id}', 			['as' => 'edit', 		'uses' => 'RecordController@edit']);
	Route::post('/', 				['as' => 'update', 		'uses' => 'RecordController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 		'uses' => 'RecordController@trash']);
	
	Route::put('/send-list-to-email-account', ['as' => 'send-list-to-email-account', 		'uses' => 'RecordController@sendListToAccount']);
	Route::post('/sent-done', ['as' => 'sent-done', 		'uses' => 'RecordController@sentDone']);
});