<?php 
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service
		Route::group(['as' => 'service.',  'prefix' => 'service'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'ServiceController@listData']);
			Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ServiceController@showEditForm']);
			Route::post('/', 				['as' => 'update', 			'uses' => 'ServiceController@update']);
			Route::get('/create', 			['as' => 'create', 			'uses' => 'ServiceController@showCreateForm']);
			Route::put('/', 				['as' => 'store', 			'uses' => 'ServiceController@store']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ServiceController@trash']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'ServiceController@updateStatus']);
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service
		Route::group(['as' => 'team.',  'prefix' => 'team'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'TeamController@listData']);
			Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TeamController@showEditForm']);
			Route::post('/', 				['as' => 'update', 			'uses' => 'TeamController@update']);
			Route::get('/create', 			['as' => 'create', 			'uses' => 'TeamController@showCreateForm']);
			Route::put('/', 				['as' => 'store', 			'uses' => 'TeamController@store']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TeamController@trash']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'TeamController@updateStatus']);
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Design
		Route::group(['as' => 'design.',  'prefix' => 'design'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'DesignController@listData']);
			Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'DesignController@showEditForm']);
			Route::post('/', 				['as' => 'update', 			'uses' => 'DesignController@update']);
			Route::get('/create', 			['as' => 'create', 			'uses' => 'DesignController@showCreateForm']);
			Route::put('/', 				['as' => 'store', 			'uses' => 'DesignController@store']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'DesignController@trash']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'DesignController@updateStatus']);
		});	

		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> News
		Route::group(['as' => 'news.',  'prefix' => 'news'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'NewsController@listData']);
			Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'NewsController@showEditForm']);
			Route::post('/', 				['as' => 'update', 			'uses' => 'NewsController@update']);
			Route::get('/create', 			['as' => 'create', 			'uses' => 'NewsController@showCreateForm']);
			Route::put('/', 				['as' => 'store', 			'uses' => 'NewsController@store']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'NewsController@trash']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'NewsController@updateStatus']);
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Client
		Route::group(['as' => 'client.',  'prefix' => 'client'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'ClientController@listData']);
			Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ClientController@showEditForm']);
			Route::post('/', 				['as' => 'update', 			'uses' => 'ClientController@update']);
			Route::get('/create', 			['as' => 'create', 			'uses' => 'ClientController@showCreateForm']);
			Route::put('/', 				['as' => 'store', 			'uses' => 'ClientController@store']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ClientController@trash']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'ClientController@updateStatus']);
		});		
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Why Choose Us
		Route::group(['as' => 'why-choose-us.',  'prefix' => 'why-choose-us'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'WhyChooseUsController@listData']);
			Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'WhyChooseUsController@showEditForm']);
			Route::post('/', 				['as' => 'update', 			'uses' => 'WhyChooseUsController@update']);
			Route::get('/create', 			['as' => 'create', 			'uses' => 'WhyChooseUsController@showCreateForm']);
			Route::put('/', 				['as' => 'store', 			'uses' => 'WhyChooseUsController@store']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'WhyChooseUsController@trash']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'WhyChooseUsController@updateStatus']);
		});

		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Page Contents
		Route::group(['as' => 'content.', 'prefix' => 'content'], function() {
			Route::get('list/{page}', 	['as' => 'list', 	'uses' => 'ContentsController@listData']);
			Route::get('/', 		    ['as' => 'edit', 	'uses' => 'ContentsController@showEditForm']);
			Route::post('/', 			['as' => 'update', 	'uses' => 'ContentsController@update']);
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Images
		Route::group(['as' => 'image.', 'prefix' => 'image'], function() {
			Route::get('list/{page}', 		['as' => 'list', 	'uses' => 'ImagesController@listData']);
			Route::get('/{slug}', 			['as' => 'edit', 	'uses' => 'ImagesController@showEditForm']);
			Route::post('/', 				['as' => 'update', 	'uses' => 'ImagesController@update']);
			Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'ImagesController@updateStatus']);
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Video
		Route::group(['as' => 'video.',  'prefix' => 'video'], function () {
			Route::get('/', 										['as' => 'list', 			'uses' => 'VideoController@listData']);
			Route::get('/{id}', 									['as' => 'edit', 			'uses' => 'VideoController@showEditForm']);
			Route::post('/', 										['as' => 'update', 			'uses' => 'VideoController@update']);
			Route::get('/create', 									['as' => 'create', 			'uses' => 'VideoController@showCreateForm']);
			Route::put('/', 										['as' => 'store', 			'uses' => 'VideoController@store']);
			Route::delete('/{id}', 									['as' => 'trash', 			'uses' => 'VideoController@trash']);
			Route::post('update-status', 							['as' => 'update-status', 	'uses' => 'VideoController@updateStatus']);
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Newsletter
		Route::group(['as' => 'newsletter.',  'prefix' => 'newsletter'], function () {
			Route::get('/', 				['as' => 'list', 			'uses' => 'NewsletterController@listData']);
			Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'NewsletterController@trash']);
		});		

		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Page Contents
		Route::group(['as' => 'company-file.', 'prefix' => 'company-file'], function() {
			Route::get('list/{page}', 	['as' => 'list', 	'uses' => 'CompanyFileController@listData']);
			Route::get('/{slug}', 		['as' => 'edit', 	'uses' => 'CompanyFileController@showEditForm']);
			Route::post('/', 			['as' => 'update', 	'uses' => 'CompanyFileController@update']);
		});