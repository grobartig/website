<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Owners

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'OwnerController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OwnerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'OwnerController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'OwnerController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'OwnerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'OwnerController@trash']);

	Route::get('{id}/properties', 		['as' => 'properties', 		'uses' => 'OwnerController@properties']);
	Route::post('{id}/properties-add', 			['as' => 'properties-add', 		'uses' => 'OwnerController@addProperty']);
	Route::delete('/{id}/properties-remove', 		['as' => 'properties-remove', 		'uses' => 'OwnerController@removeProperty']);
});	