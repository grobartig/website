<?php 
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Customer
Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'CustomerController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CustomerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CustomerController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CustomerController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'CustomerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CustomerController@trash']);
	
	Route::get('/{id}/notes', 				['as' => 'notes', 			'uses' => 'CustomerController@notes']);
	Route::get('/{id}/notes/create', 		['as' => 'create-note', 	'uses' => 'CustomerController@createNote']);
	Route::put('/notes/store', 				['as' => 'store-note', 		'uses' => 'CustomerController@storeNote']);
	Route::get('/{id}/notes/{note_id}', 	['as' => 'edit-note', 		'uses' => 'CustomerController@editNote']);
	Route::post('/{id}/notes/update', 		['as' => 'update-note', 	'uses' => 'CustomerController@updateNote']);
	Route::post('/{id}/notes/update-status',['as' => 'update-status', 	'uses' => 'CustomerController@updateNoteStatus']);
	Route::delete('notes/{id}', 			['as' => 'trash-note', 		'uses' => 'CustomerController@trashNote']);
	
	Route::get('/{id}/inquiries', 			['as' => 'inquiries', 		'uses' => 'CustomerController@inquiries']);
	Route::get('/{id}/inquiries-data', 			['as' => 'inquiries-data', 		'uses' => 'CustomerController@inquiriesData']);
	Route::post('/update-inquiries', 			['as' => 'update-inquiries', 		'uses' => 'CustomerController@inquiriesUpdate']);
	Route::post('/inquiries-store', 			['as' => 'inquiries-store', 		'uses' => 'CustomerController@inquiriesStore']);
	Route::delete('inquiries/{id}', 			['as' => 'trash-inquiries', 		'uses' => 'CustomerController@trashInquiry']);

	Route::get('/{id}/enquiries', 				['as' => 'enquiries', 			'uses' => 'CustomerController@enquiries']);
	Route::get('/{id}/enquiries/create', 		['as' => 'create-enquiry', 	'uses' => 'CustomerController@createEnquiry']);
	Route::put('/{/enquiries/store', 		['as' => 'store-enquiry', 		'uses' => 'CustomerController@storeEnquiry']);
	Route::get('/{id}/enquiries/{note_id}', 	['as' => 'edit-enquiry', 		'uses' => 'CustomerController@editEnquiry']);
	Route::post('/enquiries/update', 		['as' => 'update-enquiry', 	'uses' => 'CustomerController@updateEnquiry']);
	Route::delete('/{id}/enquiries', 			['as' => 'trash-enquiry', 		'uses' => 'CustomerController@trashEnquiry']);
	
	Route::get('/amenties', 		['as' => 'amenties', 			'uses' => 'CustomerController@amenties']);
	Route::get('/edit-amenties', 		['as' => 'edit-amenties', 			'uses' => 'CustomerController@editAmenties']);
	Route::get('/districts', 		['as' => 'districts', 			'uses' => 'CustomerController@districts']);
	Route::get('/communes', 		['as' => 'communes', 			'uses' => 'CustomerController@communes']);

	Route::get('/{id}/properties', 			['as' => 'properties', 		'uses' => 'CustomerController@properties']);
	Route::post('/properties-add', 			['as' => 'properties-add', 		'uses' => 'CustomerController@addProperty']);
	Route::delete('/{id}/properties-remove', 		['as' => 'properties-remove', 		'uses' => 'CustomerController@removeProperty']);
});	