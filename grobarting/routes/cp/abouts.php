<?php

Route::get('/', 							    ['as' => 'index', 			'uses' => 'AboutsController@index']);
	Route::get('/{id}', 						['as' => 'edit', 			'uses' => 'AboutsController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'AboutsController@update']);
	Route::get('/create', 						['as' => 'create', 			'uses' => 'AboutsController@create']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'AboutsController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'AboutsController@trash']);
	Route::post('update-status', 				['as' => 'update-status', 	'uses' => 'AboutsController@updateStatus']);
	Route::post('update-delete-status', 		['as' => 'update-delete-status', 	'uses' => 'AboutsController@updateDeletedStatus']);
	Route::delete('/delete/{id}', 				['as' => 'delete', 			'uses' => 'AboutsController@delete']);
