<?php

Route::get('/', 								['as' => 'index', 			'uses' => 'UrlController@index']);
	Route::get('/{id}', 						['as' => 'edit', 			'uses' => 'UrlController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'UrlController@update']);
	Route::get('/create', 						['as' => 'create', 			'uses' => 'UrlController@create']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'UrlController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'UrlController@trash']);
	Route::post('update-status', 				['as' => 'update-status', 	'uses' => 'UrlController@updateStatus']);
	Route::post('update-delete-status', 		['as' => 'update-delete-status', 	'uses' => 'UrlController@updateDeletedStatus']);
	Route::delete('/delete/{id}', 				['as' => 'delete', 			'uses' => 'UrlController@delete']);
