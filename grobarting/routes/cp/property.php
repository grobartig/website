<?php
//:::::::::::::::::::::::::::::::>>> Main
Route::get('/districts', 		['as' => 'districts', 			'uses' => 'MainController@districts']);
Route::get('/communes', 		['as' => 'communes', 			'uses' => 'MainController@communes']);

Route::get('/{id}/checking-role', function ($id){
	$route = 'user.property';
	if(checkRole($id, 'view-overview')){
		return redirect(route($route.'.property.edit', $id));
	}
	
	if(checkRole($id, 'view-about')){
		if(checkRole($id, 'view-price')){
			return redirect(route($route.'.about.price.index', $id));
		}
		if(checkRole($id, 'view-location')){
			return redirect(route($route.'.about.location.index', $id));
		}
		if(checkRole($id, 'view-picture')){
			return redirect(route($route.'.about.picture.index', $id));
		}
		if(checkRole($id, 'view-photo')){
			return redirect(route($route.'.about.photo.index', $id));
		}
		if(checkRole($id, 'view-image')){
			return redirect(route($route.'.about.image.index', $id));
		}
		if(checkRole($id, 'view-video')){
			return redirect(route($route.'.about.video.index', $id));
		}
		if(checkRole($id, 'view-detail')){
			return redirect(route($route.'.about.detail.index', $id));
		}
		if(checkRole($id, 'view-amenity')){
			return redirect(route($route.'.about.amenity.index', $id));
		}
	}
	if(checkRole($id, 'view-confidentail')){
		if(checkRole($id, 'view-owner')){
			return redirect(route($route.'.confidentail.owner.index', $id));
		}
		if(checkRole($id, 'view-customer')){
			return redirect(route($route.'.confidentail.customer.index', $id));
		}
	}
	if(checkRole($id, 'view-enquiries')){
		return redirect(route($route.'.enquiry.index', $id));
	}
	if(checkRole($id, 'view-tool')){
		if(checkRole($id, 'view-status')){
			return redirect(route($route.'.tool.status.index', $id));
		}
		if(checkRole($id, 'view-print')){
			return redirect(route($route.'.tool.print.index', $id));
		}
		if(checkRole($id, 'view-mailing')){
			//return redirect(route($route.'.tool.mailing.index', $id));
		}
		if(checkRole($id, 'view-listing')){
			return redirect(route($route.'.tool.listing.index', $id));
		}
		if(checkRole($id, 'view-facebook')){
			//return redirect(route($route.'.tool.facebook.index', $id));
		}
	}
	if(checkRole($id, 'view-staff')){
		return redirect(route($route.'.staff.index', $id));
	}
	
	echo "You do not have permision to access this property";

})->name('check-role');


//:::::::::::::::::::::::::::::::>>> Property
Route::group(['as' => 'property.'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'PropertyController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PropertyController@showEditForm']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'PropertyController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'PropertyController@showCreateForm']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'PropertyController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PropertyController@trash']);

	Route::get('/feature', 							['as' => 'feature', 						'uses' => 'PropertyController@feature']);
	Route::post('/re-order-feature', 				['as' => 're-order-feature', 				'uses' => 'PropertyController@reOrderFeature']);
	Route::post('remove-feature', 					['as' => 'remove-feature', 					'uses' => 'PropertyController@removeFeature']);
	Route::get('/slide', 							['as' => 'slide', 							'uses' => 'PropertyController@slide']);
	Route::post('/re-order-slide', 					['as' => 're-order-slide', 					'uses' => 'PropertyController@reOrderSlide']);
	Route::post('remove-slide', 					['as' => 'remove-slide', 					'uses' => 'PropertyController@removeSlide']);

	Route::get('/best-deal', 						['as' => 'best_deal', 						'uses' => 'PropertyController@bestDeal']);
	Route::post('/re-order-best-deal', 				['as' => 're-order-best-deal', 				'uses' => 'PropertyController@reOrderBestDeal']);
	Route::post('remove-best-deal', 				['as' => 'remove-best-deal', 				'uses' => 'PropertyController@removeBestDeal']);

	Route::get('/project-development', 				['as' => 'project_development', 			'uses' => 'PropertyController@projectDevelopment']);
	Route::post('/re-order-project-development', 	['as' => 're-order-project-development', 	'uses' => 'PropertyController@reOrderProjectDevelopment']);
	Route::post('remove-project-development', 				['as' => 'remove-project-development', 				'uses' => 'PropertyController@removeProjectDevelopment']);
	Route::get('/charges', 							['as' => 'charges', 						'uses' => 'PropertyController@charges']);
});

Route::group(['as' => 'confidentail.', 'namespace'=>'Confidentail'], function () {
	Route::get('/{id}/confidentail/redirect-url', function ($id){
		$route = 'user.property.confidentail';
		if(checkRole($id, 'view-owner')){
			return redirect(route($route.'.owner.index', $id));
		}
		if(checkRole($id, 'view-customer')){
			return redirect(route($route.'.customer.index', $id));
		}
		
		echo "You cannot access in confidentail tab";

	})->name('redirect-url');

	//:::::::::::::::::::::::::::::::>>> Owner
	Route::group(['as' => 'owner.'], function () {
		Route::get('/{id}/confidentail/owner', 			        	['as' => 'index', 			'uses' => 'OwnerController@index']);
		Route::get('/{id}/confidentail/owner/create', 			    ['as' => 'create', 			'uses' => 'OwnerController@create']);
		Route::put('/{id}/confidentail/owner', 						['as' => 'store', 			'uses' => 'OwnerController@store']);
		Route::post('/{id}/confidentail/owner', 						['as' => 'update', 			'uses' => 'OwnerController@update']);
		Route::get('/confidentail/search-owners', 			        ['as' => 'search', 			'uses' => 'OwnerController@search']);
		Route::get('/confidentail/add-owner-to-property', 			['as' => 'add-to-property', 'uses' => 'OwnerController@addToProperty']);
	});
	//:::::::::::::::::::::::::::::::>>> Customer
	Route::group(['as' => 'customer.'], function () {
		Route::get('/{id}/confidentail/customer', 			        	['as' => 'index', 			'uses' => 'CustomerController@index']);
		Route::get('/{id}/confidentail/customer/create', 			    ['as' => 'create', 			'uses' => 'CustomerController@create']);
		Route::put('/{id}/confidentail/customer', 						['as' => 'store', 			'uses' => 'CustomerController@store']);
		Route::post('/{id}/confidentail/customer', 						['as' => 'update', 			'uses' => 'CustomerController@update']);
		Route::get('/confidentail/search-customer', 			        ['as' => 'search', 			'uses' => 'CustomerController@search']);
		Route::get('/confidentail/add-customer-to-property', 			        ['as' => 'add-to-property', 'uses' => 'CustomerController@addToProperty']);
	});
});

//:::::::::::::::::::::::::::::::>>> Tool
Route::group(['as' => 'tool.', 'namespace'=>'Tool'], function () {
	
	Route::get('/{id}/tool/redirect-url', function ($id){
		$route = 'user.property.tool';
		if(checkRole($id, 'view-status')){
			return redirect(route($route.'.status.index', $id));
		}
		if(checkRole($id, 'view-print')){
			return redirect(route($route.'.print.index', $id));
		}
		if(checkRole($id, 'view-mailing')){
			//return redirect(route($route.'.mailing.index', $id));
		}
		if(checkRole($id, 'view-listing')){
			return redirect(route($route.'.listing.index', $id));
		}
		if(checkRole($id, 'view-facebook')){
			//return redirect(route($route.'.facebook.index', $id));
		}
		
		echo "You cannot access in tool tab";

	})->name('redirect-url');

	Route::group(['as' => 'status.'], function () {
		Route::get('/{id}/tool/status', 				['as' => 'index', 		'uses' => 'StatusController@index']);
		Route::post('/{id}/tool/status', 				['as' => 'update', 			'uses' => 'StatusController@update']);
	});
	Route::group(['as' => 'print.'], function () {
		Route::get('/{id}/tool/print', 				['as' => 'index', 		'uses' => 'PrintFromController@index']);
		Route::get('/{id}/tool/form', 				['as' => 'form', 		'uses' => 'PrintFromController@form']);
	});

	Route::group(['as' => 'mailing.'], function () {
		Route::get('/{id}/tool/mailings', 			['as' => 'index', 	'uses' => 'MailingController@index']);
	});

	Route::group(['as' => 'listing.'], function () {

		Route::get('{id}/tool/listings', 								['as' => 'index', 					'uses' => 'ListingController@index']);
		Route::delete('{id}/tool/listings/remove-list-from-property/{list_id}', 	['as' => 'remove-list', 			'uses' => 'ListingController@removeList']);

		Route::put('tool/listings/create-list', 									['as' => 'create-list', 			'uses' => 'ListingController@createList']);
		Route::put('tool/listings/add-properties-to-list', 						['as' => 'add-properties-to-list', 	'uses' => 'ListingController@addPropertiesToList']);
		
		Route::get('tool/listings/search-list', 						['as' => 'search-list', 						'uses' => 'ListingController@searchList']);
		Route::get('tool/listings/selected-list', 					['as' => 'selected-list', 						'uses' => 'ListingController@selectedList']);
		Route::delete('tool/listings/remove-list-from-property', 		['as' => 'remove-list-from-property', 			'uses' => 'ListingController@removeListFromProperty']);
		Route::put('tool/listings/add-list-to-property', 				['as' => 'add-list-to-property', 				'uses' => 'ListingController@addListToProperty']);
		
	});

	Route::group(['as' => 'facebook.'], function () {
		Route::get('/{id}/tool/facebook', 				['as' => 'index', 	'uses' => 'MailingController@index']);
	});

});

//:::::::::::::::::::::::::::::::>>> About
Route::group(['as' => 'about.', 'namespace'=>'About'], function () {
	
	Route::get('/{id}/about/redirect-url', function ($id){
		$route = 'user.property.about';
		if(checkRole($id, 'view-price')){
			return redirect(route($route.'.price.index', $id));
		}
		if(checkRole($id, 'view-location')){
			return redirect(route($route.'.location.index', $id));
		}
		if(checkRole($id, 'view-picture')){
			return redirect(route($route.'.picture.index', $id));
		}
		if(checkRole($id, 'view-photo')){
			return redirect(route($route.'.photo.index', $id));
		}
		if(checkRole($id, 'view-image')){
			return redirect(route($route.'.image.index', $id));
		}
		if(checkRole($id, 'view-video')){
			return redirect(route($route.'.video.index', $id));
		}
		if(checkRole($id, 'view-detail')){
			return redirect(route($route.'.detail.index', $id));
		}
		if(checkRole($id, 'view-amenity')){
			return redirect(route($route.'.amenity.index', $id));
		}
		
		echo "You cannot access in about tab";

	})->name('redirect-url');

	Route::group(['as' => 'price.'], function () {
		Route::get('/{id}/about/price', 					['as' => 'index', 		'uses' => 'PriceController@index']);
		Route::post('/about/price', 						['as' => 'update', 		'uses' => 'PriceController@update']);
	});

	Route::group(['as' => 'location.'], function () {
		Route::get('/{id}/about/location', 					['as' => 'index', 				'uses' => 'LocationXController@index']);
		Route::post('/about/location/save', 					['as' => 'update', 				'uses' => 'LocationXController@update']);
	});
	Route::group(['as' => 'picture.'], function () {
		Route::get('/{id}/about/picture', 				['as' => 'index', 			'uses' => 'PictureController@index']);
		Route::post('/about/picture', 					['as' => 'update', 			'uses' => 'PictureController@update']);
	});
	Route::group(['as' => 'photo.'], function () {
		Route::get('/{id}/about/photos', 				['as' => 'index', 			'uses' => 'PhotoController@index']);
		Route::get('/{id}/about/photos/create', 		['as' => 'create', 			'uses' => 'PhotoController@create']);
		Route::put('/about/photos', 					['as' => 'store', 			'uses' => 'PhotoController@store']);
		Route::get('/{id}/about/photos/{photo_id}', 	['as' => 'edit', 			'uses' => 'PhotoController@edit']);
		Route::post('/about/photos', 					['as' => 'update', 			'uses' => 'PhotoController@update']);
		Route::delete('/about/photos/{photo_id}', 		['as' => 'trash', 			'uses' => 'PhotoController@trash']);
	});
	Route::group(['as' => 'image.'], function () {
		Route::get('/{id}/about/images', 				['as' => 'index', 			'uses' => 'ImageController@index']);
		Route::get('/{id}/about/images/create', 		['as' => 'create', 			'uses' => 'ImageController@create']);
		Route::put('/about/images', 					['as' => 'store', 			'uses' => 'ImageController@store']);
		Route::get('/{id}/about/images/{image_id}', 	['as' => 'edit', 			'uses' => 'ImageController@edit']);
		Route::post('/about/images', 					['as' => 'update', 			'uses' => 'ImageController@update']);
		Route::delete('/about/images', 					['as' => 'trash', 			'uses' => 'ImageController@trash']);
	});
	Route::group(['as' => 'video.'], function () {
		Route::get('/{id}/about/videos', 				['as' => 'index', 			'uses' => 'VideoController@index']);
		Route::get('/{id}/about/videos/create', 		['as' => 'create', 			'uses' => 'VideoController@create']);
		Route::put('/about/videos', 					['as' => 'store', 			'uses' => 'VideoController@store']);
		Route::get('/{id}/about/videos/{video_id}', 	['as' => 'edit', 			'uses' => 'VideoController@edit']);
		Route::post('/about/videos', 					['as' => 'update', 			'uses' => 'VideoController@update']);
		Route::delete('/about/videos/{video_id}', 		['as' => 'trash', 			'uses' => 'VideoController@trash']);
	});

	Route::group(['as' => 'detail.'], function () {
		Route::get('/{id}/about/details', 					['as' => 'index', 				'uses' => 'DetailController@index']);
		Route::get('/about/details/save', 					['as' => 'save', 				'uses' => 'DetailController@save']);
		Route::post('/about/details/save-all', 					['as' => 'save-all', 				'uses' => 'DetailController@saveAll']);
		Route::get('/about/details/delete', 				['as' => 'trash', 			    'uses' => 'DetailController@trash']);

		Route::get('/about/details/search-detail', 						['as' => 'search', 						'uses' => 'DetailController@search']);
		Route::get('/about/details/selected-detail', 					['as' => 'selected', 					'uses' => 'DetailController@selected']);
		Route::delete('/about/details/remove-detail-from-property', 		['as' => 'remove', 		'uses' => 'DetailController@remove']);
		Route::put('/about/details/add-detail-to-property', 				['as' => 'add', 				'uses' => 'DetailController@add']);
		Route::post('/about/details/update-status', 	['as' => 'update-status', 	'uses' => 'DetailController@updateStatus']);
		Route::post('order', 	['as' => 'order', 	'uses' => 'DetailController@order']);
	});
	Route::group(['as' => 'amenity.'], function () {
		Route::get('/{id}/about/amenities', 					['as' => 'index', 				'uses' => 'AmenityController@index']);
		Route::get('/about/amenities/check', 					['as' => 'check', 				'uses' => 'AmenityController@check']);
	});
	
});

//:::::::::::::::::::::::::::::::>>> Staffs
Route::group(['as' => 'staff.', 'namespace'=>'Staff'], function () {
	
	Route::get('/{id}/staff', 			['as' => 'index', 		'uses' => 'StaffController@index']);
	Route::get('/search', 				['as' => 'search', 		'uses' => 'StaffController@search']);
	Route::get('/selected', 			['as' => 'selected', 	'uses' => 'StaffController@selected']);
	Route::put('/add', 					['as' => 'add', 		'uses' => 'StaffController@add']);
	Route::DELETE('/remove', 			['as' => 'remove', 		'uses' => 'StaffController@remove']);
	Route::POST('/primary', 			['as' => 'primary', 	'uses' => 'StaffController@primary']);
	Route::get('/role-access', 	    	['as' => 'role-access', 	'uses' => 'StaffController@roleAccess']);
});

//:::::::::::::::::::::::::::::::>>> Enquiries
Route::group(['as' => 'enquiries.', 'namespace'=>'Enquiries'], function () {
	
	Route::get('/{id}/enquiries', 			['as' => 'index', 		'uses' => 'EnquiriesController@index']);
	Route::get('/{id}/enquiries-data', 			['as' => 'enquiries-data', 		'uses' => 'EnquiriesController@enquiriesData']);
	Route::post('/update-enquiries', 			['as' => 'update-enquiries', 		'uses' => 'EnquiriesController@enquiriesUpdate']);
	Route::post('/enquiries-store', 			['as' => 'enquiries-store', 		'uses' => 'EnquiriesController@enquiriesStore']);
	Route::delete('/enquiries-delete/{inquiry_id}', ['as' => 'trash', 			'uses' => 'EnquiriesController@trash']);
});


