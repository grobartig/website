<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Type
Route::group(['as' => 'type.',  'prefix' => 'type'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'TypeController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TypeController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'TypeController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'TypeController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'TypeController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TypeController@trash']);
	Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'TypeController@updateStatus']);

	Route::get('/{id}/amenities', 		['as' => 'amenities', 				'uses' => 'TypeController@listFeatures']);
	Route::get('/features-and-type/check', 	['as' => 'check-features', 			'uses' => 'TypeController@checkFeatures']);
	
	Route::get('/{id}/details', 		['as' => 'details', 				'uses' => 'TypeController@listDetails']);
	Route::get('/details-and-type/check', 	['as' => 'check-details', 			'uses' => 'TypeController@checkDetails']);

	Route::post('order', 	['as' => 'order', 	'uses' => 'TypeController@order']);
});
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Featurte
Route::group(['as' => 'feature.',  'prefix' => 'feature'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'FeatureController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'FeatureController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'FeatureController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'FeatureController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'FeatureController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'FeatureController@trash']);
	Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'FeatureController@updateStatus']);
	Route::get('/{id}/types', 		['as' => 'types', 				'uses' => 'FeatureController@types']);
	Route::get('/types-and-features/check', 	['as' => 'check-types', 			'uses' => 'FeatureController@check']);

	Route::post('order', 	['as' => 'order', 	'uses' => 'FeatureController@order']);
});

//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Detail
Route::group(['as' => 'detail.',  'prefix' => 'detail'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'DetailController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'DetailController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'DetailController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'DetailController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'DetailController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'DetailController@trash']);
	Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'DetailController@updateStatus']);
	Route::get('/{id}/types', 		['as' => 'types', 				'uses' => 'DetailController@types']);
	Route::get('/types-and-details/check', 	['as' => 'check-types', 			'uses' => 'DetailController@check']);

	Route::post('order', 	['as' => 'order', 	'uses' => 'DetailController@order']);
});

//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Location
Route::group(['as' => 'location.',  'prefix' => 'location'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'LocationController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'LocationController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'LocationController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'LocationController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'LocationController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'LocationController@trash']);
	Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'LocationController@updateStatus']);
	
	Route::get('/{id}/districts', 					['as' => 'districts', 				'uses' => 'LocationController@districts']);
	Route::get('/{id}/districts/{district_id}',['as' => 'edit-districts', 			'uses' => 'LocationController@editDistrict']);
	Route::post('/districts', 						['as' => 'update-districts', 		'uses' => 'LocationController@updateDistrict']);
	Route::get('{id}/create-district', 				['as' => 'create-district', 		'uses' => 'LocationController@createDistrict']);
	Route::put('/district', 						['as' => 'store-district', 			'uses' => 'LocationController@storeDistrict']);
	Route::delete('/rash-district/{id}', 			['as' => 'trash-district', 			'uses' => 'LocationController@trashDistrict']);
	Route::post('update-status-district', 			['as' => 'update-status-district', 	'uses' => 'LocationController@updateDistrictStatus']);

	Route::get('/commnues', 						['as' => 'commune', 			'uses' => 'LocationController@communes']);
	Route::put('/commnue', 							['as' => 'store-commune', 			'uses' => 'LocationController@storeCommnue']);
	Route::delete('/commnue', 						['as' => 'delete-commune', 			'uses' => 'LocationController@trashCommnue']);
	Route::get('/commnue', 							['as' => 'get-commune', 			'uses' => 'LocationController@editCommnue']);
	Route::post('commnue', 							['as' => 'update-commune', 			'uses' => 'LocationController@updateCommnue']);
	Route::post('update-status-commnue', 			['as' => 'update-status-commune', 	'uses' => 'LocationController@updateCommnueStatus']);
});

//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Selling Price
Route::group(['as' => 'selling-price.',  'prefix' => 'selling-price'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'SellingPriceController@index']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'SellingPriceController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'SellingPriceController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'SellingPriceController@trash']);
});

//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Rental Price
Route::group(['as' => 'rental-price.',  'prefix' => 'rental-price'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'RentalPriceController@index']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'RentalPriceController@update']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'RentalPriceController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'RentalPriceController@trash']);
});
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Role
Route::group(['as' => 'role.',  'prefix' => 'role'], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'RoleController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'RoleController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'RoleController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'RoleController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'RoleController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'RoleController@trash']);
	Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'RoleController@updateStatus']);
	Route::get('/{id}/accesses', 	['as' => 'accesses', 		'uses' => 'RoleController@accesses']);
	Route::get('/accesses/check', 	['as' => 'check', 			'uses' => 'RoleController@check']);
	

});



			