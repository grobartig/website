
   var map;
    map = new GMaps({
        el: '#map',
        lat: 11.5829223,
        lng: 104.9012,
        scrollwheel: false
    });

    map.addMarker({
        lat: 11.5829223,
        lng: 104.9012,
        title: 'Marker with InfoWindow',
        infoWindow: {
            content: '<p style="padding: 10px;">#1A., St. 283, Sangkat Bueong kok1, Khan Toul Kork, Phnom Penh, Cambodia</p>'
        }
    });    