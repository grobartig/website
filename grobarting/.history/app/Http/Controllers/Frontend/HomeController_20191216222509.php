<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Message;
use App\Model\Property\Property as Property;
use App\Model\Team   as Team;
use App\Model\Website\Partner   as Partner;
use App\Model\User\User         as User;
use App\Model\Setup\Type        as Type;
use App\Model\About as About;

use Session;
use Validator;

class HomeController extends FrontendController
{
    //

  
    public  function index($locale ){
		  
        $defaultData = $this->defaultData($locale);
       
        $about= About::select('id',$locale.'_name as name',$locale.'_title as title',$locale.'_description as description','image')->get();
        $partner= Partner::select('id','title','image')->get();
       
		$agents              =   User::select('id','phone','statff_id',$locale.'_name as name','email','picture')->where('agentcy',1)->get();
        $teams              =   Team::select('id',$locale.'_title as title',$locale.'_position as position','phone','email','image')->get();
        $properties         =   Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug')->where('is_published', 1)->with(['action' => function($query, $locale="en") {
                                  $query->select('id', $locale.'_name as name'); }])->orderBy('id','DESC')->limit(6)->get(); 
        $feature_properties     =  Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug')->where('is_featured',1)->with(['action' => function($query, $locale="en") {
                                    $query->select('id', $locale.'_name as name'); }])->limit(12)->get(); 
        $property_types       =  Type::select('id',$locale.'_name as name','icon')->with('properties')->limit(6)->where('is_featured',1)->get();
        $best_deal_properties   =  Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug')->where('is_best_dealed',1)->with(['action' => function($query, $locale="en") {
                                    $query->select('id', $locale.'_name as name'); }])->with('propertyStaffs')->limit(4)->get();
		return view('frontend.home', ['locale'=>$locale,'defaultData'=>$defaultData,'agents'=>$agents,'teams'=>$teams,'properties'=>$properties,'feature_properties'=>$feature_properties,'property_types'=>$property_types,'about'=>$about,]);
	}

    public function sendMessage(Request $request){
        
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'first_name'        =>      $request->input('first_name'),
            'last_name'         =>      $request->input('last_name'),
            'subject'           =>      $request->input('subject'),
            'email'             =>      $request->input('email'),
            'message'           =>      $request->input('message'),
            'created_at'        =>      $now
        );
        
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect()->back();
    }
}
