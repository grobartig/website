<?php

Route::get('/', 							['as' => 'index', 			'uses' => 'AboutController@index']);
	Route::get('/{id}', 						['as' => 'edit', 			'uses' => 'AboutController@edit']);
	Route::post('/', 							['as' => 'update', 			'uses' => 'AboutController@update']);
	Route::get('/create', 						['as' => 'create', 			'uses' => 'AboutController@create']);
	Route::put('/', 							['as' => 'store', 			'uses' => 'AboutController@store']);
	Route::delete('/{id}', 						['as' => 'trash', 			'uses' => 'AboutController@trash']);
	Route::post('update-status', 				['as' => 'update-status', 	'uses' => 'AboutController@updateStatus']);
	Route::post('update-delete-status', 				['as' => 'update-delete-status', 	'uses' => 'AboutController@updateDeletedStatus']);
	Route::delete('/delete/{id}', 						['as' => 'delete', 			'uses' => 'AboutController@delete']);
