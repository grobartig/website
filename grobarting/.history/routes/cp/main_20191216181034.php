<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' => 'auth', 'namespace' => 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Dashboard
		Route::group(['as' => 'dashboard.',  'prefix' => 'dashboard', 'namespace' => 'Dashboard'], function () {
			require(__DIR__.'/dashboard.php');
		});

		Route::group(['as' => 'user.',  'prefix' => 'user', 'namespace' => 'User'], function () {
			require(__DIR__.'/user.php');
		});

		Route::group(['as' => 'customer.',  'prefix' => 'customer', 'namespace' => 'Customer'], function () {
			require(__DIR__.'/customer.php');
		});	

		Route::group(['as' => 'owner.',  'prefix' => 'owner', 'namespace' => 'Owner'], function () {
			require(__DIR__.'/owner.php');
		});	

		Route::group(['as' => 'website.',  'prefix' => 'website', 'namespace' => 'Website'], function () {
			require(__DIR__.'/website.php');
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Property
		Route::group(['as' => 'property.',  'prefix' => 'property', 'namespace' => 'Property'], function () {
			require(__DIR__.'/property.php');
		});
		
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Mailing
		Route::group(['as' => 'mailing.',  'prefix' => 'mailing', 'namespace'=>'Mailing'], function () {
			require(__DIR__.'/mailing.php');
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> File
		Route::group(['as' => 'file.',  'prefix' => 'file', 'namespace'=>'File'], function () {
			require(__DIR__.'/file.php');
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> message
		Route::group(['as' => 'message.',  'prefix' => 'message', 'namespace' => 'Message'], function () {
			require(__DIR__.'/message.php');
		});
		Route::group(['as' => 'about.',  'prefix' => 'about', 'namespace' => 'About'], function () {
			require(__DIR__.'/about.php');
		});

		Route::group(['as' => 'partner.',  'prefix' => 'partner', 'namespace' => 'Partner'], function () {
			require(__DIR__.'/partner.php');
		});

		Route::group(['as' => 'abouts.',  'prefix' => 'abouts', 'namespace' => 'Abouts'], function () {
			require(__DIR__.'/abouts.php');
		});
		Route::group(['as' => 'url.',  'prefix' => 'url', 'namespace' => 'Url'], function () {
			require(__DIR__.'/Url.php');
		});
		Route::group(['as' => 'contacts.',  'prefix' => 'contacts', 'namespace' => 'Contacts'], function () {
			require(__DIR__.'/contacts.php');
		});
		Route::group(['as' => 'welcome.',  'prefix' => 'welcome', 'namespace' => 'welcome'], function () {
			require(__DIR__.'/welcome.php');
		});
		//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Setup
		Route::group(['as' => 'setup.', 'prefix' => 'setup', 'namespace' => 'Setup'], function () {
			require(__DIR__.'/setup.php');
		});

	});