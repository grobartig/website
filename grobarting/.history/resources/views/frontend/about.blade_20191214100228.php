@extends('frontend.layouts.master') @section('title', 'Welcome to Grobartig') @section('active-about', 'active') @section ('content')
<!-- =========================Banner -->
@include('frontend.layouts.page-banner')
<!-- =====================Banner-end -->

     <!--Welcome Haven section-->
     <div class="welcome-haven single-services">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 welcome-pd agent">
                    <div class="welcome-title">
                        <h3 class="title-1"><span>MEET</span> OUR</h3>
                        <h4 class="title-2">GENIOUS <span>PEOPLE</span></h4>
                    </div>
                    <div class="welcome-content">
                        <p> <span>HAVEN</span> is the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea commodo equat. eiusmod tempor dolor sit amet, conse ctetur adipiscing elit</p>
                        <p class="text-2">Best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea commodo equat. eiusmod tempor dolor</p>
                        <p class="text-3">Best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="welcome-haven-img">
                        <img src="img/welcome/6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Welcome Haven section end-->

    <div class="welcome-haven">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 welcome-pd fadeInLeft wow" data-wow-delay="0.2s">
                    <div class="welcome-title">
                        <h3 class="title-1">WELCOME TO <span>Grobartig</span></h3>
                        <h4 class="title-2">{{ $welcome->name }}</h4>
                    </div>
                    <div class="welcome-content">
                        <p> {{ $welcome->description }}</p>
                    </div>
                    <div class="welcome-services">
                        <div class="row">
                        @foreach($abouts as $row)
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="w-single-services">
                                    <div class="services-img">
                                        <img src="{{ asset($row->image) }}" alt="">
                                    </div>
                                    <div class="services-desc">
                                        <h6>{{ $row->name }}</h6>
                                        <p style="font-size: 16px;"> {{  $row->description }}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="welcome-haven-img fadeInRight wow" data-wow-delay="0.2s">
                        <img src=" {{asset($welcome->image ) }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
   


    <!--Services section start-->
    <div class=" bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h3>OUR SERVICES</h3>
                         
                         <!-- <p>Grobartig the best theme for elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation.</p> -->
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach($about as $row)
                <div class="col-md-4 col-sm-4 col-sm-12">
                    <div class="single-services wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">
                        <div class="single-services-img">
                            <img src="{{ asset($row->image) }}" alt="">
                        </div>
                        <div class="single-services-desc">
                            <h5>{{ $row->name }}</h5>
                            <p style="">{{  $row->description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <!--Services section end-->
    


    @endsection

