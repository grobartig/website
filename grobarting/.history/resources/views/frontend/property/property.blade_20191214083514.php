@extends('frontend.layouts.master') 
@section('title', 'Welcome to Grobartig')
@section('active-property', 'active')
@section ('content')
<!-- =========================slide -->

        <!-- =========================Search -->
    
        @include('frontend.search.search')

        <!-- =========================End Search -->
        <!--Feature property section-->
        @if($msg)
        <div class="container pt-10">
            <div class="alert alert-info" role="alert">
                <i class="fa fa-search"></i> {{$msg}}
            </div>
        </div>
        @endif
        <div class="feature-property pt-50 ptb-50">
            <div class="container">

                @if(sizeof($data) > 0) 
                    @foreach($data->chunk(3) as $item)
                        <div class="row">
                        @foreach($item as $row)
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-property mb-40 fadeInUp wow" data-wow-delay="0.2s">
                                    <span>
                                        @if($row->action_id == 1 ) 
                                        {{__('web.for-sale')}} 
                                        @elseif( $row->action_id == 2 )  
                                            {{__('web.for-rent')}}  
                                        @else 
                                            {{__('web.rent-sale')}}  
                                        @endif
                                    </span>
                                    <div class="property-img">
                                        <a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">
                                            <img src="{{ asset('public/uploads/property/feature/property/'.$row->media->feature) }}" alt="">
                                        </a>
                                    </div>
                                    <div class="property-desc">
                                        <div class="property-desc-top">
                                            <h6><a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">{{$row->name}}</a></h6>
                                            <h4 class="price">{{displayMoney($row->price->selling_price)}}</h4>
                                            <div class="property-location">
                                                <p><img src="{{ asset('public/frontend/img/property/icon-5.png') }}" alt=""> @if($row->location) {{$row->location->address}} @endif</p>
                                            </div>
                                        </div>
                                        <div class="property-desc-bottom">
                                            <div class="property-bottom-list">
                                                <ul>
                                                    <li>
                                                        @php($details = $row->propertyDetails()->where(array('is_showed'=>1))->limit(3)->get())
                                                            @foreach($details as $line)
                                                            @php($name = $line->detail()->select($locale.'_name as name')->first() )
                                                        <img width="20px" style="margin-left: 4px;" src="{{ asset($line->detail->icon) }}" alt="">
                                                        <span style="margin-left:4px;"> {{$line->value}} {{ $line->detail->unit }} </span>  @endforeach
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pagination">
                                    <div class="pagination-inner text-center">
                                        <ul> 
                                            {{ $data->links('vendor.pagination.frontend-html') }}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                @else 
                    <div class="col-md-12">
                        <div class="find-home-title text-center">
                            
                            <h3> <i class="fa fa-search"></i>  {{__('general.data-not-found')}}</h3>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <!--Feature property section end-->
@endsection