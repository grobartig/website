<!doctype html>
<html>

<head>
    @yield('meta')
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Grobartig Realty</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link href="{{ asset('public/frontend/img/logo.png') }}" type="img/x-icon" rel="shortcut icon">
    <!-- All css files are included here. -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/core.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/shortcode/shortcodes.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/shortcode/header.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/camcyber/grobartig.css') }}">

    <!-- customizer style css -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/style-customizer.css') }}">
    <link href="#" data-style="styles" rel="stylesheet">
    <!-- Modernizr JS -->
    @if($locale=="kh")
    <link href="https://fonts.googleapis.com/css?family=Hanuman" rel="stylesheet">
    <link href="{{ asset ('public/frontend/css/kh_laugauges.css')}}" rel="stylesheet"> @endif
    <script src="{{ asset('public/frontend/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>

<body>

    <!--Preloader start-->
    <div id="fakeLoader"></div>
    <!--Preloader end-->
    <div class="wrapper white_bg">
        <!--Header section start-->
        <header class="header header-2">

            <div class="header-top-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 hidden-xs">
                            <div class="haven-call">
                                <p>+ (855) 16 200 096</p>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="header-1-top-inner">
                                <div class="header-topbar-menu">
                                    <ul>
                                        <li><span></span> De Castle Royal, 21F, ST.294 BKK1, Khan Chamkarmorn, Phnom Penh.</li>
                                    </ul>
                                </div>
                                <div class="header-search">
                                    <div class="search-icon">
                                        <a href="#"><img src="img/icon/search.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header-top sticky-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-4 col-xs-5">
                            <div class="logo">
                                <a href="{{ route('home', $locale)}}"><img class="logo-img" src="{{ asset('public/frontend/img/grobartigg/png/logo.png') }} "></a>
                            </div>
                        </div>

                        <div class="col-md-8 hidden-sm hidden-xs">
                            <div class="mgea-full-width">
                                <div class="header-menu">
                                    <nav>
                                        <ul>
                                            <li class="@yield('active-home')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('home', $locale) }}">{{__('general.home')}}</a></li>
                                            <li class="@yield('active-property')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('property', $locale) }}">{{__('general.properties')}}</a></li>
                                            <li class="@yield('active-about')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('about',$locale)}}">{{__('general.about')}}</a></li> 
                                            <li class="@yield('active-contact')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('contact',$locale)}}"> {{__('general.contact-us')}}</a></li>

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2  col-sm-6 col-xs-6">
                            <div class="attr-nav">
                                <a class="flag nav-item nav-link" href="{{route($defaultData['routeName'], $defaultData['khRouteParamenters'])}}"><img src="{{ asset('public/frontend/img/flag/kh.png') }} " ></a>
                                <a class="flag nav-item nav-link" href="{{route($defaultData['routeName'], $defaultData['enRouteParamenters'])}}"><img src="{{ asset('public/frontend/img/flag/en.png') }} " ></a>
                                <a class="flag nav-item nav-link" href="{{route($defaultData['routeName'], $defaultData['cnRouteParamenters'])}}"><img src="{{ asset('public/frontend/img/flag/cn.png') }} " ></a>

                            </div>

                        </div>
                        
                    </div>
                </div>
                <!-- Mobile menu start -->
                <div class="mobile-menu-area hidden-lg hidden-md">
                    <div class="container">
                        <div class="col-md-9">
                            <nav id="dropdown">
                                <ul>
                                    <li class="@yield('active-home')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('home', $locale) }}">{{__('general.home')}}</a></li>
                                    <li class="@yield('active-properties')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('property', $locale) }}">{{__('general.properties')}}</a></li>

                                    <li class="@yield('active-about')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('about',$locale)}}">{{__('general.about')}}</a></li>

                                    <li class="@yield('active-contact')"><a @if($locale=="kh" ) style="font-size: 17px;" @endif href="{{ route('contact',$locale)}}"> {{__('general.contact-us')}}</a></li>

                                </ul>
                            </nav>

                        </div>
                        <div class="col-md-9">

                        </div>
                    </div>
                </div>
                <!-- Mobile menu end -->

            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- <div class="haven-call">
                                <p>+012 345 678 102</p>
                            </div> -->

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--Header section end-->

        @yield('content')

        <!-- footer-start -->
        <footer class="footer wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.5s">

            <!--Footer bottom start-->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="footer-menu ">
                                <a href="{{ route('home', $locale)}}"><img  style=" max-width: 130%; margin-top: 10px;" src="{{ asset('public/frontend/img/grobartigg/png/logo.png') }} "></a>
                            </div>
                            <div class="newsletter ">
                                <p>Grobarting explain to you how all this istaolt cing pleasure .</p>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="ml-80">

                                <div class="footer-title">
                                    <h3>{{__('general.contant-us')}}</h3>
                                </div>
                                <div class="f-contact-details">
                                    <div class="single-contact-list">
                                        <div class="contact-icon">
                                            <img src="{{ asset('public/frontend/img/icon/c-1.png') }}" alt="">
                                        </div>
                                        <div class="contact-text pt-10">
                                            <p>{{ $defaultData['contacts']->address }}</p>
                                            
                                        </div>
                                    </div>
                                    <div class="single-contact-list">
                                        <div class="contact-icon">
                                            <img src="{{ asset('public/frontend/img/icon/c-2.png') }}" alt="">
                                        </div>
                                        <div class="contact-text pt-10">
                                            <p><a class="icon" href="tel:85516200096">{{ $defaultData['contacts']->phone }}</a></p>
                                            <!-- <p>{{__('general.telephone')}} : +012 745 674 152</p> -->
                                        </div>
                                    </div>
                                    <div class="single-contact-list">
                                        <div class="contact-icon">
                                            <img src="{{ asset('public/frontend/img/icon/c-3.png') }}" alt="">
                                        </div>
                                        <div class="contact-text pt-10">
                                            <p ><a class="icon" href="email:info@grobartigrealty.com">{{ $defaultData['contacts']->email }}</a> </p>
                                            <p><a  class="icon" href="http://grobartigrealty.com./en/home">www.grobartigrealty.com</a> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="single-footer-widget mb-35 ml-80">
                                <div class="footer-title">
                                    <h3> STAY CONNECTED</h3>
                                </div>
                                <ul class="social-icons">
                                    <li>
                                        <a href="{{ $defaultData['url']->facebook }}"> <img src="{{ asset('public/frontend/img/icon/facebook.png') }}" style="width: 19px;"></a>
                                    </li>
                                    <li>
                                        <a href="{{ $defaultData['url']->instagram }}"> <img src="{{ asset('public/frontend/img/icon/instagram.png') }}" style="width: 19px;"></a>
                                    </li>
                                    <li>
                                        <a  href="{{ $defaultData['url']->linkedin }}"> <img src="{{ asset('public/frontend/img/icon/linkedin.png') }}" style="width: 19px;"></a>
                                    </li>
                                    <li>
                                        <a href="{{ $defaultData['url']->twitter }}"> <img src="{{ asset('public/frontend/img/icon/twitter.png') }}" style="width: 19px;"></a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--Footer bottom end-->
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="copyright">
                                <p>
                                    <a href="http://grobartigrealty.com"></a>{{__('general.copy')}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
    </div>

    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- Map js code here -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAj9b_nyz33KEaocu6ZOXRgqwwUZkDVEAw"></script>
    <script src="{{ asset('public/frontend/js/map.js') }}"></script>

    <!-- All jquery file included here -->
    <!--Jquery 1.12.4-->
    <script src="{{ asset('public/frontend/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/vendor/jquery-1.12.0.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.nivo.slider.pack.js') }}"></script>
    <script src="{{ asset('public/frontend/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/ajax-mail.js') }}"></script>
    <script src="{{ asset('public/frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/frontend/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('public/frontend/js/style-customizer.js') }}"></script>
    <script src="{{ asset('public/frontend/js/plugins.js') }}"></script>
    <script src="{{ asset('public/frontend/js/main.js') }}"></script>
    @yield('appbottomjs') @yield('search-js')
</body>

</html>