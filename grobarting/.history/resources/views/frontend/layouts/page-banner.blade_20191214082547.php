<div class="download-apps overlay-blue" style = " background:rgba(0, 0, 0, 0) url('{{ asset("public/frontend/img/common/download.jpg") }}') no-repeat scroll center center / cover ">
    <div class="container">
        <div class="row">
            <div class="download-app-inner">
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="download-apps-desc wow fadeInDown" data-wow-delay="0.1s">
                        <div class="download-apps-title">
                            <h3 class="title-1">DOWNLOAD OUR APPS</h3>
                            <h3 class="title-2">AND GET NOTIFICATION FOR NEW PROPERTIES</h3>
                        </div>
                        <div class="download-apps-bottom">
                            <p>Haven  the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur apiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam</p>
                            <a href="#">DOWNLOAD</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="download-apps-caption-img f-right wow fadeUp" data-wow-duration="1.2s" data-wow-delay="0.2s">
                        <img  src="{{ asset('public/frontend/img/common/download-caption.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>