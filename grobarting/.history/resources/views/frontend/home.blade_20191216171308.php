@extends('frontend.layouts.master') 
@section('title', 'Welcome to Grobartig')
@section('active-home', 'active')
@section ('content')
<!-- =========================slide -->
@include('frontend.layouts.banner')
<!-- =====================slide-end -->

    <!-- =========================Search -->
    
    @include('frontend.search.search')

    <!-- =========================End Search -->

    <!--Download apps section start-->
    <div class="download-apps overlay-blue" style = " background:rgba(0, 0, 0, 0) url('{{ asset("public/frontend/img/common/download.jpg") }}') no-repeat scroll center center / cover ">
        <div class="container">
            <div class="row">
                <div class="download-app-inner">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="download-apps-desc wow fadeInDown" data-wow-delay="0.1s">
                            <div class="download-apps-title">
                                <h3 class="title-1">Grobartig Realty</h3>
                                <h3 class="title-2">We are ready to serve you with profestional services.</h3>
                            </div>
                            <div class="download-apps-bottom">
                                <p>Our agents are working hard to collect a lot of property information and list in our database system. There are more than 5,000 properties being listed, and around 100 daily updates. </p>
                                <a href="#">Find Your Properties!</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="download-apps-caption-img f-right wow fadeUp" data-wow-duration="1.2s" data-wow-delay="0.2s">
                            <img  src="{{ asset('public/frontend/img/common/download-caption.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Download apps section end-->

    <!--Feature property section start-->
    <div class="property-area fadeInUp wow ptb-50" data-wow-delay="0.2s">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="find-home-title text-center">
                        <h3>{{__('general.feature-properties')}}</h3>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="sale">
                        <div class="property-list">
                            @foreach($feature_properties as $row)                            
                            <div class="col-md-4">
                                <div class="single-property">
                                    <div class="property-img">
                                        <a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">
                                            <img src="{{ asset('public/uploads/property/feature/property/'.$row->media->feature) }}" alt="">
                                        </a>
                                    </div>
                                    <div class="property-desc">
                                        <div class="property-desc-top">
                                            <h6><a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">{{$row->name}}</a></h6>
                                            <h4 class="price">{{displayMoney($row->price->selling_price)}}</h4>
                                            <div class="property-location">
                                                <p style="margin-top: 10px;font-size: 14px;"><img src="{{ asset('public/frontend/img/property/icon-5.png') }}" alt=""> {{$row->location->address}}</p>
                                            </div>
                                        </div>
                                        <div class="property-desc-bottom">
                                            <div class="property-bottom-list">
                                                <ul>
                                                        @php($details = $row->propertyDetails()->where(array('is_showed'=>1))->limit(3)->get())
                                                @foreach($details as $line)
                                                    @php($name = $line->detail()->select($locale.'_name as name')->first() )
                                                    <img width="20px" style="margin-left: 4px;" src="{{ asset($line->detail->icon) }}" alt="">
                                                    <span style="margin-left:4px;"> {{$line->value}} {{ $line->detail->unit }} </span>  
                                                @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>     
                        @endforeach

                        </div>
                    </div> 
                </div>
            </div> 
        </div>
    </div>

    <!--Services section start-->
    <div class=" ptb-50 bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="find-home-title text-center">
                        <h3>{{__('general.our-service')}}</h3>
                        
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach($about as $row)
                <div class="col-md-4 col-sm-4 col-sm-12" style="text-align: center;">
                    <div class="single-services wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">
                        <div class="single-services-img">
                            <img src="{{ asset($row->image) }}" alt="">
                        </div>
                        <div class="single-services-desc">
                            <h5>{{ $row->name }}</h5>
                            <p style="">{{  $row->description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            
        </div>
    </div>
    <!--Services section end-->

    <!--Feature property section-->
    <div class="feature-property ptb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="find-home-title text-center">
                        <h3>{{__('general.search-propertie')}}</h3>
                    </div>
                </div>
            </div>

            @foreach($properties->chunk(3) as $item)
                <div class="row">
                    @foreach($item as $row)
                
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-property wow fadeInUp mb-40" data-wow-delay="0.2s" data-wow-duration="1s">
                            <span>
                                @if($row->action_id == 1 ) 
                                {{__('web.for-sale')}} 

                                @elseif( $row->action_id == 2 )  
                                    {{__('web.for-rent')}}  
                                @else 
                                    {{__('web.rent-sale')}}  
                                @endif
                            </span>
                            <div class="property-img">
                                <a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">
                                <img src="{{ asset('public/uploads/property/feature/property/'.$row->media->feature) }}" alt="">
                                </a>
                            </div>
                            <div class="property-desc">
                                <div class="property-desc-top">
                                    <h6><a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">{{$row->name}}</a></h6>
                                    <h4 class="price">{{displayMoney($row->price->selling_price)}}</h4>
                                    <div class="property-location">
                                        <p><img src="{{ asset('public/frontend/img/property/icon-5.png') }}" alt="">@if($row->location) {{$row->location->address}} @endif</p>
                                    </div>
                                </div>
                                <div class="property-desc-bottom">
                                    <div class="property-bottom-list">
                                        <ul>
                                            @php($details = $row->propertyDetails()->where(array('is_showed'=>1))->limit(3)->get())
                                                    @foreach($details as $line)
                                                    @php($name = $line->detail()->select($locale.'_name as name')->first() )
                                                <img width="20px" style="margin-left: 4px;" src="{{ asset($line->detail->icon) }}" alt="">
                                                <span style="margin-left:4px;"> {{$line->value}} {{ $line->detail->unit }} </span>  @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    @endforeach
                </div>
            @endforeach

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="find-home-items">
                        <button id="search-property" type="submit">
                        <a class="view" href="{{ route('property', $locale) }}" id="search-property"  > {{__('general.view')}}
                            </a>
                        </button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="find-home-title text-center>
                            <h3>Partner</h3>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="owl-carousel owl-theme partner-owl">
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>
                            <div class="item">
                                <img src="{{ asset('public/frontend/img/common/download-caption.png') }}"  class="img-center " alt=""> 
                            </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
        
 @endsection 