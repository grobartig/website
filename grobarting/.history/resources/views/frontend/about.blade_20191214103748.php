@extends('frontend.layouts.master') @section('title', 'Welcome to Grobartig') @section('active-about', 'active') @section ('content')
<!-- =========================Banner -->
@include('frontend.layouts.page-banner')
<!-- =====================Banner-end -->

    <!--About US-->
    <div class="welcome-haven single-services">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 welcome-pd agent">
                    <div class="welcome-title">
                        <h3 class="title-1"><span>WHO</span> WE</h3>
                        <h4 class="title-2">ARE <span>?</span></h4>
                    </div>
                    <div class="welcome-content">
                        <p> <span>Grobarting Realty</span> is the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea commodo equat. eiusmod tempor dolor sit amet, conse ctetur adipiscing elit</p>
                        <p class="text-2"><span>Mission</span>: &nbsp; Best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea commodo equat. eiusmod tempor dolor</p>
                        <p class="text-3"><span>Vision</span>: &nbsp;  Best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="welcome-haven-img">
                        <img src="{{asset('public/frontend/img/welcome/6.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--About US-->

    <!--Awesome Feature section-->
    <div class="awesome-feature bg-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title text-center">
                            <h3>Grobarting Core Values</h3>
                            <p>Haven  the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="awesome-feature-desc">
                            <div class="awesome-feature-img">
                                <div class="awesome-feature-img-border">
                                    <div class="awesome-feature-img-inner">
                                        <img src="{{asset('public/frontend/img/awesome/feature.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="awesome-feature-list">
                                <div class="col-4 left">
                                    <div class="single-awesome-feature one mb-87 wow fadeInLeft" data-wow-delay="0.2s" data-wow-duration="1.2s">
                                        <div class="s-awesome-feature-head">
                                            <div class="s-awesome-feature-title">
                                                <h6>Dynamic Team</h6>
                                            </div>
                                            <div class="s-awesome-feature-text">
                                                <p>Our generous agents is ready to serve you.</p>
                                            </div>
                                        </div>
                                        <div class="s-awesome-feature-icon">
                                            <i class="icofont icofont-tools-alt-2"></i>
                                        </div>
                                    </div>
                                    <div class="single-awesome-feature two mb-87 wow fadeInLeft" data-wow-delay="0.3s" data-wow-duration="1.3s">
                                        <div class="s-awesome-feature-head">
                                            <div class="s-awesome-feature-title">
                                                <h6>Happy Customers</h6>
                                            </div>
                                            <div class="s-awesome-feature-text">
                                                <p>Customers served our services always give us high rate.</p>
                                            </div>
                                        </div>
                                        <div class="s-awesome-feature-icon">
                                            <i class="icofont icofont-paint-brush"></i>
                                        </div>
                                    </div>
                                    <div class="single-awesome-feature three wow fadeInLeft" data-wow-delay="0.3s" data-wow-duration="1.4s">
                                        <div class="s-awesome-feature-head">
                                            <div class="s-awesome-feature-title">
                                                <h6>Non Stop Security</h6>
                                            </div>
                                            <div class="s-awesome-feature-text">
                                                <p>Paint cost provides ar best for <br> elit, sed do eiusmod tempe</p>
                                            </div>
                                        </div>
                                        <div class="s-awesome-feature-icon">
                                            <i class="zmdi zmdi-bug"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 right">
                                    <div class="single-awesome-feature four mb-87 wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="1.2s">
                                        <div class="s-awesome-feature-icon">
                                            <i class="icofont icofont-calculations"></i>
                                        </div>
                                        <div class="s-awesome-feature-head">
                                            <div class="s-awesome-feature-title">
                                                <h6>Rich Of Information.</h6>
                                            </div>
                                            <div class="s-awesome-feature-text">
                                                <p>We have around 5,000 properties listed, <br />in system and 100 daily updated</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-awesome-feature five mb-87 wow fadeInRight" data-wow-delay="0.3s" data-wow-duration="1.3s">
                                        <div class="s-awesome-feature-icon">
                                            <i class="fa fa-leaf" ></i>
                                        </div>
                                        <div class="s-awesome-feature-head">
                                            <div class="s-awesome-feature-title">
                                                <h6>Happy Customers</h6>
                                            </div>
                                            <div class="s-awesome-feature-text">
                                                <p>Paint cost provides ar best for <br> elit, sed do eiusmod tempe</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-awesome-feature six wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="1.4s">
                                        <div class="s-awesome-feature-icon">
                                            <i class="icofont icofont-fix-tools"></i>
                                        </div>
                                        <div class="s-awesome-feature-head">
                                            <div class="s-awesome-feature-title">
                                                <h6>Luxurious Fittings</h6>
                                            </div>
                                            <div class="s-awesome-feature-text">
                                                <p>Paint cost provides ar best for <br> elit, sed do eiusmod tempe</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Awesome Feature section end-->


    <!--Services section start-->
    <div class=" ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h3>OUR SERVICES</h3>
                         
                         <!-- <p>Grobartig the best theme for elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation.</p> -->
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach($about as $row)
                <div class="col-md-4 col-sm-4 col-sm-12">
                    <div class="single-services wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">
                        <div class="single-services-img">
                            <img src="{{ asset($row->image) }}" alt="">
                        </div>
                        <div class="single-services-desc">
                            <h5>{{ $row->name }}</h5>
                            <p style="">{{  $row->description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <!--Services section end-->
    


    @endsection

