
@section('search-js')
<script type="text/javascript">
    $(document).ready( function(){

        $("#frm-search").submit(function(e){
          e.preventDefault();
          search();
        })
        $("#province").change(function(){
          getDistricts($(this).val());
        })
        $('#district').change(function(){
            getCommunes($(this).val());
        })
        $("#search-property").click(function(e){
          e.preventDefault();
          search();
        })
        
    })
    function getDistricts(province_id){
        //Get districts
        if(province_id != 0){
        $.ajax({
                url: "{{ route('property-districts',$locale) }}?province_id="+province_id,
                type: 'GET',
                data: {},
                success: function( response ) {
                    $("#district-cnt").html(response)
                },
                error: function( response ) {
                    swal("Error!", "Sorry there is an error happens. " ,"error");
                }
        });
        }
    }

    function search(){
        //Stream Amount
        amount          = $('#amount').val();
        amount          = amount.replace("$", "");
        amount          = amount.replace(" $", "");
        amount          = amount.split("-");
        action          = $('#action').val();
        type            = $('#type').val();
        province        = $('#province').val();
        district        = $('#district').val();
        commune         = $('#commune').val();
        key             = $('#key').val();
        limit           = 10;
        url             = "?limit="+limit;

        if(action != 0){
          url += '&action='+action;
        }
        
        if(province != 0){
          url += '&province='+province;
          if(district != 0){
            url += '&district='+district;
            if(commune != 0){
              url += '&commune='+commune;
            }
          }
        }
        if(type != 0){
          url += '&type='+type;
        }

        if(key != ""){
          url += '&key='+key;
        }
       
        if(amount != ""){
          url += '&amount='+amount;
        }
        

        $(location).attr('href', '{{ route('search-property',$locale) }}'+url);
      }
</script>
@endsection
<!--Find home area start-->
<div class="find-home">
    <div class="container">
         <div class="row">
            <div class="col-md-12">
                <div class="find-home-title text-center">
                    <h3>{{__('general.advanced-search')}}</h3>
                </div>
            </div>
            <form action="#">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="find-home-item mb-40">
                        <input type="text" name="key" id="key" placeholder="{{__('general.key-word-property-name-code')}}">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="find-home-item custom-select mb-40">                  
                            <select class="selectpicker" title="{{__('general.province')}}" data-hide-disabled="true" data-live-search="true" id="province" name="province">
                                
                                <optgroup label="">
                                    <option selected="" value="0">{{__('general.province')}}</option>
                                    @php( $province = isset($appends['province'])?$appends['province']:0 )
                                    @if($province != 0)
                                        @php( $lable = DB::table('provinces')->select('id',$locale.'_name as name')->find($province) )
                                        @if( sizeof($lable) == 1 )
                                        <option value="{{$lable->id}}">{{$lable->name}}</option>
                                        @endif
                                    @endif

                                    @php($provinces = $defaultData['provinces'])
                                    @foreach( $provinces as $row)
                                        <option value="{{ $row->id }}" >{{ $row->name }}</option>
                                    @endforeach
                                </optgroup>
                                
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div id="district-cnt">
                            <div class="find-home-item custom-select" >                  
                                <select class="selectpicker" title="{{__('general.district')}}" data-hide-disabled="true" data-live-search="true" id="district" name="district">
                                        <optgroup label="">
                                            @php( $district = isset($appends['district'])?$appends['district']:0 )
                                            @if($province != 0)
                                            @php( $districts = DB::table('districts')->select('id',$locale.'_name as name')->where('province_id', $province)->get() )
                                            @if($district != 0)
                                                @php( $lable = DB::table('districts')->select('id',$locale.'_name as name')->find($district) )
                                                @if( sizeof($lable) == 1 )
                                                <option value="{{ $lable->id }}" >{{ $lable->name }}</option>
                                                @endif
                                                <option value="0">{{__('general.district')}}</option>
                                                @foreach( $districts as $row)
                                                @if($row->id != $district)
                                                    <option value="{{ $row->id }}" >{{ $row->name }}</option>
                                                @endif
                                                @endforeach
                                            @else
                                                <option value="0">{{__('general.district')}}</option>
                                                @foreach( $districts as $row)
                                                <option value="{{ $row->id }}" >{{ $row->name }}</option>
                                                @endforeach
                                            @endif
                                            @else
                                            <option value="0">{{__('general.district')}}</option>
                                            @endif
                                        </optgroup>
                                </select>
                            </div> 
                        </div>
                    </div>
    
                    <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="intro" id="commune-cnt">
                                <div class="find-home-item custom-select">                  
                                    <select class="selectpicker" title="{{__('general.commune')}}" data-hide-disabled="true" data-live-search="true" id="commune" name="commune">
                                            <optgroup label="">
                                                    @php( $commune = isset($appends['commune'])?$appends['commune']:0 )
                                                    @if($province != 0)
                                                    @if($district != 0)
                                                        @php( $communes = DB::table('communes')->select('id',$locale.'_name as name')->where('district_id', $district)->get() )
                                                        @if($commune != 0)
                                                        @php( $lable = DB::table('communes')->select('id',$locale.'_name as name')->find($commune) )
                                                        @if( sizeof($lable) == 1 )
                                                            <option value="{{ $lable->id }}" >{{ $lable->name }}</option>
                                                        @endif
                                                        <option value="0">{{__('general.commune')}}</option>
                                                        @foreach( $communes as $row)
                                                            @if($row->id != $commune)
                                                            <option value="{{ $row->id }}" >{{ $row->name }}</option>
                                                            @endif
                                                        @endforeach
                                                        @else
                                                        <option value="0">{{__('general.commune')}}</option>
                                                        @foreach( $communes as $row)
                                                            <option value="{{ $row->id }}" >{{ $row->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    @else
                                                        <option value="0">{{__('general.commune')}}</option>
                                                    @endif
                                                    @else
                                                    <option value="0">{{__('general.commune')}}</option>
                                                    @endif
                                            </optgroup>
                                    </select>
                                </div> 
                            </div>
                    </div>
                   
                </div>
                <div class="row">
                    
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="find-home-item custom-select mb-40">                  
                            <select class="selectpicker" title="{{__('general.property-type')}}" data-hide-disabled="true" data-live-search="true" id="type" name="type">
                                
                                <optgroup label="">
                                    <option class="active" value="0">{{__('general.property-type')}}</option>
                                    @foreach($defaultData['types']  as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </optgroup>
                                
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="find-home-item custom-select">                  
                            <select class="selectpicker" title="{{__('general.property-action')}}" data-hide-disabled="true" id="action" name="action">
                                <optgroup label="">
                                    <option class="active" value="0">{{__('general.property-action')}}</option>
                                    @foreach($defaultData['actions'] as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-12"> 
                        <div class="find-home-item custom-select">
                            <input type="text" name="price" id="amount" placeholder="Add Your Price" type="text">
                        </div>
                    </div>
                    {{-- <div class="col-md-6 col-sm-4 col-xs-12">
                        <div class="find-home-item custom-select">                  
                            <div class="shop-filter">
                                <div class="price_filter">
                                    <div class="price_slider_amount">
                                    <input type="submit"  value="{{__('general.price-range')}}"/> 
                                        <input  id="amount" name="price"  placeholder="Add Your Price" /> 
                                    </div>
                                    <div id="slider-range"></div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                </div>
                <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 ">
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-12 ">
                            <div class="find-home-item">
                                <button  id="search-property" type="submit">{{__('general.search')}}</button>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 ">
                        </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Find home area end-->