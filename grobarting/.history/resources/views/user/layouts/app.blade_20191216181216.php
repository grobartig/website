@extends('user/layouts.master')

@section ('headercss')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="{{ asset ('public/user/css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/user/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/user/css/main.css') }}">
    <script type="text/javascript" src="{{ asset ('public/user/js/lib/jquery/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset ('public/user/css/lib/summernote/summernote.css') }}"/>
    @yield('appheadercss')
@endsection



@section ('bodyclass')
    class="with-side-menu control-panel control-panel-compact"
@endsection

@section ('header')

<header class="site-header">
    <div class="container-fluid">
        <a target="_blank" href="{{ url('/') }}" class="site-logo">
            <img class="hidden-md-down" src="{{ asset ('public/user/img/logo2.png') }}" alt="">
            <img class="hidden-lg-up" src="{{ asset ('public/user/img/logo2.png') }}" alt="">
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset (Auth::user()->picture) }}" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="{{ route('user.user.profile.edit') }}"><span class="fa fa-user"></span> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('user.auth.logout') }}"><span class="fa fa-sign-out"></span> Logout</a>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
                
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->
@endsection

@section ('menu')
    @php ($menu = "")
    @if(isset($_GET['menu']))
        @php( $menu = $_GET['menu'])
    @endif
    

    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu">
        <ul class="side-menu-list">    
           
           
            <li class="red @yield('active-main-menu-dashboard')">
                <a href="{{ route('user.dashboard.index') }}">
                <span>
                    <i class="fa fa-desktop"></i>
                    <span class="lbl">Dashboard</span>
                </span>
                </a>
            </li>

           
            <li class="@yield('active-main-menu-property') red with-sub">
                <span>
                    <i class=" font-icon fa fa-building"></i>
                    <span class="lbl"> Properties</span>
                </span>
                <ul>
                     @if(checkPermision('user.property.property.create'))<li class=""><a href="{{ route('user.property.property.create') }}"><span class="lbl">Add New</span></a></li>@endif
                     @if(checkPermision('user.property.property.index'))<li class=""><a href="{{ route('user.property.property.index') }}"><span class="lbl">Search</span></a></li>@endif
                                                                        <li class=""><a href="{{ route('user.property.property.charges') }}"><span class="lbl">In Charge</span></a></li>
                     @if(checkPermision('user.property.property.feature'))<li class=""><a href="{{ route('user.property.property.feature') }}"><span class="lbl">Feature</span></a></li>@endif
                     @if(checkPermision('user.property.property.slide'))<li class=""><a href="{{ route('user.property.property.slide') }}"><span class="lbl">Slide</span></a></li>@endif
                    
                </ul>
            </li>
           
           @if(checkPermision('user.owner.index'))
            <li class="red @yield('active-main-menu-owners')">
                <a href="{{ route('user.owner.index') }}">
                <span>
                    <i class="fa fa-user"></i>
                    <span class="lbl">Owners</span>
                </span>
                </a>
            </li>
            @endif
            @if(checkPermision('user.customer.index'))
            <li class="red @yield('active-main-menu-customers')">
                <a href="{{ route('user.customer.index') }}">
                <span>
                    <i class="fa fa-male"></i>
                    <span class="lbl">Customers</span>
                </span>
                </a>
            </li>
            @endif

            @if(checkPermision('user.partner.index'))
            <li class="red @yield('active-main-menu-partner')">
                <a href="{{ route('user.partner.index') }}">
                <span>
                    <i class="fa fa-male"></i>
                    <span class="lbl">Customers</span>
                </span>
                </a>
            </li>
            @endif
           
            @if(checkPermision('user.setup.access'))
            <li class="@yield('active-main-menu-setup') red with-sub">
                <span>
                    <i class=" font-icon fa fa-cog"></i>
                    <span class="lbl"> Set Up</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('user.setup.type.index') }}"><span class="lbl">Type</span></a></li>
                    <li class=""><a href="{{ route('user.setup.location.index') }}"><span class="lbl">Location</span></a></li>
                    <li class=""><a href="{{ route('user.setup.feature.index') }}"><span class="lbl">Feature</span></a></li>
                    <li class=""><a href="{{ route('user.setup.detail.index') }}"><span class="lbl">Detail</span></a></li>
                    <li class=""><a href="{{ route('user.setup.selling-price.index') }}"><span class="lbl">Selling Price</span></a></li>
                    <li class=""><a href="{{ route('user.setup.rental-price.index') }}"><span class="lbl">Rental Price</span></a></li>
                    <li class=""><a href="{{ route('user.setup.role.index') }}"><span class="lbl">Role</span></a></li>
                </ul>
            </li>
            @endif
            @if(checkPermision('user.setup.access'))
            <li class="@yield('active-main-menu-setup') red with-sub">
                <span>
                    <i class=" font-icon fa fa-cog"></i>
                    <span class="lbl"> Web Management</span>
                </span>
                <ul>
                    <span>
                            <li class="@yield('active-main-menu-setup') red with-sub">
                                <i class=" font-icon fa fa-cog"></i>
                                <span class="lbl"> About </span>
                                <ul>
                                        <li class=""><a href="{{ route('user.about.index') }}"><span class="lbl">Our Service</span></a></li> 
                                         <li class=""><a href="{{ route('user.welcome.index') }}"><span class="lbl">Welcome To Grobartig</span></a></li>
                                        <li class=""><a href="{{ route('user.abouts.index') }}"><span class="lbl">About Grobarting</span></a></li>
                                </span>
                                </ul>
                          </li>
                    <li class=""><a href="{{ route('user.url.index') }}"><span class="lbl">Stay Connected</span></a></li>
                    <li class=""><a href="{{ route('user.contacts.index') }}"><span class="lbl">Contact Us</span></a></li>
                </ul>
            </li>
            @endif
           
            
            @if(checkPermision('user.message.index'))
            <li class="red @yield('active-main-menu-file')">
                <a href="{{ route('user.message.index') }}">
                <span>
                    <i class="fa fa-file"></i>
                    <span class="lbl">Message</span>
                </span>
                </a>
            </li>
            @endif
         
            @if(Auth::user()->position_id == 1)
            <li class=" @yield('active-main-menu-user') red with-sub">
                <span>
                    <i class=" font-icon fa fa-globe"></i>
                    <span class="lbl"> Users</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('user.user.user.index') }}"><span class="lbl">User</span></a></li>
                    <li class=""><a href="{{ route('user.user.permision.index') }}"><span class="lbl">Permision</span></a></li>
                </ul>
            </li>
            @endif
           
        </ul>
    </nav><!--.side-menu-->

@endsection

@section ('content')
    <div class="page-content">
        
        @yield ('page-content')
        
    </div>
@endsection




@section ('bottomjs')
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        @yield ('imageuploadjs')
        <script type="text/javascript" src="{{ asset ('public/user/js/lib/tether/tether.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/user/js/lib/bootstrap/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/user/js/plugins.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/user/js/lib/lobipanel/lobipanel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/user/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/user/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset ('public/user/js/lib/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{ asset ('public/user/js/lib/select2/select2.full.min.js')}}"></script>
       <script src="{{ asset ('public/user/js/lib/summernote/summernote.min.js') }}"></script>

       <script>
            $(document).ready(function() {
                $('.summernote').summernote();
            });
        </script>

        <script src="{{ asset ('public/user/js/app.js') }}"></script>
        <script src="{{ asset ('public/user/js/camcyber.js') }}"></script>
        @yield('appbottomjs')

        @if(Session::has('msg'))
        <script type="text/JavaScript">
            toastr.success("{!!Session::get('msg')!!}");
        </script>
        @endif
@endsection