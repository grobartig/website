@extends($route.'.main')
@section ('section-title', 'All Services')
@section ('display-btn-add-new', 'display:none')
@section ('section-css')



@endsection
@section ('section-js')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#btn-search").click(function(){
				search();
			})
		});
		function search(){
			key 	= $('#key').val();
			d_from 		= $('#from').val();
			d_till 		= $('#till').val();
			limit 		= $('#limit').val();

			url="?limit="+limit;
			if(key!=""){
				url+='&key='+key;
			}
			if(isDate(d_from)){
				if(isDate(d_till)){
					url+='&from='+d_from+'&till='+d_till;
				}
			}
			$(location).attr('href', '{{ route($route.'.index') }}'+url);
		}
		
	</script>
@endsection

@section ('section-content')

@if(sizeof($data) > 0)
<div class="table-responsive">
	<table id="table-edit" class="table table-bordered table-hover">
		<thead>
		<tr>
						<th>#</th>
						<th>Title</th>
						<th>Image</th>					
						<th>Created Date</th>
						<th></th>
					</tr>
		</thead>
		<tbody>
	
			@php ($i = 1)
			@foreach ($data as $row)
				<tr>
				<td>{{ $i++ }}</td>
							<td>{{ $row->title }}</td>
						
							<td>
								@if($row->image!="")
									<img src="{{ asset ($row->image) }}" style="max-width:50px" alt="" data-toggle="tooltip" data-placement="bottom">
								@else
									No Image Avaiable
								@endif

							</td>
							
					<td>{{ $row->created_at }}</td>
					 <td style="white-space: nowrap; width: 1%;">
						<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
							   <div class="btn-group btn-group-sm" style="float: none;">
							   
							   <a href="{{ route($route.'.edit', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="glyphicon glyphicon-pencil"></span></a>
		                      
                           	</div>
                       </div>
                    </td> 
				</tr>
			@endforeach
		</tbody>
	</table>
</div >
@else
	<span>No Data</span>
@endif


@endsection