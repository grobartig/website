<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('accounts')->insert(
            [
                [
                    'name'              => 'Account 1',
                    'email'              => 'khouch.koeun@gmail.com',
                    'phone'              => '0965416704',
                    'is_subscribed'      => 1,
                ],
                [
                    'name'              => 'Account 2',
                    'email'              => 'koeun@camcyber.com',
                    'phone'              => '0965416704',
                    'is_subscribed'      => 1,
                ],
               
            ]
        );
	}
}
