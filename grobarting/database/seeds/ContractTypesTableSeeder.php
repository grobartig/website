<?php

use Illuminate\Database\Seeder;

class ContractTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('contract_types')->insert(
            [
                [
                    'name'              => 'None exclusive',
                ],
                [
                    'name'              => 'Exclusive'
                ]
               
               
            ]
        );
	}
}
