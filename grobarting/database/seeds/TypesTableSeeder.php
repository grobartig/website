<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('types')->insert(
            [
                [
                    'kh_name'              => 'Apartment',
                    'en_name'              => 'Apartment',
                    'cn_name'              => 'Apartment', 
                    'abbre'                => 'AP', 
                    'status'               => 1
                ],
                [
                    'kh_name'              => 'Commercial',
                    'en_name'              => 'Commercial',
                    'cn_name'              => 'Commercial', 
                    'abbre'                => 'CL', 
                    'status'               => 1
                ],
                [
                    'kh_name'              => 'Condo',
                    'en_name'              => 'Condo',
                    'cn_name'              => 'Condo', 
                    'abbre'                => 'CD', 
                    'status'               => 1
                ],
              
               
            ]
        );
	}
}
