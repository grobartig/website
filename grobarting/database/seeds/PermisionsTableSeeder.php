<?php

use Illuminate\Database\Seeder;

class PermisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	    //=================================================>> Dashbord
        $category_id    = 1;
        $route          = 'user.dashbord.item';
        $dashbord = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.item1', 'title'=>'Dashbord Item 1' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.item2', 'title'=>'Dashbord Item 2' ], 
                        );

        //=================================================>> Property
        $category_id    = 2;
        $route          = 'user.property';
        $property = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.property.create', 'title'=>'Create' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.property.index', 'title'=>'Search' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.property.print', 'title'=>'print' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.property.feature', 'title'=>'See Featured Properties' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.property.slide', 'title'=>'See Slide Properties' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.staff.add', 'title'=>'Assign Permision' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.tool.listing.index', 'title'=>'Create Listing' ], 
                        );

        //=================================================>> Owner
        $category_id    = 3;
        $route          = 'user.owner';
        $owner = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.index', 'title'=>'Search' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.edit', 'title'=>'View' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.update', 'title'=>'Update' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create', 'title'=>'Create' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash', 'title'=>'Delete' ],
                            
                            [ 'category_id'=> $category_id, 'route'=> $route.'.properties', 'title'=>'Properties' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create-property', 'title'=>'Add Property' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash-property', 'title'=>'Remove Property' ]
                        );
        //=================================================>> Customer
        $category_id    = 4;
        $route          = 'user.customer';
        $customer = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.index', 'title'=>'Search' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.edit', 'title'=>'View' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.update', 'title'=>'Update' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create', 'title'=>'Create' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash', 'title'=>'Delete' ],

                            [ 'category_id'=> $category_id, 'route'=> $route.'.enquiries', 'title'=>'Enquiries' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.edit-enquiry', 'title'=>'View Enquiry' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.update-enquiry', 'title'=>'Update Enquiry' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create-enquiry', 'title'=>'Create Enquiry' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash-enquiry', 'title'=>'Delete Enquiry' ],

                            [ 'category_id'=> $category_id, 'route'=> $route.'.notes', 'title'=>'Notes' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.edit-note', 'title'=>'View Note' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.update-note', 'title'=>'Update Note' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create-note', 'title'=>'Create Note' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash-note', 'title'=>'Delete Note' ],

                            [ 'category_id'=> $category_id, 'route'=> $route.'.inquiries', 'title'=>'Inquiries' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.edit-inquiry', 'title'=>'View Inquiry' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.update-inquiry', 'title'=>'Update Inquiry' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create-inquiry', 'title'=>'Create Inquiry' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash-inquiry', 'title'=>'Delete Inquiry' ],

                            [ 'category_id'=> $category_id, 'route'=> $route.'.properties', 'title'=>'Properties' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.add-property', 'title'=>'Add Property' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.remove-property', 'title'=>'Remove Property' ]
                        );

        //=================================================>> Mailing
        $category_id    = 5;
        $route          = 'user.mailing';
        $mailing = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.access', 'title'=>'Manage Everything in Mailing' ] 
                        );
        //=================================================>> Setup
        $category_id    = 6;
        $route          = 'user.setup';
        $setup = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.access', 'title'=>'Manage Everything in Setup' ] 
                        );
        //=================================================>> Website
        $category_id    = 7;
        $route          = 'user.website';
        $website = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.access', 'title'=>'Manage Everything in Website content' ] 
                        );
        //=================================================>> Setup
        $category_id    = 8;
        $route          = 'user.file';
        $file = array(
                            [ 'category_id'=> $category_id, 'route'=> $route.'.index', 'title'=>'Access' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.edit', 'title'=>'View' ], 
                            [ 'category_id'=> $category_id, 'route'=> $route.'.update', 'title'=>'Update' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.create', 'title'=>'Create' ],
                            [ 'category_id'=> $category_id, 'route'=> $route.'.trash', 'title'=>'Delete' ],
                        );

        DB::table('permisions')->insert(array_merge($dashbord, $property, $owner, $customer, $mailing, $setup, $website, $file));
	}
}
