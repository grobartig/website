<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('clients')->insert(
            [
                [
                    'name'              => 'Andrew MCCarthy',
                    'en_position'              => 'Selller',
                    'cn_position'              => 'Selller',
                    'kh_position'              => 'Selller', 
                    'en_description'        => 'Sed perspiciatis unde omnisiste natus error voluptatem remopa accusantium doloremque laudantium totam rem.', 
                    'kh_description'        => 'Sed perspiciatis unde omnisiste natus error voluptatem remopa accusantium doloremque laudantium totam rem.',
                    'cn_description'        => 'Sed perspiciatis unde omnisiste natus error voluptatem remopa accusantium doloremque laudantium totam rem.',
                    'image'             => 'public/frontend/images/agents/agent-1.jpg',
                    
                ],
               
               
            ]
        );
	}
}
