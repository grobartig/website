<?php

use Illuminate\Database\Seeder;

class SellingPriceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('selling_price_types')->insert(
            [
                [
                    
                    'name' => "USD/Property"
                ],
                [
                    
                    'name' => "USD/Sq.m"
                ]

            ]);
    }
}
