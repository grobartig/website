<?php

use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('contents')->insert(
            [
                [
                    'page'              => 'home',
                    'slug'              => 'who-we-are', 
                    'name'              => 'Content', 
                    'en_content'        => 'We have a total of 25+ years combined experience dealing exclusively with New York buyers and sellers ipsum dolor sit amet, consectetur adipiscing elit.', 
                    'kh_content'        => 'We have a total of 25+ years combined experience dealing exclusively with New York buyers and sellers ipsum dolor sit amet, consectetur adipiscing elit.',
                    'cn_content'        => 'We have a total of 25+ years combined experience dealing exclusively with New York buyers and sellers ipsum dolor sit amet, consectetur adipiscing elit.',
                    'note'              => '',
                    'image_required'    => 1,
                    'image'             => 'public/frontend/images/content/demo-1.jpg',
                    'width'             => 370,
                    'height'            => 224,
                    'editor_required'   => 0,
                    'updater_id'        => 1,
                ],
                
                [
                    'page'              => 'About Us',
                    'slug'              => 'about-us', 
                    'name'              => 'Content', 
                    'en_content'        => 'We have a total of 25+ years combined experience dealing exclusively with New York buyers and sellers ipsum dolor sit amet, consectetur adipiscing elit.', 
                    'kh_content'        => 'We have a total of 25+ years combined experience dealing exclusively with New York buyers and sellers ipsum dolor sit amet, consectetur adipiscing elit.',
                    'cn_content'        => 'We have a total of 25+ years combined experience dealing exclusively with New York buyers and sellers ipsum dolor sit amet, consectetur adipiscing elit.',
                    'note'              => '',
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
               
            ]
        );
	}
}
