<?php

use Illuminate\Database\Seeder;

class ListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('lists')->insert(
            [
                [
                    'name'              => 'List 1'
                ],
                [
                    'name'              => 'List 2'
                ],
               
            ]
        );
	}
}
