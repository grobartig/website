<?php

use Illuminate\Database\Seeder;

class DesignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('designs')->insert(
            [
                [
                    'title'              => 'Duis vel tellu',
                    'is_featured'              => 1,
                    
                    'image'             => 'public/frontend/images/slides/Untitled-1.jpg',
                    
                ],
               
               
            ]
        );
	}
}
