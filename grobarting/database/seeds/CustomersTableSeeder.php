<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	     DB::table('customers')->insert(
            [
                [
                    
                    'cus_id' => "0001",
                    'title_id' => 1,
                    'name' => "Customer 1",
                    'email' => 'cusomer1@yaorealty.com', 
                    'phone1' => '012345611', 
                    'phone2' => '012345612',
                    'password' => bcrypt('123456')
                ],
                [
                    
                    'cus_id' => "0002",
                    'title_id' => 2,
                    'name' => "Customer 2",
                    'email' => 'cusomer2@yaorealty.com', 
                    'phone1' => '012345621', 
                    'phone2' => '012345622',
                    'password' => bcrypt('123456')
                ],
               

            ]);
	}
}
