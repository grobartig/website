<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('districts')->insert(
            [
                [
                    'province_id'          => 1,
                    'kh_name'              => 'Doun Penh',
                    'en_name'              => 'Doun Penh',
                    'cn_name'              => 'Doun Penh',
                    'status'               => 1,
                ],
                [
                    'province_id'          => 1,
                    'kh_name'              => '7 Makara',
                    'en_name'              => '7 Makara',
                    'cn_name'              => '7 Makarah',
                    'status'               => 1,
                ],
                [
                    'province_id'          => 1,
                    'kh_name'              => 'Sen Sok',
                    'en_name'              => 'Sen Sok',
                    'cn_name'              => 'Sen Sok',
                    'status'               => 1,
                ],
               
               
            ]
        );
	}
}
