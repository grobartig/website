<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('provinces')->insert(
            [
                [
                    'kh_name'              => 'Phnom Penh',
                    'en_name'              => 'Phnom Penh',
                    'cn_name'              => 'Phnom Penh',
                    'abbre'              => 'PP',
                    'status'                => 1,
                ],
                [
                    'kh_name'              => 'Kandal',
                    'en_name'              => 'Kandal',
                    'cn_name'              => 'Kandal',
                    'abbre'              => 'KD',
                    'status'                => 1,
                ],
                [
                    'kh_name'              => 'Keb',
                    'en_name'              => 'Keb',
                    'cn_name'              => 'Keb',
                    'abbre'              => 'KB',
                    'status'                => 1,
                ],
               
               
            ]
        );
	}
}
