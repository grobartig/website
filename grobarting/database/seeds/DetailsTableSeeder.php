<?php

use Illuminate\Database\Seeder;

class DetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('details')->insert(
            [
                [
                    'kh_name'              => 'Detail 1',
                    'en_name'              => 'Detail 1',
                    'cn_name'              => 'Detail 1',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 2',
                    'en_name'              => 'Detail 2',
                    'cn_name'              => 'Detail 2',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 3',
                    'en_name'              => 'Detail 3',
                    'cn_name'              => 'Detail 3',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 4',
                    'en_name'              => 'Detail 4',
                    'cn_name'              => 'Detail 4',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 5',
                    'en_name'              => 'Detail 5',
                    'cn_name'              => 'Detail 5',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 6',
                    'en_name'              => 'Detail 6',
                    'cn_name'              => 'Detail 6',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 7',
                    'en_name'              => 'Detail 7',
                    'cn_name'              => 'Detail 7',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 8',
                    'en_name'              => 'Detail 8',
                    'cn_name'              => 'Detail 8',
                    'unit'              => 'm',
                ],
                [
                    'kh_name'              => 'Detail 9',
                    'en_name'              => 'Detail 9',
                    'cn_name'              => 'Detail 9',
                    'unit'              => 'm',
                ],
               
               
            ]
        );
	}
}
