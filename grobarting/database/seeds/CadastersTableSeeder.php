<?php

use Illuminate\Database\Seeder;

class CadastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('cadasters')->insert(
            [
                [
                    'kh_name'              => 'Soft',
                    'en_name'              => 'Soft',
                    'cn_name'              => 'Soft',
                ],
                [
                    'kh_name'              => 'Hard',
                    'en_name'              => 'Hard',
                    'cn_name'              => 'Hard',
                ],
                [
                    'kh_name'              => 'N/A',
                    'en_name'              => 'N/A',
                    'cn_name'              => 'N/A',
                ]
               
               
            ]
        );
	}
}
