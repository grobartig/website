<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User
        $this->call(PositionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PermisionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(AccessesTableSeeder::class);

        //Website
        $this->call(VideosTableSeeder::class);
        $this->call(ContentsTableSeeder::class);
        $this->call(ChooseUsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(DesignsTableSeeder::class);
        
        //Setup
        $this->call(ActionsTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(RentalPriceTypeTableSeeder::class);
        $this->call(SellingPriceTypeTableSeeder::class);
        $this->call(ContractTypesTableSeeder::class);
        $this->call(CadastersTableSeeder::class);
        
        $this->call(ProvincesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(CommunesTableSeeder::class);

        $this->call(FeaturesTableSeeder::class);
        $this->call(DetailsTableSeeder::class);

        
        $this->call(OwnersTableSeeder::class);
        $this->call(CustomersTableSeeder::class);

        //Mailing
        $this->call(AccountsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(ListsTableSeeder::class);

        //Property
    }
}
