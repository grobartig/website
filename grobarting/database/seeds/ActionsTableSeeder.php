<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('actions')->insert(
            [
                [
                    'kh_name'              => 'លក់',
                    'en_name'              => 'Sell',
                    'cn_name'              => '卖',
                ],
                [
                    'kh_name'              => 'ជួល',
                    'en_name'              => 'Rent',
                    'cn_name'              => '出租',
                ],
                [
                    'kh_name'              => '卖 ឫ ជួល',
                    'en_name'              => 'Buy or Rent',
                    'cn_name'              => '出售  或  出租',
                ]
               
               
            ]
        );
	}
}
