<?php

use Illuminate\Database\Seeder;

class OwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	     DB::table('owners')->insert(
            [
                [
                    
                    'name' => "Owner 1",
                    'email' => 'cusomer1@yaorealty.com', 
                    'phone1' => '012345611', 
                    'phone2' => '012345612',
                    'address' => 'simple address'
                    
                ],
                [
                    
                    'name' => "Owner 2",
                    'email' => 'owner2@yaorealty.com', 
                    'email' => 'cusomer2@yaorealty.com', 
                    'phone1' => '012345621', 
                    'phone2' => '012345622', 
                    'address' => 'simple address'
                ]

            ]);
	}
}
