<?php

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('features')->insert(
            [
                [
                    'kh_name'              => 'ចំនួនជាន់ក្រោមដី',
                    'en_name'              => 'Basement',
                    'cn_name'              => '地下室',
                ],
                [
                    'kh_name'              => 'ចំនួនជាន់ (ពីផ្ទៃដី)',
                    'en_name'              => 'Storey (Start from Ground)',
                    'cn_name'              => '层楼',
                ],
                [
                    'kh_name'              => 'បន្ទប់បរិភោគអាហារ',
                    'en_name'              => 'Dinning Room',
                    'cn_name'              => '饭厅',
                ],
                [
                    'kh_name'              => 'បន្ទប់ផ្ទះបាយ',
                    'en_name'              => 'Kitchen Room',
                    'cn_name'              => '厨房',
                ],
                [
                    'kh_name'              => 'បន្ទប់ទទួលភ្ញៀវ',
                    'en_name'              => 'Living Room',
                    'cn_name'              => '客厅',
                ],
                [
                    'kh_name'              => 'បន្ទប់គេង',
                    'en_name'              => 'Bed Room',
                    'cn_name'              => '卧室',
                ],
                [
                    'kh_name'              => 'បន្ទប់ទឹក',
                    'en_name'              => 'Bath Room',
                    'cn_name'              => '浴室',
                ],
                [
                    'kh_name'              => 'ចំណតរថយន្ត',
                    'en_name'              => 'Car Parking',
                    'cn_name'              => '停车场',
                ],
                [
                    'kh_name'              => 'ចំណតម៉ូតូ',
                    'en_name'              => 'Motor Parking',
                    'cn_name'              => '汽车停车场',
                ],
               
               
            ]
        );
	}
}
