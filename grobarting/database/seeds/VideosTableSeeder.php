<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	     DB::table('videos')->insert(
            [
                [
                    
                    'title' => "Title",
                    'youtube_id' => 'EuKbPGoWJOw',
                    'is_featured' => 1
                ],
                [
                    
                    'title' => "Title 2",
                    'youtube_id' => 'EuKbPGoWJOw',
                    'is_featured' => 0
                ],
                [
                    
                    'title' => "Title 3",
                    'youtube_id' => 'EuKbPGoWJOw',
                    'is_featured' => 0
                ],
                [
                    
                    'title' => "Title 4",
                    'youtube_id' => 'EuKbPGoWJOw',
                    'is_featured' => 0
                ],

            ]);
	}
}
