<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('groups')->insert(
            [
                [
                    'name'              => 'Group 1'
                ],
                [
                    'name'              => 'Group 2'
                ],
               
            ]
        );
	}
}
