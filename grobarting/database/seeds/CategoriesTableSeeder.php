<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('categories')->insert(
            [
                [
                    'title'              => 'Dashboard'
                ],
                [
                    'title'              => 'Property'
                ],
                [
                    'title'              => 'Owner'
                ],
                [
                    'title'              => 'Customer'
                ],
                [
                    'title'              => 'Mailing'
                ],
                [
                    'title'              => 'SetUp'
                ],
                [
                    'title'              => 'Web Management'
                ],
                [
                    'title'              => 'File'
                ]
                
               
               
            ]
        );
	}
}
