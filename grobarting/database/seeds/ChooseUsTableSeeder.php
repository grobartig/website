<?php

use Illuminate\Database\Seeder;

class ChooseUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('chooseus')->insert(
            [
                [
                     
                    'en_title'        => 'Designed for your business', 
                    'kh_title'        => 'Designed for your business',
                    'cn_title'        => 'Designed for your business',
                    'en_description'        => 'Designed for your business ', 
                    'kh_description'        => 'Designed for your business',
                    'cn_description'        => 'Designed for your business',
                ],
                
                [
                     
                    'en_title'        => 'Fully responsive', 
                    'kh_title'        => 'Fully responsive',
                    'cn_title'        => 'Fully responsive',
                    'en_description'        => 'Fully responsive', 
                    'kh_description'        => 'Fully responsive',
                    'cn_description'        => 'Fully responsive',
                ],
                [
                     
                    'en_title'        => 'Ample customizations', 
                    'kh_title'        => 'Ample customizations',
                    'cn_title'        => 'Ample customizations',
                    'en_description'        => 'Ample customizations', 
                    'kh_description'        => 'Ample customizations',
                    'cn_description'        => 'Ample customizations',
                ],

                [
                     
                    'en_title'        => 'Bootstrap Compatible', 
                    'kh_title'        => 'Bootstrap Compatible',
                    'cn_title'        => 'Bootstrap Compatible',
                    'en_description'        => 'Bootstrap Compatible', 
                    'kh_description'        => 'Bootstrap Compatible',
                    'cn_description'        => 'Bootstrap Compatible',
                ],

                [
                     
                    'en_title'        => 'Unique Design', 
                    'kh_title'        => 'Unique Design',
                    'cn_title'        => 'Unique Design',
                    'en_description'        => 'Unique Design', 
                    'kh_description'        => 'Unique Design',
                    'cn_description'        => 'Unique Design',
                ],
               
            ]
        );
	}
}
