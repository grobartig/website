<?php

use Illuminate\Database\Seeder;

class CommunesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('communes')->insert(
            [
                [
                    'district_id'           => 1,
                    'kh_name'              => 'Phar Kandal 1',
                    'en_name'              => 'Phar Kandal 1',
                    'cn_name'              => 'Phar Kandal 1',
                    'status'               => 1,
                ],
                [
                    'district_id'           => 1,
                    'kh_name'              => 'Phar Kandal 2',
                    'en_name'              => 'Phar Kandal 2',
                    'cn_name'              => 'Phar Kandal 2',
                    'status'               => 1,
                ],
                [
                    'district_id'           => 1,
                    'kh_name'              => 'Phar Kandal 3',
                    'en_name'              => 'Phar Kandal 3',
                    'cn_name'              => 'Phar Kandal 3',
                    'status'               => 1,
                ],

                [
                    'district_id'           => 2,
                    'kh_name'              => 'Ouresy 1',
                    'en_name'              => 'Ouresy 1',
                    'cn_name'              => 'Ouresy 1',
                    'status'               => 1,
                ],
                [
                    'district_id'           => 2,
                    'kh_name'              => 'Ouresy 2',
                    'en_name'              => 'Ouresy 2',
                    'cn_name'              => 'Ouresy 2',
                    'status'               => 1,
                ],
                [
                    'district_id'           => 2,
                    'kh_name'              => 'Ouresy 3',
                    'en_name'              => 'Ouresy 3',
                    'cn_name'              => 'Ouresy 3',
                    'status'               => 1,
                ],

                [
                    'district_id'           => 3,
                    'kh_name'              => 'Phnom Penh Tmey',
                    'en_name'              => 'Phnom Penh Tmey',
                    'cn_name'              => 'Phnom Penh Tmey',
                    'status'               => 1,
                ],
                [
                    'district_id'           => 3,
                    'kh_name'              => 'Teak Thlar',
                    'en_name'              => 'Teak Thlar',
                    'cn_name'              => 'Teak Thlar',
                    'status'               => 1,
                ],
                [
                    'district_id'           => 3,
                    'kh_name'              => 'Phnom Penh Jas',
                    'en_name'              => 'Phnom Penh Jas',
                    'cn_name'              => 'Phnom Penh Jas',
                    'status'               => 1,
                ],
               
               
            ]
        );
	}
}
