<?php

use Illuminate\Database\Seeder;

class AccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('accesses')->insert(
            [
                ['name'              => 'view-overview'     ],
                ['name'              => 'update-overview'   ],
                ['name'              => 'delete-overview'   ],

                ['name'              => 'view-about'        ],
                ['name'              => 'view-price'        ],
                ['name'              => 'update-price'      ],
                ['name'              => 'view-location'     ],
                ['name'              => 'view-location'     ],
                ['name'              => 'view-picture'      ],
                ['name'              => 'update-picture'    ],
                ['name'              => 'view-photo'        ],
                ['name'              => 'create-photo'      ],
                ['name'              => 'update-photo'      ],
                ['name'              => 'delete-photo'      ],
                ['name'              => 'view-image'        ],
                ['name'              => 'create-image'      ],
                ['name'              => 'update-image'      ],
                ['name'              => 'delete-image'      ],
                ['name'              => 'view-video'        ],
                ['name'              => 'create-video'      ],
                ['name'              => 'update-video'      ],
                ['name'              => 'delete-video'      ],
                ['name'              => 'view-detail'       ],
                ['name'              => 'create-detail'      ],
                ['name'              => 'update-detail'      ],
                ['name'              => 'delete-detail'      ],
                ['name'              => 'view-amenity'      ],
                ['name'              => 'check-amenity'     ],

                ['name'              => 'view-confidentail'  ],
                ['name'              => 'create-customer'   ],
                ['name'              => 'update-customer'   ],
                ['name'              => 'change-customer'   ],
                ['name'              => 'create-owner'      ],
                ['name'              => 'view-owner'        ],
                ['name'              => 'create-owner'      ],
                ['name'              => 'update-owner'      ],
                ['name'              => 'change-owner'      ],

                ['name'              => 'view-inquiry'      ],
                ['name'              => 'delete-inquiry'    ],

                ['name'              => 'view-tool'         ],
                ['name'              => 'view-status'     ],
                ['name'              => 'update-status'     ],
                ['name'              => 'view-print'        ],
                ['name'              => 'view-listing'      ],
                ['name'              => 'create-listing'    ],
                ['name'              => 'view-mailing'      ],
                ['name'              => 'view-facebook'     ],
                
                ['name'              => 'view-staff'      ],
                ['name'              => 'change-staff'      ],
                
            ]
        );
	}
}
