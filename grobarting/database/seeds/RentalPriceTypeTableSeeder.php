<?php

use Illuminate\Database\Seeder;

class RentalPriceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	     DB::table('rental_price_types')->insert(
            [
                [
                    
                    'name' => "USD/Month"
                ],
                [
                    
                    'name' => "USD/Year"
                ]

            ]);
	}
}
