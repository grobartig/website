<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('kh_name', 50)->default('');
            $table->string('en_name', 50)->default('');
            $table->string('cn_name', 50)->default('');
            $table->string('abbre', 5)->unique();
            $table->string('zip_code', 10)->nullable();
            $table->string('lat', 50)->default('11.544873');
            $table->string('lng', 50)->default('104.892167');
            $table->boolean('status')->default(0);
          
            //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
