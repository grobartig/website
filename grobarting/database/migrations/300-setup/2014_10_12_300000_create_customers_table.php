<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('cus_id', 10)->default('')->nullable();
            $table->string('title_id', 10)->default('')->nullable();
            $table->string('name', 100)->default('')->nullable();
            $table->string('phone1', 50)->unique();
            $table->string('phone2', 50)->default('')->unique()->nullable();
            $table->string('email', 50)->default('')->unique()->nullable();
            $table->string('address', 200)->default('')->nullable();
            $table->string('occupation', 100)->default('')->nullable();
           
             //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
