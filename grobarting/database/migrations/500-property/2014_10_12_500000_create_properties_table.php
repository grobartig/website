<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('action_id')->unsigned()->index()->nullable();
            $table->foreign('action_id')->references('id')->on('actions');
            $table->integer('type_id')->unsigned()->index()->nullable();
            $table->foreign('type_id')->references('id')->on('types');
            $table->integer('owner_id')->unsigned()->index()->nullable();
            $table->foreign('owner_id')->references('id')->on('owners');
            $table->integer('customer_id')->unsigned()->index()->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->integer('cadaster_id')->unsigned()->index()->nullable();
            $table->foreign('cadaster_id')->references('id')->on('cadasters');
            $table->integer('contract_type_id')->unsigned()->index()->nullable();
            $table->foreign('contract_type_id')->references('id')->on('contract_types');
            $table->integer('province_id')->unsigned()->index()->nullable();
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->integer('district_id')->unsigned()->index()->nullable();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->integer('commune_id')->unsigned()->index()->nullable();
            $table->foreign('commune_id')->references('id')->on('communes');
           

            $table->string('listing_code', 50)->default('0000');
            $table->boolean('is_published')->default(0);
            $table->boolean('is_featured')->default(0);
            $table->boolean('featured_order')->unsigned()->index()->nullable();
            $table->boolean('is_slided')->default(0);
            $table->boolean('slided_order')->unsigned()->index()->nullable();
            $table->date('exclusive_from')->nullable();
            $table->date('exclusive_till')->nullable();
            $table->integer('num_of_views')->unsigned()->index()->nullable();

            $table->string('kh_name', 50)->default('');
            $table->string('en_name', 50)->default('');
            $table->string('cn_name', 50)->default('');
            $table->string('slug', 50)->unique();

            $table->string('kh_excerpt', 200)->nullable();
            $table->string('en_excerpt', 200)->nullable();
            $table->string('cn_excerpt', 200)->nullable();

            $table->text('kh_description')->nullable();
            $table->text('en_description')->nullable();
            $table->text('cn_description')->nullable();

            $table->date('purchase_date')->nullable();
            //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            
            
            
            
            
            
            
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
