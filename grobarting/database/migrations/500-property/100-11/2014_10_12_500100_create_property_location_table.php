<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_location', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('property_id')->unsigned()->index()->nullable();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');

            $table->string('address', 250)->default('')->nullable();
            $table->string('zip_code', 10)->default('')->nullable();
            $table->boolean('is_actual_map')->default(0);
            $table->string('lat', 50)->default('0')->nullable();
            $table->string('lng', 50)->default('0')->nullable();

            //The field that will appear for almost tables
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->foreign('updater_id')->references('id')->on('users');
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_location');
    }
}
