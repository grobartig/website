<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('slug', 150)->default('');
            $table->string('en_title', 150)->default('');
            $table->string('kh_title', 150)->default('');
            $table->string('cn_title', 150)->default('');
            $table->text('en_description');
            $table->text('kh_description');
            $table->text('cn_description');
            $table->text('en_content');
            $table->text('kh_content');
            $table->text('cn_content');
            $table->string('image', 250)->default('');
			$table->boolean('is_featured')->default(0);
			
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
