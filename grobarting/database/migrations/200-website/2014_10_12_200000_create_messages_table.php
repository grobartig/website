<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id', 11);
            
            $table->string('name', 150)->default('');
			$table->string('phone', 150)->default('');
			$table->string('email', 150)->default('');
			$table->string('message', 250)->default('');
			$table->boolean('is_seen')->default(0);
            $table->integer('seer_id')->unsigned()->index()->nullable();
            $table->date('seen_at')->nullable();
            
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->foreign('updater_id')->references('id')->on('users');
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
