<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_price', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('property_id')->unsigned()->index()->nullable();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            
            $table->decimal('selling_price', 11, 2)->default(0)->nullable();
            $table->integer('selling_price_type_id')->unsigned()->index()->nullable();
            $table->foreign('selling_price_type_id')->references('id')->on('selling_price_types');
            $table->decimal('actual_selling_price', 11, 2)->default(0)->nullable();
            $table->integer('actual_selling_price_type_id')->unsigned()->index()->nullable();
            

            $table->decimal('rental_price', 11, 2)->default(0)->nullable();
            $table->integer('rental_price_type_id')->unsigned()->index()->nullable();
            $table->foreign('rental_price_type_id')->references('id')->on('rental_price_types');
            $table->decimal('actual_rental_price', 11, 2)->default(0)->nullable();
            $table->integer('actual_rental_price_type_id')->unsigned()->index()->nullable();
            
            $table->integer('deposit')->default(0)->nullable();

            //The field that will appear for almost tables
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->foreign('updater_id')->references('id')->on('users');
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_price');
    }
}
