<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChooseusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chooseus', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('en_title', 150)->default('')->nullable();
            $table->string('kh_title', 150)->default('')->nullable();
            $table->string('cn_title', 150)->default('')->nullable();
            $table->text('en_description')->nullable()->nullable(); 
            $table->text('kh_description')->nullable();
            $table->text('cn_description')->nullable();

            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chooseus');
    }
}
