<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists_groups', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('list_id')->unsigned()->index()->nullable();
            $table->foreign('list_id')->references('id')->on('lists')->onDelete('cascade');
			$table->integer('group_id')->unsigned()->index()->nullable();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
           
            $table->string('subject', 150)->default('');
			$table->string('message', 150)->default('')->nullable();
            $table->boolean('is_sent')->default(0);
            $table->integer('sender_id')->unsigned()->index()->nullable();
            $table->datetime('sent_at')->nullable();
           
            //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lists_groups');
    }
}
