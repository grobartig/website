<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerNeedsFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_needs_features', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('customer_need_id')->unsigned()->index()->nullable();
            $table->foreign('customer_need_id')->references('id')->on('customer_needs')->onDelete('cascade');
            $table->integer('feature_id')->unsigned()->index()->nullable();
            $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
            
			
             //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_needs_features');
    }
}
