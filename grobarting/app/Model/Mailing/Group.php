<?php

namespace App\Model\Mailing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
   	use SoftDeletes;
    protected $table = 'groups';
    protected $dates = ['deleted_at'];
    
    public function groupAccounts() {
        return $this->hasMany('App\Model\Mailing\Account_Group', 'group_id');
    }
    public function accounts() {
        return $this->belongsToMany('App\Model\Mailing\Account', 'accounts_groups');
    }
    // public function mailings() {
    //     return $this->hasMany('App\Model\Mailing', 'group_id');
    // }
    
   
}
