<?php

namespace App\Model\Mailing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Record extends Model
{
    use SoftDeletes;
    protected $table = 'lists_groups';
    protected $fillable = [
        'group_id', 'list_id', 'creator_id', 'created_at', 'updated_at'
    ];
    public function list() {
        return $this->belongsTo('App\Model\Mailing\Listing', 'list_id');
    }

    public function group(){
        return $this->belongsTo('App\Model\Mailing\Group', 'group_id');
    }
    public function creator(){
        return $this->belongsTo('App\Model\User\User', 'creator_id');
    }
   
}
