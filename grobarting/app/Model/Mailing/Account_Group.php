<?php

namespace App\Model\Mailing;
use Illuminate\Database\Eloquent\Model;

class Account_Group extends Model
{
   
    protected $table = 'accounts_groups';
    protected $fillable = [
        'account_id', 'group_id', 'creator_id', 'created_at', 'updated_at'
    ];
    public function account() {
        return $this->belongsTo('App\Model\Mailing\Account');
    }

    public function group(){
        return $this->belongsTo('App\Model\Mailing\Group');
    }
    
}
