<?php

namespace App\Model\Mailing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listing extends Model
{
   	use SoftDeletes;
    protected $table = 'lists';
    protected $dates = ['deleted_at'];
    
    public function listProperties() {
        return $this->hasMany('App\Model\Property\PropertyList', 'list_id');
    }
    public function properties() {
        return $this->belongsToMany('App\Model\Property\Property', 'properties_lists', 'list_id');
    }
    //  public function mailings() {
    //     return $this->hasMany('App\Model\Mailing', 'list_id');
    // }
    
   
}
