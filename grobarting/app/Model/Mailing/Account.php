<?php

namespace App\Model\Mailing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
   	use SoftDeletes;
    protected $table = 'accounts';
    protected $dates = ['deleted_at'];
    
    public function accountGroups() {
        return $this->hasMany('App\Model\Mailing\Account_Group');
    }
    public function groups() {
        return $this->belongsToMany('App\Model\Mailing\Group', 'accounts_groups');
    }
    
    // public function properties() {
    //     return $this->belongsToMany('App\Model\Property', 'properties_mailings');
    // }
    // public function mailProperties() {
    //     return $this->hasMany('App\Model\Property_Mailing', 'mail_id');
    // }
    
   
}
