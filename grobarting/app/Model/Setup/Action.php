<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
   
    protected $table = 'actions';
    
    public function properties() {
        return $this->hasMany('App\Model\Property\Property');
    }
   
}
