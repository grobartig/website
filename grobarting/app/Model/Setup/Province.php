<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;
    protected $table = 'provinces';
    protected $dates = ['deleted_at'];
    
    public function districts() {
        return $this->hasMany('App\Model\Setup\District');
    }

    public function commnunes(){
        return $this->hasManyThrough('App\Model\Setup\Commune', 'App\Model\Setup\District');
    }

    public function properties() {
        return $this->hasMany('App\Model\Property\Property');
    }
   
}
