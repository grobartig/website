<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerNeed extends Model
{
   	use SoftDeletes;
    protected $table = 'customer_needs';
    
    public function customer() {
        return $this->belongsTo('App\Model\Setup\Customer');
    }
    public function action() {
        return $this->belongsTo('App\Model\Setup\Action');
    }
    public function type() {
        return $this->belongsTo('App\Model\Setup\Type');
    }

    public function Features() {
        return $this->belongsToMany('App\Model\Setup\Features','customer_needs_features','customer_need_id');
    }
    public function customerNeedFeatures() {
        return $this->hasMany('App\Model\Setup\customerNeedFeatures','customer_need_id');
    }

    public function customerNeedLocation1() {
        return $this->hasOne('App\Model\Setup\CustomerNeedLocation1','customer_need_id');
    }
    public function customerNeedLocation2() {
        return $this->hasOne('App\Model\Setup\CustomerNeedLocation2','customer_need_id');
    }
    
   
}
