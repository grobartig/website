<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
   
    protected $table = 'communes';
    
    public function district() {
        return $this->belongsTo('App\Model\Setup\District');
    }

    public function properties() {
        return $this->hasMany('App\Model\Property\Property');
    }
   
}
