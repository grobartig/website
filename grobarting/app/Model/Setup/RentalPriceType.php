<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RentalPriceType extends Model
{
   	use SoftDeletes;
    protected $table = 'rental_price_types';
   
}
