<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
   
    use SoftDeletes;
    protected $table = 'owners';
    public function properties() {
        return $this->hasMany('App\Model\Property\Property');
    }

}
