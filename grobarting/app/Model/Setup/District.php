<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;
    protected $table = 'districts';
    public function province() {
        return $this->belongsTo('App\Model\Setup\Province');
    }
    public function commnunes() {
        return $this->hasMany('App\Model\Setup\Commune');
    }
    public function properties() {
        return $this->hasMany('App\Model\Property\Property');
    }
   
}
