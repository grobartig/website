<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model
{
    use SoftDeletes;
    protected $table = 'features';
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function featureTypes(){
        return $this->hasMany('App\Model\Setup\FeatureType');
    }
    public function types(){
        return $this->belongsToMany('App\Model\Setup\Type', 'features_types');
    }
    public function featureProperties(){
        return $this->hasMany('App\Model\Property\PropertyFeature');
    }
   
}
