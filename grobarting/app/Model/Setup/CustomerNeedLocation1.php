<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerNeedLocation1 extends Model
{
   	use SoftDeletes;
    protected $table = 'customer_need_location1';
    
    public function customerNeed() {
        return $this->belongsTo('App\Model\Setup\CustomerNeed');
    }

    public function province(){
        return $this->belongsTo('App\Model\Setup\Province');
    }
    public function district(){
        return $this->belongsTo('App\Model\Setup\District');
    }
    public function commune(){
        return $this->belongsTo('App\Model\Setup\Commune');
    }
    
   
}
