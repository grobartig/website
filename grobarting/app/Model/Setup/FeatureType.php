<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;

class FeatureType extends Model
{
   
    protected $table = 'features_types';
    public function feature() {
        return $this->belongsTo('App\Model\Setup\Feature');
    }
    public function type() {
        return $this->belongsTo('App\Model\Setup\Type');
    }

}
