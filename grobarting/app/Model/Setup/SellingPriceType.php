<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellingPriceType extends Model
{
    use SoftDeletes;
    protected $table = 'selling_price_types';
     public function sellingPrice() {
        return $this->hasMany('App\Model\Property\Price', 'selling_price_type_id');
    }
   
}
