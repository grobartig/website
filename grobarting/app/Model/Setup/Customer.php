<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
   
    use SoftDeletes;
    protected $table = 'customers';
   	public function notes(){
        return $this->hasMany('App\Model\Setup\CustomerNote');
    }
    public function needs(){
        return $this->hasMany('App\Model\Setup\CustomerNeed');
    }

    public function properties(){
        return $this->hasMany('App\Model\Property\Property');
    }
    public function enquiries(){
        return $this->hasMany('App\Model\Property\PropertyEnquiry', 'customer_id');
    }
}
