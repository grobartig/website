<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerNote extends Model
{
   	use SoftDeletes;
    protected $table = 'customer_notes';
    
    public function customer() {
        return $this->belongsTo('App\Model\SetUp\Customer');
    }

    
   
}
