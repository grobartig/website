<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customerNeedFeatures extends Model
{
   	//use SoftDeletes;
    protected $table = 'customer_needs_features';
    
     public function customerNeed() {
        return $this->belongsTo('App\Model\Setup\CustomerNeed');
    }
    
   
}
