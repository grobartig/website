<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail extends Model
{
    use SoftDeletes;
    protected $table = 'details';
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function types(){
        return $this->belongsToMany('App\Model\Setup\Type', 'details_types');
    }
    public function detailTypes(){
    	return $this->hasMany('App\Model\Setup\DetailType');
    }
    public function detailProperties(){
    	return $this->hasMany('App\Model\Property\PropertyDetail');
    }
   
}
