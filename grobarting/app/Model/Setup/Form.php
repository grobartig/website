<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
   
    protected $table = 'forms';
    public function types() {
        return $this->hasMany('App\Model\Setup\Type');
    }
   
}
