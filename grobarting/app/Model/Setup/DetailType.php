<?php

namespace App\Model\Setup;
use Illuminate\Database\Eloquent\Model;

class DetailType extends Model
{
   
    protected $table = 'details_types';
    
    public function detail() {
        return $this->belongsTo('App\Model\Setup\Detail');
    }
    public function type() {
        return $this->belongsTo('App\Model\Setup\Type');
    }

}
