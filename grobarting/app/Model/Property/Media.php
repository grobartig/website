<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
   
    protected $table = 'property_media';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
   
   public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
}
