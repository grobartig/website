<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
   
    protected $table = 'property_images';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
   
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
}
