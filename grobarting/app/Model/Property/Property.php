<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;
    protected $table = 'properties';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    //One-to-One relationship
    public function media() {
        return $this->hasOne('App\Model\Property\Media');
    }
    public function price() {
        return $this->hasOne('App\Model\Property\Price');
    }
    public function location() {
        return $this->hasOne('App\Model\Property\Location');
    }

    //Many-to-One Relationship
    public function action(){
        return $this->belongsTo('App\Model\Setup\Action');
    }
    
    public function type(){
        return $this->belongsTo('App\Model\Setup\Type');
    }
    public function agent(){
        return $this->belongsTo('App\Model\Setup\Agent');
    }
    public function owner(){
        return $this->belongsTo('App\Model\Setup\Owner');
    }
    public function customer(){
        return $this->belongsTo('App\Model\Setup\Customer');
    }
    public function province(){
        return $this->belongsTo('App\Model\Setup\Province');
    }
    public function district(){
        return $this->belongsTo('App\Model\Setup\District');
    }
    public function commune(){
        return $this->belongsTo('App\Model\Setup\Commune');
    }
    public function creator(){
        return $this->belongsTo('App\Model\User\User', 'creator_id');
    }
    
   
   
    //Many-to-Many
    
    public function features(){
        return $this->belongsToMany('App\Model\Setup\Feature', 'properties_features');
    }
     public function details(){
        return $this->belongsToMany('App\Model\Setup\Detail', 'properties_details');
    }
    public function lists(){
        return $this->belongsToMany('App\Model\Mailing\Listing', 'properties_lists', 'property_id', 'list_id');
    }
    public function staffs(){
        return $this->belongsToMany('App\Model\User\User', 'properties_staffs_roles', 'property_id', 'staff_id');
    }

    //One-to-Many relationship
    public function videos() {
        return $this->hasMany('App\Model\Property\Video','property_id');
    }
    public function photos() {
        return $this->hasMany('App\Model\Property\Photo');
    }
    public function images() {
        return $this->hasMany('App\Model\Property\Image');
    }
    public function propertyCustomers(){
        return $this->hasMany('App\Model\Property\PropertyCustomer');
    }
    public function propertyFeatures(){
        return $this->hasMany('App\Model\Property\PropertyFeatureX');
    }
    public function propertyDetails(){
        return $this->hasMany('App\Model\Property\PropertyDetail');
    }
    public function propertyLists(){
        return $this->hasMany('App\Model\Property\PropertyList');
    }
    public function propertyStaffs(){
        return $this->hasMany('App\Model\Property\PropertyStaff');
    }
	public function enquiries(){
        return $this->hasMany('App\Model\Property\PropertyEnquiry', 'property_id');
    }
}
