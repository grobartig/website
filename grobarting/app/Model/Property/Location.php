<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
   
    protected $table = 'property_location';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
   
}
