<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyFeatureX extends Model
{
   
    protected $table = 'properties_features';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function feature(){
        return $this->belongsTo('App\Model\Setup\Feature');
    }
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
}
