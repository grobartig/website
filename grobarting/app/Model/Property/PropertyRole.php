<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyRole extends Model
{
   
    protected $table = 'properties_staffs_roles';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function role(){
        return $this->belongsTo('App\Model\Setup\Role');
    }
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
   
}
