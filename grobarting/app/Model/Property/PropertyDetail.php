<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class propertyDetail extends Model
{
   
    protected $table = 'properties_details';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function detail(){
        return $this->belongsTo('App\Model\Setup\Detail');
    }
   
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
}
