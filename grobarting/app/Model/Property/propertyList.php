<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyList extends Model
{
   
    protected $table = 'properties_lists';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property', 'property_id', 'list_id');
    }
    public function listing(){
        return $this->belongsTo('App\Model\Mailing\Listing', 'list_id', 'property_id');
    }
   
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
}
