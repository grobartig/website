<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
   
    protected $table = 'property_videos';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property','property_id');
    }
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
   
}
