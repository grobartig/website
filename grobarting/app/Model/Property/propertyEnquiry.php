<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyEnquiry extends Model
{
   
    protected $table = 'properties_enquiries';
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function customer(){
        return $this->belongsTo('App\Model\Setup\Customer');
    }
   	public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
   
}
