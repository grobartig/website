<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
   
    protected $table = 'property_price';
    
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
    public function selling_price_type(){
        return $this->belongsTo('App\Model\Setup\SellingPriceType');
    }
    public function rental_price_type(){
        return $this->belongsTo('App\Model\Setup\RentalPriceType');
    }
   
   
}
