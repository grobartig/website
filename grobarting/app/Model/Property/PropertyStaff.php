<?php

namespace App\Model\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyStaff extends Model
{
   
    protected $table = 'properties_staffs_roles';
    //protected $fillable = ['is_primary', 'updated_at', 'updater_id'];
    public function property(){
        return $this->belongsTo('App\Model\Property\Property');
    }
    public function staff(){
        return $this->belongsTo('App\Model\User\User', 'staff_id');
    }
    public function role(){
        return $this->belongsTo('App\Model\Setup\Role', 'role_id');
    }
   
    public function user(){
        return $this->belongsTo('App\Model\User\User' ,'updater_id');
    }
}
