<?php

namespace App\Model\Website;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
   
    protected $table = 'messages';
    public function property(){
        return $this->belongsTo('App\Model\Property');
    }
   
}
