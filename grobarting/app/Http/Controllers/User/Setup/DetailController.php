<?php

namespace App\Http\Controllers\User\Setup;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Setup\Detail as Model;
use App\Model\Setup\Type as Type;

class DetailController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.setup.detail";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::orderBy('data_order', 'ASC')->get();
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function create(){
        return view($this->route.'.create', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $user_id  = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'en_name' =>   $request->input('en_name'),
                    'kh_name' =>   $request->input('kh_name'),
                    'cn_name' =>   $request->input('cn_name'),
                    'unit' =>   $request->input('unit'),
                    'creator_id' =>   $user_id,
                    'updater_id' =>   $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'en_name' => 'required|min:1|max:500',
                            'kh_name' => 'required|min:1|max:500',
                            'cn_name' => 'required|min:1|max:500',
                            'image' => [
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(200)->maxHeight(200),
                                        ]
                        ])->validate();

  
        $image = FileUpload::uploadFile($request, 'image', 'uploads/detail');
        if($image != ""){
            $data['icon'] = $image; 
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id  = Auth::id();
        Validator::make(
        				$request->all(), 
			        	[
                            
                            'en_name' => 'required|min:1|max:500',
                            'kh_name' => 'required|min:1|max:500',
                            'cn_name' => 'required|min:1|max:500',
                            'image' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(200)->maxHeight(200),
                                        ]
                            
                        ],
                        [
                            
                        ])->validate();

		
		$data = array(
                    'en_name' =>   $request->input('en_name'),
                    'kh_name' =>   $request->input('kh_name'),
                    'cn_name' =>   $request->input('cn_name'),
                    'unit' =>   $request->input('unit'),
                    'updater_id' =>   $user_id,
                );

        $image = FileUpload::uploadFile($request, 'image', 'uploads/detail');
        if($image != ""){
            $data['icon'] = $image; 
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

    public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('status' => $request->input('status'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Feature status has been updated.'
      ]);
    }
    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }

    public function types($id = 0){
        $this->validObj($id);
        $types = Type::get();
        $details = Model::find($id)->detailTypes()->select('type_id')->get();
        return view($this->route.'.types', ['route'=>$this->route, 'id'=>$id, 'types'=>$types, 'details'=>$details]);
    }


    public function check(Request $request){
        $detail_id    = $_GET['detail_id'];
        $type_id     = $_GET['type_id'];
        $now        = date('Y-m-d H:i:s');
       
        
        $type = Type::find($type_id);
        $data = $type->detailTypes()->where(['detail_id' => $detail_id])->first();
        if(sizeof($data) == 1){
            $type->detailTypes()->where('id', $data->id)->delete();
            return response()->json([
                  'status' => 'success',
                  'msg' => 'Feature has been removed.'
            ]);
        }else{
            $data_type=array(
                'detail_id' => $detail_id,
                'type_id' => $type_id,
                
                'created_at' => $now, 
                'updated_at' => $now
                );
             $type->detailTypes()->insert($data_type);
             return response()->json([
                  'status' => 'success',
                  'msg' => 'Type has been added.'
              ]);
        }
    }
   
}
