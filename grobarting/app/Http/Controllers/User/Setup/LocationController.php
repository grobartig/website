<?php

namespace App\Http\Controllers\User\Setup;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Setup\Province as Model;
use App\Model\Setup\District as District;
use App\Model\Setup\Commune as Commune;


class LocationController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.setup.location";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::orderBy('en_name', 'ASC')->get();
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function create(){
        return view($this->route.'.create', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $user_id  = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'en_name' =>   $request->input('en_name'),
                    'kh_name' =>   $request->input('kh_name'),
                    'cn_name' =>   $request->input('cn_name'),
                    'abbre' =>   $request->input('abbre'),
                    'zip_code' =>   $request->input('zip_code'),
                    'lat' =>   $request->input('lat'),
                    'lng' =>   $request->input('lng'),
                    'creator_id' => $user_id, 
                    'updater_id' => $user_id, 
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'en_name' => 'required',
                            'kh_name' => 'required',
                            'cn_name' => 'required',
                            'lat' => 'required',
                            'lng' => 'required',
                            'zip_code' => 'required|min:5',
                            'abbre' => 'required|min:2|max:3',


                        ])->validate();

        
        if($request->input('active')=="")
        {
            $data['status']=0;
        }else{
            $data['status']=1;
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
         $user_id  = Auth::id();
        Validator::make(
        				$request->all(), 
			        	[
                            
                            'en_name' => 'required',
                            'kh_name' => 'required',
                            'cn_name' => 'required',
                            'lat' => 'required',
                            'lng' => 'required',
                            'zip_code' => 'required|min:5',
                            'abbre' => 'required|min:2|max:3',
                            
                        ])->validate();

		
		$data = array(
                    'en_name' =>   $request->input('en_name'),
                    'kh_name' =>   $request->input('kh_name'),
                    'cn_name' =>   $request->input('cn_name'),
                    'abbre' =>   $request->input('abbre'),
                    'zip_code' =>   $request->input('zip_code'),
                    'lat' =>   $request->input('lat'),
                    'lng' =>   $request->input('lng'),
                    'updater_id' => $user_id, 
                );

        if($request->input('active')=="")
        {
            $data['status']=0;
        }else{
            $data['status']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

    public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('status' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Feature status has been updated.'
      ]);
    }

     public function districts($id = 0){
        $this->validObj($id);
        $districts = Model::find($id)->districts()->select('*')->get();
        return view($this->route.'.districts', ['route'=>$this->route, 'id'=>$id, 'districts'=>$districts]);
    }
    public function editDistrict($id = 0, $district_id = 0){
        $this->validObj($id);
        $data = Model::find($id)->districts()->find($district_id);
        //dd($data);
        return view($this->route.'.editDistrict', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function updateDistrict(Request $request){
        $id = $request->input('district_id');

        Validator::make(
                        $request->all(), 
                        [
                            
                            'en_name' => 'required',
                            'kh_name' => 'required',
                            'cn_name' => 'required',
                            'lat' => 'required',
                            'lng' => 'required',
                            'zip_code' => 'required|min:5'
                            
                        ],
                        [
                            
                        ])->validate();

        $user_id  = Auth::id();
        $data = array(
                    'en_name' =>   $request->input('en_name'),
                    'kh_name' =>   $request->input('kh_name'),
                    'cn_name' =>   $request->input('cn_name'),
                    'province_id' =>   $request->input('id'),
                    'zip_code' =>   $request->input('zip_code'),
                    'lat' =>   $request->input('lat'),
                    'lng' =>   $request->input('lng'),
                    'updater_id'=>$user_id
                );

        if($request->input('active')=="")
        {
            $data['status']=0;
        }else{
            $data['status']=1;
        }
        district::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    public function createDistrict($id = 0){
        $this->validObj($id);
        return view($this->route.'.createDistrict', ['route'=>$this->route,'id'=>$id]);
    }
    public function storeDistrict(Request $request) {
         $user_id  = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'en_name' =>   $request->input('en_name'),
                    'kh_name' =>   $request->input('kh_name'),
                    'cn_name' =>   $request->input('cn_name'),
                    'status' =>   $request->input('status'),
                    'province_id' =>   $request->input('province_id'),
                    'zip_code' =>   $request->input('zip_code'),
                    'lat' =>   $request->input('lat'),
                    'lng' =>   $request->input('lng'),
                    'creator_id' => $user_id, 
                    'updater_id' => $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                             'en_name' => 'required',
                            'kh_name' => 'required',
                            'cn_name' => 'required',
                            'lat' => 'required',
                            'lng' => 'required',
                            'zip_code' => 'required|min:5'

                        ])->validate();

        if($request->input('active')=="")
        {
            $data['status']=0;
        }else{
            $data['status']=1;
        }
       $province_id =$request->input('province_id'); 
        $id=district::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
        return redirect(route($this->route.'.districts', $province_id));
    }
    function updateDistrictStatus(Request $request){
      $id   = $request->input('id');
      $data = array('status' => $request->input('active'));
      District::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Feature status has been updated.'
      ]);
    }

     public function trashDistrict($id){
        District::where('id', $id)->update(['deleter_id' => Auth::id()]);
        District::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

    public function communes(){
        $district_id = isset($_GET['district_id'])?$_GET['district_id']:0;
        $communes = District::find($district_id)->commnunes;
        return view($this->route.'.communes', ['route'=>$this->route,'communes'=>$communes]);
    }

    public function storeCommnue(Request $request) {
        Validator::make(
                        $request->all(), 
                        [
                            'kh_name' => 'required|min:1|max:50',
                            'en_name' => 'required|min:1|max:50',
                            'cn_name' => 'required|min:1|max:50',
                            'lat' => 'required|min:1|max:50',
                            'lng' => 'required|min:1|max:50',
                        ],
                        [
                        
                        ])->validate();

        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'kh_name' =>   $request->input('kh_name'), 
                    'en_name' =>  $request->input('en_name'), 
                    'cn_name' =>   $request->input('cn_name'),
                    'district_id' =>   $request->input('district_id'),
                    'zip_code' =>   $request->input('zip_code'),
                    'lat' =>   $request->input('lat'), 
                    'lng' =>  $request->input('lng'),
                    'created_at' =>   $now, 
                    'updated_at' =>   $now, 
                );
        
        if($request->input('active')=="")
        {
            $data['status']=0;
        }else{
            $data['status']=1;
        }

        Commune::insert($data);
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been created.'
        ]);
    }

    public function trashCommnue(Request $request){
        $id = $request->input('id');
        Commune::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }
    public function editCommnue(){
        $id = isset($_GET['commnue_id'])?$_GET['commnue_id']:0;
       
        $data = Commune::find($id);
        //dd($commnue);
        return view($this->route.'.editCommnue', ['route'=>$this->route,'data'=>$data]);
    }

    public function updateCommnue(Request $request) {
        
        Validator::make(
                        $request->all(), 
                        [
                            'kh_name' => 'required|min:1|max:50',
                            'en_name' => 'required|min:1|max:50',
                            'cn_name' => 'required|min:1|max:50',
                            'lat' => 'required|min:1|max:50',
                            'lng' => 'required|min:1|max:50',
                        ],
                        [
                        
                        ])->validate();

        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'kh_name' =>   $request->input('kh_name'), 
                    'en_name' =>  $request->input('en_name'), 
                    'cn_name' =>   $request->input('cn_name'),
                    'district_id' =>   $request->input('district_id'),
                    'zip_code' =>   $request->input('zip_code'),
                    'lat' =>   $request->input('lat'), 
                    'lng' =>  $request->input('lng'),
                    'created_at' =>   $now, 
                    'updated_at' =>   $now, 
                );
        if($request->input('active')=="")
        {
            $data['status']=0;
        }else{
            $data['status']=1;
        }
        $id = $request->input('id');
        Commune::where('id', $id)->update($data);
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been updated.'
        ]);
    }

    function updateCommnueStatus(Request $request){
      $id   = $request->input('id');
      $data = array('status' => $request->input('active'));
      Commune::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Feature status has been updated.'
      ]);
    }
   
}
