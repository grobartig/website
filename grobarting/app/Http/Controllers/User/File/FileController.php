<?php

namespace App\Http\Controllers\User\File;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Setup\File as Model;




class FileController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.file";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $this->checkPermision($this->route.'.index');
        $data = Model::select('*')->get();
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function create(){
        $this->checkPermision($this->route.'.create');
        return view($this->route.'.create', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $this->checkPermision($this->route.'.create');
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');

       
        $data = array(
                    'name' =>   $request->input('name'), 
                      //Must include for all store
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        $v   =   Validator::make(
                       $request->all(), 
                        [
                            
                            'name' => ['required'],
                            'file' => [
                                            'required'
                                        ]
                                        
                        ]);
        $v->validate();

        $file = FileUpload::uploadFile($request, 'file', 'uploads/file/');
        if($file != ""){
            $data['file'] = $file; 
        }
      
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->checkPermision($this->route.'.edit');
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $this->checkPermision($this->route.'.update');
        $id = $request->input('id');

        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $data = array(
                    'name' =>   $request->input('name'), 
                    //Must include for all update
                    'updater_id' =>  $user_id,
                    'updated_at' => $now
                );

      
		 $v   =   Validator::make(
                       $request->all(), 
                        [
                            
                            'name' => ['required'],
                            'file' => [
                                            'required'
                                        ]
                                        
                        ]);
        $v->validate();

        $file = FileUpload::uploadFile($request, 'file', 'uploads/file/');
        if($file != ""){
            $data['file'] = $file; 
        }
      
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        $this->checkPermision($this->route.'.trash');
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }


    
}
