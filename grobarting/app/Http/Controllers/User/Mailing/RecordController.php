<?php

namespace App\Http\Controllers\User\Mailing;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule; 
use App\Http\Controllers\CamCyber\FunctionController;

use Illuminate\Support\Facades\Mail as MailSupport;
use App\Mail\PropertyMailing;

use App\Model\Mailing\Record as Model;
use App\Model\Mailing\Group as Group;
use App\Model\Mailing\Listing as Listing;
use App\Model\User\User as Creator;
 use App\Model\Mailing\Account as Account;

class RecordController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.mailing.record";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }
   
    public function index(){
        $data = Model::select('*');
        $limit    =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $creator    =   intval(isset($_GET['creator'])?$_GET['creator']:0); 
        $key      =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('subject', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }

         if( $creator > 0 ){
            $data = $data->where('creator_id', $creator);
             $appends['creator'] = $creator;
        }

        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        //$groups     = Group::select('id', 'name')->get();
        $creators     = Creator::select('id', 'en_name as name')->where('visible', 1)->get();
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends, 'creators'=>$creators]);
    }
   
    
    public function create($id = 0){
        $groups     = Group::select('id', 'name')->has('accounts', '>', 0)->get();
        $lists     = Listing::select('id', 'name')->has('properties', '>', 0)->get();
        return view($this->route.'.create', ['route'=>$this->route, 'groups'=>$groups, 'lists'=>$lists]);
    }
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(

                    'group_id' =>  $request->input('group_id'),
                    'list_id' =>  $request->input('list_id'),
                     'subject' =>  $request->input('subject'),
                    'message' =>  $request->input('message'),
                    
                    //Must include for all store
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                           'group_id' => 'exists:groups,id',
                           'list_id' => 'exists:lists,id',
                           'subject' => 'required'
                        ])->validate();


        $id=Model::insertGetId($data);

        Session::flash('msg', 'Data has been Created!');
        return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }


    public function update(Request $request){
        $id = $request->input('id');
        Validator::make(
                        $request->all(), 
                        [
                            'subject' => 'required',
                            'message' => 'required',
                           
                        ])->validate();

        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $data = array(
                    'subject' =>   $request->input('subject'), 
                    'message' =>  $request->input('message'), 
                     //Must include for all update
                    'updater_id' =>  $user_id,
                    'updated_at' => $now
                );
        
    
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

    function sendListToAccount(Request $request){
        
        $record_id = $request->input('record_id');
        $mail_id = $request->input('mail_id');
        $list_id = $request->input('list_id');
        
        $mail = Account::find($mail_id); 
        if( $mail->is_subscribed == 1){
            $message = Model::find($record_id);

            $properties = $message->list->properties()->select('properties.id', 'en_name as name', 'is_published', 'slug')->where('is_published', 1)->get();
            if(count($properties)>0){
                $user_id    = Auth::id();
                $now        = date('Y-m-d H:i:s');
                $status ="no";
                foreach( $properties as $row ){
                    if($row->is_published == 1){
                        $data       = array(
                            'mail_id' =>  $mail_id,
                            'property_id' =>  $row->id,
                            'creator_id' =>  $user_id,
                            'created_at' => $now, 
                            'updated_at' => $now
                        );
                        
                        //$row->propertyMails()->insert($data);
                        $status = "Yes";
                    }
                    
                }

                MailSupport::to($mail->email)->send(new PropertyMailing($message, $properties));

                return response()->json([
                    'status' => 'success',
                    'mail_id' => $mail_id
                ]);
            }else{
                return response()->json([
                    'status' => 'false',
                    'mail_id' => $mail_id,
                ]);
            }
        }else{
            return response()->json([
                'status' => 'false',
                'mail_id' => $mail_id,
            ]);
        }

           
    }

    function sentDone(Request $request){
        $record_id = $request->input('record_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
            'is_sent' =>  1,
            'sender_id' =>  $user_id,
            'sent_at' =>  $now
        );
        Model::where('id', $record_id)->update($data);
        echo "Sent Done";
    }

   
}
