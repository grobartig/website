<?php

namespace App\Http\Controllers\User\Mailing;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Mailing\Group as Model;
use App\Model\Mailing\Account;

class GroupController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.mailing.group";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends]);
    }
   
    
    public function create($id = 0){
        return view($this->route.'.create', ['route'=>$this->route]);
    }
     public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    
                    'name' =>  $request->input('name'),
                    'note' =>  $request->input('note'),
                    //Must include for all store
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'name' => 'required'
                        ])->validate();


        $id=Model::insertGetId($data);

        Session::flash('msg', 'New data has been Created!');
        return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        Validator::make(
        				$request->all(), 
			        	[
						   'name' => 'required',

						])->validate();

		
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
		 $data       = array(
                    
                    'name' =>  $request->input('name'), 
                    'note' =>  $request->input('note'),
                     //Must include for all update
                    'updater_id' =>  $user_id,
                    'updated_at' => $now
                    );
        
       
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    public function accounts($id=0){
        // $this->validObj($id);
        // $data = Model::find($id)->groupAccounts()->orderBy('id', 'DESC')->get();
        // return view( $this->route.'.accounts', [ 'route'=>$this->route, 'id'=>$id, 'data'=>$data]);

         $this->validObj($id);
        $data = Model::find($id)->groupAccounts();

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        if( $limit == 0 ){
            $limit = 10;
        }
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->whereHas('account', function($query){
                $key       =   isset($_GET['key'])?$_GET['key']:"";
                $query->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('phone', 'like', '%'.$key.'%');
            });
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('accounts_groups.created_at', [$from, $till]);
            }
        }

        $data= $data->orderBy('accounts_groups.created_at', 'DESC')->paginate($limit);
        return view($this->route.'.accounts', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'appends'=>$appends]);

    }
    public function removeAccount($id, $account_id){
        Model::find($id)->groupAccounts()->where('account_id', $account_id)->delete();
        Session::flash('msg', 'Account has been removed from group!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Account has been removed from group'
        ]);
    }
    public function searchAccount(Request $request){
        $id = $request->input('id');
        $key = $request->input('key');
        //DB::enableQueryLog();
        $data = Account::select('id', 'name', 'email')->where('name', 'like', '%'.$key.'%')->where('email', 'like', '%'.$key.'%')->whereDoesntHave('accountGroups', function ($query) {
            $id = $_GET['id'];
            $query->where('group_id', '=', $id);
        })->limit(100)->orderBy('id', 'DESC')->get();
        //dd(DB::getQueryLog());
        return view( $this->route.'.searchAccount', ['data'=>$data]);
    }
    public function selectedAccount(Request $request){
        $id = $request->input('id');
        $data = Model::find($id)->accounts()->select('accounts.id', 'name', 'email')->orderBy('id', 'DESC')->get();
        return view( $this->route.'.selectedAccount', ['data'=>$data]);
    }
    public function removeAccountFromGroup(Request $request){
        $id = $request->input('id');
        $account_id = $request->input('account_id');
        Model::find($id)->groupAccounts()->where('account_id', $account_id)->delete();
        echo $this->selectedAccount($request);
    }
    public function addAccountToGroup(Request $request){
        $id = $request->input('id');
        $account_id = $request->input('account_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'group_id' =>  $id,
                    'account_id' =>  $account_id,
                    'creator_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Model::find($id)->groupAccounts()->insert($data);
        echo $this->selectedAccount($request);
    }
}
