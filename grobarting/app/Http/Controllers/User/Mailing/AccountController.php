<?php

namespace App\Http\Controllers\User\Mailing;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FunctionController;
use Illuminate\Support\Facades\DB;

use App\Model\Mailing\Account as Model;
use App\Model\Mailing\Group;

class AccountController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.mailing.account";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends]);
    }
   
    
    public function create($id = 0){
        return view($this->route.'.create', ['route'=>$this->route]);
    }
     public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    
                    'name' =>  $request->input('name'),
                    'email' =>  $request->input('email'),
                    'phone' =>  $request->input('phone'),
                    'is_subscribed' =>  $request->input('is_subscribed'),

                    //Must include for all store
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        //dd($data);
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'name' => 'required',
                            'email' => [
                                            'required',
                                            'email',
                                            Rule::unique('accounts')
                                        ],
                            'phone' => [
                                            'required',
                                            Rule::unique('accounts')
                                        ],
                        ])->validate();


        $id=Model::insertGetId($data);

        Session::flash('msg', 'New account has been Created!');
        return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        Validator::make(
        				$request->all(), 
			        	[
						   'name' => 'required',
                            'email' => [
                                            'required',
                                            'email',
                                            Rule::unique('mails')->ignore($id)
                                        ],
						])->validate();

		$user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
		 $data       = array(
                    
                    'name' =>  $request->input('name'),
                    'email' =>  $request->input('email'),
                    'is_subscribed' =>  $request->input('is_subscribed') , 
                     //Must include for all update
                    'updater_id' =>  $user_id,
                    'updated_at' => $now
                    );
        
       
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

    public function groups($id=0){
        $this->validObj($id);
        $data = Model::find($id)->accountGroups();

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        if( $limit == 0 ){
            $limit = 10;
        }
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->whereHas('group', function($query){
                $key       =   isset($_GET['key'])?$_GET['key']:"";
                $query->where('name', 'like', '%'.$key.'%');
            });
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('accounts_groups.created_at', [$from, $till]);
            }
        }

        $data= $data->orderBy('accounts_groups.created_at', 'DESC')->paginate($limit);
        return view($this->route.'.groups', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'appends'=>$appends]);
    }

    function createAccountGroup(Request $request){
        $name = $request->input('name');
        $list = json_decode($request->input('list'));
        $now        = date('Y-m-d H:i:s');
        $user_id    = Auth::id();
        $data       = array(
                            'name'          =>  $name,
                            'creator_id'    =>  $user_id,
                            'created_at'    =>  $now, 
                            'updated_at'    =>  $now
                            );
        $id = Group::insertGetId($data);
        foreach( $list as $row ){
            Group::find($id)->groupAccounts()->insert(['group_id'=>$id, 'account_id'=>$row, 'creator_id'=>$user_id,'updater_id'=>$user_id, 'created_at'=>$now, 'updated_at'=>$now]);
        }
        return response()->json([
            'status' => 'success',
            'msg' => 'A new group of mails has been created!'
        ]);

    }

    public function removeGroup($id, $group_id){
        Model::find($id)->accountGroups()->where('group_id', $group_id)->delete();
        Session::flash('msg', 'Data has been removed!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been removed.'
        ]);
    }

    public function searchGroup(Request $request){
        $id = $request->input('id');
        $key = $request->input('key');
        DB::enableQueryLog();
        $data = Group::select('id', 'name')->where('name', 'like', '%'.$key.'%')->whereDoesntHave('groupAccounts', function ($query) {
            $id = $_GET['id'];
            $query->where('account_id', '=', $id);
        })->limit(100)->orderBy('id', 'DESC')->get();
        //dd(DB::getQueryLog());
        return view( $this->route.'.searchGroup',  ['id'=>$id,'data'=>$data]);
    }

    public function selectedGroup(Request $request){
        $id = $request->input('id');
        $data = Model::find($id)->groups()->select('groups.id', 'name')->orderBy('id', 'DESC')->get();
        return view( $this->route.'.selectedGroup', ['data'=>$data]);
    }

    public function removeGroupFromAccount(Request $request){
        $id = $request->input('id');
        $group_id = $request->input('group_id');
        Model::find($id)->accountGroups()->where('group_id', $group_id)->delete();
        echo $this->selectedGroup($request);
    }

    public function addGroupToAccount(Request $request){
        $id = $request->input('id');
        $group_id = $request->input('group_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'account_id' =>  $id,
                    'group_id' =>  $group_id,
                    'creator_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Model::find($id)->accountGroups()->insert($data);
        echo $this->selectedGroup($request);
    }

    public function addAccountsToGruop(Request $request){
       
        $group_id = $request->input('group_id');
        $accounts = json_decode($request->input('list'));
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        
        $num_of_added = 0;
        $num_of_unadded = 0;
        $submitted_accounts = count($accounts);
       
        $groupAccounts = Group::find($group_id)->groupAccounts();
        foreach( $accounts as $account ){
            
            $existing = Group::find($group_id)->groupAccounts()->select('id')->where(['account_id'=> $account, 'group_id'=> $group_id])->count();
            //echo $account.' '.$existing.'<br /> ';
            if($existing == 0){
                Group::find($group_id)->groupAccounts()->insert(['group_id'=>$group_id, 'account_id'=>$account, 'creator_id'=>$user_id, 'created_at'=>$now, 'updated_at'=>$now]);
                $num_of_added++ ;
            }else{
                $num_of_unadded++;
            }
        }

        $msg = $num_of_added." account(s) has been added to a group. ";

        if($num_of_added != $submitted_accounts){
            $msg .= $num_of_unadded." not added becase already existed in target group.";
        }

        if( $num_of_unadded == $submitted_accounts){
            $msg = " No accounts added becase already existed in that group.";
        }
        

        return response()->json([
            'status' => 'success',
            'msg' => $msg
        ]);
        
    }

    

    public function properties($id=0){
        $this->validObj($id);
        $data = Model::find($id)->mailProperties();

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        if( $limit == 0 ){
            $limit = 10;
        }
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->whereHas('property', function($query){
                $key       =   isset($_GET['key'])?$_GET['key']:"";
                $query->where('en_name', 'like', '%'.$key.'%')->orWhere('listing_code', 'like', '%'.$key.'%');
            });
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('properties_mailings.created_at', [$from, $till]);
            }
        }

        $data= $data->orderBy('properties_mailings.created_at', 'DESC')->paginate($limit);
        return view($this->route.'.properties', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'appends'=>$appends]);
    }
}
