<?php

namespace App\Http\Controllers\User\Mailing;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Mailing\Listing as Model;
use App\Model\Property\Property as Property;

class ListController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.mailing.list";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('name', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends]);
    }
   
    
    public function create($id = 0){
        return view($this->route.'.create', ['route'=>$this->route]);
    }
     public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    
                    'name' =>  $request->input('name'),
                    'note' =>  $request->input('note'),
                     //Must include for all store
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'name' => 'required',
                        ])->validate();


        $id=Model::insertGetId($data);

        Session::flash('msg', 'Data has been Created!');
        return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        Validator::make(
        				$request->all(), 
			        	[
						   'name' => 'required',
						])->validate();

		$user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
		 $data       = array(
                    
                    'name' =>  $request->input('name'),
                    'note' =>  $request->input('note'), 
                     //Must include for all update
                    'updater_id' =>  $user_id,
                    'updated_at' => $now
                     );
        
       
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

    public function properties($id=0){
        $this->validObj($id);
        $data = Model::find($id)->properties()->orderBy('id', 'DESC')->get();
        return view( $this->route.'.properties', [ 'route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }
    public function removeProperty($id, $property_id){
        Model::find($id)->listProperties()->where('property_id', $property_id)->delete();
        Session::flash('msg', 'Property has been removed from list!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed from list'
        ]);
    }
    public function searchProperty(Request $request){
        $id = $request->input('id');
        //DB::enableQueryLog();
        $data = Property::select('id', 'listing_code', 'en_name as name');
        $data = $data->where('is_published', 1)->where(function ($query) {
                    $key = $_GET['key'];
                    $query->where('en_name', 'like', '%'.$key.'%')->orWhere('listing_code', 'like', '%'.$key.'%');
                
                })->whereDoesntHave('propertyLists', function ($query) {
                    $id = $_GET['id'];
                    $query->where('list_id', '=', $id);
                })->limit(100)->orderBy('id', 'DESC')->get();
        //dd(DB::getQueryLog());
        return view( $this->route.'.searchProperty', ['data'=>$data]);
    }
    public function selectedProperty(Request $request){
        $id = $request->input('id');
        $data = Model::find($id)->properties()->select('properties.id', 'listing_code', 'en_name as name')->orderBy('id', 'DESC')->get();
        return view( $this->route.'.selectedProperty', ['data'=>$data]);
    }
    public function removePropertyFromList(Request $request){
        $id = $request->input('id');
        $property_id = $request->input('property_id');
        Model::find($id)->listProperties()->where('property_id', $property_id)->delete();
        echo $this->selectedProperty($request);
    }
    public function addPropertyToList(Request $request){
        $id = $request->input('id');
        $property_id = $request->input('property_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'list_id' =>  $id,
                    'property_id' =>  $property_id,
                    'creator_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Model::find($id)->listProperties()->insert($data);
        echo $this->selectedProperty($request);
    }
}
