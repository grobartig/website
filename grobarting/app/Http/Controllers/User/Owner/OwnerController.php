<?php

namespace App\Http\Controllers\User\Owner;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Setup\Owner as Model;
use App\Model\Property\Property as Property;

class OwnerController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.owner";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('phone1', 'like', '%'.$key.'%')->orWhere('phone2', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('id', 'DESC')->paginate($limit);
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends]);
    }
   
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'), 
                    'phone1' =>  $request->input('phone1'), 
                    'phone2' =>  $request->input('phone2'), 
                    'email' =>  $request->input('email'),
                    'address' =>  $request->input('address'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        
        Session::flash('invalidData', $data );
        $v   =   Validator::make(
                        $data, 
                        [
                            
                            'phone1' => [
                                            'required', 
                                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                            Rule::unique('owners')
                                        ]
                        ]);

        $v->sometimes(['phone2'], ['required','regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', Rule::unique('owners')], function ($input) {
            return $input->phone2 != "";
        });

        $v->sometimes(['email'], ['email', Rule::unique('owners')], function ($input) {
            return $input->email != "";
        });

        $v->validate();

      
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'), 
                    'phone1' =>  $request->input('phone1'), 
                    'phone2' =>  $request->input('phone2'), 
                    'email' =>  $request->input('email'),
                    'address' =>  $request->input('address'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        

        $v   =   Validator::make(
        				$data, 
			        	[
                            
                            'phone1' => [
                                            'required', 
                                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                            Rule::unique('owners')->ignore($id)
                                        ]
						]);

        $v->sometimes(['phone2'], ['required','regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', Rule::unique('owners')->ignore($id)], function ($input) {
            return $input->phone2 != "";
        });

        $v->sometimes(['email'], ['email', Rule::unique('owners')->ignore($id)], function ($input) {
            return $input->email != "";
        });

        $v->validate();

      
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

    public function properties($id = 0){
        $this->checkPermision($this->route.'.properties');
       
        $data = Model::find($id)->properties()->get();
        return view($this->route.'.properties', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }
    public function addProperty(Request $request){
        $this->checkPermision($this->route.'.add-property');
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $owner_id = isset($_GET['owner_id'])?$_GET['owner_id']:0;

        $listing_code = $request->input('listing_code');
        $property_id = $this->getPropertyIdByListingCode($listing_code);

        if($property_id != 0 && $owner_id!=0){
          
            $propertyData = ['owner_id'=>$owner_id]; 
            Property::where('id', $property_id)->update($propertyData);
            Session::flash('msg', 'Property has been added to the owner.' );
            return response()->json([
                'status' => 'success',
                'msg' => 'Property has been added.'
            ]);
        }else{
            return response()->json([
                'status' => 'erorr',
                'msg' => 'No Property in here.'
            ]);
        }
    }
    public function removeProperty($id){
        $this->checkPermision($this->route.'.remove-property');
        $propertyData = ['owner_id'=>null]; 
        Property::where('id', $id)->update($propertyData);
        Session::flash('msg', 'Property has been removed!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed!'
        ]);
    }

}
