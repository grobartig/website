<?php

namespace App\Http\Controllers\User\Property;
use App\Http\Controllers\User\Property\MainController;
use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Property\Property as Model;
use App\Model\User\User as User;
use App\Model\Setup\Action as Action;
use App\Model\Setup\Type as Type;
use App\Model\Setup\Owner as Owner;
use App\Model\Setup\Province as Province;

use App\Model\Setup\Role as Role;



class PropertyController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property";
    }
	public function index(){
        $data = Model::select('*');

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $action     =   intval(isset($_GET['action'])?$_GET['action']:0); 
        $type       =   intval(isset($_GET['type'])?$_GET['type']:0); 
        $province   =   intval(isset($_GET['province'])?$_GET['province']:0); 
        $district   =   intval(isset($_GET['district'])?$_GET['district']:0); 
        $commune    =   intval(isset($_GET['commune'])?$_GET['commune']:0); 
        $road       =   isset($_GET['road'])?$_GET['road']:""; 
        $map       =   intval(isset($_GET['map'])?$_GET['map']:0); 

        $listing_code       =   isset($_GET['listing_code'])?$_GET['listing_code']:"";
        $min        =   intval(isset($_GET['min'])?$_GET['min']:0); 
        $max        =   intval(isset($_GET['max'])?$_GET['max']:0); 

        $print       =   isset($_GET['print'])?$_GET['print']:0;

        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";

        if($limit <= 0 || $limit > 100){
            $limit = 10;
        }
        $appends=array('limit'=>$limit);


        //=====================>>> check Listing Code if avaiable and correct, it will redirect to the page
        if( $listing_code != "" ){
            $property_id = $this->getPropertyIdByListingCode($listing_code);
            if($property_id != 0){
                return redirect(route($this->route.'.property.edit', $property_id));
            }
            
        }

        //=====================>>> Check on action
        if( $action > 0 ){
            $data = $data->where('action_id', $action);
             $appends['action'] = $action;
        }

        //=====================>>> Type
        if( $type > 0 ){
            $data = $data->where('type_id', $type);
            $appends['type'] = $type;
        }

        //=====================>>> Province
        if( $province > 0 ){
            $data = $data->where('province_id', $province);
            $appends['province'] = $province;

            if( $district > 0){
                $data = $data->where('district_id', $district);
                $appends['district'] = $district;
                if( $commune > 0 ){
                    $data = $data->where('commune_id', $commune);
                    $appends['commune'] = $commune;
                }
            }
        }
        if( $road != "" ){
            $data = $data->whereHas('location', function ($query) {
                $road       =   isset($_GET['road'])?$_GET['road']:""; 
                $query->where('road', 'like', '%'.$road.'%');
            });
            $appends['road'] = $road;
        }
            
        //=====================>>> Date range created
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }

        //=====================>>> Min & Max price
        if( $min != 0 ){
            $appends['min'] = $min;
            if( $action == 1 ){
                $data = $data->whereHas('price', function ($query) {
                    $min        =   intval(isset($_GET['min'])?$_GET['min']:0); 
                    $query->where('selling_price', '>=', $min);
                });
            } else if( $action == 2 ){
                $data = $data->whereHas('price', function ($query) {
                    $min        =   intval(isset($_GET['min'])?$_GET['min']:0); 
                    $query->where('rental_price', '>=', $min);
                });
            }
        }

        if( $max != 0 ){
            $appends['max'] = $max;
            if( $action == 1 ){
                $data = $data->whereHas('price', function ($query) {
                    $max        =   intval(isset($_GET['max'])?$_GET['max']:0); 
                    $query->where('selling_price', '<=', $max);
                });
            } else if( $action == 2 ){
                $data = $data->whereHas('price', function ($query) {
                    $max        =   intval(isset($_GET['max'])?$_GET['max']:0); 
                    $query->where('rental_price', '<=', $max);
                });
            }
        }
       
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        
        if( $print == 1 ){
            $this->checkPermision('user.property.property.print');
            return view($this->route.'.property.print', ['data'=>$data]);
        }else{
            $actions     = Action::select('id', 'en_name as name')->get();
            $types       = Type::select('id', 'en_name as name')->orderBy('data_order', 'ASC')->get();
            $provinces   = Province::select('id', 'en_name as name', 'abbre')->orderBy('en_name', 'ASC')->where('status', 1)->get();
            
            $appends['map'] = $map;
            return view($this->route.'.property.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends, 'actions'=>$actions, 'types'=>$types, 'provinces'=>$provinces]);
        }
       

    }

    public function showCreateForm(){
        $actions     = Action::select('id', 'en_name as name')->get();
        $types       = Type::select('id', 'en_name as name', 'abbre')->where('status', 1)->orderBy('data_order', 'ASC')->get();
        $provinces   = Province::select('id', 'en_name as name', 'abbre')->where('status', 1)->orderBy('en_name', 'ASC')->get();
        $roles = Role::select('id', 'name')->get();
        $staffs = User::select('id', 'en_name as name', 'en_position as position')->where(['visible'=>1, 'status'=>1])->get();
        return view($this->route.'.property.createForm', ['route'=>$this->route, 'actions'=>$actions, 'types'=>$types, 'provinces'=>$provinces, 'roles'=>$roles, 'staffs'=>$staffs]);
    }
    
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'action_id' =>   $request->input('action-id'), 
                    'type_id' =>   $request->input('type-id'), 
                    'province_id' =>  $request->input('province-id'),
                    'cadaster_id' =>  $request->input('cadaster_id'),
                    'contract_type_id' =>  1,
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,

                    'listing_code' =>   $this->codeGenerator($request->input('province-id'), $request->input('type-id')),
                    'kh_name' =>  $request->input('kh-name'),
                    'en_name' =>  $request->input('en-name'),
                    'cn_name' =>  $request->input('cn-name'),
                    'slug'      =>   FunctionController::generateSlug('properties', $request->input('en-name')),
                    'kh_excerpt' =>  '',
                    'en_excerpt' =>  '',
                    'cn_excerpt' => '',
                    'kh_description' => '',
                    'en_description' =>  '',
                    'cn_description' =>  '',
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            'action_id' => 'exists:actions,id',
                            'type_id' => 'exists:types,id',
                            'province_id' => 'exists:provinces,id',
                            'en_name' => 'required|min:2|max:100',
                            'listing_code' => 'required'
                            
                        ],
                        [
                            'action_id.exists' => "Please select property's action.",
                            'type_id.exists' => "Please select property's type.",
                            'province_id.exists' => "Please select a province."
                            
                        ])->validate();

        if(is_null($request->input('kh-name'))){
            $data['kh_name'] = $data['en_name'];
        }
        if(is_null($request->input('cn-name'))){
            $data['cn_name'] = $data['en_name'];
        }

        $id=Model::insertGetId($data);
        $property = Model::find($id);

        
        $data = array('property_id' => $id, 'updater_id' => $user_id, 'created_at' => $now, 'updated_at'=> $now);
       
        //Media
        $property ->media()->insert($data);
         //Location
        $dataLocation = $data;
        $dataLocation['lat'] = $property->province->lat;
        $dataLocation['lng'] = $property->province->lng;
        $property ->location()->insert($dataLocation);
         //Price
        $property ->price()->insert($data);

        //Detail
        $details = Type::find($request->input('type-id'))->details;
        if(count($details)>0){
            $dataDetail = array();
            foreach($details as $row){
                $dataDetail[] = array('property_id'=>$id, 'detail_id'=>$row->id, 'creator_id'=>$user_id, 'updater_id'=>$user_id, 'created_at'=>$now, 'updated_at'=>$now);
            }
            Model::find($id)->propertyDetails()->insert($dataDetail);
        }

        Session::flash('msg', 'New property has been Created!');

        //===========================>> Assisnge creator to be the property management
        if(checkPermision('user.property.staff.add')){
            
            $property_id = $id;

            //Finding role id. Get one that has the biggest number of access
            $staff_id = $request->input('staff-id');
            $role_id = $request->input('role-id');
            if($staff_id != 0){
                if($role_id != 0){
                   
                    $data       = array(
                                'property_id' =>  $property_id,
                                'staff_id' =>  $staff_id,
                                'role_id' =>  $role_id,
                                'is_primary' =>  1,
                                'creator_id' =>  $user_id,
                                'created_at' => $now, 
                                'updated_at' => $now
                            );
                    Model::find($id)->propertyStaffs()->insert($data);
                }
            }
        }
        
        if(checkRole($id, 'view-overview')){
            return redirect(route($this->route.'.property.edit', $id));
        }else{
            //echo "xxx";
            $data = Model::find($id);
            $l = '';
            if($data->is_published == 0){
                $l = 'L';
            }
            $listing_code = $l.$data->province->abbre.'-'.$data->type->abbre.$data->listing_code;
            return redirect(route($this->route.'.property.create').'?listing_code='.$listing_code);
        }
        
    }

    public function showEditForm($id = 0){
        $this->checkRole($id, 'view-overview');

        $data = Model::find($id);
        $actions     = Action::select('id', 'en_name as name')->get();
        $types       = Type::select('id', 'en_name as name', 'abbre')->where('status', 1)->orderBy('data_order', 'ASC')->get();
        return view($this->route.'.property.editForm', ['route'=>$this->route, 'id'=>$id, 'actions'=>$actions, 'types'=>$types, 'data'=>$data]);
     
    }

    public function update(Request $request){
        $id = $request->input('id');
        $this->checkRole($id, 'update-overview');

        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $obj = Model::find($id);
       

        //If current type is changed, all feature must be deleted
        $type_id = $obj->type_id;
        if($type_id != $request->input('type-id')){
            Model::find($id)->propertyFeatures()->where('property_id', $id)->delete();
        }
        //If current type is changed, all detail must be deleted and add requarded to nefw type
        if($type_id != $request->input('type-id')){
            Model::find($id)->propertyDetails()->where('property_id', $id)->delete();
            $details = Type::find($request->input('type-id'))->details;
            if(count($details)>0){
                $dataDetail = array();
                foreach($details as $row){
                    $dataDetail[] = array('property_id'=>$id, 'detail_id'=>$row->id, 'creator_id'=>$user_id, 'updater_id'=>$user_id, 'created_at'=>$now, 'updated_at'=>$now);
                }
                Model::find($id)->propertyDetails()->insert($dataDetail);
            }
        }

        

        $listing_code = $request->input('listing_code');
        if($type_id != $request->input('type-id')){
           $listing_code = $this->codeGenerator($obj->province_id, $request->input('type-id'));
        }


     

        $data = array(
                    'action_id' =>   $request->input('action-id'), 
                    'type_id' =>   $request->input('type-id'), 
                    'updater_id' =>  $user_id,
                    'cadaster_id' =>  $request->input('cadaster_id'),
                    'listing_code' =>  $listing_code,
                    'kh_name' =>  $request->input('kh-name'),
                    'en_name' =>  $request->input('en-name'),
                    'cn_name' =>  $request->input('cn-name'),
                    'slug'      =>   FunctionController::generateSlug('properties', $request->input('en-name'), $id),
                    'kh_excerpt' =>  $request->input('kh-excerpt'),
                    'en_excerpt' =>  $request->input('en-excerpt'),
                    'cn_excerpt' =>  $request->input('cn-excerpt'),
                    'kh_description' =>  $request->input('kh-description'),
                    'en_description' =>  $request->input('en-description'),
                    'cn_description' =>  $request->input('cn-description')
                   
                );

       

        $v = Validator::make(
                        $data, 
                        [
                            'action_id' => 'exists:actions,id',
                            'type_id' => 'exists:types,id',
                            'province_id' => 'exists:provinces,id',
                            'en_name' => 'required|min:2|max:100',
                            'listing_code' => 'required|min:4'
                            
                        ],
                        [
                            'action_id.exists' => "Please select property's action.",
                            'type_id.exists' => "Please select property's type.",
                        ]);

      
        $v->validate();
        
       
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Property has been updated!' );
        return redirect()->back();
       
    }


    public function trash($id){
        $this->checkRole($id, 'update-overview');
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }
  

    function feature(){
        $data = Model::where('is_featured', 1)->orderBy('featured_order', 'ASC')->get();
        return view($this->route.'.property.feature', ['route'=>$this->route, 'data'=>$data]);
    }

    function reOrderFeature(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['featured_order'=>$row->order]);
        }
        echo 'done';
    }

    function removeFeature(Request $request){
         $id = $request->input('id');
        Model::where('id', $id)->update(['is_featured' => 0, 'featured_order'=>0]);
        Session::flash('msg', 'Property has been removed from featured list!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed from featured list!'
        ]);
    }

    function bestDeal(){
        $data = Model::where('is_best_dealed', 1)->orderBy('best_deal_ordered', 'ASC')->get();
        return view($this->route.'.property.best_deal', ['route'=>$this->route, 'data'=>$data]);
    }

    function reOrderBestDeal(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['best_deal_ordered'=>$row->order]);
        }
        echo 'done';
    }

    function removeBestDeal(Request $request){
         $id = $request->input('id');
        Model::where('id', $id)->update(['is_best_dealed' => 0, 'best_deal_ordered'=>0]);
        Session::flash('msg', 'Property has been removed from best deal list!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed from best deal list!'
        ]);
    }

    function slide(){
        $data = Model::where('is_slided', 1)->orderBy('slided_order', 'ASC')->get();
        return view($this->route.'.property.slide', ['route'=>$this->route, 'data'=>$data]);
    }

    function reOrderSlide(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['slided_order'=>$row->order]);
        }
        echo 'done';
    }

    function removeSlide(Request $request){
        $id = $request->input('id');
        Model::where('id', $id)->update(['is_slided' => 0, 'slided_order'=>0]);
        Session::flash('msg', 'Property has been removed from slided list!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed from slides!'
        ]);
    }

    public function charges(){
        $data = User::find(Auth::id())->staffProperties();
        $limit=intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $role=intval(isset($_GET['role'])?$_GET['role']:0); 
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";

        if($limit <= 0 || $limit > 100){
            $limit = 10;
        }

        $appends=array('limit'=>$limit);
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
         //=====================>>> Check on role
        if( $role > 0 ){
            $data = $data->where('role_id', $role);
             $appends['role'] = $role;
        }

        $roles = Role::get();
        $data= $data->whereHas('property', function($query){
            $query->whereNull('deleted_at');
        });
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        return view($this->route.'.property.charges', ['route'=>$this->route, 'data'=>$data, 'roles'=>$roles, 'appends'=>$appends]);
    }

    function projectDevelopment(){
        $data = Model::where('is_project_developmented', 1)->orderBy('project_development_ordered', 'ASC')->get();
        return view($this->route.'.property.project_development', ['route'=>$this->route, 'data'=>$data]);
    }

    function reOrderProjectDevelopment(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['project_development_ordered'=>$row->order]);
        }
        echo 'done';
    }

    function removeProjectDevelopment(Request $request){
         $id = $request->input('id');
        Model::where('id', $id)->update(['is_project_developmented' => 0, 'project_development_ordered'=>0]);
        Session::flash('msg', 'Property has been removed from project development list!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed from project development list!'
        ]);
    }
}
