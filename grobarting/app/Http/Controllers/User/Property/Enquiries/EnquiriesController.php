<?php

namespace App\Http\Controllers\User\Property\Enquiries;

use Auth;
use Session;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Property\MainController;

use App\Model\Property\Property as Model;
use App\Model\Setup\Customer as Customer;
use App\Model\Property\PropertyEnquiry as PropertyEnquiry;

class EnquiriesController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property.enquiries";
    }
    function index($id){
        $this->checkPermision($this->route.'.index');
        $this->validObj($id);
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id]);
    }
    
    public function enquiriesData($id=0){
       $this->validObj($id);
       $data = Model::find($id)->enquiries()->whereHas('customer', function($query){
            $query->whereNull('deleted_at');
       })->orderBy('id', 'DESC')->get();
       //dd($data);
       return view($this->route.'.data', ['route'=>$this->route, 'id'=>$id,'data'=>$data]); 
    }
    
    public function enquiriesUpdate(Request $request){
        $id = $request->input('id');
        $data = array(
                    'message' =>   $request->input('message')
                );
        
        
        PropertyEnquiry::where('id', $id)->update($data);
        return response()->json([
            'status' => 'success',
            'msg' => 'Enquiries message has been Update.'
        ]);
    }

    public function enquiriesStore(Request $request){
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        

        $key = $request->input('key');
        //Finding customer
        //Find by Email
        $customer_data = Customer::select('id')->where('email', $key)->first();
        if(count($customer_data) == 1){
            return $this->saveInquiry($customer_data->id);
        }
        //Find by Phone 1
        $customer_data = Customer::select('id')->where('phone1', $key)->first();
        if(count($customer_data) == 1){
            return $this->saveInquiry($customer_data->id);
        }
         //Find by Phone 2
        $customer_data = Customer::select('id')->where('phone2', $key)->first();
        if(count($customer_data) == 1){
            return $this->saveInquiry($customer_data->id);
        }


        // All conditions above not valide, need to save this as new customer
        //Check if it is valide phone number
        $v   =   Validator::make(
                        ['phone1'=>$key], 
                        [
                            
                            'phone1' => [
                                            'required', 
                                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                            Rule::unique('customers')
                                        ]
                                        
                        ]);
     
        $validate = $v->validate();
        //echo die;
        if($validate==''){
            
            $user_id  = Auth::id();
            $now      = date('Y-m-d H:i:s');

            $cus_id = 1;
            $data = Customer::select('cus_id')->orderBy('id', 'DESC')->first();
            if(count($data) == 1){
                $cus_id = intval($data->cus_id)+1;
            }

            if($cus_id >=0 && $cus_id < 10 ){
                $cus_id = "000".$cus_id;
            }else if( $cus_id >= 10 && $cus_id < 100 ){
                $cus_id = "00".$cus_id;
            }else if( $cus_id >= 100 && $cus_id < 1000 ){
                $cus_id = "0".$cus_id;
            }else if($cus_id>=10000){
                echo "No more more avaiable customer ID"; die;
            }
            $data = array(
                        'cus_id' =>   $cus_id,
                        'title_id' =>   1,
                        'phone1' =>  $key, 
                        'phone2' =>  $key, 
                          //Must include for all store
                        'creator_id' =>  $user_id,
                        'updater_id' =>  $user_id,
                        'created_at' => $now, 
                        'updated_at' => $now
                    );
          
            $customer_id=Customer::insertGetId($data);
            return $this->saveInquiry($customer_id);
       }else{
            echo "Inavlide Key";
       }
    }
    function saveInquiry($customer_id){
         $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $property_id = isset($_GET['property_id'])?$_GET['property_id']:0;
        if($customer_id != 0){
            $data = array(
                    
                    'customer_id' =>   $customer_id, 
                    'property_id' =>   $property_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
            $id = PropertyEnquiry::insertGetId($data);
            return response()->json([
                'status' => 'success',
                'msg' => 'Enquiry has been created!'
            ]);
        }else{
            return response()->json([
                'status' => 'erorr',
                'msg' => 'No Customer email in here.'
            ]);
        }
    }
     function trash($inquiry_id){
        $id = $_GET['id'];
        $user_id    = Auth::id();
        Model::find($id)->enquiries()->where('id', $inquiry_id)->update(['deleter_id' => $user_id]);
        Model::find($id)->enquiries()->where('id', $inquiry_id)->delete();
        Session::flash('msg', 'Photo has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Photo has been deleted'
        ]);
    }

}
