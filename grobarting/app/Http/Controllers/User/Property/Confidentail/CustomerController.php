<?php

namespace App\Http\Controllers\User\Property\Confidentail;

use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\Setup\Customer as Customer;

class CustomerController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property.confidentail.customer";
    }
	public function index($id = 0){
        $this->checkRole($id, 'view-customer');
        $data = Model::find($id)->customer;
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function create($id = 0){
       $this->checkRole($id, 'create-customer');
        return view($this->route.'.create', ['route'=>$this->route, 'id'=>$id]);
    }

    public function store(Request $request, $id){
        $this->checkRole($id, 'create-customer');
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $customer_id = 0;
        $phone1 = $request->input('phone1');
        $data_customer= Customer::select('id')->where('phone1', $phone1)->first();
        if(empty($data_customer)){
            $cus_id = 1;
            $data = Customer::select('cus_id')->orderBy('id', 'DESC')->first();
            if(count($data) == 1){
                $cus_id = intval($data->cus_id)+1;
            }

            if($cus_id >=0 && $cus_id < 10 ){
                $cus_id = "000".$cus_id;
            }else if( $cus_id >= 10 && $cus_id < 100 ){
                $cus_id = "00".$cus_id;
            }else if( $cus_id >= 100 && $cus_id < 1000 ){
                $cus_id = "0".$cus_id;
            }else if($cus_id>=10000){
                echo "No more more avaiable customer ID"; die;
            }
            $data = array(
                        'cus_id' =>   $cus_id,
                        'title_id' =>   $request->input('title_id'),
                        'first_name' =>   $request->input('first_name'),
                        'last_name' =>  $request->input('last_name'), 
                        'phone' =>  $request->input('phone'),
                        'email' =>  $request->input('email'), 
                        'address_line_1' =>  $request->input('address'),

                          //Must include for all store
                        'creator_id' =>  $user_id,
                        'updater_id' =>  $user_id,
                        'created_at' => $now, 
                        'updated_at' => $now
                    );
            Session::flash('invalidData', $data );
            $v   =   Validator::make(
                            $data, 
                            [
                                
                                'phone' => [
                                                'required', 
                                                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                                Rule::unique('customers')
                                            ]
                                            
                            ]);

            // $v->sometimes(['phone2'], ['required','regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', Rule::unique('customers')], function ($input) {
            //     return $input->phone2 != "";
            // });

            // $v->sometimes(['email'], ['email', Rule::unique('customers')], function ($input) {
            //     return $input->email != "";
            // });

            $v->validate();
          
            $customer_id=Customer::insertGetId($data);
         }else{
            $customer_id = $data_customer->id;
        }

        $propertyData = ['customer_id'=>$customer_id]; 
        Model::where('id', $id)->update($propertyData);
        Session::flash('msg', 'Customer has been Created!');
        return redirect(route($this->route.'.index', $id));
    }
    public function update(Request $request, $id){
        $this->checkRole($id, 'update-customer');
        $id = $request->input('id');

        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $data = array(
                    'title_id' =>   $request->input('title_id'),
                    'first_name' =>   $request->input('first_name'),
                    'last_name' =>  $request->input('last_name'), 
                    'phone' =>  $request->input('phone'),
                    'email' =>  $request->input('email'), 
                    'address_line_1' =>  $request->input('address'), 
                    //Must include for all update
                    'updater_id' =>  $user_id,
                    'updated_at' => $now
                );

        $v   =   Validator::make(
                        $data, 
                        [
                            
                            'phone' => [
                                            'sometimes',
                                            'required', 
                                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                            //Rule::unique('customers')->ignore($id)
                                        ]
                        ]);

        // $v->sometimes(['phone2'], ['required','regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', Rule::unique('customers')->ignore($id)], function ($input) {
        //     return $input->phone2 != "";
        // });

        // $v->sometimes(['email'], ['email', Rule::unique('customers')->ignore($id)], function ($input) {
        //     return $input->email != "";
        // });

        $v->validate();

        
        Customer::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    public function search(Request $request){

        $id = $request->input('id');
        $key = $request->input('key');
        //DB::enableQueryLog();
        $data = Customer::select('id', 'first_name', 'last_name', 'email', 'phone')->where('first_name', 'like', '%'.$key.'%')->orWhere('last_name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('phone', 'like', '%'.$key.'%')->limit(50)->orderBy('id', 'DESC')->get();
        //dd(DB::getQueryLog());
        return view( $this->route.'.search', ['id'=>$id, 'data'=>$data]);
    }
    public function addToProperty(Request $request){
        $id = $request->input('id');
        $this->checkRole($id, 'change-customer');
        $customer_id = $request->input('customer_id');
        $propertyData = ['customer_id'=>$customer_id, 'purchase_date'=>date('Y-m-d')]; 
        Model::where('id', $id)->update($propertyData);
        Session::flash('msg', 'Customer has been changed.' );
        echo "added";
    }

}
