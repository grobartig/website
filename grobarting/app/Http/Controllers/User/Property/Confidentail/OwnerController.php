<?php

namespace App\Http\Controllers\User\Property\Confidentail;

use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\Setup\Owner as Owner;

class OwnerController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property.confidentail.owner";
    }
	public function index($id = 0){
        $this->checkRole($id, 'view-owner');
        $data = Model::find($id)->owner;
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function create($id = 0){
        $this->checkRole($id, 'create-owner');
        return view($this->route.'.create', ['route'=>$this->route, 'id'=>$id]);
    }

    public function store(Request $request, $id){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $owner_id = 0;
        $phone1 = $request->input('phone1');
        $data_owner = Owner::select('id')->where('phone1', $phone1)->first();
        
        if(empty($data_owner)){
            $data = array(
                        'name' =>   $request->input('name'), 
                        'phone1' =>  $request->input('phone1'), 
                        'phone2' =>  $request->input('phone2'), 
                        'email' =>  $request->input('email'),
                        'address' =>  $request->input('address'),
                        'updater_id' => $user_id,
                        'updated_at' => $now
                    );
            
            Session::flash('invalidData', $data );
            $v   =   Validator::make(
                            $data, 
                            [
                                
                                'phone1' => [
                                                'required', 
                                                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                                Rule::unique('owners')
                                            ]
                            ]);

            $v->sometimes(['phone2'], ['required','regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', Rule::unique('owners')], function ($input) {
                return $input->phone2 != "";
            });

            $v->sometimes(['email'], ['email', Rule::unique('owners')], function ($input) {
                return $input->email != "";
            });

            $v->validate();
            $owner_id=Owner::insertGetId($data);
        }else{
            $owner_id = $data_owner->id;
        }
      
        

        $propertyData = ['owner_id'=>$owner_id]; 
        Model::where('id', $id)->update($propertyData);
        Session::flash('msg', 'Owner has been Created!');
        return redirect(route($this->route.'.index', $id));
    }
    public function update(Request $request, $id){
        $this->checkRole($id, 'update-owner');
       $id = $request->input('id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'name' =>   $request->input('name'), 
                    'phone1' =>  $request->input('phone1'), 
                    'phone2' =>  $request->input('phone2'), 
                    'email' =>  $request->input('email'),
                    'address' =>  $request->input('address'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        

        $v   =   Validator::make(
                        $data, 
                        [
                            
                            'phone1' => [
                                            'required', 
                                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                            Rule::unique('owners')->ignore($id)
                                        ]
                        ]);

        $v->sometimes(['phone2'], ['required','regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', Rule::unique('owners')->ignore($id)], function ($input) {
            return $input->phone2 != "";
        });

        $v->sometimes(['email'], ['email', Rule::unique('owners')->ignore($id)], function ($input) {
            return $input->email != "";
        });

        $v->validate();
      
        Owner::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }
    public function search(Request $request){

        $id = $request->input('id');
        $key = $request->input('key');
        //DB::enableQueryLog();
        $data = Owner::select('id', 'name', 'email', 'phone1', 'phone2')->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('phone1', 'like', '%'.$key.'%')->orWhere('phone2', 'like', '%'.$key.'%')->limit(50)->orderBy('id', 'DESC')->get();
        //dd(DB::getQueryLog());
        return view( $this->route.'.ownerSearch', ['id'=>$id, 'data'=>$data]);
    }
    public function addToProperty(Request $request){
        $id = $request->input('id');
        $this->checkRole($id, 'change-owner');
        $owner_id = $request->input('owner_id');
        $propertyData = ['owner_id'=>$owner_id]; 
        Model::where('id', $id)->update($propertyData);
        Session::flash('msg', 'Owner has been changed.' );
        echo "added";
    }

}
