<?php

namespace App\Http\Controllers\User\Property\About;
use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\Setup\SellingPriceType as SellingPriceType;
use App\Model\Setup\RentalPriceType as RentalPriceType;

class PriceController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property.about.price";
    }
    public function index($id = 0){
        $this->checkRole($id, 'view-price');
        $data = Model::find($id)->price;
        $selling_price_types = SellingPriceType::get();
        $rental_price_types = RentalPriceType::get();
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data,'selling_price_types'=>$selling_price_types,'rental_price_types'=>$rental_price_types ]);
    }
   
    public function update(Request $request){
        
        $id = $request->input('id'); 
        $this->checkRole($id, 'update-price');
        $property_id = $request->input('property_id');
        $user_id = Auth::id();

        Validator::make(
                        $request->all(), 
                        [
                            'selling_price' => 'numeric|min:0',
                            'selling_price_type_id' => 'exists:selling_price_types,id',
                            'actual_selling_price' => 'numeric|min:0',
                            'actual_selling_price_type_id' => 'exists:selling_price_types,id',
                            'rental_price' => 'numeric|min:0',
                            'rental_price_type_id' => 'exists:rental_price_types,id',
                            'actual_rental_price' => 'numeric|min:0',
                            'actual_rental_price_type_id' => 'exists:rental_price_types,id',
                            
                        ],
                        [
                            'selling_price_type_id.exists' => "Please select selling price unit.",
                            'actual_selling_price_type_id.exists' => "Please select actual selling price unit.",
                            'rental_price_type_id.exists' => "Please select rental price unit.",
                            'actual_rental_price_type_id.exists' => "Please select actual rental price unit.",
                        ])->validate();

        $data = array(
                    'selling_price' =>   $request->input('selling_price'), 
                    'selling_price_type_id' =>   $request->input('selling_price_type_id'),
                    'actual_selling_price' =>   $request->input('actual_selling_price'), 
                    'actual_selling_price_type_id' =>   $request->input('actual_selling_price_type_id'),
                    'rental_price' =>   $request->input('rental_price'),
                    'rental_price_type_id' =>   $request->input('rental_price_type_id'),
                    'actual_rental_price' =>   $request->input('rental_price'),
                    'actual_rental_price_type_id' =>   $request->input('actual_rental_price_type_id'),
                    'deposit' =>   $request->input('deposit'),
                );
        //print_r($data); die;
        Model::find($property_id)->price()->update($data);
        Session::flash('msg', 'Priceing has been updated!' );
        return redirect()->back();
    }


}
