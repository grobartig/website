<?php

namespace App\Http\Controllers\User\Property\About;

use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\User\Property\MainController;

use Image;

use App\Model\Property\Property as Model;


class PhotoController extends MainController
{
    
	protected $route; 
    public function __construct(){
        $this->route = "user.property.about.photo";
    }
    public function index($id = 0){
        $this->checkRole($id, 'view-photo');
        $data = Model::find($id)->photos;
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function create($id = 0){
        $this->checkRole($id, 'create-photo');
        return view($this->route.'.create', ['route'=>$this->route, 'id'=>$id]);
    }

    public function store(Request $request) {
        
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $this->checkRole($property_id, 'create-photo');

        $data       = array(
                    'property_id' =>  $property_id,
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'deleter_id' =>  1,
                    'created_at' =>  $now, 
                    'updated_at' =>  $now
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'photo' => [
                                            'required',
                                            'mimes:jpeg,png',
                            ]
                        ],
                        [
                           'photo.dimensions' => 'Please provide valide dimensions for photo with 416x270.'
                        ])->validate();

        // $photo = FileUpload::uploadFile($request, 'photo', 'uploads/property/photo');
        // if($photo != ""){
        //     $data['photo'] = $photo; 
        // }

        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoname = time().'.'.$photo->getClientOriginalExtension(); 
            Image::make($photo->getRealPath())->resize(750, 487)->save(public_path('uploads/property/photo/750x487/'.$photoname));
            //Image::make($photo->getRealPath())->resize(416, 270)->save(public_path('uploads/property/photo/416x270/'.$photoname));
            $data['photo']=$photoname;
        }
        $id=Model::find($property_id)->photos()->insertGetId($data);
       
        Session::flash('msg', 'A photo has been uploaded!');
        return redirect(route($this->route.'.edit', ['id'=>$property_id, 'photo_id'=>$id]));
    }

    function edit($id, $photo_id){
        $this->checkRole($id, 'view-photo');
        $data = Model::find($id)->photos()->find($photo_id);
        if( sizeof($data) == 1){
            return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
        }else{
            echo "ivalide photo";
        }
    }

    public function update(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
         $this->checkRole($property_id, 'update-photo');
        $photo_id = $request->input('photo_id');
        $data       = array(
                    'updater_id' =>  $user_id,
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'photo' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                            ]
                        ],
                        [
                           'photo.dimensions' => 'Please provide valide dimensions for photo with 416x270.'
                        ])->validate();


        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoname = time().'.'.$photo->getClientOriginalExtension(); 
            Image::make($photo->getRealPath())->resize(750, 487)->save(public_path('uploads/property/photo/750x487/'.$photoname));
            //Image::make($photo->getRealPath())->resize(416, 270)->save(public_path('uploads/property/photo/416x270/'.$photoname));
            $data['photo']=$photoname;
        }

        Model::find($property_id)->photos()->where('id', $photo_id)->update($data);
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit', ['id'=>$property_id, 'photo_id'=>$photo_id]));
    }

    function trash($photo_id){
        $id = $_GET['id'];
        $this->checkRole($id, 'delete-photo');
        $user_id    = Auth::id();
        Model::find($id)->photos()->where('id', $photo_id)->update(['deleter_id' => $user_id]);
        Model::find($id)->photos()->where('id', $photo_id)->delete();
        Session::flash('msg', 'Photo has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Photo has been deleted'
        ]);
    }

}
