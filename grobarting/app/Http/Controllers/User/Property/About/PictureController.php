<?php

namespace App\Http\Controllers\User\Property\About;

use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\User\Property\MainController;
use Image;
use App\Model\Property\Property as Model;


class PictureController extends MainController
{
    
	protected $route; 
    public function __construct(){
        $this->route = "user.property.about.picture";
    }
    public function index($id = 0){
        $this->checkRole($id, 'view-picture');
        $data = Model::find($id)->media;
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $this->checkRole($id, 'update-price');
        $property_id = $request->input('property_id');
        $user_id = Auth::id();
        Validator::make(
                        $request->all(), 
                        [
                            
                            'feature' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                            ],
                            'video_image' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                            ],
                            'slide' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->width(1900)->height(630),
                            ],
                            
                        ],
                        [
                           
                           'slide.dimensions' => 'Please provide valide dimensions for slided image with 1900x630.',
                        ])->validate();

        
        $data = array(
                        'updater_id' =>  $user_id
                    );
        
        
        if($request->hasFile('feature')) {
            $feature = $request->file('feature');
            $featurename = time().'.'.$feature->getClientOriginalExtension(); 
            
            Image::make($feature->getRealPath())->resize(476, 270)->save(public_path('uploads/property/feature/'.$featurename));
            Image::make($feature->getRealPath())->resize(416, 270)->save(public_path('uploads/property/feature/property/'.$featurename));
            $data['feature']=$featurename;
        }
        if($request->hasFile('video_image')) {
            $video_image = $request->file('video_image');
            $video_imagename = time().'.'.$video_image->getClientOriginalExtension(); 
            
            Image::make($video_image->getRealPath())->resize(582, 457)->save(public_path('uploads/property/video_image/'.$video_imagename));
            //Image::make($feature->getRealPath())->resize(416, 270)->save(public_path('uploads/property/video_image/property/'.$video_imagename));
            $data['video_image']=$video_imagename;
        }

        $slide = FileUpload::uploadFile($request, 'slide', 'uploads/property/slide');
        if($slide != ""){
            $data['slide'] = $slide; 
        }
        $company_design = FileUpload::uploadFile($request, 'company_design', 'uploads/property/company_design');
        if($company_design != ""){
            $data['company_design'] = $company_design; 
        }
        
        Model::find($property_id)->media()->where('id', $id)->update($data);
        Session::flash('msg', 'Property media has been updated!' );
        return redirect()->back();
    }
    public function showVideosCreateForm($id = 0){
        $this->validObj($id);
        return view($this->route.'.videosCreateForm', ['route'=>$this->route, 'id'=>$id]);
    }

    public function storeVideos(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $data       = array(
                    'property_id' =>  $property_id,
                    'youtube_id' =>  $request->input('youtube-id'),
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'deleter_id' =>  1,
                    'created_at' =>  $now, 
                    'updated_at' =>  $now, 

                   
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'youtube-id' => 'required|min:6|max:20',
                          
                            
                        ])->validate();

       
        $id=Model::find($property_id)->videos()->insertGetId($data);
       
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit-videos', ['id'=>$property_id, 'video_id'=>$id]));
    }

    function showVideosEditForm($id, $video_id){
        $this->validObj($id);
        $data = Model::find($id)->videos()->find($video_id);
        if( sizeof($data) == 1){
            return view($this->route.'.videosEditForm', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
        }else{
            echo "xxx";
        }
    }

    public function updateVideos(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $video_id = $request->input('video_id');
        $data       = array(
                    'property_id' =>  $property_id,
                    'youtube_id' =>  $request->input('youtube-id'),
                    'updater_id' =>  $user_id,

                   
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                           
                            'youtube-id' => 'required|min:6|max:20',
                          
                            
                        ])->validate();

       
        Model::find($property_id)->videos()->where('id', $video_id)->update($data);
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit-videos', ['id'=>$property_id, 'video_id'=>$video_id]));
    }

    public function destroyVideos($video_id){
        $id = $_GET['id'];
        $this->validObj($id);
        $user_id    = Auth::id();
        Model::find($id)->videos()->where('id', $video_id)->update(['deleter_id' => $user_id]);
        Model::find($id)->videos()->where('id', $video_id)->delete();
        Session::flash('msg', 'Video has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Video has been deleted'
        ]);
    }

    public function showPhotosCreateForm($id = 0){
        $this->validObj($id);
        return view($this->route.'.photosCreateForm', ['route'=>$this->route, 'id'=>$id]);
    }

    public function storePhotos(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $data       = array(
                    'property_id' =>  $property_id,
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'deleter_id' =>  1,
                    'created_at' =>  $now, 
                    'updated_at' =>  $now
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                           
                            'photo' => [
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(100)->minHeight(100)->maxWidth(500)->maxHeight(500),
                            ],
                            
                        ])->validate();

        $photo = FileUpload::uploadFile($request, 'photo', 'uploads/property/photo');
        if($photo != ""){
            $data['photo'] = $photo; 
        }
        $id=Model::find($property_id)->photos()->insertGetId($data);
       
        Session::flash('msg', 'A photo has been Created!');
        return redirect(route($this->route.'.edit-photos', ['id'=>$property_id, 'photo_id'=>$id]));
    }

    function showPhotosEditForm($id, $video_id){
        $this->validObj($id);
        $data = Model::find($id)->photos()->find($video_id);
        if( sizeof($data) == 1){
            return view($this->route.'.photosEditForm', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
        }else{
            echo "ivalide video";
        }
    }

    public function updatePhotos(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $photo_id = $request->input('photo_id');
        $data       = array(
                    'updater_id' =>  $user_id,
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'photo' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(100)->minHeight(100)->maxWidth(500)->maxHeight(500),
                                        ]
                        ])->validate();

        $photo = FileUpload::uploadFile($request, 'photo', 'uploads/property/photo');
        if($photo != ""){
            $data['photo'] = $photo; 
        }

        Model::find($property_id)->photos()->where('id', $photo_id)->update($data);
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit-photos', ['id'=>$property_id, 'photo_id'=>$photo_id]));
    }

    function destroyPhotos($photo_id){
        $id = $_GET['id'];
        $this->validObj($id);
        $user_id    = Auth::id();
        Model::find($id)->photos()->where('id', $photo_id)->update(['deleter_id' => $user_id]);
        Model::find($id)->photos()->where('id', $photo_id)->delete();
        Session::flash('msg', 'Photo has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Photo has been deleted'
        ]);
    }

}
