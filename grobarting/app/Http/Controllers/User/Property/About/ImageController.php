<?php

namespace App\Http\Controllers\User\Property\About;

use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;


class ImageController extends MainController
{
    
	protected $route; 
    public function __construct(){
        $this->route = "user.property.about.image";
    }
    public function index($id = 0){
        $this->checkRole($id, 'view-image');
        $data = Model::find($id)->images;
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function create($id = 0){
        $this->checkRole($id, 'create-image');
        return view($this->route.'.create', ['route'=>$this->route, 'id'=>$id]);
    }

    public function store(Request $request) {
        
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $this->checkRole($property_id, 'create-image');

        $data       = array(
                    'property_id' =>  $property_id,
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'deleter_id' =>  1,
                    'created_at' =>  $now, 
                    'updated_at' =>  $now
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'photo' => [
                                            'required',
                                            'mimes:jpeg,png',
                            ]
                        ])->validate();

        $photo = FileUpload::uploadFile($request, 'photo', 'uploads/property/image');
        if($photo != ""){
            $data['photo'] = $photo; 
        }
        $id=Model::find($property_id)->images()->insertGetId($data);
       
        Session::flash('msg', 'An image has been uploaded!');
        return redirect(route($this->route.'.edit', ['id'=>$property_id, 'photo_id'=>$id]));
    }

    function edit($id, $photo_id){
        $this->checkRole($id, 'view-photo');
        $data = Model::find($id)->images()->find($photo_id);
        if( sizeof($data) == 1){
            return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
        }else{
            echo "ivalide photo";
        }
    }

    public function update(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
         $this->checkRole($property_id, 'update-photo');
        $photo_id = $request->input('photo_id');
        $data       = array(
                    'updater_id' =>  $user_id,
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'photo' => [
                                            'required',
                                            'mimes:jpeg,png'
                            ]
                        ])->validate();


        $photo = FileUpload::uploadFile($request, 'photo', 'uploads/property/photo');
        if($photo != ""){
            $data['photo'] = $photo; 
        }

        Model::find($property_id)->images()->where('id', $photo_id)->update($data);
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit', ['id'=>$property_id, 'photo_id'=>$photo_id]));
    }

    function trash(){
        $image_id = $_GET['image_id'];
        $id = $_GET['id'];
        $this->checkRole($id, 'delete-image');
        $user_id    = Auth::id();
        Model::find($id)->images()->where('id', $image_id)->update(['deleter_id' => $user_id]);
        Model::find($id)->images()->where('id', $image_id)->delete();
        Session::flash('msg', 'Image has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Photo has been deleted'
        ]);
    }

}
