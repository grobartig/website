<?php

namespace App\Http\Controllers\User\Property\About;

use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;


class VideoController extends MainController
{
    
    protected $route; 
    public function __construct(){
        $this->route = "user.property.about.video";
    }
    public function index($id = 0){
        $this->checkRole($id, 'view-video');
        $data = Model::find($id)->videos;
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

 
    public function create($id = 0){
        $this->checkRole($id, 'create-video');
        return view($this->route.'.create', ['route'=>$this->route, 'id'=>$id]);
    }

    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $this->checkRole($property_id, 'create-video');
        $data       = array(
                    'property_id' =>  $property_id,
                    'youtube_id' =>  $request->input('youtube-id'),
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'deleter_id' =>  1,
                    'created_at' =>  $now, 
                    'updated_at' =>  $now, 

                   
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'youtube-id' => 'required|min:6|max:20',
                          
                            
                        ])->validate();

       
        $id=Model::find($property_id)->videos()->insertGetId($data);
       
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit', ['id'=>$property_id, 'video_id'=>$id]));
    }

    function edit($id, $video_id){
        $this->checkRole($id, 'view-video');
        $data = Model::find($id)->videos()->find($video_id);
        if( sizeof($data) == 1){
            return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
        }else{
            echo "Invalide URL";
        }
    }

    public function update(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $property_id = $request->input('property_id');
        $this->checkRole($property_id, 'update-video');
        $video_id = $request->input('video_id');
        $data       = array(
                    'property_id' =>  $property_id,
                    'youtube_id' =>  $request->input('youtube-id'),
                    'updater_id' =>  $user_id,
                );
        //print_r($data); die;
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                           
                            'youtube-id' => 'required|min:6|max:20',
                          
                            
                        ])->validate();

       
        Model::find($property_id)->videos()->where('id', $video_id)->update($data);
        Session::flash('msg', 'New property has been Created!');
        return redirect(route($this->route.'.edit', ['id'=>$property_id, 'video_id'=>$video_id]));
    }

    public function trash($video_id){
       $id = $_GET['id'];
        $this->checkRole($id, 'delete-video');
        $user_id    = Auth::id();
        Model::find($id)->videos()->where('id', $video_id)->update(['deleter_id' => $user_id]);
        Model::find($id)->videos()->where('id', $video_id)->delete();
        Session::flash('msg', 'Video has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Video has been deleted'
        ]);
    }

  
}
