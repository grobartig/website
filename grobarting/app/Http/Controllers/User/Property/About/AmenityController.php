<?php

namespace App\Http\Controllers\User\Property\About;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\Setup\Type as Type;

class AmenityController extends MainController
{
    
    protected $route; 
    public function __construct(){
        $this->route = "user.property.about.amenity";
    }
	public function index($id = 0){
        $this->validObj($id);
        $property = Model::find($id);
        $dataFeature = $property->propertyFeatures()->select('feature_id')->get();

        $features = Type::find($property->type_id)->features()->select('features.id', 'features.en_name as name')->get(); 
        // dd($features);
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'dataFeature'=>$dataFeature, 'features'=>$features]);
    }
    public function check(Request $request){
        $property_id    = $_GET['property_id'];
        $feature_id     = $_GET['feature_id'];
        $now        = date('Y-m-d H:i:s');
        $user_id    = Auth::id();

        $property = Model::find($property_id);
        $dataFeature = $property->propertyFeatures()->select('feature_id')->get(); 

        $is_feature_existed = 0;
        foreach($dataFeature as $row){
            if($row->feature_id == $feature_id){
                $is_feature_existed = 1;
            }
        }
       
        if($is_feature_existed == 1){
            $property->propertyFeatures()->where('feature_id', $feature_id)->delete();
            return response()->json([
                  'status' => 'success',
                  'msg' => 'Feature has been removed.'
            ]);
        }else{
            $data_feature=array(
                'property_id' => $property_id,
                'feature_id' => $feature_id,
                'creator_id' => $user_id, 
                'updater_id' => $user_id,
                'created_at' => $now, 
                'updated_at' => $now
                );
            $property->propertyFeatures()->insert($data_feature);
             return response()->json([
                  'status' => 'success',
                  'msg' => 'Feature has been added.'
              ]);
        }
    }

}
