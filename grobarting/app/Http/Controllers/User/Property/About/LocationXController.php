<?php

namespace App\Http\Controllers\User\Property\About;
use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\Setup\Province as Province;
use App\Model\Setup\District as District;
use App\Model\Setup\Commune as Commune;

class LocationXController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property.about.location";
    }
    public function index($id = 0){
        $this->validObj($id);
        $data = Model::find($id)->location;
        $provinces   = Province::select('id', 'en_name as name', 'lat', 'lng')->get();
        $districts    = District::select('id', 'en_name as name', 'lat', 'lng')->where('province_id', $data->property->province_id)->get();
        $communes    = Commune::select('id', 'en_name as name', 'lat', 'lng')->where('district_id', $data->property->district_id)->get();
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'provinces'=>$provinces, 'districts'=>$districts, 'communes'=>$communes, 'data'=>$data]);
    }
   
    public function update(Request $request){
        
        $id = $request->input('id'); 
        $property_id = $request->input('property_id');

        Validator::make(
                        $request->all(), 
                        [
                            'province-id' => 'exists:provinces,id',
                            'district-id' => 'exists:districts,id',
                            'commune-id' => 'exists:communes,id',
                            'address' => 'required',
                            'road' => 'required',
                            'lat' => 'required',
                            'lng' => 'required'
                        ],
                        [
                            
                            'province-id.exists' => "Please select a province.",
                            'district-id.exists' => "Distrit was not selected for ".Province::find($request->input('province-id'))->en_name.".",
                            'commune-id.exists' => "Please select a commune."
                            
                        ])->validate();
        $user_id = Auth::id();
        $obj = Model::find($property_id);

       
        $dataProperty = array(
                    'province_id' =>   $request->input('province-id'), 
                    'district_id' =>   $request->input('district-id'),
                    'commune_id' =>   $request->input('commune-id')
                );

        if($obj->province_id != $request->input('province-id')){
           $dataProperty['listing_code'] = $this->codeGenerator($request->input('province-id'), $obj->type_id);
        }
        //print_r($dataProperty); die;
        $dataLocation = array(
                    'address' =>   $request->input('address'), 
                    'road' =>   $request->input('road'),
                    'zip_code' =>   $request->input('zip_code'), 
                    'is_actual_map' =>   $request->input('is-actual-map'),
                    'lat' =>   $request->input('lat'), 
                    'lng' =>   $request->input('lng'), 
                    'updater_id' =>  $user_id
                );
        $property = Model::find($property_id);
        
        Model::where('id', $property_id)->update($dataProperty);
        //print_r($dataProperty); die;
        Model::find($property_id)->location()->update($dataLocation);
        Session::flash('msg', 'Location has been updated!' );
        return redirect()->back();
    }

}
