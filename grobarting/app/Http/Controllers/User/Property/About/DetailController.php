<?php

namespace App\Http\Controllers\User\Property\About;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\Property\propertyDetail as propertyDetail;
use App\Model\Setup\Type as Type;
use App\Model\Setup\Detail as Detail;

class DetailController extends MainController
{
    
    protected $route; 
    public function __construct(){
        $this->route = "user.property.about.detail";
    }
    public function index($id = 0){
        $this->checkRole($id, 'view-detail');
        $data = Model::find($id)->propertyDetails()->orderBy('data_order', 'ASC')->get();
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }
    public function save(Request $request){
        $id = $request->input('id');
        $property_detail_id = $request->input('property_detail_id'); 
        $value = $request->input('value');
        if(is_null($value)){
        	$value="";
        }
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data = array('value'=>$value, 'updater_id'=>$user_id, 'updated_at'=>$now);
        Model::find($id)->propertyDetails()->where('id', $property_detail_id)->update($data);
        return response()->json([
            'status' => 'success',
            'msg' => 'Detail value has been updated.'
        ]);
    }
    public function trash(Request $request){
    	$id = $request->input('id'); 
        $property_detail_id = $request->input('property_detail_id'); 
        Model::find($id)->propertyDetails()->where('id', $property_detail_id)->delete();
        Session::flash('msg', 'Property detail has been removed');
        return redirect(route($this->route.'.index', $id));
    }

   

    public function search(Request $request){
        $id = $request->input('id');
        $key = $request->input('key');
        
        $data = Detail::select('id', 'en_name')->where('en_name', 'like', '%'.$key.'%')->whereDoesntHave('detailProperties', function ($query) {
            $id = $_GET['id'];
            $query->where('property_id', '=', $id);
        })->limit(50)->orderBy('id', 'DESC')->get();
        
        return view( $this->route.'.search', ['id'=>$id, 'data'=>$data]);
    }

    public function selected(Request $request){
        $id = $request->input('id');
        $data = Model::find($id)->details()->select('details.id', 'en_name')->orderBy('id', 'DESC')->get();
        return view( $this->route.'.selected', ['data'=>$data]);
    }
    public function remove(Request $request){
        $id = $request->input('id');
        $detail_id = $request->input('detail_id');
        Model::find($id)->propertyDetails()->where('detail_id', $detail_id)->delete();
        echo $this->selected($request);
    }
    public function add(Request $request){
        $id = $request->input('id');
        $detail_id = $request->input('detail_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'property_id' =>  $id,
                    'detail_id' =>  $detail_id,
                    'creator_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Model::find($id)->propertyDetails()->insert($data);
        echo $this->selected($request);
    }
    function order(Request $request){
       $id = $request->input('id');
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die
        foreach($data as $row){
            Model::find($id)->propertyDetails()->where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
    
    public function saveAll(Request $request){
        $id = $request->input('id');
        $items = json_decode($request->input('items'));
         
        //dd($values);
        foreach($items as $item){
            $property_detail_id = $item[0];
            $value = $item[1];
            if(is_null($value)){
                $value="";
            }
            $user_id    = Auth::id();
            $now        = date('Y-m-d H:i:s');
            $data = array('value'=>$value, 'updater_id'=>$user_id, 'updated_at'=>$now);
            Model::find($id)->propertyDetails()->where('id', $property_detail_id)->update($data);
        }
        
        
        return response()->json([
            'status' => 'success',
            'msg' => 'Detail value has been updated.'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_showed' => $request->input('status'));
      propertyDetail::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Showed status has been updated.'
      ]);
    }

}
