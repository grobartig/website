<?php
namespace App\Http\Controllers\User\Property\Tool;
use Auth;
use Session;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
class StatusController extends MainController
{
 
    protected $route; 
    public function __construct(){
        $this->route = "user.property.tool.status";
    }
    public function index($id){
        $this->checkRole($id, 'view-status');
        $data = Model::find($id);
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('property_id');
        $this->checkRole($id, 'update-status');

        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $obj = Model::find($id);
        $is_published = intval($request->input('is_published')); 
        $is_featured  = intval($request->input('is_featured'));
        $featured_order = 0;
        $is_slided  = intval($request->input('is_slided'));
        $slided_order = 0;
        $is_best_dealed  = intval($request->input('is_best_dealed'));
        $best_deal_ordered = 0;
        $is_project_developmented = intval($request->input('is_project_developmented'));
        $project_development_ordered = 0;

        //If property is not published, it cannot be featured or slided.
        if($is_published == 0){
            $is_featured = 0;
            $is_slided = 0;
        }

        if($is_featured == 1){
            if($obj->is_featured == 0){
                 $features = Model::select('id')->where('is_featured', 1)->orderBy('featured_order', 'ASC')->get();
                 Model::where('id', $id)->update(['featured_order'=>1]);
                 $i=2;
                 foreach($features as $row){
                    Model::where('id', $row->id)->update(['featured_order'=>$i]);
                    $i++;
                 }
            }
        }
        if($is_slided == 1){
            if($obj->is_slided == 0){
                 $slides = Model::select('id')->where('is_slided', 1)->orderBy('slided_order', 'ASC')->get();
                 Model::where('id', $id)->update(['slided_order'=>1]);
                 $i=2;
                 foreach($slides as $row){
                    Model::where('id', $row->id)->update(['slided_order'=>$i]);
                    $i++;
                 }
            }
        }
        if($is_best_dealed == 1){
            if($obj->is_best_dealed == 0){
                 $best_deals = Model::select('id')->where('is_best_dealed', 1)->orderBy('best_deal_ordered', 'ASC')->get();
                 Model::where('id', $id)->update(['best_deal_ordered'=>1]);
                 $i=2;
                 foreach($best_deals as $row){
                    Model::where('id', $row->id)->update(['best_deal_ordered'=>$i]);
                    $i++;
                 }
            }
        }
        if($is_project_developmented == 1){
            if($obj->is_project_developmented == 0){
                 $best_deals = Model::select('id')->where('is_project_developmented', 1)->orderBy('project_development_ordered', 'ASC')->get();
                 Model::where('id', $id)->update(['project_development_ordered'=>1]);
                 $i=2;
                 foreach($best_deals as $row){
                    Model::where('id', $row->id)->update(['project_development_ordered'=>$i]);
                    $i++;
                 }
            }
        }
        $contract_type_id = $request->input('contract_type_id'); 
        $exclusive_from = $request->input('exclusive_from'); 
        $exclusive_till = $request->input('exclusive_till'); 
        if($contract_type_id == 1){
            $exclusive_from = $request->input('x'); 
            $exclusive_till = $request->input('x'); 
        }


        $data = array(
                  
                    'updater_id'                      =>  $user_id,
                    'contract_type_id'                =>  $contract_type_id,
                    'exclusive_from'                  =>  $exclusive_from,
                    'exclusive_till'                  =>  $exclusive_till,
                    'is_published'                    =>  $is_published,
                    'is_featured'                     =>  $is_featured,
                    'is_slided'                       =>  $is_slided,
                    'is_best_dealed'                  =>  $is_best_dealed,
                    'is_project_developmented'        =>  $is_project_developmented,
                );

       

        $v = Validator::make(
                        $data, 
                        [
                            'contract_type_id' => 'exists:contract_types,id'
                        ]);

        $v->sometimes(['exclusive_from', 'exclusive_till'], 'date', function ($input) {
            return $input->contract_type_id == 2;
        });
        $v->validate();
        
       
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Property has been updated!' );
        return redirect()->back();
       
    }

   

}
