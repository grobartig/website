<?php
namespace App\Http\Controllers\User\Property\Tool;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
class PrintFromController extends MainController
{
 
    protected $route; 
    public function __construct(){
        $this->route = "user.property.tool.print";
    }
    public function index($id){
        $this->checkRole($id, 'view-print');
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id]);
    }
    public function form($id){
        // $this->validObj($id);
        // $data = Model::find($id);
        // return view($this->route.'.form', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
        echo "Data not avaiable";
    }

    public function removeList($id, $property_id){
        Model::find($id)->propertyLists()->where('list_id', $property_id)->delete();
        Session::flash('msg', 'List has been removed from property!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'List has been removed from property'
        ]);
    }

    public function searchList(Request $request){
       
        $id = $request->input('id');
        $key = $request->input('key');
        
        $data = Listing::select('id', 'name')->where('name', 'like', '%'.$key.'%')->whereDoesntHave('listProperties', function ($query) {
            $id = $_GET['id'];
            $query->where('property_id', '=', $id);
        })->limit(50)->orderBy('id', 'DESC')->get();
        
        return view( $this->route.'.searchList', ['id'=>$id, 'data'=>$data]);
    }

    public function selectedList(Request $request){
        $id = $request->input('id');
        $data = Model::find($id)->lists()->select('lists.id', 'name')->orderBy('id', 'DESC')->get();
        return view( $this->route.'.selectedList', ['data'=>$data]);
    }
    public function removeListFromProperty(Request $request){
        $id = $request->input('id');
        $list_id = $request->input('list_id');
        Model::find($id)->propertyLists()->where('list_id', $list_id)->delete();
        echo $this->selectedList($request);
    }
    public function addListToProperty(Request $request){
        $id = $request->input('id');
        $list_id = $request->input('list_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'property_id' =>  $id,
                    'list_id' =>  $list_id,
                    'creator_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Model::find($id)->propertyLists()->insert($data);
        echo $this->selectedList($request);
    }

  
    function mailings($id){
         
        $data = Model::find($id)->propertyMails();

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        if( $limit == 0 ){
            $limit = 10;
        }
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->whereHas('mail', function($query){
                $key       =   isset($_GET['key'])?$_GET['key']:"";
                $query->where('name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%');
            });
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('properties_mailings.created_at', [$from, $till]);
            }
        }

        $data= $data->orderBy('properties_mailings.created_at', 'DESC')->paginate($limit);
        return view($this->route.'.mailings', ['route'=>$this->route, 'id'=>$id, 'data'=>$data, 'appends'=>$appends]);
    }

    public function addPropertiesToList(Request $request){
        $list_id = $request->input('list_id');
        $properties = json_decode($request->input('list'));
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        
        $num_of_added = 0;
        $num_of_unadded = 0;
        $submitted_properties = count($properties);
       
        $listProperties = Listing::find($list_id)->listProperties();
        foreach( $properties as $property ){
            if($property != 0){
                $existing = Listing::find($list_id)->listProperties()->select('id')->where(['property_id'=> $property, 'list_id'=> $list_id])->count();
                if($existing == 0){
                    Listing::find($list_id)->listProperties()->insert(['list_id'=>$list_id, 'property_id'=>$property, 'creator_id'=>$user_id, 'created_at'=>$now, 'updated_at'=>$now]);
                    $num_of_added++ ;
                }else{
                    $num_of_unadded++;
                }
            }
        }

        $msg = $num_of_added." property(s) has been added to a list. ";

        if($num_of_added != $submitted_properties){
            $msg .= $num_of_unadded." not added becase already existed in target list.";
        }

        if( $num_of_unadded == $submitted_properties){
            $msg = " No property added becase already existed in that list.";
        }
        

        return response()->json([
            'status' => 'success',
            'msg' => $msg
        ]);
        
    }

    
    function createPropertyList(Request $request){
        $name = $request->input('name');
        $list = json_decode($request->input('list'));

        $now        = date('Y-m-d H:i:s');
        $user_id    = Auth::id();
        $data       = array(
                            'name'          =>  $name,
                            'creator_id'    =>  $user_id,
                            'created_at'    =>  $now, 
                            'updated_at'    =>  $now
                            );
        $id = Listing::insertGetId($data);
        foreach( $list as $row ){
            if($row != 0){
                Listing::find($id)->listProperties()->insert(['list_id'=>$id, 'property_id'=>$row, 'creator_id'=>$user_id, 'updater_id'=>$user_id, 'created_at'=>$now, 'updated_at'=>$now]);
            }
        }

        return response()->json([
            'status' => 'success',
            'msg' => 'A new group of property has been created!'
        ]);

    }
   

}
