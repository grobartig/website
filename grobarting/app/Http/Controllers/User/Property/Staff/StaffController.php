<?php

namespace App\Http\Controllers\User\Property\Staff;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\User\Property\MainController;
use App\Model\Property\Property as Model;
use App\Model\User\User as Staff;
use App\Model\Setup\Role as Role;
use App\Model\Setup\Access as Access;
use Telegram\Bot\Laravel\Facades\Telegram;

class StaffController extends MainController
{
    protected $route; 
    public function __construct(){
        $this->route = "user.property.staff";
        
    }
    function index($id){;
        $this->checkRole($id, 'view-staff');
        $data = Model::find($id)->propertyStaffs()->orderBy('is_primary', 'DESC')->get();
        
        return view($this->route.'.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }
    
    public function search(Request $request){
        $id = $request->input('id');
       
        $data = Staff::select('id', 'en_name as name', 'email', 'en_position as position')->where(['status'=>1, 'visible'=>1])->where(function ($query) {
                $key = $_GET['key'];
                $query->where('en_name', 'like', '%'.$key.'%')
                      ->where('email', 'like', '%'.$key.'%')
                      ->where('phone', 'like', '%'.$key.'%');
            })->whereDoesntHave('staffProperties', function ($query) {
                $id = $_GET['id'];
                $query->where('property_id', '=', $id);
            })->limit(50)->orderBy('id', 'DESC')->get();
        
        $roles = Role::select('id', 'name')->get();
        return view( $this->route.'.search', ['id'=>$id, 'data'=>$data, 'roles'=>$roles]);
    }
     public function selected(Request $request){
        $id = $request->input('id');
        $data = Model::find($id)->propertyStaffs()->select('id', 'staff_id', 'role_id')->orderBy('id', 'DESC')->get();
        return view( $this->route.'.selected', ['data'=>$data]);
    }
    public function remove(Request $request){
        $id = $request->input('id');
        $staff_id = $request->input('staff_id');
        Model::find($id)->propertyStaffs()->where('staff_id', $staff_id)->delete();
        
        $user = Auth::user('user');
        $staff_data     = Staff::find($staff_id);
        $property  = Model::find($id);
        //==================================================== Get Property Code 
        $l = '';
        if($property->is_published == 0){
            $l = 'L';
        }
        $display_listing_code = $l.$property->province->abbre.'-'.$property->type->abbre.$property->listing_code;
        // if($user->telegram_chat_id != '' && $user->telegram_chat_id != null){
            
        //     $response = Telegram::sendMessage([
        //       'chat_id' => $user->telegram_chat_id, 
        //       'text' => '<b>System Allert</b>:You have removed <b>'.$staff_data->en_name.' </b> From Property Code:<b> '.$display_listing_code.'</b>. Thanks! ',
        //       'parse_mode' => 'HTML'
        //     ]);

        // }
        // if($staff_data->telegram_chat_id != '' && $staff_data->telegram_chat_id != null){
        //     $response = Telegram::sendMessage([
        //       'chat_id' => $staff_data->telegram_chat_id, 
        //       'text' => '<b>System Allert</b>:You have been removed From Property Code:<b> '.$display_listing_code.'</b> by <b>'.$user->en_name.'</b>. Thanks! ',
        //       'parse_mode' => 'HTML'
        //     ]);

        // }
        echo $this->selected($request);
    }
    public function add(Request $request){
        //echo $url;die;
        $id = $request->input('id');
        $this->checkRole($id, 'manage-staff');
        $staff_id = $request->input('staff_id');
        $role_id = $request->input('role_id');
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');
        $data       = array(
                    'property_id' =>  $id,
                    'staff_id' =>  $staff_id,
                    'role_id' =>  $role_id,
                    'is_primary' =>  0,
                    'creator_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Model::find($id)->propertyStaffs()->insert($data);
            $user = Auth::user('user');
            $staff_data     = Staff::find($staff_id);
            $property  = Model::find($id);
            //==================================================== Get Property Code 
            $l = '';
            if($property->is_published == 0){
                $l = 'L';
            }
            $display_listing_code = $l.$property->province->abbre.'-'.$property->type->abbre.$property->listing_code;
            // if($user->telegram_chat_id != '' && $user->telegram_chat_id != null){
            //     $role_name      = Role::find($role_id)->name;
            //     $response = Telegram::sendMessage([
            //       'chat_id' => $user->telegram_chat_id, 
            //       'text' => '<b>System Allert</b>:You have assigned <b>'.$staff_data->en_name.'</b> to be <b>'.$role_name.'</b> of Property Code:<b>'.$display_listing_code.'</b>. Thanks!',
            //       'parse_mode' => 'HTML'
            //     ]);

            // }
            // if($staff_data->telegram_chat_id != '' && $staff_data->telegram_chat_id != null){
            //     $role_name      = Role::find($role_id)->name;
            //     $response = Telegram::sendMessage([
            //       'chat_id' => $staff_data->telegram_chat_id, 
            //       'text' => '<b>System Allert</b>:You have been assigned to be <b>'.$role_name.'</b> of Property Code:<b>'.$display_listing_code.'</b> by <b>'.$user->en_name.'</b>. Thanks!',
            //       'parse_mode' => 'HTML'
            //     ]);

            // }
        echo $this->selected($request);
    }

    function primary(Request $request){
        $id         = $request->input('id');
        $primary_id = $request->input('primary_id');
        $staffs        = Model::find($id)->propertyStaffs();
        $record        = $staffs->find($primary_id);
        if(count($record)>0){
            Model::find($id)->propertyStaffs()->where('is_primary', 1)->update(['is_primary'=>0]);
            
            $user_id    = Auth::id();
            $now        = date('Y-m-d H:i:s');
            Model::find($id)->propertyStaffs()->where('id', $primary_id)->update(['is_primary'=>1, 'updater_id'=>$user_id, 'updated_at'=>$now]);
            
            Session::flash('msg', 'Primary has been changed' );
            return response()->json([
                'status' => 'success',
                'msg' => 'Primary has been changed.'
            ]);
        }
            
    }
     public function roleAccess(){
        $role_id    = $_GET['role_id'];
        $accesses = Access::get();
        $role = Role::find($role_id);
        $data = $role->roleAccesses()->select('access_id')->get();
        return view($this->route.'.accesses', ['route'=>$this->route, 'role'=>$role, 'accesses'=>$accesses, 'data'=>$data]);
    }


}
