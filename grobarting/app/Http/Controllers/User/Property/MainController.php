<?php

namespace App\Http\Controllers\User\Property;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Property\Property as Model;
use App\Model\Setup\province as Province;
use App\Model\Setup\District as District;
use App\Model\Setup\Commune as Commune;
use App\Model\Setup\Type as Type;
class MainController extends Controller
{
   
    public function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function checkRole($id, $access){
       if( ! checkRole($id, $access)){
            echo "In valide access!"; die;
       }
    }

    public function codeGenerator($province_id, $type_id){
        
        //echo $province_id; die;
        $listing_code = 1;
        $data = Model::select('listing_code')->where(['province_id'=>$province_id, 'type_id'=>$type_id])->orderBy('id', 'DESC')->first();
        if(count($data) == 1){
            $listing_code = intval($data->listing_code)+1;
        }

        if($listing_code >=0 && $listing_code < 10 ){
            $listing_code = "000".$listing_code;
        }else if( $listing_code >= 10 && $listing_code < 100 ){
            $listing_code = "00".$listing_code;
        }else if( $listing_code >= 100 && $listing_code < 1000 ){
            $listing_code = "0".$listing_code;
        }

        return $listing_code;
    }

    public function districts(){
        $province_id = isset($_GET['province_id'])?$_GET['province_id']:'';
        if(  is_numeric ($province_id) ){
            if( $province_id != 0 ){
                $data = District::select('id', 'en_name as name', 'lat', 'lng')->where(['province_id'=>$province_id, 'status'=>1])->get();
                return response()->json($data);
            }else{
                echo "Not vaide data";
            }
        }else{
            echo "Not vaide data: ";
        }
       
    }

    public function communes(){
        $district_id = isset($_GET['district_id'])?$_GET['district_id']:'';
        if(  is_numeric ($district_id) ){
            if( $district_id != 0 ){
                $data = Commune::select('id', 'en_name as name', 'lat', 'lng')->where(['district_id'=>$district_id, 'status'=>1])->get();
                return response()->json($data);
            }else{
                echo "Not vaide data";
            }
        }else{
            echo "Not vaide data: ";
        }
       
    }

   
}
