<?php

namespace App\Http\Controllers\User\Customer;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Setup\Customer as Model;
use App\Model\Setup\CustomerNote as Note;
use App\Model\Property\Property as Property;
use App\Model\Setup\CustomerNeed as Enquiry;
use App\Model\Setup\CustomerNeedLocation1 as Location1;
use App\Model\Setup\CustomerNeedLocation2 as Location2;
use App\Model\Setup\customerNeedFeatures as customerNeedFeatures;
use App\Model\Property\PropertyEnquiry as Inquiry;

use App\Model\Setup\Action as Action;
use App\Model\Setup\Type as Type;

use App\Model\Setup\Province as Province;
use App\Model\Setup\District as District;
use App\Model\Setup\Commune as Commune;



class CustomerController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.customer";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $this->checkPermision($this->route.'.index');
        $data = Model::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('first_name', 'like', '%'.$key.'%')->orWhere('last_name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('phone', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('created_at', 'DESC')->paginate($limit);
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data, 'appends'=>$appends]);
    }
   
    public function create(){
        $this->checkPermision($this->route.'.create');
        return view($this->route.'.create', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $this->checkPermision($this->route.'.create');
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');

        $cus_id = 1;
        $data = Model::select('cus_id')->orderBy('id', 'DESC')->first();
        if(count($data) == 1){
            $cus_id = intval($data->cus_id)+1;
        }

        if($cus_id >=0 && $cus_id < 10 ){
            $cus_id = "000".$cus_id;
        }else if( $cus_id >= 10 && $cus_id < 100 ){
            $cus_id = "00".$cus_id;
        }else if( $cus_id >= 100 && $cus_id < 1000 ){
            $cus_id = "0".$cus_id;
        }else if($cus_id>=10000){
            echo "No more more avaiable customer ID"; die;
        }
        $data = array(
                    'cus_id'         =>   $cus_id,
                    'title_id'       =>   $request->input('title_id'),
                    'apply'          =>   $request->input('apply'),
                    'reason'         =>   $request->input('reason'),
                    'first_name'     =>   $request->input('first_name'), 
                    'last_name'      =>   $request->input('last_name'),
                    'company'        =>  $request->input('company'),
                    'department'     =>  $request->input('department'), 
                    'phone'          =>  $request->input('phone'), 
                    'email'          =>  $request->input('email'), 
                    'address_line_1' =>  $request->input('address_line_1'), 
                    'address_line_2' =>  $request->input('address_line_2'), 
                    'address_line_3' =>  $request->input('address_line_3'),
                    'country'        =>  $request->input('country'),
                    'postal_code'    =>  $request->input('postal_code'),
                    'inquiry'        =>   $request->input('inquiry'),
                      //Must include for all store
                    'creator_id' =>  $user_id,
                    'updater_id' =>  $user_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
       $v   =   Validator::make(
                        $data, 
                        [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'phone' => [
                                            'required'
                                        ]
                                        
                        ]);

        $v->validate();
      
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->checkPermision($this->route.'.edit');
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $this->checkPermision($this->route.'.update');
        $id = $request->input('id');

        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $data = array(
                    'title_id'          =>  $request->input('title_id'),
                    'apply'             =>  $request->input('apply'),
                    'reason'            =>  $request->input('reason'),
                    'first_name'        =>  $request->input('first_name'), 
                    'last_name'         =>  $request->input('last_name'),
                    'company'           =>  $request->input('company'),
                    'department'        =>  $request->input('department'), 
                    'phone'             =>  $request->input('phone'), 
                    'email'             =>  $request->input('email'), 
                    'address_line_1'    =>  $request->input('address_line_1'), 
                    'address_line_2'    =>  $request->input('address_line_2'), 
                    'address_line_3'    =>  $request->input('address_line_3'),
                    'country'           =>  $request->input('country'),
                    'postal_code'       =>  $request->input('postal_code'),
                    'inquiry'           =>  $request->input('inquiry'),
                    //Must include for all update
                    'updater_id'        =>  $user_id,
                    'updated_at'        =>  $now
                );

        $v   =   Validator::make(
                        $data, 
                        [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'phone' => [
                                            'required'
                                        ]
                        ]);

        $v->validate();

		
      
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        $this->checkPermision($this->route.'.trash');
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

     public function notes($id=0){
        $this->checkPermision($this->route.'.notes');
        $this->validObj($id);
        $data = Model::find($id)->notes()->get();
        return view($this->route.'.notes', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);

    }
    public function createNote($id=0){
        $this->checkPermision($this->route.'.create-note');
        $this->validObj($id);
        return view($this->route.'.createNote',['route'=>$this->route, 'id'=>$id]);

    }
    public function storeNote(Request $request) {
        $this->checkPermision($this->route.'.create-note');
        $data = array(
                    'customer_id' =>   $request->input('id'), 
                    'note' =>  $request->input('note')
                );
        $date = $request->input('date');
        $is_notified = $request->input('is_notified');
        if( $is_notified == 1){
            if(FunctionController::isValidDate($date)){
                $data['is_notified'] = $is_notified;
                $data['date'] = $date;
            }else{
                $data['is_notified'] = 0;
            }
            
        }
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'note' => [
                                            'required'
                                        ],
                        ],
                        [
                        ])->validate();

        
        $customer_id = $request->input('id');

        $id=Note::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
        if(checkPermision($this->route.'.edit-note')){
             return redirect(route($this->route.'.edit-note', ['id'=>$customer_id,'note_id'=>$id]));
        }else{
            return redirect(route($this->route.'.create-note', ['id'=>$customer_id]));
        }
       
    }

     public function editNote($id = 0,$note_id = 0){
         $this->checkPermision($this->route.'.edit-note');
        $this->validObj($id);
        $data = Model::find($id)->notes()->find($note_id);
        return view($this->route.'.editNote', ['route'=>$this->route, 'id'=>$id,'note_id'=>$note_id, 'data'=>$data]);
    }

    public function updateNote(Request $request){
        $this->checkPermision($this->route.'.update-note');
        $id = $request->input('note_id');
        Validator::make(
                        $request->all(), 
                        [
                           
                            'note' => [
                                            'required'
                                        ],
                        ])->validate();

        
        $data = array(
                    
                    'customer_id' =>   $request->input('id'), 
                    'note' =>  $request->input('note'), 
                    'is_notified' =>  $request->input('is_notified')
                );
        $date = $request->input('date');
        $is_notified = $request->input('is_notified');
        if( $is_notified == 1){
            if(FunctionController::isValidDate($date)){
                $data['is_notified'] = $is_notified;
                $data['date'] = $date;
            }else{
                $data['is_notified'] = 0;
                 $data['date'] = '2017-01-01';
            }
            
        }else{
            $data['date'] = '2017-01-01';
        }
        Note::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    public function trashNote($id){
         $this->checkPermision($this->route.'.trash-note');
        //Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Note::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

     function updateNoteStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_notified' => $request->input('active'));
      Note::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Property status has been updated.'
      ]);
    }

   
    public function inquiries($id=0){
        $this->checkPermision($this->route.'.inquiries');
        $this->validObj($id);
        return view($this->route.'.inquiries', ['route'=>$this->route, 'id'=>$id]);
    }

    public function inquiriesData($id=0){
       $this->checkPermision($this->route.'.inquiries');
       $this->validObj($id);
       $data = Model::find($id)->enquiries()->get();
       return view($this->route.'.inquiries_data', ['route'=>$this->route, 'id'=>$id,'data'=>$data]); 
    }

    public function inquiriesUpdate(Request $request){
        $this->checkPermision($this->route.'.update-inquiry');
        $id = $request->input('id');
        $data = array(
                    'message' =>   $request->input('message')
                );
        
        
        Inquiry::where('id', $id)->update($data);
        return response()->json([
            'status' => 'success',
            'msg' => 'Enquiries message has been Update.'
        ]);
    }
    public function inquiriesStore(Request $request){
        $this->checkPermision($this->route.'.create-inquiry');
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $cutomer_id = isset($_GET['cutomer_id'])?$_GET['cutomer_id']:0;

        $listing_code = $request->input('listing_code');
        $property_id = $this->getPropertyIdByListingCode($listing_code);

        if($property_id != 0){
            $data = array(
                    'customer_id' =>   $cutomer_id, 
                    'property_id' =>   $property_id,
                    'created_at' => $now, 
                    'updated_at' => $now
                );
            $id = Inquiry::insertGetId($data);
            return response()->json([
                'status' => 'success',
                'msg' => 'Enquiry has been created!'
            ]);
        }else{
            return response()->json([
                'status' => 'erorr',
                'msg' => 'No Property in here.'
            ]);
        }
    }
    public function trashInquiry($id){
         $this->checkPermision($this->route.'.trash-inquiry');
        //Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Inquiry::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }


    public function enquiries($id=0){
        $this->checkPermision($this->route.'.enquiries');
        $this->validObj($id);
        $data = Model::find($id)->needs()->get();
        return view($this->route.'.enquiries', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }
     public function createEnquiry($id=0){
        $this->checkPermision($this->route.'.create-enquiry');
        $types       = Type::get();

        $provinces   = Province::select('id', 'en_name as name', 'lat', 'lng')->get();
        $this->validObj($id);
        return view($this->route.'.createEnquiry',['route'=>$this->route, 'id'=>$id, 'types'=>$types, 'provinces'=>$provinces]);

    }
    public function amenties(){
        $type_id = isset($_GET['type_id'])?$_GET['type_id']:0;
        if($type_id != 0){
            $data = Type::find($type_id)->features;
            if(count($data) > 0){
              return view($this->route.'.amenties',['data'=>$data]);  
              }else{
                echo "No amenties avaiable";
              }
        }else{
            echo "Please slect a type";
        }
    }

    public function districts(){
        $province_id = isset($_GET['province_id'])?$_GET['province_id']:'';
        if(  is_numeric ($province_id) ){
            if( $province_id != 0 ){
                $data = District::select('id', 'en_name as name', 'lat', 'lng')->where(['province_id'=>$province_id, 'status'=>1])->get();
                return response()->json($data);
            }else{
                return response()->json('no data');
            }
        }else{
            echo "Not vaide data: ";
        }
       
    }

    public function communes(){
        $district_id = isset($_GET['district_id'])?$_GET['district_id']:'';
        if(  is_numeric ($district_id) ){
            if( $district_id != 0 ){
                $data = Commune::select('id', 'en_name as name', 'lat', 'lng')->where(['district_id'=>$district_id, 'status'=>1])->get();
                return response()->json($data);
            }else{
                echo "Not vaide data";
            }
        }else{
            echo "Not vaide data: ";
        }
       
    }

    public function storeEnquiry(Request $request) {
        $this->checkPermision($this->route.'.create-enquiry');
       $amenties = $request->input('amenties');
       //print_r($amenties);die;
       $data = array(
                    
                    'customer_id' =>   $request->input('id'), 
                    'action_id' =>   $request->input('action_id'),
                    'type_id' =>   $request->input('type-id'),
                    'min_price' =>   $request->input('min_price'),
                    'max_price' =>   $request->input('max_price'),
                    'purpose' =>  $request->input('purpose'),
                    'other' =>  $request->input('other')
                );
        $data_location1 =array(
                    
                    'province_id' =>   $request->input('province-id-1'), 
                    'district_id' =>   $request->input('district-id-1'),
                    'commune_id' =>   $request->input('commune-id-1'),
                );
        $date = $request->input('date');
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            'type-id' => 'exists:types,id',
                            'min_price' => 'required|min:1|max:10',
                            'max_price' => 'required|min:1|max:10',
                            'purpose' => 'required|min:2|max:250',
                            'province-id-1' => 'exists:provinces,id',
                            'district-id-1' => 'exists:districts,id',
                            'commune-id-1' => 'exists:communes,id',
                        ],
                        [
                            'type-id.exists' => "Please select a type.",
                            'province-id-1.exists' => "Please select a province.",
                            'district-id-1.exists' => "Distrit was not selected. ",
                            'commune-id-1.exists' => "Please select a commune."
                        ])->validate();

        
        $customer_id = $request->input('id');

        $id=Enquiry::insertGetId($data);

        $data_location1['customer_need_id'] = $id;
        
        Location1::insert($data_location1);

        $province_id_2 = $request->input('province-id-2');
        $data_location2['customer_need_id'] = $id;
        if($province_id_2 != 0){

            $data_location2['province_id'] = $request->input('province-id-2');
            if($request->input('district-id-2')!=0){
                $data_location2['district_id'] = $request->input('district-id-2');

                if($request->input('commune-id-2')){
                    $data_location2['commune_id'] = $request->input('commune-id-2');
                }
            }

        }
        Location2::insert($data_location2);
        

        $amenties = $request->input('amenties');
        $result = count($amenties);

        for ($i = 0; $i < sizeof($amenties); $i++){
            customerNeedFeatures::insert(['customer_need_id' => $id, 'feature_id' => $amenties[$i]]);
        }
        Session::flash('msg', 'Data has been Created!');

        //return redirect(route($this->route.'.edit-enquiry', ['id'=>$buyer_id,'note_id'=>$id]));

        if(checkPermision($this->route.'.edit-enquiry')){
             return redirect(route($this->route.'.edit-enquiry', ['id'=>$customer_id,'enquiry_id'=>$id]));
        }else{
            return redirect(route($this->route.'.create-enquiry', ['id'=>$customer_id]));
        }
    }

    public function editEnquiry($id = 0,$need_id = 0){
        $this->checkPermision($this->route.'.edit-enquiry');
        $actions     = Action::get();
        $types       = Type::get();

         $this->validObj($id);
        //$data = Model::find($id)->needs()->find($need_id);
        $data = Model::find($id)->needs()->find($need_id);

        $provinces   = Province::select('id', 'en_name as name', 'lat', 'lng')->get();
        
        return view($this->route.'.editEnquiry', ['route'=>$this->route, 'id'=>$id,'need_id'=>$need_id, 'data'=>$data, 'actions'=>$actions, 'types'=>$types, 'provinces'=>$provinces]);
    }

    public function editAmenties(){
        $type_id = isset($_GET['type_id'])?$_GET['type_id']:0;
        $customer_need_id = isset($_GET['customer_need_id'])?$_GET['customer_need_id']:0;
        
        if($type_id != 0){
            $data = Type::find($type_id)->features;

            $data_customer_need_feature = customerNeedFeatures::where('customer_need_id',$customer_need_id)->get();
            
            if(count($data) > 0){
              return view($this->route.'.amentiesCheck',['data'=>$data,'data_customer_need_feature'=>$data_customer_need_feature]);  
            }else{
                echo "No amenties avaiable";
            }
        }else{
            echo "Please slect an type";
        }
    }

    public function updateEnquiry(Request $request){
        $this->checkPermision($this->route.'.update-enquiry');
        $id = $request->input('need_id');
        Validator::make(
                    $request->all(), 
                    [
                        'type-id' => 'exists:types,id',

                        'min_price' => 'required|min:1|max:10',
                        'max_price' => 'required|min:1|max:10',
                        'purpose' => 'required|min:2|max:250',
                        'province-id-1' => 'exists:provinces,id',
                        'district-id-1' => 'exists:districts,id',
                        'commune-id-1' => 'exists:communes,id',
                        ],
                        [
                            'type-id.exists' => "Please select a type.",
                            'province-id-1.exists' => "Please select a province.",
                            'district-id-1.exists' => "Distrit was not selected. ",
                            'commune-id-1.exists' => "Please select a commune."
                        ])->validate();

        
        $data = array(
                    
                    'customer_id' =>   $request->input('id'), 
                    'action_id' =>   $request->input('action-id'),
                    'type_id' =>   $request->input('type-id'),
                    'min_price' =>   $request->input('min_price'),
                    'max_price' =>   $request->input('max_price'),
                    'purpose' =>  $request->input('purpose'),
                    'other' =>  $request->input('other')
                );
        $date = $request->input('date');
        
        $data_location1 =array(
                    
                    'province_id' =>   $request->input('province-id-1'), 
                    'district_id' =>   $request->input('district-id-1'),
                    'commune_id' =>   $request->input('commune-id-1'),
                );

        Enquiry::where('id', $id)->update($data);
        
        Enquiry::find($id)->customerNeedFeatures()->delete();
        $amenties = $request->input('amenties');
        $result = count($amenties);
        for ($i = 0; $i < sizeof($amenties); $i++){
            customerNeedFeatures::insert(['customer_need_id' => $id, 'feature_id' => $amenties[$i]]);
        }

        $data_location1['customer_need_id'] = $id;
        $customer_need_location1_id = $request->input('customer_need_location1');
        
        Location1::where('id', $customer_need_location1_id)->update($data_location1);
  
        

        if($request->input('province-id-2')!=0){
            $data_location2['customer_need_id'] = $id;
            $data_location2['province_id'] = $request->input('province-id-2');
            if($request->input('district-id-2')!=0){
                $data_location2['district_id'] = $request->input('district-id-2');
                if($request->input('commune-id-2')){
                    $data_location2['commune_id'] = $request->input('commune-id-2');
                }
            }
            $customer_need_location2_id = $request->input('customer_need_location2');
            Location2::where('id', $customer_need_location2_id)->update($data_location2);

        }
        

        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    public function trashEnquiry($id){
        $this->checkPermision($this->route.'.trash-enquiry');
        //Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Enquiry::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }

    public function properties($id=0){
        $this->checkPermision($this->route.'.properties');
        $this->validObj($id);
        $data = Model::find($id)->properties()->get();
        return view($this->route.'.properties', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }
    public function addProperty(Request $request){
        $this->checkPermision($this->route.'.add-property');
        $user_id  = Auth::id();
        $now      = date('Y-m-d H:i:s');
        $customer_id = isset($_GET['customer_id'])?$_GET['customer_id']:0;

        $listing_code = $request->input('listing_code');
        $property_id = $this->getPropertyIdByListingCode($listing_code);

        if($property_id != 0 && $customer_id!=0){
          
            $propertyData = ['customer_id'=>$customer_id, 'purchase_date'=>date('Y-m-d')]; 
            Property::where('id', $property_id)->update($propertyData);
            Session::flash('msg', 'Property has been added to the customer.' );
            return response()->json([
                'status' => 'success',
                'msg' => 'Property has been added.'
            ]);
        }else{
            return response()->json([
                'status' => 'erorr',
                'msg' => 'No Property in here.'
            ]);
        }
    }
    public function removeProperty($id){
        $this->checkPermision($this->route.'.remove-property');
        $propertyData = ['customer_id'=>null]; 
        Property::where('id', $id)->update($propertyData);
        Session::flash('msg', 'Property has been removed!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Property has been removed!'
        ]);
    }
    
}
