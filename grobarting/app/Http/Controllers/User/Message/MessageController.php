<?php

namespace App\Http\Controllers\User\Message;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Message as Model;

class MessageController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.message";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

     public function index(){
        $data = Model::select('*')->orderBy('id','DESC');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);

        if( $key != "" ){
            $data = $data->where('first_name', 'like', '%'.$key.'%')->orWhere('last_name', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%')->orWhere('subject', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }

        $data= $data->orderBy('id', 'DESC')->paginate($limit);
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data,'appends'=>$appends]);
    }
   
    public function store(Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'first_name'    =>  $request->input('first_name'), 
                    'last_name'     =>  $request->input('last_name'),
                    'email'         =>  $request->input('email'),
                    'subject'       =>  $request->input('subject'),
                    'message'       =>  $request->input('message'),
                   
                    'created_at'    => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'first_name' => 'required',
                           'last_name'  => 'required',
                           'subject'    => [
                                            'required',
                                            Rule::unique('messages')
                                        ],
                            'email'     => [
                                            'required',
                                            'email',
                                            Rule::unique('messages')
                                        ]
                        ])->validate();
        $id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
        return redirect(route($this->route.'.edit', $id));
    }

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }

}