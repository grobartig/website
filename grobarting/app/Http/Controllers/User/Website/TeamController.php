<?php

namespace App\Http\Controllers\User\Website;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Website\Team as Model;

class TeamController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.website.team";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function listData(){
        $data = Model::orderBy('id', 'DESC')->get();
        return view($this->route.'.list', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function showCreateForm(){
        return view($this->route.'.createForm', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'en_title' =>   $request->input('en_title'),
                    'kh_title' =>   $request->input('kh_title'),
                    'cn_title' =>   $request->input('cn_title'),
                    'en_position' =>   $request->input('en_position'),
                    'kh_position' =>   $request->input('kh_position'),
                    'cn_position' =>   $request->input('cn_position'),
                    'phone' =>   $request->input('phone'),
                    'email' =>   $request->input('email'),
                    'created_at' => $now
                    
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'en_title' => 'required|min:1|max:550',
                            'kh_title' => 'required|min:1|max:550',
                            'cn_title' => 'required|min:1|max:550',
                            'en_position' => 'required|min:1|max:550',
                            'kh_position' => 'required|min:1|max:550',
                            'cn_position' => 'required|min:1|max:550',
                            'phone' => [
                                            'required'
                                        ],
                            'email' => [
                                            'required',
                                            'email'
                                        ],
                             'image' => [
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(370)->maxHeight(390),
                                        ]
                        ])->validate();

        if($request->input('active')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        $image = FileUpload::uploadFile($request, 'image', 'uploads/team');
        if($image != ""){
            $data['image'] = $image; 
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function showEditForm($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.editForm', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $now        = date('Y-m-d H:i:s');
        $id = $request->input('id');
        Validator::make(
        				$request->all(), 
			        	[
                            
                            'en_title' => 'required|min:1|max:550',
                            'kh_title' => 'required|min:1|max:550',
                            'cn_title' => 'required|min:1|max:550',
                            'en_position' => 'required|min:1|max:550',
                            'kh_position' => 'required|min:1|max:550',
                            'cn_position' => 'required|min:1|max:550',
                            'phone' => [
                                            'required'
                                        ],
                            'email' => [
                                            'required',
                                            'email'
                                        ],
                             'image' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(370)->maxHeight(390),
                                        ]
                        ],
                        [
                            
                        ])->validate();

		
		$data = array(
                    'en_title' =>   $request->input('en_title'),
                    'kh_title' =>   $request->input('kh_title'),
                    'cn_title' =>   $request->input('cn_title'),
                    'en_position' =>   $request->input('en_position'),
                    'kh_position' =>   $request->input('kh_position'),
                    'cn_position' =>   $request->input('cn_position'),
                    'phone' =>   $request->input('phone'),
                    'email' =>   $request->input('email'),
                    'updated_at' => $now
                );
        $image = FileUpload::uploadFile($request, 'image', 'uploads/team');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('active')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

    public function trash($id){
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Status has been updated.'
      ]);
    }
   
}
