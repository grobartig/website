<?php

namespace App\Http\Controllers\User\Website;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Website\Service as Model;

class ServiceController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.website.service";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function listData(){
        $data = Model::orderBy('id', 'DESC')->get();
        return view($this->route.'.list', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function showCreateForm(){
        return view($this->route.'.createForm', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'en_title' =>   $request->input('en_title'),
                    'kh_title' =>   $request->input('kh_title'),
                    'cn_title' =>   $request->input('cn_title'),
                    'en_description' =>   $request->input('en_description'),
                    'kh_description' =>   $request->input('kh_description'),
                    'cn_description' =>   $request->input('cn_description'),
                    'en_content' =>   $request->input('en_content'),
                    'kh_content' =>   $request->input('kh_content'),
                    'cn_content' =>   $request->input('cn_content'),
                    'slug'      =>   FunctionController::generateSlug('news', $request->input('en_title')),
                    'created_at' => $now
                    
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'en_title' => 'required|min:1|max:550',
                            'kh_title' => 'required|min:1|max:550',
                            'cn_title' => 'required|min:1|max:550',

                            'image' => [
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(1400)->maxHeight(595),
                            ],
                            'feature_image' => [
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(600)->maxHeight(600),
                            ]
                        ])->validate();

        if($request->input('active')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        $image = FileUpload::uploadFile($request, 'image', 'uploads/service');
        if($image != ""){
            $data['image'] = $image; 
        }
        $feature_image = FileUpload::uploadFile($request, 'feature_image', 'uploads/service/feature');
        if($feature_image != ""){
            $data['feature_image'] = $feature_image; 
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function showEditForm($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.editForm', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $now        = date('Y-m-d H:i:s');
        $id = $request->input('id');
        Validator::make(
        				$request->all(), 
			        	[
                            
                            'en_title' => 'required|min:1|max:550',
                            'kh_title' => 'required|min:1|max:550',
                            'cn_title' => 'required|min:1|max:550',
                             'image' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(1400)->maxHeight(595),
                            ],
                            'feature_image' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->minWidth(10)->minHeight(10)->maxWidth(600)->maxHeight(600),
                            ]
                        ],
                        [
                            
                        ])->validate();

		
		$data = array(
                    'en_title' =>   $request->input('en_title'),
                    'kh_title' =>   $request->input('kh_title'),
                    'cn_title' =>   $request->input('cn_title'),
                    'en_description' =>   $request->input('en_description'),
                    'kh_description' =>   $request->input('kh_description'),
                    'cn_description' =>   $request->input('cn_description'),
                    'en_content' =>   $request->input('en_content'),
                    'kh_content' =>   $request->input('kh_content'),
                    'slug'      =>   FunctionController::generateSlug('news', $request->input('en_title')),
                    'cn_content' =>   $request->input('cn_content'),
                    'updated_at' => $now
                );
        $image = FileUpload::uploadFile($request, 'image', 'uploads/service');
        if($image != ""){
            $data['image'] = $image; 
        }
        $feature_image = FileUpload::uploadFile($request, 'feature_image', 'uploads/service/feature');
        if($feature_image != ""){
            $data['feature_image'] = $feature_image; 
        }
        if($request->input('active')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

    public function trash($id){
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Data has been deleted'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Status has been updated.'
      ]);
    }
   
}
