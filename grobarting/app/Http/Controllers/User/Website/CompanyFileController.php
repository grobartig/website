<?php

namespace App\Http\Controllers\User\Website;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;

use App\Model\Setup\File as Model;



class CompanyFileController extends Controller
{
    
    function __construct (){
       $this->route = "user.website.company-file";
    }

    public function listData($page = ""){   
      $data = Model::where('page', $page)->get();
      if(!empty($data)){
        return view($this->route.'.list', ['route'=>$this->route,'page' => $page, 'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
    public function showEditForm($slug = ""){   
      $data = Model::where('slug', $slug)->first();
      if(!empty($data)){
        return view($this->route.'.editForm', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
    
    public function update(Request $request){
        
      $id = $request->input('id');
      $slug = $request->input('slug');


      $validate = array(
                      
                  );
      $data = array(
                  'name' =>   $request->input('name')
              );

      
      //print_r($validate); die;
      Validator::make($request->all(), $validate)->validate();
  
      $file = FileUpload::uploadFile($request, 'file', 'uploads/file/');
        if($file != ""){
            $data['file'] = $file; 
        }

      Model::where('id', $id)->update($data);
      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back();

    }

   
    
}
