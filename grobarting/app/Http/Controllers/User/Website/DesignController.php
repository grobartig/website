<?php

namespace App\Http\Controllers\User\Website;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Website\Design as Model;

class DesignController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "user.website.design";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function listData(){
        $data = Model::orderBy('id', 'DESC')->get();
        return view($this->route.'.list', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function showCreateForm(){
        return view($this->route.'.createForm', ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'title' =>   $request->input('title'),
                    'url' =>   $request->input('url'),
                    'created_at' => $now, 
                    'updated_at' => $now
                );
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                            'title' => 'required|min:1|max:500',
                             'image' => [
                                            'required',
                                            'mimes:jpeg,png',
                                             Rule::dimensions()->width(1663)->height(525),
                            ]
                        ])->validate();

         if($request->input('active')=="")
        {
            $data['is_featured']=0;
        }else{
            $data['is_featured']=1;
        }
        $image = FileUpload::uploadFile($request, 'image', 'uploads/property/design');
        if($image != ""){
            $data['image'] = $image; 
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function showEditForm($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.editForm', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        Validator::make(
        				$request->all(), 
			        	[
                            
                            'title' => 'required|min:1|max:500',
                             'image' => [
                                            'sometimes',
                                            'required',
                                            'mimes:jpeg,png',
                                            Rule::dimensions()->width(1663)->height(525),
                            ]
                        ],
                        [
                            
                        ])->validate();

		
		$data = array(
                    'title' =>   $request->input('title'),
                    'url' =>   $request->input('url')
                );
        $image = FileUpload::uploadFile($request, 'image', 'uploads/property/design');
        if($image != ""){
            $data['image'] = $image; 
        }

        if($request->input('active')=="")
        {
            $data['is_featured']=0;
        }else{
            $data['is_featured']=1;
        }

        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

    public function trash($id){
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'User has been deleted'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_featured' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Slide Desin status has been updated.'
      ]);
    }
   
}
