<?php

namespace App\Http\Controllers\User\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setup\Province as Province;
use App\Model\User\User as User;
use App\Model\Property\Property as Property;
use App\Model\Setup\Action as Action;
use App\Model\Setup\Type as Type;
class MainController extends Controller
{
   public function __construct(){
        $this->route = "user.dashboard";
    }
    public function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }


    public function index(){
        $provinces  = Province::select('*')->withCount('properties as num_of_properties')->get();
        $users      = User::select('*')->withCount('staffProperties as num_of_properties')->where('visible', 1)->get();
        $properties = Property::select('*')->get();
        $properties_sell = Property::select('*')->where('action_id', 1)->get();
        $properties_rent = Property::select('*')->where('action_id', 2)->get();
        $properties_rent_sell = Property::select('*')->where('action_id', 3)->get();
        $actions     = Action::select('id', 'en_name as name')->get();
        $types     = Type::select('id', 'en_name as name')->withCount('properties as num_of_properties')->get();

        //print_r($types);

        if(!empty($provinces && $users)){
            return view($this->route.'.index', ['route'=>$this->route, 'provinces'=>$provinces,'users'=>$users,'properties'=>$properties,'properties_sell'=>$properties_sell,'properties_rent'=>$properties_rent,'properties_rent_sell'=>$properties_rent_sell,'actions'=>$actions, 'types'=>$types]);
        }else{
            return redirect(route('404',$locale));
        }
       
    }
   
}
