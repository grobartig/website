<?php
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
    use App\Model\User\User as Staff;
    use App\Model\Setup\Role as Role;
    use App\Model\Setup\Access as Access;
    use App\Model\Property\Property as Property;
	function checkPermision($route)
	{
        if(Auth::user()->position_id == 1){
    	   return True;
        }else{
            $permision = DB::table('permisions')->select('id')->where('route', $route)->first();
            if(count($permision) == 1){
                $permision_id = $permision->id;
                $user_id = Auth::id();
                $credentail = DB::table('users_permisions')->select('id')->where(['user_id'=>$user_id, 'permision_id'=>$permision_id])->first();
                if(count($credentail) == 1){
                    return True;
                }else{
                    return False;
                }
            }else{
                return False;
            }
        }
	}
    function checkRole($property_id, $access){   
        if(Auth::user()->position_id == 1){
            return True;
        }else{
            //Find access_id
            $access = Access::select('id')->where('name', $access)->first();
            if(count($access) == 1){
                $access_id = $access->id;
                //Finding role_id
                $staff_id = Auth::id();
                $dataRole = DB::table('properties_staffs_roles')->select('role_id', 'is_primary')->where([ 'property_id'=>$property_id ,'staff_id'=>$staff_id])->first();
                if(count($dataRole) == 1){
                    $is_primary = $dataRole->is_primary; 
                    if($is_primary != 1){
                        $role_id = $dataRole->role_id;
                        $roleAccess = Role::find($role_id)->roleAccesses()->select('access_id')->get();
                        foreach($roleAccess as $row){
                            if($row->access_id == $access_id){
                                return True;
                            }
                        }
                        return False;
                    }else{
                        return True;
                    }
                }else{
                    return False;
                }
            }else{
                return False;
            }
        }
    }

    function checkIfHasRole($property_id){   
        if(Auth::user()->position_id == 1){
            return True;
        }else{
            $staff_id = Auth::id();
            $dataRole = DB::table('properties_staffs_roles')->select('role_id', 'is_primary')->where([ 'property_id'=>$property_id ,'staff_id'=>$staff_id])->first();
            if(count($dataRole) == 1){
                return True;
            }else{
                return False;
            }
        }
    }

    function makeListingCode($property=array()){
        if(!empty($property)){
            
            $l = '';
            $listing_code = '';
            if($property->is_published == 0){
               $l = 'L'; 
           }
            $listing_code = $l.$property->province->abbre.'-'.$property->type->abbre.$property->listing_code;
            return $listing_code;
           
        }else{
            return "Invalide Listing Code";
        }
    }
    function makeListingCodeByID($id){
        if($id!=0){
            $property = Property::find($id);
            //dd($property);
            return makeListingCode($property);
        }else{
            return "Invalide Listing Code";
        }
    }
    function staffInCharge($property=array()){
        if(!empty($property)){
            
            $data = $property->propertyStaffs()->where('is_primary', 1)->first();
            //dd($data);
            return $data;

        }else{
            return [];
        }
    }
    function displayMoney($value){
        
        if(is_null($value)){
            return '$ 0';
        }else{
            return '$ ' .$value;
        }
                 
    }
    function getContent($slug,$locale){
      $data = DB::table('contents')->select($locale.'_content as content')->where('slug', $slug)->first();
      if(count($data) == 1){
        return $data->content;
      }else{
        return "";
      }
      
    }
    function getFile($slug){
      $data = DB::table('files')->where('slug', $slug)->first();
      if(count($data) == 1){
        return $data->file;
      }else{
        return "";
      }
      
    }

    function getImage($slug,$locale){
      $data = DB::table('contents')->select('image')->where('slug', $slug)->first();
      if(count($data) == 1){
        return $data->image;
      }else{
        return "";
      }
      
    }
?>