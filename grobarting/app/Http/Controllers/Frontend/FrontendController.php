<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

// Get Location
use App\Model\Setup\Province as Province;
use App\Model\Setup\Distrit as Distrit;
use App\Model\Setup\Commune as Commune;

// Model Property
use App\Model\Property\Property as Model;
use App\Model\Setup\Action as Action;
use App\Model\Setup\Type as Type;

use App\Model\Url as Url;
use App\Model\Contacts as Contacts;

class FrontendController extends Controller
{
    
    public $defaultData = array();
    public function __construct(){
      
    }

    public function defaultData($locale= "en"){
        App::setLocale($locale);
        //Current Language
        $parameters                                 = Route::getCurrentRoute()->parameters();
        $enRouteParamenters                         = $parameters;
        $enRouteParamenters['locale']               = 'en';
        $this->defaultData['enRouteParamenters']    = $enRouteParamenters;
        $khRouteParamenters                         = $parameters;
        $khRouteParamenters['locale']               = 'kh';
        $this->defaultData['khRouteParamenters']    = $khRouteParamenters;
        $cnRouteParamenters                         = $parameters;
        $cnRouteParamenters['locale']               = 'cn';
        $this->defaultData['cnRouteParamenters']    = $cnRouteParamenters;
        $this->defaultData['routeName']             = Route::currentRouteName();

        $this->defaultData['provinces']             = Province::select('id', $locale.'_name as name')->where('status', 1)->get();
        $this->defaultData['types']                 = Type::select('id',$locale.'_name as name','en_name')->get();
        $this->defaultData['actions']               = Action::select('id', $locale.'_name as name')->get();
        $this->defaultData['url']                   = Url::select('id','facebook','twitter','instagram','linkedin')->first();
        $this->defaultData['contacts']              = Contacts::select('id','address','phone','email')->first();
       
        return $this->defaultData;
    }
    

    public function getDistricts($locale = "en"){
        $province_id = isset($_GET['province_id'])?$_GET['province_id']:'';
        if(  is_numeric ($province_id) ){
            if( $province_id != 0 ){
                $data = District::select('id', $locale.'_name as name', 'lat', 'lng')->where(['province_id'=>$province_id, 'status'=>1])->get();
                return response()->json($data);
            }else{
                return response()->json('no data');
            }
        }else{
            echo "Not vaide data: ";
        }
       
    }

    public function getCommunes($locale = "en"){
        $district_id = isset($_GET['district_id'])?$_GET['district_id']:'';
        if(  is_numeric ($district_id) ){
            if( $district_id != 0 ){
                $data = Commune::select('id', $locale.'_name as name', 'lat', 'lng')->where(['district_id'=>$district_id, 'status'=>1])->get();
                return response()->json($data);
            }else{
                echo "Not vaide data";
            }
        }else{
            echo "Not vaide data: ";
        }
       
    }

 }
