<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Property\Property as Property;


class PropertyDetailController extends FrontendController
{
    //

  
    public  function index($locale = "en"){
			   $defaultData        = $this->defaultData($locale);
			   $properties         =   Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug')->where('is_published', 1)->with(['action' => function($query, $locale="en") {
				$query->select('id', $locale.'_name as name'); }])->orderBy('id','DESC')->limit(6)->get(); 
			   $feature_properties     =  Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug')->where('is_featured',1)->with(['action' => function($query, $locale="en") {
				$query->select('id', $locale.'_name as name'); }])->limit(12)->get(); 
		return view('frontend.property.property-detail', ['locale'=>$locale,'defaultData'=>$defaultData,'properties'=>$properties,'feature_properties'=>$feature_properties]);
	}
	
}
