<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Property\Property as Property;
use App\Model\Property\PropertyEnquiry as PropertyEnquiry;
use App\Model\Setup\Customer as Customer;
use App\Model\Setup\Province as Province;
use App\Model\Setup\Type as Type;
use App\Model\Setup\District as District;
use App\Model\Setup\Commune as Commune;
use App\Model\Setup\Message as Message;
use App\Model\Setup\SellingPriceType as SellingPriceType;
use App\Model\Setup\RentalPriceType as RentalPriceType;

class PropertyController extends FrontendController
{
    //

  
    public  function index($locale = "en"){
    	
    	$data   =   Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug','created_at')->where('is_published', 1)->with(['action' => function($query, $locale="en") {
                            $query->select('id', $locale.'_name as name'); }])->orderBy('id','DESC')->paginate(9); 
        $defaultData = $this->defaultData($locale);
        $msg = '';
		return view('frontend.property.property', ['locale'=>$locale,'data'=>$data,'defaultData'=>$defaultData, 'msg'=> $msg]);
	}

	 public function detail($locale = "en",$slug = "") {
        $defaultData = $this->defaultData($locale);
        $feature_properties     =  Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug')->where('is_featured',1)->with(['action' => function($query, $locale="en") {
            $query->select('id', $locale.'_name as name'); }])->limit(4)->get(); 
        $related_properties    = Property::select('id',$locale.'_name as name',$locale.'_excerpt as description','action_id', 'slug','created_at')
                                ->where('is_featured',1)->with(['action' => function($query, $locale="en") {
                                $query->select('id', $locale.'_name as name'); }])->orderBy('id','DESC')->limit(2)->get();

        $data    =  Property::select('id',$locale.'_name as name',$locale.'_excerpt as excerpt',$locale.'_description as description','slug','action_id','type_id','province_id','district_id','commune_id')
                    ->with(['action' => function($query, $locale="en") {
                            $query->select('id', $locale.'_name as name'); }])
                    ->where('slug',$slug)->first();
       
        if(sizeof($data) > 0){
            $dataFeature = $data->propertyFeatures()->select('feature_id')->get();
            $features = Type::find($data->type_id)->features()->select('features.id', 'features.'.$locale.'_name as name')->get(); 
        
            $property_staffs = Property::find($data->id)->propertyStaffs()->orderBy('is_primary', 'DESC')->get();
            $show_details = Property::find($data->id)->propertyDetails()->orderBy('data_order', 'ASC')->where('is_showed',1)->get();
            $details = Property::find($data->id)->propertyDetails()->orderBy('data_order', 'ASC')->get();
            return view('frontend.property.property-detail', ['locale'=>$locale,'dataFeature'=>$dataFeature,'features'=>$features,'defaultData'=>$defaultData,'data'=>$data,'property_staffs'=>$property_staffs,'related_properties'=>$related_properties,'show_details'=>$show_details,'details'=>$details,'feature_properties'=>$feature_properties]);
            }else{
                return redirect(route('404',$locale));
            }
    }
        public function search($locale = "en", Request $request) {
        $amount = isset($_GET['amount'])?$_GET['amount']:"";
        $amount = explode( ',', $amount );
        // return $amount;
        $defaultData = $this->defaultData($locale);
        $data    =  Property::select('id',$locale.'_name as name','created_at',$locale.'_excerpt as description','slug','action_id','type_id','province_id','district_id','commune_id')->with(['action' => function($query, $locale="en") {
                            $query->select('id', $locale.'_name as name'); }]);
        

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $action     =   intval(isset($_GET['action'])?$_GET['action']:0); 
        $type       =   intval(isset($_GET['type'])?$_GET['type']:0); 
        //$price_type       =   intval(isset($_GET['price-type'])?$_GET['price-type']:0); 
        $province   =   intval(isset($_GET['province'])?$_GET['province']:0); 
        $district    =   intval(isset($_GET['district'])?$_GET['district']:0); 
        $commune    =   intval(isset($_GET['commune'])?$_GET['commune']:0); 
        
        $key       =   isset($_GET['key'])?$_GET['key']:"";

        $min        =   intval(isset($_GET['min'])?$_GET['min']:"");

        if($limit <= 0 || $limit > 100){
            $limit = 10;
        }
        $appends=array('limit'=>$limit);
        
        if( $key != "" ){
            $data = $data->where($locale.'_name', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
        if( $action > 0 ){
            $data = $data->where('action_id', $action);
             $appends['action'] = $action;
        }

        if( $type > 0 ){
            $data = $data->where('type_id', $type);
            $appends['type'] = $type;
        }

        if( $province > 0 ){
            $data = $data->where('province_id', $province);
            $appends['province'] = $province;
            if( $district > 0){
                $data = $data->where('district_id', $district);
                $appends['district'] = $district;
                if( $commune > 0 ){
                    $data = $data->where('commune_id', $commune);
                    $appends['commune'] = $commune;
                }
            }
        }
        //=====================>>> Amount
        if( $amount ){
            $appends['amount'] = $amount;
            if( $action == 1 ){
                $data = $data->whereHas('price', function ($query) use($amount) {
                    $query->whereBetween('selling_price', $amount);
                });
            } else if( $action == 2 ){
                $data = $data->whereHas('price', function ($query) use($amount) {
                    $query->whereBetween('rental_price', $amount);
                });
            }else{
                $data = $data->whereHas('price', function ($query) use($amount) {
                    $query->whereBetween('selling_price', $amount);
                });
            }
        }
        $data= $data->where(array('is_published'=> 1))->orderBy('created_at', 'DESC')->paginate($limit);
        $msg = '';
        // if($request){
        //     $msg = 'You have searched property by input : '. $key . ' in Tek thla Sen Sok , Phnom Penh By choise action as Apartment in purpose to sell and price between 1000 - 2000 $';
        // }

        return view('frontend.property.property', ['locale'=>$locale,'defaultData'=>$defaultData,'data'=>$data,'appends'=>$appends, 'msg'=> $msg]);
    }

    public function districts($locale = "en"){

        $province_id = isset($_GET['province_id'])?$_GET['province_id']:'';
        if(  is_numeric ($province_id) ){
            if( $province_id != 0 ){
                $data = District::select('id', $locale.'_name as name', 'lat', 'lng')->where(['province_id'=>$province_id])->get();
                return view('frontend.search.district-data', ['data'=>$data,'locale'=>$locale]);
            }else{
                echo "Not vaide data";
            }
        }else{
            echo "Not vaide data ";
        }
       
    }
    public function communes($locale = "en"){
        $district_id = isset($_GET['district_id'])?$_GET['district_id']:'';
        if(  is_numeric ($district_id) ){
            if( $district_id != 0 ){
                $data = Commune::select('id', $locale.'_name as name', 'lat', 'lng')->where(['district_id'=>$district_id])->get();
                return view('frontend.search.commune-data', ['data'=>$data,'locale'=>$locale]);
            }else{
                echo "Not vaide data";
            }
        }else{
            echo "Not vaide data ";
        }
       
    }

    

}