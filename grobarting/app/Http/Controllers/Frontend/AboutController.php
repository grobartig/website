<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\About as About;
use App\Model\Abouts as Abouts;

use App\Model\Welcome as Welcome;


class AboutController extends FrontendController
{
    //

  
    public  function index($locale = "en"){

		  $defaultData        = $this->defaultData($locale); 
		  $welcome= welcome::select('id',$locale.'_name as name',$locale.'_description as description','image')->first();
		  $about= about::select('id',$locale.'_name as name',$locale.'_title as title',$locale.'_description as description','image')->get();
		  $abouts= abouts::select('id',$locale.'_name as name',$locale.'_title as title',$locale.'_description as description','image')->get();
		  
		return view('frontend.about', ['locale'=>$locale,'defaultData'=>$defaultData,'about'=>$about,'abouts'=>$abouts,'welcome'=>$welcome,]);
	}
}