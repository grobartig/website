<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Property\Property as Property;
use App\Model\Website\Team as Team;
use App\Model\User\User as User;
use App\Model\Website\Content as Content;

class OurTeamController extends FrontendController
{
    //
		
  
    public  function index($locale = "en"){
		  $teams 		 =   Team::select('id',$locale.'_title as title',$locale.'_position as position','phone','email','image')->get();
        $content     =   Content::select('id',$locale.'_content as content','image','email','phone')->where('slug','message-from-md')->first();
        $defaultData = $this->defaultData($locale);
		return view('frontend.our-team',['locale'=>$locale,'defaultData'=>$defaultData,'content'=>$content,'teams'=>$teams]);
	}
}