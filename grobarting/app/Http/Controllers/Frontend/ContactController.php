<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Mail;

use App\Model\Message;
use Session;
use Validator;

class ContactController extends FrontendController
{
   
    public  function index($locale = "en"){
		  $defaultData        = $this->defaultData($locale); 
		return view('frontend.contact', ['locale'=>$locale,'defaultData'=>$defaultData,]);
	}
	public function sendMessage(Request $request){
        
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'name'              =>      $request->input('name'),
            'email'             =>      $request->input('email'),
            'message'           =>      $request->input('message'),
            'created_at'        =>      $now
        );
        
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required',
                'message' => 'required',
                'email' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect()->back();
    }
}