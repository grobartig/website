<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;



class OurServiceController extends FrontendController
{
    

  
    public  function index($locale = "en"){
		  
		     $defaultData        = $this->defaultData($locale);
		return view('frontend.our-service', ['locale'=>$locale,'defaultData'=>$defaultData,]);
	}

}
