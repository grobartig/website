<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;



use App\Model\Website\News as Model;

class NewController extends FrontendController
{
    
    public function index($locale = "en") {
        $defaultData = $this->defaultData($locale);
        $data = Model::select('id',$locale.'_title as title',$locale.'_description as description','created_at','slug','image')->orderBy('created_at','DESC')->paginate(4);
        
        return view('frontend.new', ['locale'=>$locale,'defaultData'=>$defaultData,'data'=>$data]);
    }
    public function detail($locale = "en", $slug="") {
        $defaultData = $this->defaultData($locale);
         $data = Model::select('id',$locale.'_title as title',$locale.'_content as content','created_at','image','slug',$locale.'_description as description')->where('slug',$slug)->first();
        if(sizeof($data) > 0){
           
           
            $relate_posts = Model::select('id',$locale.'_title as title',$locale.'_description as description','created_at','slug','image')->orderBy('id','DESC')->limit(2)->get();
           
            return view('frontend.news-detail', ['locale'=>$locale,'defaultData'=>$defaultData,'data'=>$data,'relate_posts'=>$relate_posts]);
        }else{
            return redirect(route('404',$locale));
        }
    }
}
