<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
class NotFoundController extends FrontendController
{
    public  function index($locale = "en"){
        $defaultData = $this->defaultData($locale);
		return view('frontend.404', ['locale'=>$locale,'defaultData'=>$defaultData]);
	}
}