@extends('frontend.layouts.master') 
@section('title', 'Welcome to Grobartig')
@section('active-property-detail', 'current')

@section ('appbottomjs')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqb3fT3SbMSDMggMEK7fJOIkvamccLrjA&sensor=false"></script>
<script src="{{ asset('public/frontend/js/map-single.js' ) }}"></script>
<!-- google maps -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyA8S3pD12gYeCiM2vUf5MuObhznkGbWNCk"></script>
    <script type="text/JavaScript">
        $(document).ready(function(){
            @if($data->location->is_actual_map == 1)
                @if($data->location->lat != 0 && $data->location->lng !=0 )
                    makeMap({{$data->location->lat}}, {{$data->location->lng}}, 15, '{{asset('public/frontend/images/maker-icon.png')}}');
                @else 
                    makeMap(11.437886, 103.910652, 15, '{{asset('public/frontend/images/maker-icon.png')}}');// Map of phnom penh
                @endif
            @else 
                @if($data->location->lat != 0 && $data->location->lng !=0 )
                    makeMap({{$data->location->lat}}, {{$data->location->lng}}, 15, '{{asset('public/frontend/images/surrounded.png')}}');
                @else 
                    makeMap(11.437886, 103.910652, 15, '{{asset('public/frontend/images/surrounded.png')}}');// Map of phnom penh
                @endif
            @endif
        })
        //var marker ="";
        function makeMap(lat, lng, zoom = 10, image){
            var latlng = new google.maps.LatLng(lat, lng);
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: zoom,
                center: latlng,
                gestureHandling: 'none',
                zoomControl: false,
                icon:image,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: '',
                icon:image,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function (event) {
                $("#lat").val(this.getPosition().lat());
                $("#lng").val(this.getPosition().lng());
            });
        }

        function enLargeMap(){

        }
</script>
@endsection 

@section ('content')
<!-- =========================slide -->
@include('frontend.layouts.banner')
<!-- =====================slide-end -->
        <!--Feature property section-->
        <div class="feature-property properties-list pt-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12 ">
                        <div class="single-property-details">

                            <div class="single-property-area">
                                
                                <div class="single-property-tab-content tab-content">
                                    @php($photos = $data->photos) 
                                    @php($i = 0)
                                    @foreach($photos as $row)
                                    <div class="tab-pane @if($i++ == 1) active @endif" id="image-{{$row->id}}">
                                        <img src="{{ asset('public/uploads/property/photo/750x487/'.$row->photo ) }}" alt="">
                                    </div>
                                    @endforeach
                                   
                                </div>
                                <div class="single-property-tab-slider indicator-style2 mt-20">
                                    @php($photos = $data->photos) 
                                    @foreach($photos as $row)
                                    <div class="item">
                                        <a href="#image-{{$row->id}}" data-toggle="tab">
                                            <img src="{{ asset('public/uploads/property/photo/750x487/'.$row->photo ) }}" alt="" />
                                        </a>
                                    </div>
                                    @endforeach
                                   
                                </div>
                            </div>

                            <div class="pt-50">
                                <div class="find-home-title">
                                    <h3 class="price-title"> 
                                        @if($data->action_id == 1 ) 
                                        {{__('web.for-sale')}} :
                                        @elseif( $data->action_id == 2 )  
                                            {{__('web.for-rent')}} : 
                                        @else 
                                            {{__('web.rent-sale')}} : 
                                        @endif

                                        @if($data->action_id == 1 ) {{displayMoney($data->price->selling_price)}}  @if(!empty($data->price->selling_price_type->name)) {{ $data->price->selling_price_type->name}} @endif
                                        @elseif( $data->action_id == 2 ) {{displayMoney($data->price->rental_price)}}  @if(!empty($data->price->rental_price_type->name)) {{ $data->price->rental_price_type->name}}  @endif
                                        @else Sell: {{displayMoney($data->price->selling_price)}}
                                        & Rent: {{displayMoney($data->price->rental_price)}} @endif
                                    </h3>
                                </div>
                                <div class="description-inner">
                                <p  style="font-size:18px;"><i class="fa fa-map-marker"></i> @if($data->location) {{$data->location->road}} {{$data->location->address}} @endif </p>
                                </div>
                            </div>

                            <div class="">
                                <div class="aside-title">
                                    <h5>{{$data->name ?? ''}}</h5>
                                </div>
                                <div class="description-inner">
                                    <p class="text-1"> 
                                        {!! $data->description ?? 'N/A' !!}
                                    </p>
                                </div>
                            </div>

                            <div class="pt-50">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="property-condition">
                                            <div class="aside-title">
                                                <h5> Additional Details </h5>
                                            </div>
                                            <div class="property-condition-list">
                                                <ul>
                                                    @foreach($details as $row)
                                                    @if(!empty($row->value)) 
                                                        @php($name = $row->detail()->select($locale.'_name as name')->first() )
                                                        <li>
                                                            <img width="20px" src="{{ asset($row->detail->icon)}}" alt="">
                                                            <span>  <b style="font-weight: bold;">{{$name->name}} :</b>  @if(!empty($row->value)) {{$row->value}}  @if(!empty($row->detail->unit)) {{ $row->detail->unit }} @endif @endif </span>
                                                        </li>
                                                        @endif
                                                    @endforeach
                                                    
                                                </ul>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pt-50">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="amenities">
                                            <div class="aside-title">
                                                <h5>Amenities</h5>
                                            </div>
                                            <div>
                                                <div class="amenities-list">
                                                    <ul>
                                                        @if(sizeof($features) > 1) 
                                                            @foreach($features as $feature)
                                                                @foreach($dataFeature as $row)
                                                                    @if($row->feature_id == $feature->id)
                                                                    <li><i class="fa fa-check-square-o"></i> <span> {{ $feature->name }} </span></li>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endif
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pt-50">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="property-condition">
                                            <div class="aside-title">
                                                <h5>Location</h5>
                                            </div>
                                            <div class="property-list-map">
                                                <div id="map" style="width:100%;height:430px !important;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(count($property_staffs)>0)
                            <div class="pt-50">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="property-condition">
                                            <div class="aside-title">
                                                <h5>Agent</h5>
                                            </div>
                                            @foreach($property_staffs as $row)
                                                <div class="single-feedback mb-35 fix">
                                                    <div class="feedback-img">
                                                        <img src="{{ asset($row->staff->picture ) }}" alt="">
                                                    </div>
                                                    <div class="feedback-desc">
                                                        @php($staff_name = $row->staff()->select($locale.'_name as name')->first())
                                                        @php($staff_position = $row->staff()->select($locale.'_position as position')->first())
                                                        <div class="feedback-title">
                                                            <h6>{{$staff_name->name}}</h6>
                                                        </div>
                                                        <p class="feedback-post">
                                                            
                                                            <i class="fa fa-tag icon"></i> {{$staff_position->position}} <i class="fa fa-envelope icon"></i> {{$row->staff->email}} <i class="fa fa-phone icon"></i> {{$row->staff->personal_phone}}
                                                        </p>
                                                    
                                                    </div>
                                                </div>
                                            @endforeach
                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                  

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="sidebar right-side">
                          
                            <aside class="single-side-box feature">
                                <div class="aside-title">
                                    <h5>{{__('general.feature-properties')}}</h5>
                                </div>
                                <div class="feature-property">
                                    <div class="row">
                                    @foreach($feature_properties as $row)       
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="single-property">
                                                <div class="property-img">
                                                    <a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">
                                                        <img src="{{ asset('public/uploads/property/feature/property/'.$row->media->feature) }}" alt="">
                                                    </a>
                                                </div>
                                                <div class="property-desc text-center">
                                                    <div class="property-desc-top">
                                                        <h6 sytle="font-weight: 400;line-height: 25px !importen; margin-bottom: 6px;"><a href="{{ route('property-detail', ['locale'=>$locale, 'slug'=>$row->slug]) }}">{{$row->name}}</a></h6>
                                                        <h4 class="price">{{displayMoney($row->price->selling_price)}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Feature property section end-->      
@endsection