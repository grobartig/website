
<div class="find-home-item custom-select" >   
  <select class="search-select" title="{{__('general.destrict')}}" data-hide-disabled="true" data-live-search="true" id="district" name="district">
      <optgroup label="">
        <option value="0">{{__('general.district')}}</option>
        @foreach( $data as $row)
        <option value="{{$row->id}}">{{$row->name}}</option>
        @endforeach
      </optgroup>
  </select>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#district').change(function(){
        getCommunes($(this).val());
      })

  })
function getCommunes(district_id){

    if(district_id != 0){
      $.ajax({
              url: "{{ route('communes',$locale) }}?district_id="+district_id,
              type: 'GET',
              data: {},
              success: function( response ) {
                $("#commune-cnt").html(response)
              },
              error: function( response ) {
                 swal("Error!", "Sorry there is an error happens. " ,"error");
              }
            
      });
    }
  }
</script>
