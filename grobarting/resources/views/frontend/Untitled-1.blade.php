@extends('frontend/layouts.master')

@section('title', 'Rakcenter ')
@section('home', '')

@section('active-contact-us', 'active-menu')

@section ('appbottomjs')

<!--Google Map Active-->
<script>
    function myMap() {
    var mapProp= {
        center: new google.maps.LatLng(11.5441376,104.9173304),
        zoom: 16,
        scrollwheel: false,
        styles: 
            [
                {
                    "featureType": "administrative.country",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                }
            ]
        
    };
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
    var marker = new google.maps.Marker({
    position: map.getCenter(),
    animation:google.maps,
    animation:google.maps.Animation.BOUNCE,
    icon: '{{asset('public/frontend/img/map-icon.png')}}',
    map: map
    });
    var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<div id="bodyContent">'+
    '<p>Rak Center, </br>N 59. CD, Mao Tse Toung Blvd. (Corner Road 105), Sangkat Beoung Keng Kaoung 3 Khan Chamkmorn  Phnom Penh</p>'+       
    '</div>'+
    '</div>';
    var infowindow = new google.maps.InfoWindow({
    content: contentString,
    });
    infowindow.open(map, marker);
    }
    </script>
	<!--Google Map-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8&amp;callback=myMap"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  <script type="text/javascript">
    $(document).ready(function(){
      $("#submit-contact").submit(function(event){
        submit(event);
      })

      @if (Session::has('invalidData'))
        $("#form").show();
      @endif

    })
	@if(Session::has('msg'))
			toastr.success("{{ Session::get('msg') }}");
	@endif



    function submit(event){
      
      first_name = $("#first_name").val();
      last_name = $("#last_name").val();
	  subject = $("#subject").val();
      email = $("#email").val();
      message = $("#message").val();
      recaptcha =$('#g-recaptcha-response').val();

      if(first_name != 0){
          if(last_name ){
			
                  if(validateEmail(email)){

					if(subject ){
					
                     	if(recaptcha != ""){
							
							@if (Session::has('msg'))
							toastr.error("Please check robot verification");
							@endif
						}else{
							toastr.error("Please check robot verification");
							event.preventDefault();
							$("#g-recaptcha-response").focus();
						}
					}else{
						toastr.error("Please Select Subject");
						event.preventDefault();
						$("#subject").focus();
					}
                  }else{
                      toastr.error("Please give us correct email");
                      event.preventDefault();
                      $("#email").focus();
                  }
                }else{
                    toastr.error("Please Input Your Last Name.");
                    event.preventDefault();
                    $("#last_name").focus();
                }

                       
             
          }else{
              toastr.error("Please enter your first name");
              event.preventDefault();
              $("#first_name").focus();
          }
    }
    
    function showApplicationForm(){
      form = $("#form");
      if(form.is(":visible")){
        form.hide();
      }else{
        form.show();
      }
    }

    function validateEmail(email) { 
          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
      }
      function validatePhone(phone) {
          return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
      }

  </script>

@endsection

@section('css-section')
	<style>
		.active-porcelain{
			background: #9e821c;
		}
		.active-item{
			color: #9e821c !important;
		}
		.text-line{
			line-height: 2 !important;
		}
	</style>
@endsection

@section ('content')
	<div class="page-banner2-area">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="">
						<h2><strong style="text-transform:uppercase;">{{__('general.contact-us')}}</strong></h2>
					</div>
					<div class="breadcrumb-content">
						<ul>
							<li><a href="{{route('home',$locale)}}">{{__('general.home')}}</a></li>
							<li class="active"><a href="{{route('contact-us', ['locale'=>$locale])}}">{{__('general.contact-us')}}</a></li>
						</ul>
						<div class="description mt-2">
						<h5 class="text-line">{!! getProductContent($locale, 2)->content ?? '' !!}</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="contact-area">
            <div class="container">
            <div class="row pt-5">
                    <div class="col-lg-6">
                    <h2 class="pb-4"><strong>{{__('general.contact-us')}}</strong></h2>
                        <div class="anadi-address">
                            <ul>
                                <li><i class="fa fa-fax"></i> Address : N 59. CD, Mao Tse Toung Blvd. (Corner Road 105), Sangkat Beoung Keng Kaoung 3 Khan Chamkmorn  Phnom Penh </li>
                                <li><i class="fa fa-phone"></i>(855)-23 211 473/12 245 678/12 381 166</li>
                                <li><i class="fa fa-envelope-o"></i>info@rakcenters.com </li>
                                <li><i class="fa fa-globe"></i>www.rakcenters.com</li>
                            </ul>
                        </div>
                    <h2><strong>{{__('general.follow-us')}}</strong></h2>
                        <ul class="social-icons">
                            @foreach ( $defaultData['links'] as $row)
                            <li><a href="{{ $row->url }}"> <img src="{{ asset( $row->icon) }}" style="width: 19px;"></a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-6">
                    <h2 class="pb-4"><strong>{{__('general.size-map')}}</strong></h2>
                    <div class="google-map-area">
                        <div id="googleMap"></div>
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-12 col-lg-12">
                        <img src="{{ asset('public/frontend/img/contactus.png') }}" class="w-100">
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-12 col-lg-12">
                        <div class="section-title text-center">
                            <h3>{{__('general.subcribe')}}</h3>
							<h4>{{__('general.keep')}}</h4>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="contact-ms contact-form-wrap">
                            @if(Session::has('msg'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('msg') }}
                            </div>
                            @endif
                            @if (count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                    Sending fail! Please Try again!
                                </div>
                            @endif
                            

                            @if (count($errors) > 0)
                                
                                @foreach ($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
                                <form id="submit-contact" action="{{ route('sendmessage', $locale) }}"  method="post">  
                                    {{ csrf_field() }} 
                                        {{ method_field('PUT') }}
                                    <div class="row">
                                    <div class="col-md-6">
                                            <div class="contact-form-style mb-20">
                                                <input name="first_name" id="first_name" placeholder="{{__('general.fistname')}}" type="text">

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="contact-form-style mb-20">

                                                <input name="last_name" id="last_name" placeholder="{{__('general.lastname')}}" type="text">

                                            </div>
                                        </div>

                                       
                                        
                                        <div class="col-md-6">
                                            <div class="contact-form-style mb-20">
                                                
                                                <input name="email"  id="email" placeholder="{{__('general.email')}}" type="email">

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="contact-form-style mb-20">
                                            
                                                <input name="subject"  id="subject" placeholder="{{__('general.subject')}}" type="text">

                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="contact-form-style">
                                                <textarea name="message" id="message" placeholder="{{__('general.type')}}"></textarea>
                                                <div class="g-recaptcha" data-sitekey="6Ldm8jQUAAAAABPK7FEBhKxkWZDSNNLb7ghQ2pDk"></div>
                                                <br>
                                                <button class="form-button" type="submit"><span>{{__('general.sendemail')}}</span></button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            <p class="form-messege"></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
	
@endsection