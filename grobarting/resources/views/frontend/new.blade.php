@extends('frontend.layouts.master') 
@section('title', 'Welcome to Grobartig')
@section('active-new', 'active')
@section ('content')
<!-- =========================slide -->
<!-- =====================slide-end -->
<!--Feature property section-->
<div class="breadcrumbs overlay-black" style="background-image:url('{{ asset('public/frontend/img/common/breadcrumbs.jpg') }}');no-repeat scroll center center / cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbs-inner">
                           <!--  <div class="breadcrumbs-title text-center">
                                <h1>{{__('general.news')}}</h1>
                            </div> -->
                            <div class="breadcrumbs-menu">
                                <ul>
                                    <li><a href="{{ route('home', $locale) }}">{{__('general.home')}} /</a></li>
                                    <li>{{__('general.news')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-property ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title text-center">
                            <h3>SEARCH BY PROPERTY</h3>
                            <p>Grobartig  the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                     @foreach($data as $row)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-property wow fadeInUp mb-40" data-wow-delay="0.2s" data-wow-duration="1s">
                            <span>FOR SALE</span>
                            <div class="property-img">
                                <a href="single-properties.html">
                                   <img src="{{ asset ($row->image)}}" alt="">
                                </a>
                            </div>
                            <div class="property-desc">
                                <div class="property-desc-top">
                                    <h6><a href="single-properties.html">{{$row->title}}</a></h6>
                                    <h4 class="price">{{$row->description}}</h4>
                                   
                                </div>
                                
                            </div>
                        </div>
                    </div>
                       @endforeach  
                </div>
            </div>
        </div>
        <!--Feature property section end-->
        @endsection