@extends('frontend.layouts.master') 
@section('title', 'Welcome to Grobartig')
@section('active-service', 'active')
@section ('content')
<!-- =========================slide -->
<!-- =====================slide-end -->
<!--Services section start-->
<div class="breadcrumbs overlay-black" style="background-image:url('{{ asset('public/frontend/img/common/breadcrumbs.jpg') }}');no-repeat scroll center center / cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbs-inner">
                            <!-- <div class="breadcrumbs-title text-center">
                                <h1>{{__('general.our-service')}}</h1>
                            </div> -->
                            <div class="breadcrumbs-menu">
                                <ul>
                                    <li><a href="{{ route('home', $locale) }}">{{__('general.home')}} /</a></li>
                                    <li>{{__('general.our-service')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="services-section ptb-50 bg-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title text-center">
                            <h3>{{__('general.our-service')}}</h3>
                            <p>Grobartig  the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-sm-12">
                        <div class="single-services wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">
                            <div class="single-services-img">
                               <img src="{{ asset('public/frontend/img/service/1.png') }}" alt="">
                            </div>
                            <div class="single-services-desc">
                                <h5>{{__('general.buy-property')}}</h5>
                                <p>Grobartig the best theme for  elit, sed do eiumod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-sm-12">
                        <div class="single-services wow fadeInUp" data-wow-duration="1.4s" data-wow-delay="0.2s">
                            <div class="single-services-img">
                               <img src="{{ asset('public/frontend/img/service/2.png') }}" alt="">
                            </div>
                            <div class="single-services-desc">
                                <h5>{{__('general.sale-property')}}</h5>
                                <p>Grobartig the best theme for  elit, sed do eiumod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-sm-12">
                        <div class="single-services wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.4s">
                            <div class="single-services-img">
                               <img src="{{ asset('public/frontend/img/service/3.png') }}" alt="">
                            </div>
                            <div class="single-services-desc">
                                <h5>{{__('general.rent-property')}}</h5>
                                <p>Grobartig the best theme for  elit, sed do eiumod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Services section end-->

        <!--Team area start-->
        <div class="team-area bg-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title text-center">
                            <h3>{{__('general.our-agent')}}</h3>
                            <p>Grobartig  the best theme for  elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="single-team wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">
                            <div class="team-img">
                                <a href="agent-details.html">
                                   <img src="{{ asset('public/frontend/img/team/1.jpg') }}" alt="">
                                </a>
                            </div>
                            <div class="team-desc">
                                <div class="team-member-title">
                                    <h6><a href="agent-details.html">Stephen de Smith</a></h6>
                                    <p>Real Estate Agent</p>
                                </div>
                                <div class="team-member-social">
                                    <a href="#"><i class="zmdi zmdi-phone-in-talk"></i></a>
                                    <a  href="#"><i class="fa fa-facebook"></i></a>
                                    <a  href="#"><i class="fa fa-twitter"></i></a>
                                    <a  href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="single-team wow fadeInUp" data-wow-delay="0.3s" data-wow-duration="1.3s">
                            <div class="team-img">
                                <a href="agent-details.html">
                                   <img src="{{ asset('public/frontend/img/team/2.jpg') }}" alt="">
                                </a>
                            </div>
                            <div class="team-desc">
                                <div class="team-member-title">
                                    <h6>
                                        <a href="agent-details.html">Stephen de Smith</a>
                                    </h6>
                                    <p>Real Estate Agent</p>
                                </div>
                                <div class="team-member-social">
                                    <a href="#"><i class="zmdi zmdi-phone-in-talk"></i></a>
                                    <a  href="#"><i class="fa fa-facebook"></i></a>
                                    <a  href="#"><i class="fa fa-twitter"></i></a>
                                    <a  href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="single-team wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="1.4s">
                            <div class="team-img">
                                <a href="agent-details.html">
                                   <img src="{{ asset('public/frontend/img/team/3.jpg') }}" alt="">
                                </a>
                            </div>
                            <div class="team-desc">
                                <div class="team-member-title">
                                    <h6>
                                        <a href="agent-details.html">Stephen de Smith</a>
                                    </h6>
                                    <p>Real Estate Agent</p>
                                </div>
                                <div class="team-member-social">
                                    <a href="#"><i class="zmdi zmdi-phone-in-talk"></i></a>
                                    <a  href="#"><i class="fa fa-facebook"></i></a>
                                    <a  href="#"><i class="fa fa-twitter"></i></a>
                                    <a  href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-sm col-xs-12">
                        <div class="single-team wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1.5s">
                            <div class="team-img">
                                <a href="agent-details.html">
                                   <img src="{{ asset('public/frontend/img/team/4.jpg') }}" alt="">
                                </a>
                            </div>
                            <div class="team-desc">
                                <div class="team-member-title">
                                    <h6>
                                        <a href="agent-details.html">Stephen de Smith</a>
                                    </h6>
                                    <p>Real Estate Agent</p>
                                </div>
                                <div class="team-member-social">
                                    <a href="#"><i class="zmdi zmdi-phone-in-talk"></i></a>
                                    <a  href="#"><i class="fa fa-facebook"></i></a>
                                    <a  href="#"><i class="fa fa-twitter"></i></a>
                                    <a  href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Team area end-->
        @endsection