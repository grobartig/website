@extends('frontend.layouts.master') @section('title', 'Welcome to Grobartig') @section('active-contact', 'active') @section ('content') 

@section ('appbottomjs')


<script src='https://www.google.com/recaptcha/api.js'></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <script type="text/javascript">
    $(document).ready(function(){
      $("#submit-contact").submit(function(event){
        submit(event);
      })

      @if (Session::has('invalidData'))
        $("#form").show();
      @endif

    })
	@if(Session::has('msg'))
			toastr.success("{{ Session::get('msg') }}");
	@endif



    function submit(event){
      
      first_name = $("#first_name").val();
      last_name = $("#last_name").val();
	  subject = $("#subject").val();
      email = $("#email").val();
      message = $("#message").val();
      recaptcha =$('#g-recaptcha-response').val();

      if(first_name != 0){
          if(last_name ){
			
                  if(validateEmail(email)){

					if(subject ){
					
                     	if(recaptcha != ""){
							
							@if (Session::has('msg'))
							toastr.error("Please check robot verification");
							@endif
						}else{
							toastr.error("Please check robot verification");
							event.preventDefault();
							$("#g-recaptcha-response").focus();
						}
					}else{
						toastr.error("Please Select Subject");
						event.preventDefault();
						$("#subject").focus();
					}
                  }else{
                      toastr.error("Please give us correct email");
                      event.preventDefault();
                      $("#email").focus();
                  }
                }else{
                    toastr.error("Please Input Your Last Name.");
                    event.preventDefault();
                    $("#last_name").focus();
                }
  
          }else{
              toastr.error("Please enter your first name");
              event.preventDefault();
              $("#first_name").focus();
          }
    }
    
    function showApplicationForm(){
      form = $("#form");
      if(form.is(":visible")){
        form.hide();
      }else{
        form.show();
      }
    }

    function validateEmail(email) { 
          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
      }
      function validatePhone(phone) {
          return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
      }

  </script>
@endsection
<!-- =========================slide -->
@include('frontend.layouts.banner')
<!-- =====================slide-end -->

<!--Contact page start-->
<div class="contact-page pt-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <section class="contact-area">
            <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA8S3pD12gYeCiM2vUf5MuObhznkGbWNCk
                &amp;q=?Street+K4A,+Phnom+Penh" allowfullscreen="">
            </iframe>
        </section>
               <div class="contact-form-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!--Section Title Start-->
                        <div class="section-title text-center">
                            <h3>{{__('general.subcribe')}}</h3>
                            <h6>{{__('general.keep')}}</h6>
                        </div>
                        <!--Section Title End-->
                    </div>
                </div>
                <div class="contact-ms p-4" id="submit-contacted">
                        
                      @if(Session::has('msg'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('msg') }}
                        </div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                                Sending fail! Please Try again!
                            </div>
                        @endif
                        

                        @if (count($errors) > 0)
                            
                            @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                {{ $error }}
                            </div>
                            @endforeach
                        @endif
                     <form id="submit-contact" action="{{ route('sendmessage', $locale) }}"  method="post">  
                         {{ csrf_field() }} 
                            {{ method_field('PUT') }}
                        <div class="row">
                              
                        <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <input name="first_name" id="first_name" placeholder="{{__('general.fistname')}}" type="text">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">

                                    <input name="last_name" id="last_name" placeholder="{{__('general.lastname')}}" type="text">

                                </div>
                            </div>                     
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    
                                    <input name="email"  id="email" placeholder="{{__('general.email')}}" type="email">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                
                                    <input name="subject"  id="subject" placeholder="{{__('general.subject')}}" type="text">

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="contact-form-style">
                                    <textarea name="message" id="message" placeholder="{{__('general.type')}}"></textarea>
                                    <!-- <div class="g-recaptcha" data-sitekey="6Ldm8jQUAAAAABPK7FEBhKxkWZDSNNLb7ghQ2pDk"></div>  -->
                                    <br>
                                    <button class="form-button" type="submit"><span>{{__('general.sendemail')}}</span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <p class="form-messege"></p>
                    </div>
                </div>
                 </div>
            </div>
        </div>
    </div>
</div>
<!--Contact page end-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8S3pD12gYeCiM2vUf5MuObhznkGbWNCk"></script>
<script src="js/map.js"></script>
<script src="js/map-2.js"></script>
@endsection