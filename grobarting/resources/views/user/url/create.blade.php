@extends($route.'.main') @section ('section-title', 'Create New Service') @section ('section-css')
<link href="{{ asset ('public/user/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset ('public/user/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css" />
<!-- some CSS styling changes and overrides -->
<style>
    .kv-avatar .file-preview-frame,
    .kv-avatar .file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    
    .kv-avatar .file-input {
        display: table-cell;
        max-width: 220px;
    }
</style>
@endsection @section ('imageuploadjs')
<script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection @section ('section-js')
<script>
    var btnCust = '';

    var btnCust = '';
    $("#image").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="http://via.placeholder.com/66X66" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 66X66 px with .jpg or .png type</i></span>',
        layoutTemplates: {
            main2: '{preview} ' + btnCust + ' {remove} {browse}'
        },
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
</script>
@endsection @section ('section-content')
<div class="container-fluid">
    @include('user.layouts.error') 
    @php ($facebook = "") 
    @php ($instagram = "") 
    @php ($linkedin = "") 
    @php ($twitter = "") 
    @if (Session::has('invalidData')) 
    @php ($invalidData = Session::get('invalidData'))
    @php ($facebook = $invalidData['facebook']) 
    @php ($instagram = $invalidData['instagram']) 
    @php ($linkedin = $invalidData['linkedin']) 
    @php ($twitter = $invalidData['twitter']) 
     @endif

    <form id="form" action="{{ route($route.'.store') }}" name="form" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }} {{ method_field('PUT') }}

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (EN) </label>
            <div class="col-sm-10">
                <input id="facebook" name="facebook" value="{{$facebook}}" type="text" placeholder="Please enter facebook" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (Kh) </label>
            <div class="col-sm-10">
                <input id="instagram" name="instagram" value="{{$instagram}}" type="text" placeholder="Please enter kh name" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (EN) </label>
            <div class="col-sm-10">
                <input id="linkedin" name="linkedin" value="{{$linkedin}}" type="text" placeholder="Please enter facebook" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (Kh) </label>
            <div class="col-sm-10">
                <input id="twitter" name="twitter" value="{{$twitter}}" type="text" placeholder="Please enter kh name" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>
        
        <!-- <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="email">Image</label>
            <div class="col-sm-10">
                <div class="kv-avatar center-block">
                    <input id="image" name="image" type="file" class="file-loading">
                </div>
            </div>
        </div> -->
        <div class="form-group row">
            <label class="col-sm-2 form-control-label"></label>
            <div class="col-sm-10">

                <button type="submit" class="btn btn-success">
                    <fa class="fa fa-plus">
                        </i> Create</button>
            </div>
        </div>
    </form>
</div>

@endsection