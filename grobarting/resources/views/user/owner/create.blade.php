@extends ($route.'.main')
@section ('section-title', 'Create New Owner')
@section ('section-css')
	
@endsection

@section ('imageuploadjs')
   
@endsection

@section ('section-js')
	<script type="text/JavaScript">
		// $(document).ready(function(event){
		
		// 	$('#form').validate({
		// 		modules : 'file',
		// 		submit: {
		// 			settings: {
		// 				inputContainer: '.form-group',
		// 				errorListClass: 'form-tooltip-error'
		// 			}
		// 		}
		// 	}); 
			
		// }); 
		
	</script>

	
@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('user.layouts.error')

		@php ($name = "")
		@php ($email = "")
		@php ($phone1 = "")
		@php ($phone2 = "")
		@php ($address = "")
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($name = $invalidData['name'])
            @php ($email = $invalidData['email'])
            @php ($phone1 = $invalidData['phone1'])
            @php ($phone2 = $invalidData['phone2'])
            @php ($address = $invalidData['address'])
            
       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="name">Name</label>
				<div class="col-sm-10">
					<input 	id="name"
							name="name"
						   	value = "{{$name}}"
						   	type="text"
						   	placeholder = "Eg. Jhon Son"
						   	class="form-control"
						   	data-validation="[L>=2, L<=18, MIXED]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone 1</label>
				<div class="col-sm-10">
					<input 	id="phone1"
							name="phone1"
						   	value = "{{$phone1}}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control"
						   	data-validation="[L>=9, L<=10, numeric]"
							data-validation-message="$ is not correct." 
							data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
							data-validation-regex-message="$ must start with 0 and has 9 or 10 digits" />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone 2</label>
				<div class="col-sm-10">
					<input 	id="phone2"
							name="phone2"
						   	value = "{{$phone2}}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control" />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">E-mail</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{$email}}"
							type="text"
							placeholder = "Eg. you@example.com"
						   	class="form-control"
						   	data-validation="[EMAIL]">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Adress</label>
				<div class="col-sm-10">
					<input 	id="address"
							name="address"
							value = "{{$address}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection