@extends($route.'.tab')
@section ('section-title', 'Overview')
@section ('tab-active-edit', 'active')
@section ('tab-css')
	
@endsection

@section ('imageuploadjs')
   
@endsection

@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			// $('#form').validate({
			// 	modules : 'file',
			// 	submit: {
			// 		settings: {
			// 			inputContainer: '.form-group',
			// 			errorListClass: 'form-tooltip-error'
			// 		}
			// 	}
			// }); 
			
		}); 
		
	</script>

	

	
@endsection

@section ('tab-content')
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="name">Name</label>
			<div class="col-sm-10">
				<input 	id="name"
						name="name"
					   	value = "{{ $data->name }}"
					   	type="text"
					   	placeholder = "Eg. Jhon Son"
					   	class="form-control" />
						
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="phone">Phone 1</label>
			<div class="col-sm-10">
				<input 	id="phone1"
						name="phone1"
					   	value = "{{ $data->phone1 }}"
					   	type="text" 
					   	placeholder = "Eg. 093123457"
					   	class="form-control"
					   	data-validation="[L>=9, L<=10, numeric]"
						data-validation-message="$ is not correct." 
						data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
						data-validation-regex-message="$ must start with 0 and has 9 or 10 digits" />
						
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="phone">Phone 2</label>
			<div class="col-sm-10">
				<input 	id="phone2"
						name="phone2"
					   	value = "{{ $data->phone2 }}"
					   	type="text" 
					   	placeholder = "Eg. 093123457"
					   	class="form-control" />
						
			</div>
		</div>
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">E-mail</label>
			<div class="col-sm-10">
				<input 	id="email"
						name="email"
						value = "{{ $data->email }}"
						type="text"
						placeholder = "Eg. you@example.com"
					   	class="form-control" >
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">Address</label>
			<div class="col-sm-10">
				<input 	id="address"
						name="address"
						value = "{{ $data->address }}"
						type="text"
					   	class="form-control">
			</div>
		</div>

		
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				@if(checkPermision($route.'.update'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
				@if(checkPermision($route.'.trash'))<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.index') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>@endif
			</div>
		</div>
	</form>
@endsection