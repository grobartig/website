@extends($route.'.main')
@section ('section-title', 'Edit Why Choose Us')

@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			

		}); 
		
	</script>


@endsection

@section ('section-content')
<div class="container-fluid">
	<br />
	@if (count($errors) > 0)
	    <div class="form-error-text-block">
	        <h2 style="color:red"> Error Occurs</h2>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">
		
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_title">Title (KH)</label>
				<div class="col-sm-10">
					<input 	id="kh_title"
							name="kh_title"
						   	value = "{{$data->kh_title}}"
						   	type="text"
						   	placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_title">Title (En)</label>
				<div class="col-sm-10">
					<input 	id="en_title"
							name="en_title"
						   	value = "{{$data->en_title}}"
						   	type="text"
						   	placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_title">Title (CN)</label>
				<div class="col-sm-10">
					<input 	id="cn_title"
							name="cn_title"
						   	value = "{{$data->cn_title}}"
						   	type="text"
						   	placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_description">Description (KH)</label>
				<div class="col-sm-10">
					<textarea 
							id="kh_description"
							name="kh_description"
						   	value = ""
						   	placeholder = ""
						   	class="form-control"
						   	
					>{{ $data->kh_description }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_description">Description (En)</label>
				<div class="col-sm-10">
					<textarea 
							id="en_description"
							name="en_description"
						   	value = ""
						   	placeholder = ""
						   	class="form-control"
						   	
					>{{ $data->en_description }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_description">Description (Cn)</label>
				<div class="col-sm-10">
					<textarea 
							id="cn_description"
							name="cn_description"
						   	value = ""
						   	placeholder = ""
						   	class="form-control"
						   	
					>{{ $data->cn_description }}</textarea>
				</div>
			</div>
			
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.list') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
			</div>
		</div>
	</form>
</div>
@endsection