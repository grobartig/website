@extends('user/layouts.app')
@section('active-main-menu-web-management', 'opened')
@section('active-main-menu-web-management-about-us', 'opened')
@section('title', 'Edit Content')
@section ('appheadercss')
	<link href="{{ asset ('public/user/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/user/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection
@if($data->image_required)
@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection
@section ('appbottomjs')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			$('#form').submit(function(event){
				event.prevenDefault();
				alert('This is form submit.');
			})

		}); 
		
	</script>
@endsection
@endif


@section ('page-content')
	<header class="page-content-header">
		<div class="container-fluid">
			<div class="tbl">
				<div class="tbl-row">
					<div class="tbl-cell">
						<h3>Content</h3>
					</div>
					<div class="tbl-cell tbl-cell-action">
						@if(isset($_GET['page']))
							@if($_GET['page'] != "")
							 	@php ($menu = "")
							    @if(isset($_GET['menu']))
							        @php( $menu = $_GET['menu'])
							    @endif
								<a href="{{ route($route.'.list', ['page' => $_GET['page']]) }}?menu={{$menu}}&page={{ $_GET['page'] }}" class="btn"><i class="fa fa-arrow-left"></i></a>
							@endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid">
			<div class="box-typical box-typical-padding">
				<div class="container-fluid">
					<h5 class="m-t-lg with-border">{{ $data->name }}</h5>
					@if (count($errors) > 0)
					    <div class="form-error-text-block">
					        <h2 style="color:red"> Error Occurs</h2>
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
	              
					<form id="form" action="{{ route($route.'.update') }}?" name="form" method="POST"  enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('POST') }}
						<input type="hidden" name="id" value="{{ $data->id }}">
						<input type="hidden" name="slug" value="{{ $data->slug }}">
						
						<!-- <div class="form-group row">
							<label class="col-sm-2 form-control-label" for="name">Name</label>
							<div class="col-sm-10">
								<input 	id="name"
										name="name"
										disabled 
									   	value = "{{ $data->name }}"
									   	type="text"
									   	placeholder = "Eg. Jhon Son"
									   	class="form-control"
									   	data-validation="[L>=2, L<=18, MIXED]"
										data-validation-message="$ must be between 2 and 18 characters. No special characters allowed." />
										
							</div>
						</div> -->
						<div class="form-group row">
							<label class="col-sm-2 form-control-label" for="email">Change File</label>
							<div class="col-sm-10">
								<div class="drop-zone">
									<i class="font-icon font-icon-cloud-upload-2"></i>
									<div class="drop-zone-caption">Drag file to upload</div>
									<span class="btn btn-rounded btn-file">
										<span>Choose file</span>
										<input type="file" id="file" name="file" multiple="">
									</span>
								</div>
							</div>
						</div>


						<div class="form-group row">
							<label class="col-sm-2 form-control-label"></label>
							<div class="col-sm-10">
								<a target="_blank" href="{{asset ($data->file)}}" class="btn btn-warning"> <fa class="fa fa-download"></i> Download</a>
								<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
								
							</div>
						</div>
					</form>
				</div>
			</div><!--.box-typical-->
			
	</div><!--.container-fluid-->


	

@endsection