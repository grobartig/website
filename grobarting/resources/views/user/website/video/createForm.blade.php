@extends($route.'.main')
@section ('section-title', 'Create New Videos')
@section ('section-css')
	
@endsection


@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			$("#video-cnt").hide();
			$("#youtube-id").blur(function(){
				youtube_id= $(this).val();
				if(youtube_id != ""){
					$("#video-cnt").show();
					$("#iframe").attr("src", "//www.youtube.com/embed/"+youtube_id);
				}
			})
			
		}); 
		
	</script>
@endsection

@section ('section-content')
	<div class="container-fluid">
		<br />
		@if (count($errors) > 0)
		    <div class="form-error-text-block">
		        <h2 style="color:red"> Error Occurs</h2>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		@php ($title = "")
		@php ($youtube_id = "")
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($title = $invalidData['title'])
             @php ($youtube_id = $invalidData['youtube_id'])
       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="title">Title</label>
				<div class="col-sm-10">
					<input 	id="title"
							name="title"
						   	value = "{{$title}}"
						   	type="text"
						   	placeholder = "enter video's title"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Youtube ID</label>
				<div class="col-sm-10">
					<input 	id="youtube-id"
							name="youtube-id"
							value = "{{ $youtube_id }}"
							type="text"
							placeholder = "Youtube ID"
						   	class="form-control"
						   	data-validation="[L>=6, L<=18]" />
				</div>
			</div>
			<div id="video-cnt" class="form-group row">
				<label class="col-sm-2 form-control-label" >Video</label>
				<div class="col-sm-7">
					<div class="embed-responsive embed-responsive-4by3">
						<iframe id="iframe" class="embed-responsive-item" src=""></iframe>
					</div>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection