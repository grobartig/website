@extends($route.'.main')
@section ('section-title', 'Video')

@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 

			$("#youtube-id").blur(function(){
				youtube_id= $(this).val();
				if(youtube_id != ""){
					$("#iframe").attr("src", "//www.youtube.com/embed/"+youtube_id);
				}
			})
			

		}); 
		
	</script>
@endsection

@section ('section-content')
<div class="container-fluid">
	<br />
	@if (count($errors) > 0)
	    <div class="form-error-text-block">
	        <h2 style="color:red"> Error Occurs</h2>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="title">Title</label>
			<div class="col-sm-10">
				<input 	id="title"
						name="title"
					   	value = "{{$data->title}}"
					   	type="text"
					   	placeholder = "enter title" 
					   	class="form-control"
					   	data-validation="[L>=2, L<=18, MIXED]"
						data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
						
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Youtube ID</label>
			<div class="col-sm-10">
				<input 	id="youtube-id"
						name="youtube-id"
						value = "{{ $data->youtube_id }}"
						type="text"
						placeholder = "Youtube ID"
					   	class="form-control"
					   	data-validation="[L>=6, L<=18]"
					   	/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Video</label>
			<div class="col-sm-7">
				<div class="embed-responsive embed-responsive-4by3">
					<iframe id="iframe" class="embed-responsive-item" src="//www.youtube.com/embed/{{ $data->youtube_id }}"></iframe>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.list') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
			</div>
		</div>
	</form>
</div>
@endsection