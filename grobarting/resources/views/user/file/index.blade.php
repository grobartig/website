@extends($route.'.main')
@section ('section-title', 'All File')
@section ('display-btn-add-new', 'display:none')
@section ('section-css')

@endsection


@section ('section-content')


@if(sizeof($data) > 0)
<div class="table-responsive">
	<table id="table-edit" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Updated Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@php( $is_able_to_view = checkPermision($route.'.edit') )
			@php( $is_able_to_delete = checkPermision($route.'.trash') )
			@php ($i = 1)
			@foreach ($data as $row)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{ $row->name }}</td>
					<td>{{ $row->updated_at }}</td>
					<td style="white-space: nowrap; width: 1%;">
						<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                           	<div class="btn-group btn-group-sm" style="float: none;">
                           		<a target="_blank" href="{{asset ($row->file)}}" class="tabledit-edit-button btn btn-sm btn-warning" style="float: none;"><span class="fa fa-download"></span></a>
                           		@if($is_able_to_view)<a href="{{ route($route.'.edit', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>@endif
                           		@if($is_able_to_delete)<a href="#" onclick="deleteConfirm('{{ route($route.'.trash', $row->id) }}', '{{ route($route.'.index') }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></a>@endif
                           	</div>
                       </div>
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div >
@else
	<span>No Data</span>
@endif


@endsection