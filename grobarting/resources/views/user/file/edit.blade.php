@extends($route.'.main')
@section ('section-title', 'Overview')
@section ('tab-active-edit', 'active')
@section ('tab-css')
	
@endsection


@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			

		}); 
		
	</script>

	

	
@endsection

@section ('section-content')
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="name">Name</label>
			<div class="col-sm-10">
				<input 	id="name"
						name="name"
					   	value = "{{ $data->name }}"
					   	type="text"
					   	placeholder = "Eg. Jhon Son"
					   	class="form-control"
					   	data-validation="[L>=2, L<=18, MIXED]"
						data-validation-message="$ must be between 2 and 18 characters. No special characters allowed." />
						
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">Change File</label>
			<div class="col-sm-10">
				<div class="drop-zone">
					<i class="font-icon font-icon-cloud-upload-2"></i>
					<div class="drop-zone-caption">Drag file to upload</div>
					<span class="btn btn-rounded btn-file">
						<span>Choose file</span>
						<input type="file" id="file" name="file" multiple="">
					</span>
				</div>
			</div>
		</div>

		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<a target="_blank" href="{{asset ($data->file)}}" class="btn btn-warning"> <fa class="fa fa-download"></i> Download</a>
				@if(checkPermision($route.'.update'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
				@if(checkPermision($route.'.trash'))<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.index') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>@endif
			</div>
		</div>
	</form>
@endsection