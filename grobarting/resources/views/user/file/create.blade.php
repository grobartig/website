@extends($route.'.main')
@section ('section-title', 'Create New')

@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			

		}); 
		
	</script>

@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('user.layouts.error')


		@php ($name = "")
		
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($name = $invalidData['name'])
          
            
       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="name">Name</label>
				<div class="col-sm-10">
					<input 	id="name"
							name="name"
						   	value = "{{$name}}"
						   	type="text"
						   	placeholder = "Eg. Enquiry Form"
						   	class="form-control"
						   	data-validation="[L>=2, L<=18, MIXED]"
							data-validation-message="$ must be between 2 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">File</label>
				<div class="col-sm-10">
					<div class="drop-zone">
						
						<i class="font-icon font-icon-cloud-upload-2"></i>
						<div class="drop-zone-caption">Drag file to upload</div>
						<span class="btn btn-rounded btn-file">
							<span>Choose file</span>
							<input type="file" id="file" name="file" multiple="">
						</span>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>

		</form>
	</div>

@endsection