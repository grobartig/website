@extends($route.'.tab')
@section ('section-title', 'Destrict')
@section ('tab-active-district', 'active')
@section ('tab-css')
	
@endsection


@section ('tab-js')
<script type="text/javascript">
	$(document).ready(function(){
		$(".commnues-cnt").hide();
		$("#form-commnue").submit(function(event){
			event.preventDefault();
		});
		$("#form-edit-commnue").submit(function(event){
			event.preventDefault();
		});
	})
	
	function showCommunes(id){
		$(".commnues-cnt").hide();
		$("#commnues-cnt-"+id).show();
		$.ajax({
	        url: "{{ route($route.'.commune') }}?district_id="+id,
	        type: 'GET',
	        data: {},
	        success: function( response ) {
	           $("#data-cnt-"+id).html(response);
	        },
	        error: function( response ) {
	           swal("Error!", "Sorry there is an error happens. " ,"error");
	        }
		})
	}
	
	var district_id=0;

	function showAddCommuneForm(id){
		district_id = id;
		$("#modal").modal("show");
	}

	function create(){
		
		kh_name = $("#kh-name").val();
    	en_name = $("#en-name").val();
    	cn_name = $("#cn-name").val();
    	zip_code = $("#zip-code").val();
    	lat 	= $("#lat").val();
    	lng 	= $("#lng").val();
    	
    	if(kh_name != ""){
    		if(en_name != ""){
    			if(cn_name != ""){
    				if(zip_code != ""){
	    				if(lat != ""){
	    					if(lng != ""){
					    			$.ajax({
								        url: "{{ route($route.'.store-commune') }}",
								        type: 'PUT',
								        data: { kh_name:kh_name, en_name:en_name, cn_name:cn_name, zip_code:zip_code, district_id:district_id, lat:lat, lng:lng },
								        success: function( response ) {
								            if ( response.status === 'success' ) {
								            	swal("Nice!", response.msg ,"success");
								            	showCommunes(district_id);
								            	$("#modal").modal("hide");
								            }else{
								            	swal("Error!", "Sorry there is an error happens. " ,"error");
								            }
								        },
								        error: function( response ) {
								           swal("Error!", "Sorry there is an error happens. " ,"error");
								        }
										
									});
							}else{
								swal("Error!", "Sorry! Lon cannot be empty " ,"error");
								$("#lng").addClass("setup-error");
				    			$("#lng").click(function(){
				    				$("#lng").removeClass("setup-error");
				    			})
							}
						}else{
							swal("Error!", "Sorry! Lat cannot be empty " ,"error");
							$("#lat").addClass("setup-error");
			    			$("#lat").click(function(){
			    				$("#lat").removeClass("setup-error");
			    			})
						}
					}else{
						swal("Error!", "Sorry! Zip Code cannot be empty " ,"error");
						$("#zip-code").addClass("setup-error");
		    			$("#zip-code").click(function(){
		    				$("#zip-code").removeClass("setup-error");
		    			})
					}
				}else{
					swal("Error!", "Sorry! Name Chinese cannot be empty " ,"error");
					$("#cn-name").addClass("setup-error");
	    			$("#cn-name").click(function(){
	    				$("#cn-name").removeClass("setup-error");
	    			})
				}
			}else{
				swal("Error!", "Sorry! Name in English cannot be empty " ,"error");
				$("#en-name").addClass("setup-error");
    			$("#en-name").click(function(){
    				$("#en-name").removeClass("setup-error");
    			})
			}	
    	}else{
    		swal("Error!", "Sorry! Name in Khmer cannot be empty " ,"error");
    		$("#kh-name").addClass("setup-error");
    		$("#kh-name").click(function(){
    			$("#kh-name").removeClass("setup-error");
    		})
    		
    	}
    	
    }


    function delete_data(id){
    	swal({
							title: "Are you sure you want to delete this handller?",
							text: "",
							type: "warning",
							showCancelButton: true,
							cancelButtonClass: "btn-default",
							confirmButtonClass: "btn-warning",
							confirmButtonText: "Okay!",
							closeOnConfirm: false
						},
						function(){
							swal({
								title: "Deleted!",
								text: "Your imaginary file has been deleted.",
								type: "success",
								confirmButtonClass: "btn-success"
							});
							$.ajax({
					        url: "{{ route($route.'.delete-commune') }}",
					        type: 'POST',
					        data: {id:id},
					        success: function( response ) {
					            if ( response.status === 'success' ) {
					            	swal("Nice!", response.msg ,"success");
					            	showCommunes(district_id);
					            }else{
					            	swal("Error!", "Sorry there is an error happens. " ,"error");
					            }
					        },
					        error: function( response ) {
					           swal("Error!", "Sorry there is an error happens. " ,"error");
					        }
					
							});
						});
	}

	var commnue_id=0;
	function showEditCommuneForm(id){
		commnue_id = id;
		$("#modal-edit-commnue").modal("show");

		$.ajax({
	        url: "{{ route($route.'.get-commune') }}?commnue_id="+id,
	        type: 'GET',
	        data: {},
	        success: function( response ) {
	           $("#data-edit").html(response);
	        },
	       	error: function( response ) {
	           swal("Error!", "Sorry there is an error happens. " ,"error");
	        }
		})
		
	}

	function update(id) {
    	kh_name = $("#kh-name-edit").val();
    	en_name = $("#en-name-edit").val();
    	cn_name = $("#cn-name-edit").val();
    	zip_code = $("#zip-code-edit").val();
    	lat 	= $("#lat-edit").val();
    	lng 	= $("#lng-edit").val();
    	district_id 	= $("#district-id-edit").val();
    	active 	= $('#active-edit').val();
				if(active == 0){
					$('#active-edit').val(1);
				}else{
					$('#active-edit').val(0);
				}
    	
    	if(kh_name != ""){
    		if(en_name != ""){
    			if(cn_name != ""){
    				if(zip_code != ""){
	    				if(lat != ""){
	    					if(lng != ""){
				    			$.ajax({
							        url: "{{ route($route.'.update-commune') }}",
							        type: 'POST',
							        data: {id:id, en_name:en_name, kh_name:kh_name, cn_name:cn_name,zip_code:zip_code,active:active,lat:lat, lng:lng, district_id:district_id },
							        success: function( response ) {
							            if ( response.status === 'success' ) {
							            	swal("Nice!", response.msg ,"success");
							            	//get_routes();
							            }else{
							            	swal("Error!", "Sorry there is an error happens. " ,"error");
							            }
							        },
							        error: function( response ) {
							           swal("Error!", "Sorry there is an error happens. " ,"error");
							        }
									
								});
							}else{
								swal("Error!", "Sorry! Lon cannot be empty " ,"error");
								$("#lng-edit").addClass("setup-error");
				    			$("#lng-edit").click(function(){
				    				$("#lng-edit").removeClass("setup-error");
				    			})
							}
						}else{
							swal("Error!", "Sorry! Lat cannot be empty " ,"error");
							$("#lat-edit").addClass("setup-error");
			    			$("#lat-edit").click(function(){
			    				$("#lat-edit").removeClass("setup-error");
			    			})
						}
					}else{
						swal("Error!", "Sorry! Zip code cannot be empty " ,"error");
						$("#zip-code-edit").addClass("setup-error");
		    			$("#zip-code-edit").click(function(){
		    				$("#zip-code-edit").removeClass("setup-error");
		    			})
					}
				}else{
					swal("Error!", "Sorry! Named cannot be empty " ,"error");
					$("#cn-name-edit").addClass("setup-error");
	    			$("#cn-name-edit").click(function(){
	    				$("#cn-name-edit").removeClass("setup-error");
	    			})
				}
			}else{
				swal("Error!", "Sorry! Named cannot be empty " ,"error");
				$("#en-name-edit").addClass("setup-error");
    			$("#en-name-edit").click(function(){
    				$("#en-name-edit").removeClass("setup-error");
    			})
			}	
    	}else{
    		swal("Error!", "Sorry! Named cannot be empty " ,"error");
    		$("#kh-name-edit").addClass("setup-error");
    		$("#kh-name-edit").click(function(){
    			$("#kh-name-edit").removeClass("setup-error");
    		})
    		
    	}
    }

</script>
<script type="text/JavaScript">
			function updateStatusDistrict(id){
		     	thestatus = $('#status-'+id);
		     	active = thestatus.attr('data-value');

		     	if(active == 1){
		     		active = 0;
		     		thestatus.attr('data-value', 1);
		     	}else{
		     		active = 1;
		     		thestatus.attr('data-value', 0);
		     	}

		     	$.ajax({
			        url: "{{ route($route.'.update-status-district') }}",
			        method: 'POST',
			        data: {id:id, active:active },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});
			}
			function updateStatusCommune(id){
		     	thestatus = $('#status-'+id);
		     	active = thestatus.attr('data-value');

		     	if(active == 1){
		     		active = 0;
		     		thestatus.attr('data-value', 1);
		     	}else{
		     		active = 1;
		     		thestatus.attr('data-value', 0);
		     	}

		     	$.ajax({
			        url: "{{ route($route.'.update-status-commune') }}",
			        method: 'POST',
			        data: {id:id, active:active },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});
			}
	</script>
@endsection

@section ('tab-content')
	
	@if(sizeof($districts) > 0)
		
		<a href="{{ route($route.'.create-district',$id) }}" style="float: right;"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"> Add New district</span></a>
		
		<div style="padding-top: 10px;" class="table-responsive">
		
			<table id="table-edit" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Zip Code</th>
						<th>Published</th>
						<th>N.Communes</th>
						<th>N.Properties</th>
						<th>Updated Date</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

					@php ($i = 1)
					@foreach ($districts as $row)
						
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{ $row->en_name }}</td>
							<td>{{ $row->zip_code }}</td>
							<td>
								<div class="checkbox-toggle">
							        <input onclick="updateStatusDistrict({{ $row->id }})" type="checkbox" id="status-{{ $row->id }}" @if ($row->status == 1) checked data-value="1" @else data-value="0" @endif >
							        <label for="status-{{ $row->id }}"></label>
						        </div>
							</td>
							<td>{{ count($row->commnunes) }}</td>
							<td>{{ count($row->properties) }}</td>
							<td>{{ $row->updated_at }}</td>
							<td style="white-space: nowrap; width: 1%;">
								<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
		                           	<div class="btn-group btn-group-sm" style="float: none;">
		                           		<a href="{{ route($route.'.edit-districts', ['id'=>$id,'district_id'=>$row->id]) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
		                           		<button onclick="showCommunes({{$row->id}})" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-bank"></span></button>
		                           		<a href="#" onclick="deleteConfirm('{{ route($route.'.trash-district', $row->id) }}', '{{ route($route.'.districts',$id) }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></a>
		                           	</div>
		                       </div>
		                    </td>
						</tr>
						<tr id="commnues-cnt-{{$row->id}}" class="commnues-cnt">
							<td colspan="8">
						
								Commnues<br/><br/>

								<div id="data-cnt-{{$row->id}}">
								
								</div >
								
								<br/>
								<a href="#" onclick="showAddCommuneForm({{$row->id}})"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"> Add New Communes</span></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div >

	@else
		<a href="{{ route($route.'.create-district',$id) }}"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"> Add New district</span></a>
		<span>No Data</span>
	@endif
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Commnue </h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		
                                       			<br/>
                                       			<form id="form-commnue" action="#" name="form" method="POST"  enctype="multipart/form-data">
													
													<div class="form-group row">
															<label class="col-sm-2 form-control-label" for="kh_name">Name (KH)</label>
															<div class="col-sm-10">
																<input 	id="kh-name"
																		name="kh_name"
																	   	value = ""
																	   	type="text"
																	   	placeholder = "enter Commnue name"
																	   	class="form-control"
																	   	data-validation="[L>=1, L<=200]"
																		 />
																		
															</div>
														</div>
													<div class="form-group row">
														<label class="col-sm-2 form-control-label" for="en_name">Name (En)</label>
														<div class="col-sm-10">
															<input 	id="en-name"
																	name="en_name"
																   	value = ""
																   	type="text"
																   	placeholder = "enter Commnue name"
																   	class="form-control"
																   	data-validation="[L>=1, L<=200]"
																	 />
																	
														</div>
													</div>
													
													<div class="form-group row">
														<label class="col-sm-2 form-control-label" for="cn_name">Name (CN)</label>
														<div class="col-sm-10">
															<input 	id="cn-name"
																	name="cn_name"
																   	value = ""
																   	type="text"
																   	placeholder = "enter Commnue name"
																   	class="form-control"
																   	data-validation="[L>=1, L<=200]"
																	 />
																	
														</div>
													</div>
													
													<div class="form-group row">
														<label class="col-sm-2 form-control-label" for="zip_code">Zip Code</label>
														<div class="col-sm-10">
															<input 	id="zip-code"
																	name="zip_code"
																   	value = ""
																   	type="number"
																   	placeholder = "enter zip code"
																   	class="form-control"
																   	data-validation="[L>=1, L<=6]"
																	 />
																	
														</div>
													</div>

													<div class="form-group row">
														<label class="col-sm-2 form-control-label" for="lat">Lat</label>
														<div class="col-sm-10">
															<input 	id="lat"
																	name="lat"
																   	value = ""
																   	type="text"
																   	placeholder = "enter lat"
																   	class="form-control"
																   	data-validation="[L>=1, L<=200]"
																	 />
																	
														</div>
													</div>
													
													<div class="form-group row">
														<label class="col-sm-2 form-control-label" for="lng">Lng</label>
														<div class="col-sm-10">
															<input 	id="lng"
																	name="lng"
																   	value = ""
																   	type="text"
																   	placeholder = "enter lng"
																   	class="form-control"
																   	data-validation="[L>=1, L<=200]"
																	 />
																	
														</div>
													</div>
													
													<div class="form-group row">
														<label class="col-sm-2 form-control-label"></label>
														<div class="col-sm-10">
															
															<button onclick="create()" type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
														</div>
													</div>
												</form>
                                       			
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->

<div class="modal fade" id="modal-edit-commnue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Edit Commnue </h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		
                                       		<br/>
                                       		<div id="data-edit">
                                       			<form id="form-edit-commnue" action="#" name="form" method="POST"  enctype="multipart/form-data">
                                       			</form>
                                       		</div>	
                                       			
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection