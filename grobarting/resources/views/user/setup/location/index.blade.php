@extends($route.'.main')
@section ('section-title', 'Location')
@section ('hide-btn-back', 'display:none')
@section ('section-css')

@endsection
@section ('section-js')
	<script type="text/JavaScript">
			function updateStatus(id){
		     	thestatus = $('#status-'+id);
		     	active = thestatus.attr('data-value');

		     	if(active == 1){
		     		active = 0;
		     		thestatus.attr('data-value', 1);
		     	}else{
		     		active = 1;
		     		thestatus.attr('data-value', 0);
		     	}

		     	$.ajax({
			        url: "{{ route($route.'.update-status') }}",
			        method: 'POST',
			        data: {id:id, active:active },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});
			}
	</script>
@endsection

@section ('section-content')
	@if(sizeof($data) > 0)
		<div class="table-responsive">
			<table id="table-edit" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Abbreviation</th>
						<th>Zip Code</th>
						<th>Published</th>
						<th>N. districts</th>
						<th>N. Communes</th>
						<th>N. Properties</th>
						<th>Updated Date</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

					@php ($i = 1)
					@foreach ($data as $row)
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{ $row->en_name }}</td>
							<td>{{ $row->abbre }}</td>
							<td>{{ $row->zip_code }}</td>
							<td>
								<div class="checkbox-toggle">
							        <input onclick="updateStatus({{ $row->id }})" type="checkbox" id="status-{{ $row->id }}" @if ($row->status == 1) checked data-value="1" @else data-value="0" @endif >
							        <label for="status-{{ $row->id }}"></label>
						        </div>
							</td>
							<td>{{ count($row->districts) }}</td>
							<td>{{ count($row->commnunes) }}</td>
							<td>{{ count($row->properties) }}</td>
							<td>{{ $row->updated_at }}</td>
							<td style="white-space: nowrap; width: 1%;">
								<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
		                           	<div class="btn-group btn-group-sm" style="float: none;">
		                           		<a href="{{ route($route.'.edit', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
		                           		<a href="#" onclick="deleteConfirm('{{ route($route.'.trash', $row->id) }}', '{{ route($route.'.index') }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></a>
		                           	</div>
		                       </div>
		                    </td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div >
	@else
		<span>No Data</span>
	@endif
@endsection