@extends($route.'.tab')
@section ('section-title', 'Edit Form district')
@section ('tab-active-district', 'active')


@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			

		}); 
		
	</script>

	
@endsection

@section ('tab-content')
<div class="container-fluid">
	@include('user.layouts.error')

	@php ($status = 0)
		@php ($en_name = "")
		@php ($kh_name = "")
		@php ($cn_name = "")
		@php ($zip_code = "")
		@php ($lat = "")
		@php ($lng = "")
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($status = $invalidData['status'])
            @php ($en_name = $invalidData['en_name'])
            @php ($kh_name = $invalidData['kh_name'])
            @php ($cn_name = $invalidData['cn_name'])
            @php ($zip_code = $invalidData['zip_code'])
            @php ($lat = $invalidData['lat'])
            @php ($lng = $invalidData['lng'])

            
       	@endif
	<form id="form" action="{{ route($route.'.store-district') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<input type="hidden" name="province_id" value="{{ $id }}">
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_name">Title (KH)</label>
				<div class="col-sm-10">
					<input 	id="kh_name"
							name="kh_name"
						   	value = "{{$kh_name}}"
						   	type="text"
						   	placeholder = "enter name in Khmer"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_name">Title (En)</label>
				<div class="col-sm-10">
					<input 	id="en_name"
							name="en_name"
						   	value = "{{$en_name}}"
						   	type="text"
						   	placeholder = "enter name in English"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_name">Title (CN)</label>
				<div class="col-sm-10">
					<input 	id="cn_name"
							name="cn_name"
						   	value = "{{$cn_name}}"
						   	type="text"
						   	placeholder = "enter name in Chineses"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="zip_code">Zip Code</label>
				<div class="col-sm-10">
					<input 	id="zip_code"
							name="zip_code"
						   	value = "{{$zip_code}}"
						   	type="number"
						   	placeholder = "enter zip_code"
						   	class="form-control"
						   	data-validation="[L>=5, L<=10]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="lat">Lat</label>
				<div class="col-sm-10">
					<input 	id="lat"
							name="lat"
						   	value = "{{$lat}}"
						   	type="text"
						   	placeholder = "enter lat"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="lng">Lng</label>
				<div class="col-sm-10">
					<input 	id="lng"
							name="lng"
						   	value = "{{$lng}}"
						   	type="text"
						   	placeholder = "enter lng"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="status">Published</label>
				<div class="col-sm-10">
					<div class="checkbox-toggle">
						<input name="status" id="status" type="checkbox"  @if($status ==1 ) checked @endif >
						<label onclick="change_active()" for="status"></label>
					</div>
					<input type="hidden" name="status" id="status" value="{{$status}}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>
@endsection