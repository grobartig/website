@extends($route.'.tab')
@section ('section-title', 'Overview')
@section ('tab-active-edit', 'active')


@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			

		}); 
		function change_active(){
			val 	= $('#active').val();
			if(val == 0){
				$('#active').val(1);
			}else{
				$('#active').val(0);
			}
		}
	</script>

	
@endsection

@section ('tab-content')

<div class="container-fluid">
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">
		
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_name">Name (KH)</label>
				<div class="col-sm-10">
					<input 	id="kh_name"
							name="kh_name"
						   	value = "{{$data->kh_name}}"
						   	type="text"
						   	placeholder = "enter name in Khmer"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="en_name">Name (En)</label>
				<div class="col-sm-10">
					<input 	id="en_name"
							name="en_name"
						   	value = "{{$data->en_name}}"
						   	type="text"
						   	placeholder = "enter name in English"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="cn_name">Name (CN)</label>
				<div class="col-sm-10">
					<input 	id="cn_name"
							name="cn_name"
						   	value = "{{$data->cn_name}}"
						   	type="text"
						   	placeholder = "enter name in Chinese"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="abbre">Abbreviation</label>
				<div class="col-sm-10">
					<input 	id="abbre"
							name="abbre"
						   	value = "{{$data->abbre}}"
						   	type="text"
						   	placeholder = "enter abbre"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="zip_code">Zip Code</label>
				<div class="col-sm-10">
					<input 	id="zip_code"
							name="zip_code"
						   	value = "{{$data->zip_code}}"
						   	type="number"
						   	placeholder = "enter zip code"
						   	class="form-control"
						   	data-validation="[L>=5, L<=10]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="lat">Latitude</label>
				<div class="col-sm-10">
					<input 	id="lat"
							name="lat"
						   	value = "{{$data->lat}}"
						   	type="text"
						   	placeholder = "enter lat"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="lng">Longitude</label>
				<div class="col-sm-10">
					<input 	id="lng"
							name="lng"
						   	value = "{{$data->lng}}"
						   	type="text"
						   	placeholder = "enter lng"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							 />
							
				</div>
			</div>

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="status">Published</label>
			<div class="col-sm-10">
				<div class="checkbox-toggle">
					<input name="active" id="active" type="checkbox"  @if($data->status ==1 ) checked @endif >
					<label onclick="change_active()" for="active"></label>
				</div>
				
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.index') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
			</div>
		</div>
	</form>
</div>
@endsection