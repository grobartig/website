
	<input type="hidden" id="district-id-edit" name="district_id" value="{{ $data->district_id }}">												
	<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="kh_name">Name (KH)</label>
			<div class="col-sm-10">
				<input 	id="kh-name-edit"
						name="kh_name"
					   	value = "{{$data->kh_name}}"
					   	type="text"
					   	placeholder = "enter name in Khmer"
					   	class="form-control"
					   	data-validation="[L>=1, L<=200]"
						 />
						
			</div>
		</div>
	<div class="form-group row">
		<label class="col-sm-2 form-control-label" for="en_name">Name (En)</label>
		<div class="col-sm-10">
			<input 	id="en-name-edit"
					name="en_name"
				   	value = "{{$data->en_name}}"
				   	type="text"
				   	placeholder = "enter name in English"
				   	class="form-control"
				   	data-validation="[L>=1, L<=200]"
					 />
					
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-sm-2 form-control-label" for="cn_name">Name (CN)</label>
		<div class="col-sm-10">
			<input 	id="cn-name-edit"
					name="cn_name"
				   	value = "{{$data->cn_name}}"
				   	type="text"
				   	placeholder = "enter name in Chinese"
				   	class="form-control"
				   	data-validation="[L>=1, L<=200]"
					 />
					
		</div>
	</div>
	
	<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="zip_code">Zip Code</label>
				<div class="col-sm-10">
					<input 	id="zip-code-edit"
							name="zip_code"
						   	value = "{{$data->zip_code}}"
						   	type="number"
						   	placeholder = "enter zip_code"
						   	class="form-control"
						   	data-validation="[L>=1, L<=6]"
							 />
							
				</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2 form-control-label" for="lat">Lat</label>
		<div class="col-sm-10">
			<input 	id="lat-edit"
					name="lat"
				   	value = "{{$data->lat}}"
				   	type="text"
				   	placeholder = "enter lat"
				   	class="form-control"
				   	data-validation="[L>=1, L<=200]"
					 />
					
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-sm-2 form-control-label" for="lng">Lng</label>
		<div class="col-sm-10">
			<input 	id="lng-edit"
					name="lng"
				   	value = "{{$data->lng}}"
				   	type="text"
				   	placeholder = "enter lng"
				   	class="form-control"
				   	data-validation="[L>=1, L<=200]"
					 />
					
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-sm-2 form-control-label"></label>
		<div class="col-sm-10">
			
			<button onclick="update({{$data->id}})" type="submit" class="btn btn-success"> </i> Update</button>
		</div>
	</div>
