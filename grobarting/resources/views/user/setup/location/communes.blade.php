@if(sizeof($communes) > 0)
<table class="table table-bordered table-hover">
	<tbody>
		@foreach($communes as $commune)
		<tr>
			
			<td>{{$commune->en_name}}</td>
			<td>{{$commune->zip_code}}</td>
			<td>
				<div class="checkbox-toggle">
			        <input onclick="updateStatusCommune({{ $commune->id }})" type="checkbox" id="status-{{ $commune->id }}" @if ($commune->status == 1) checked data-value="1" @else data-value="0" @endif >
			        <label for="status-{{ $commune->id }}"></label>
		        </div>
			</td>
			<td>{{$commune->updated_at}}</td>
			<td>
				<button onclick="showEditCommuneForm({{ $commune->id }})" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="glyphicon glyphicon-pencil"></span></button>
				<button onclick="delete_data({{ $commune->id }})" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></button>
			</td>
		</tr>
		@endforeach
	</tbody>

</table>
@else 
No Data

@endif