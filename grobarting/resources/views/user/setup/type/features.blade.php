@extends($route.'.tab')
@section ('section-title', 'Amenities')
@section ('tab-active-feature', 'active')
@section ('tab-css')
	
@endsection


@section ('tab-js')
<script type="text/javascript">
	$(document).ready(function(){
		$('.item').click(function(){
			check_id = $(this).attr('for');
			feature_id = $("#"+check_id).attr('type-id');
			features(feature_id);
		})
	})
	function features(feature_id){
		
		$.ajax({
		        url: "{{ route($route.'.check-features') }}?type_id={{ $id }}&feature_id="+feature_id,
		        type: 'GET',
		        data: { },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	toastr.success(response.msg);
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
		});
	}
</script>
@endsection

@section ('tab-content')

	@if(sizeof($features) > 0)
		<div class="row m-t-lg">
			@foreach($features as $row)
				@php( $check = "" )
				@foreach($types as $type)
					@if($type->feature_id==$row->id)
						@php( $check = "checked" )
					@endif
					
				@endforeach
				<div class="col-sm-6 col-sm-4 col-md-3 col-lg-3">
					<div class="checkbox-bird">
						<input type="checkbox" type-id="{{ $row->id }}" id="feature-{{ $row->id }}" {{ $check }} >
						<label class="item" for="feature-{{ $row->id }}">{{ $row->en_name }}</label>
					</div>
				</div>
			@endforeach
		</div>
	@else
	<p>No data Here</p>
	@endif
@endsection