@extends($route.'.tab')
@section ('section-title', 'Types')
@section ('tab-active-type', 'active')


@section ('tab-js')
<script type="text/javascript">
	$(document).ready(function(){
		$('.item').click(function(){
			check_id = $(this).attr('for');
			type_id = $("#"+check_id).attr('type-id');
			types(type_id);
		})
	})
	function types(type_id){
		
		$.ajax({
		        url: "{{ route($route.'.check-types') }}?detail_id={{ $id }}&type_id="+type_id,
		        type: 'GET',
		        data: { },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	toastr.success(response.msg);
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
		});
	}
</script>
@endsection

@section ('tab-content')

	@if(sizeof($types) > 1)
		<div class="row m-t-lg">
			@foreach($types as $row)
				@php( $check = "" )
				@foreach($details as $detail)
					@if($detail->type_id==$row->id)
						@php( $check = "checked" )
					@endif
					
				@endforeach
				<div class="col-sm-6 col-sm-4 col-md-3 col-lg-2">
					<div class="checkbox-bird">
						<input type="checkbox" type-id="{{ $row->id }}" id="type-{{ $row->id }}" {{ $check }} >
						<label class="item" for="type-{{ $row->id }}">{{ $row->en_name }}</label>
					</div>
				</div>
			@endforeach
		</div>
	@else
	<p>No data Here</p>
	@endif
@endsection