@extends($route.'.tab')
@section ('section-title', 'Properties')
@section ('tab-active-group', 'active')
@section ('tab-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}

	</style>
@endsection


@section ('tab-js')
	<script type="text/javaScript">
		$(document).ready(function(){
			
			$("#btn-add-more-data").click(function(){
				properties(); 
				 search();
			})

			$("#search").keypress(function(e) {
			    if(e.which == 13) {
			       search();
			    }
			});
		})

		function search(){
			key 		= $('#search').val();
			$.ajax({
			        url: "{{ route($route.'.search-property') }}?id={{$id}}&key="+key,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#result").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
		function properties(){
			$.ajax({
			        url: "{{ route($route.'.selected-property') }}?id="+{{ $id }},
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function removeFromList(property_id){
			$.ajax({
			        url: "{{ route($route.'.remove-property-from-list') }}?id={{ $id }}&property_id="+property_id,
			        type: 'DELETE',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          search();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function addToList(property_id){
			$.ajax({
			        url: "{{ route($route.'.add-property-to-list') }}?id={{ $id }}&property_id="+property_id,
			        type: 'PUT',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          search();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
	</script>
@endsection

@section ('tab-content')
	<div>
		<div class="col-md-12">
			<button id="btn-add-refresh" onclick="window.location.href='{{ route($route.'.properties', $id) }}'"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;margin-left: 5px;"><span class="fa fa-refresh"></span></button>
			<button id="btn-add-more-data" data-toggle="modal" data-target="#modal" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;"><span class="fa fa-plus"></span></button>
		</div>
	</div><!--.row-->

	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Code</th>
					<th>Action</th>
					<th>Title</th>
					<th>N. of Lists</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>
							@php( $l = '')
							@if($row->is_published == 0)
								@php( $l = 'L')
							@endif
							@php( $display_listing_code = $l.$row->province->abbre.'-'.$row->type->abbre.$row->listing_code )
							{{$display_listing_code}}
						</td>
						<td>{{ $row->action->en_name }}</td>
						<td>{{ $row->en_name }}</td>
					
						<td>{{ count($row->lists) }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<button type="button" onclick="window.location.href='{{ route('user.property.property.edit', $row->id) }}'" class="tabledit-edit-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-eye"></span></button><button type="button" onclick="deleteConfirm('{{ route($route.'.remove-property', ['id'=>$id, 'property_id'=>$row->id]) }}', '{{ route($route.'.properties', $id) }}')" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-trash"></span></button>
	                           	</div>
                       		</div>
                       	</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	No properties in this list.
	@endif
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Property</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-5">
                                       				<div class="chat-list-search">
														<input type="text" id="search" class="form-control form-control-rounded" placeholder="Code or Name of Property">
													
													</div>
                                       				<div id="result">
                                       					
                                       				</div>
                                       			</div>
                                       			<div class="col-xs-7">
                                       				<div class="chat-area-header">
														<div class="clean">Existing properties in this list</div>
													</div>
													<div id="selected">
                                       					
                                       				</div>
                                       			</div>
                                       			
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection