@extends($route.'.tab')
@section ('section-title', 'Overview')
@section ('tab-active-edit', 'active')
@section ('tab-css')
	
@endsection


@section ('tab-js')
	
@endsection

@section ('tab-content')
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
		
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name</label>
				<div class="col-sm-10">
					<input 	id="name"
							name="name"
							value = "{{ $data->name }}"
							type="text"
							placeholder = "Name"
							class="form-control"
						    data-validation="[L>=2, L<=200]"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Note</label>
				<div class="col-sm-10">
					<textarea 	id="note"
							name="note"
							type="text"
							placeholder = "note"
							class="form-control">{{ $data->note }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >N. of Properties</label>
				<div class="col-sm-10">
					<span>{{ count($data->properties) }}</span>
				</div>
			</div>
			
		
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
					<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.index') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
				</div>
			</div>
	</form>
	
@endsection