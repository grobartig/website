
<table id="seleted-table" class="table pop">
	<thead>
		<th width=10%>No.</th><th>Code.</th><th>Name.</th><th width=8%>.</th>
	</thead>
	<tbody>
		@php( $i=1 )
		@foreach($data as $row)
		<tr>
			<td>{{ $i++ }}</td><td>{{ $row->code }}</td><td>{{ $row->name }}</td><td><i onclick="removeFromList({{ $row->id }})" class="fa fa-times"></i></td>
		</tr>
		@endforeach
	</tbody>
</table>
