@extends($route.'.tab')
@section ('section-title', 'Overview')
@section ('tab-active-edit', 'active')
@section ('tab-css')
	
@endsection


@section ('tab-js')
	
@endsection

@section ('tab-content')
	<br />
		@if (count($errors) > 0)
		    <div class="form-error-text-block">
		        <h2 style="color:red"> Error Occurs</h2>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
		
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name</label>
				<div class="col-sm-10">
					<input 	id="name"
							name="name"
							value = "{{ $data->name }}"
							type="text"
							placeholder = "Name"
							class="form-control"
						    data-validation="[L>=2, L<=200]"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >E-mail</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{ $data->email }}"
							type="email"
							placeholder = "Email"
						   	class="form-control"
						   	data-validation="[L>=5, L<=200]" />
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone</label>
				<div class="col-sm-10">
					<input 	id="phone"
							name="phone"
						   	value = "{{$data->phone}}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control"
						   	data-validation="[L>=9, L<=10, numeric]"
							data-validation-message="$ is not correct." 
							data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
							data-validation-regex-message="$ must start with 0 and has 10 or 11 digits" />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Subscribe</label>
				<div class="col-sm-10">
					<div class="checkbox-toggle">
						<input id="is_subscribed-status" type="checkbox"  @if($data->is_subscribed ==1 ) checked @endif >
						<label onclick="booleanForm('is_subscribed')" for="is_subscribed-status"></label>
					</div>
					<input type="hidden" name="is_subscribed" id="is_subscribed" value="{{ $data->is_subscribed }}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">N. of Groups</label>
				<div class="col-sm-10">
					<span>{{ count($data->groups) }}</span>
				</div>
			</div>
			
		
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
					<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.index') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
				</div>
			</div>
	</form>
	
@endsection