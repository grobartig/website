@extends($route.'.tab')
@section ('section-title', 'Properties')
@section ('tab-active-property', 'active')
@section ('tab-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}

	</style>
@endsection


@section ('tab-js')
	<script type="text/javaScript">
		$(document).ready(function(){
			
			$("#btn-search").click(function(){
				search();
			})
			$("#limit").change(function(){
				search();
			})
				
		})

		function search(){
			key 		= $('#key').val();
			d_from 		= $('#from').val();
			d_till 		= $('#till').val();
			limit 		= $('#limit').val();

			url="?limit="+limit;
			
			if(key != ""){
				url += '&key='+key;
			}
			if(isDate(d_from)){
				if(isDate(d_till)){
					url+='&from='+d_from+'&till='+d_till;
				}
			}
			
			$(location).attr('href', '{{ route($route.'.properties', $id) }}'+url);
		}
	</script>
@endsection

@section ('tab-content')
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<input id="key" type='text' class="form-control" value="{{ isset($appends['key'])?$appends['key']:'' }}" placeholder="code" />
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="till-cnt" class='input-group date' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
		</div>
	</div><!--.row-->

	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Code</th>
					<th>Action</th>
					<th>Type</th>
					<th>Title</th>
					<th>Publish</th>
					<th>Sent By</th>
					<th>Sent Date&Time</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $row->property->listing_code }}</td>
						<td>{{ $row->property->action->en_name }}</td>
						<td>{{ $row->property->type->en_name }}</td>
						<td>{{ $row->property->en_name }}</td>
						
						<td>
							<div class="checkbox-toggle">
						        <input type="checkbox" id="status-{{ $row->id }}" @if ($row->property->published == 1) checked data-value="1" @else data-value="0" @endif >
						        <label for="status-xx"></label>
					        </div>
						</td>
						<td>{{ $row->sender->name }}</td>
						<td>{{ $row->created_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<button type="button" onclick="window.location.href='{{ route('user.property.edit', $row->property->id) }}'" class="tabledit-edit-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-eye"></span>
	                           	</div>
                       		</div>
                       	</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	No properties sent to this email account
	@endif
	<div class="row">
		<div class="col-xs-1" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:100%">
				@if(isset($appends['limit']))
				<option value="{{ $appends['limit'] }}">{{ $appends['limit'] }}</option>
				@endif
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="40">40</option>
				<option value="50">50</option>
				<option value="60">60</option>
				<option value="70">70</option>
				<option value="80">80</option>
				<option value="90">90</option>
				<option value="100">100</option>
			</select>
		</div>
		<div class="col-xs-1">
			<button style="margin-top:15px" id="btn-list" class="tabledit-delete-button btn btn-sm btn-primary" style="float: left;"><span class="fa fa-list"></span></button>
		</div>
		<div class="col-xs-10">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
	
	
@endsection
