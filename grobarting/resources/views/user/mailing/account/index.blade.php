@extends($route.'.main')
@section ('section-title', 'All')
@section ('display-btn-add-new', 'display:none')
@section ('section-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}
	</style>
@endsection
@section ('section-js')
<script type="text/javascript" src="{{ asset ('public/user/js/lib/blockUI/jquery.blockUI.js') }}"></script>
<script type="text/javascript">	
	$(document).ready(function(){
		$("#btn-search").click(function(){
			search();
		})
		$("#limit").change(function(){
			search();
		})

		//============check
    	$("#check-all").click(function(){
    		checkAll();
    	})
    	$(".check-list-item").click(function(){
    		checkItem($(this).attr('data-id'));
    	})
    	$("#btn-create-group").click(function(){
    		createGroup();
    	})
    	$("#btn-add-to-group").click(function(){
    		$('#modal').modal('show');
    		displaySelectedAccounts();
    		displayAvaiableGroups();
    	})
		
	})

	

    function search(){
		key 		= $('#key').val();
		d_from 		= $('#from').val();
		d_till 		= $('#till').val();
		limit 		= $('#limit').val();

		url="?limit="+limit;
		
		if(key != ""){
			url += '&key='+key;
		}
		if(isDate(d_from)){
			if(isDate(d_till)){
				url+='&from='+d_from+'&till='+d_till;
			}
		}
		
		$(location).attr('href', '{{ route($route.'.index') }}'+url);
	}

	var check = 'check-all';
	function checkAll(){
		//console.log(check);
		items = $(".check-list-item");
		if( check == 'check-all' ){
			items.removeClass('fa-square-o');
			items.addClass('fa-check-square-o');
			check = '';
		}else{
			items.removeClass('fa-check-square-o');
			items.addClass('fa-square-o');
			check = 'check-all';
		}
	}
	function checkItem(id){
		
		item = $("#item-"+id);
		if(item.hasClass('fa-square-o')){
			item.removeClass('fa-square-o');
			item.addClass('fa-check-square-o');
		}else{
			item.removeClass('fa-check-square-o');
			item.addClass('fa-square-o');
		}
	}
	
	function createGroup(){
		items = $(".check-list-item");
		//console.log(items);
		checkedItems = []; 
		for(i=0; i<items.length; i++){
			item = items[i];
			if(item.classList.contains('fa-check-square-o')){
				checkedItems.push(item.getAttribute('data-id'));
			}
		}
		//console.log(checkedItems);
		if( checkedItems.length != 0 ){
			swal({
				title: "Email group for Mailing",
				text: "Please name this group:",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: "Write something"
			}, function (inputValue) {
				if (inputValue === false) return false;
				if (inputValue === "") {
					swal.showInputError("You need to write something!");
					return false
				}
				
				//swal("Nice!", "You wrote: " + inputValue, "success");
				$.ajax({
			        url: "{{ route($route.'.create-account-group') }}",
			        method: 'PUT',
			        data: { name:inputValue, list:JSON.stringify(checkedItems) },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});

			});
		}else{
			swal("Please select at least an account.");
			
		}
	}

	function displaySelectedAccounts(){
		items = $(".check-list-item");
		$("#selected-cnt").html('');
		for(i=0; i<items.length; i++){
			item = items[i];
			if(item.classList.contains('fa-check-square-o')){
				$("#selected-cnt").append('<tr><td>'+item.getAttribute('data-name')+'</td><td>'+item.getAttribute('data-email')+'</td></tr>');
			}
		}
	}

    function displayAvaiableGroups(){
    	key 		= $('#search').val();
		$.ajax({
		        url: "{{ route($route.'.search-group') }}?id=0&key="+key,
		        type: 'GET',
		        data: {},
		        success: function( response ) {
		          $("#result").html(response);

		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
					
		});
    }

	function addAccountsToGroup(group_id){
		items = $(".check-list-item");
		//console.log(items);
		checkedItems = []; 
		for(i=0; i<items.length; i++){
			item = items[i];
			if(item.classList.contains('fa-check-square-o')){
				checkedItems.push(item.getAttribute('data-id'));
			}
		}
		//console.log(checkedItems);
		if( checkedItems.length != 0 ){
			$('.modal-content').block({
				message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6>Email account are being added to a group. <br> Please be patient.</h6></div>',
				overlayCSS:  {
					background: 'rgba(142, 159, 167, 0.8)',
					opacity: 1,
					cursor: 'wait'
				},
				css: {
					width: '50%'
				},
				blockMsgClass: 'block-msg-default'
			});

			

			$.ajax({
		        url: "{{ route($route.'.add-accounts-to-group') }}",
		        method: 'POST',
		        data: { group_id:group_id, list:JSON.stringify(checkedItems) },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	swal("Nice!", response.msg ,"success");
		            	displayAvaiableGroups();
		            	$('.modal-content').unblock();
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
			});


		}else{
			swal("Please select at least an account.");
			
		}
	}





</script>
@endsection

@section ('section-content')
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<input id="key" type='text' class="form-control" value="{{ isset($appends['key'])?$appends['key']:'' }}" placeholder="Key" />
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="till-cnt" class='input-group date' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
		</div>
	</div><!--.row-->

	@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><i id="check-all" class="fa fa-square-o check-list"></i></th>
					<th>Name</th>
					<th>E-mail</th>
					<th>Subscribe</th>
					<th>N. of Groups</th>
					<th>Updated Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td><i id="item-{{ $row->id }}" data-id="{{ $row->id }}" data-name="{{ $row->name }}" data-email="{{ $row->email }}" class="fa fa-square-o  @if ($row->is_subscribed == 1) check-list-item check-list  @else uncheckable @endif  "></i></td>
						<td>{{ $row->name }}</td>
						<td>{{ $row->email }}</td>
						<td>
							<div class="checkbox-toggle">
						        <input onclick="updateStatus({{ $row->id }})" type="checkbox" id="status-{{ $row->id }}"  @if ($row->is_subscribed == 1) checked @endif >
						        <label for="status-xxx"></label>
					        </div>
						</td>
						<td>{{ count($row->groups) }}</td>
						<td>{{ $row->updated_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<a href="{{ route($route.'.edit', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
	                           		<a href="#" onclick="deleteConfirm('{{ route($route.'.trash', $row->id) }}', '{{ route($route.'.index') }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="fa fa-trash"></span></a>
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	<span>No Data</span>
	@endif
	<div class="row">
		<div class="col-xs-1" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:100%">
				@if(isset($appends['limit']))
				<option>{{ $appends['limit'] }}</option>
				@endif
				<option>10</option>
				<option>20</option>
				<option>30</option>
				<option>40</option>
				<option>50</option>
				<option>60</option>
				<option>70</option>
				<option>80</option>
				<option>90</option>
				<option>100</option>
			</select>
		</div>
		<div class="col-xs-2">
			<button style="margin-top:15px" id="btn-create-group" class="tabledit-delete-button btn btn-sm btn-primary" style="float: left;"><span class="fa fa-list"></span></button>
			<button style="margin-top:15px" id="btn-add-to-group" class="tabledit-delete-button btn btn-sm btn-primary" style="float: left;"><span class="fa fa-clipboard"></span></button>
		</div>
		<div class="col-xs-9">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
	
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add To Group</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div id="block-ui" class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-6">
                                       				<div class="chat-area-header">
														<div class="clean">Add Email Accounts Groups</div>
													</div>
													<div id="selected">
														<table id="seleted-table" class="table pop">
															<thead>
																<th>Name.</th><th>Email.</th><th width=8%>.</th>
															</thead>
															<tbody id="selected-cnt">
																
															</tbody>
														</table>
                                       				</div>
                                       			</div>
                                       			<div class="col-xs-6">
                                       				<div class="chat-list-search">
														<input type="text" id="search" class="form-control form-control-rounded" placeholder="Name of Group">
													</div>
                                       				<div id="result">
                                       					
                                       				</div>
                                       			</div>
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection