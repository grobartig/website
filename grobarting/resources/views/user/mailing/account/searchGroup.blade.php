@if(count($data)>0)
<table id="pop-table" class="table pop">
	<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{ $row->code }}</td><td>{{ $row->name }} ({{count($row->accounts)}})</td><td width=8%><i @if( $id != 0) onclick="addGroupToAccount({{$row->id}})" @else onclick="addAccountsToGroup({{$row->id}})" @endif class="fa fa-plus"></i></td>
			</tr>
		@endforeach
		
	</tbody>
</table>
@else
	No Data Avaiable
@endif
