@extends($route.'.main')
@section ('section-title', 'Create New Mail Account')
@section ('section-css')
	
@endsection

@section ('imageuploadjs')
  
@endsection

@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
		
		}); 

	
	</script>


@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('user.layouts.error')

		@php ($name = '')
		@php ($email = '')
		@php ($phone = '')
		@php ($is_subscribed = 0)
		
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))

			@php ($name = $invalidData['name'])
			@php ($email = $invalidData['email'])
			@php ($phone = $invalidData['phone'])
			@php ($is_subscribed = $invalidData['is_subscribed'])

       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name</label>
				<div class="col-sm-10">
					<input 	id="name"
							name="name"
							value = "{{ $name }}"
							type="text"
							placeholder = "Name"
							class="form-control"
						    data-validation="[L>=2, L<=200]"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >E-mail</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{ $email }}"
							type="email"
							placeholder = "Email"
						   	class="form-control"
						   	data-validation="[L>=5, L<=200]" />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone</label>
				<div class="col-sm-10">
					<input 	id="phone"
							name="phone"
						   	value = "{{$phone}}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control"
						   	data-validation="[L>=9, L<=10, numeric]"
							data-validation-message="$ is not correct." 
							data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
							data-validation-regex-message="$ must start with 0 and has 10 or 11 digits" />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Subscribe</label>
				<div class="col-sm-10">
					<div class="checkbox-toggle">
						<input id="is_subscribed-status" type="checkbox"  @if($is_subscribed ==1 ) checked @endif >
						<label onclick="booleanForm('is_subscribed')" for="is_subscribed-status"></label>
					</div>
					<input type="hidden" name="is_subscribed" id="is_subscribed" value="{{ $is_subscribed }}">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection