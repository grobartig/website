@extends($route.'.tab')
@section ('section-title', 'Groups')
@section ('tab-active-group', 'active')
@section ('tab-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}

	</style>
@endsection


@section ('tab-js')
	<script type="text/javaScript">
		$(document).ready(function(){
			$("#btn-search").click(function(){
				search();
			})
			$("#limit").change(function(){
				search();
			})
			$("#btn-add-more-data").click(function(){
				groups(); 
				searchGroup();
			})

			$("#search-group").keypress(function(e) {
			    if(e.which == 13) {
			       searchGroup();
			    }
			});
		})

		function search(){
			key 		= $('#key').val();
			d_from 		= $('#from').val();
			d_till 		= $('#till').val();
			limit 		= $('#limit').val();

			url="?limit="+limit;
			
			if(key != ""){
				url += '&key='+key;
			}
			if(isDate(d_from)){
				if(isDate(d_till)){
					url+='&from='+d_from+'&till='+d_till;
				}
			}
			
			$(location).attr('href', '{{ route($route.'.groups', $id) }}'+url);
		}

		function searchGroup(){
			key 		= $('#search-group').val();
			$.ajax({
			        url: "{{ route($route.'.search-group') }}?id={{$id}}&key="+key,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#result").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
		function groups(){
			$.ajax({
			        url: "{{ route($route.'.selected-group') }}?id="+{{ $id }},
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens." ,"error");
			        }	
			});
		}
		function removeGroupFromAccount(group_id){
			$.ajax({
			        url: "{{ route($route.'.remove-group-from-account') }}?id={{ $id }}&group_id="+group_id,
			        type: 'DELETE',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          searchGroup();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function addGroupToAccount(group_id){
			$.ajax({
			        url: "{{ route($route.'.add-group-to-account') }}?id={{ $id }}&group_id="+group_id,
			        type: 'PUT',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          searchGroup();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}

	</script>
@endsection

@section ('tab-content')

	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<input id="key" type='text' class="form-control" value="{{ isset($appends['key'])?$appends['key']:'' }}" placeholder="Name of Groups" />
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="till-cnt" class='input-group date' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button id="btn-add-refresh" onclick="window.location.href='{{ route($route.'.groups', $id) }}'"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;margin-left: 5px;"><span class="fa fa-refresh"></span></button>
			<button id="btn-add-more-data" data-toggle="modal" data-target="#modal" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;"><span class="fa fa-plus"></span></button>
			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
		</div>
	</div><!--.row-->
	
	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Number of Accounts</th>
					<th>Added Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					@php($group=$row->group)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $group->name }}</td>
						<td>{{ count($group->accounts) }}</td>
						<td>{{ $row->created_at }}</td>
						
	                    <td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<button type="button" onclick="window.location.href='{{ route('user.mailing.group.edit', $row->group->id) }}'" class="tabledit-edit-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-eye"></span></button><button type="button" onclick="deleteConfirm('{{ route($route.'.remove-group', ['id'=>$id, 'group_id'=>$group->id]) }}', '{{ route($route.'.groups', $id) }}')" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-trash"></span></button>
	                           	</div>
                       		</div>
                       	</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	This email address is not found.
	@endif

	<div class="row">
		<div class="col-xs-1" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:100%">
				@if(isset($appends['limit']))
				<option value="{{ $appends['limit'] }}">{{ $appends['limit'] }}</option>
				@endif
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="40">40</option>
				<option value="50">50</option>
				<option value="60">60</option>
				<option value="70">70</option>
				<option value="80">80</option>
				<option value="90">90</option>
				<option value="100">100</option>
			</select>
		</div>
		<div class="col-xs-1">
			
		</div>
		<div class="col-xs-10">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
	
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Group</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-5">
                                       				<div class="chat-list-search">
														<input type="text" id="search-group" class="form-control form-control-rounded" placeholder="Type name of groups and press Enter">
													
													</div>
                                       				<div id="result">
                                       					
                                       				</div>
                                       			</div>
                                       			<div class="col-xs-7">
                                       				<div class="chat-area-header">
														<div class="clean">Existing groups in this email account</div>
													</div>
													<div id="selected">
                                       					
                                       				</div>
                                       			</div>
                                       			
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection