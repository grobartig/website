
<table id="seleted-table" class="table pop">
	<thead>
		<th width=10%>No.</th><th>Name.</th><th width=8%>.</th>
	</thead>
	<tbody>
		@php( $i=1 )
		@foreach($data as $row)
		<tr>
			<td>{{ $i++ }}</td><td>{{ $row->name }} ({{ count($row->accounts) }})</td><td><i onclick="removeGroupFromAccount({{ $row->id }})" class="fa fa-times"></i></td>
		</tr>
		@endforeach
	</tbody>
</table>
