@extends($route.'.main')
@section ('section-title', 'New Record')
@section ('section-css')
	
@endsection

@section ('imageuploadjs')
  
@endsection

@section ('section-js')
	<script type="text/javascript" src="{{ asset ('public/user/js/lib/blockUI/jquery.blockUI.js') }}"></script>
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}

				
			});

			$("#send").click(function(event){
				event.preventDefault();
				send({{$data->id}})
			})
		}); 

		var mail_id = 0;
		var num_of_account =0;
		var index =0;
		var falses =0;
		var succeses =0;
		var accounts = [];

		function send(record_id){
			@php ($accounts = $data->group->accounts)
			@php($i=0)
			@foreach( $accounts as $row )
				accounts[{{$i++}}] = ['{{ $row->id }}', '{{ $row->name }}', '{{ $row->email }}'];
			@endforeach
			$("#unsent").hide();
			$("#progress").show();
			$("#sending").show();
			sending(accounts[0][0]);

		}
		@if( $data->is_sent == 0 )
		
		function sending(mail_id){
			$("#btn-update").hide();
			if(mail_id != 0){
				$("#sending").html('<i class="fa fa-spinner fa-pulse  fa-fw"></i> Sending... to '+accounts[index][2]+'<br />'+(index+1)+'/'+accounts.length);
				$.ajax({
			        url: "{{ route($route.'.send-list-to-email-account') }}",
			        type: 'PUT',
			        data: { record_id: {{ $data->id }}, mail_id:mail_id, list_id: {{ $data->list->id }} },
			        success: function( response ) {
			          	if(response.status == 'success'){
			          		$("#progress").attr("value", index+1);
			          		console.log('Success:'+mail_id);
			          		succeses ++;
			          	}else{
			          		console.log('False:'+mail_id);
			          		falses++; 
			          	}

			          	index++; 
			          	var minuts = 1000;
		          		if( index == accounts.length ){
		          			mail_id =0;
		          			minuts =0;
		          		}else{
		          			mail_id = accounts[index][0];
		          			
		          		}
		          		
		          		setTimeout(function() {
							sending(mail_id);
						}, minuts);
			          	
			        },
			        error: function( response ) {
			           console.log('False:'+mail_id);
			        }
						
				});
			}else{
				$("#sending").html('Done!<br /> Success:'+succeses+' False:'+falses);
				$.ajax({
			        url: "{{ route($route.'.sent-done') }}",
			        type: 'POST',
			        data: { record_id: {{ $data->id }} },
			        success: function( response ) {
			          	console.log(response);
			          	swal("Nice!", "Sending has been finished." ,"success");
			        },
			        error: function( response ) {
			        	console.log('Error');
			        }
				})
			}
		}

		@endif

		function done(){
			$("#sending").html('Done!<br /> Success:'+succeses+' False:'+falses);
				$.ajax({
			        url: "{{ route($route.'.sent-done') }}",
			        type: 'POST',
			        data: { record_id: {{ $data->id }} },
			        success: function( response ) {
			          	console.log(response);
			          	swal("Nice!", "Sending has been finished." ,"success");
			        },
			        error: function( response ) {
			        	console.log('Error');
			        }
				})
		}

	</script>
@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('user.layouts.error')
		<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
			<div class="form-group row">
				<label for="action-id" class="col-sm-2 form-control-label">Account Group</label>
				<div class="col-sm-10">
					<p class="form-control">  {{ $data->group->name }} <a href="{{ route('user.mailing.group.accounts', $data->group->id) }}">({{ count($accounts) }} email accounts) </a></p>
				</div>
			</div>
			<div class="form-group row">
				<label for="action-id" class="col-sm-2 form-control-label">Property List</label>
				<div class="col-sm-10">
					<p class="form-control">{{ $data->list->name }} <a href="{{ route('user.mailing.list.properties', $data->list->id) }}">({{ count($data->list->properties) }} properties)</a></p>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Subject</label>
				<div class="col-sm-10">
					<input 	id="subject"
							name="subject"
							value = "{{ $data->subject }}"
							type="text"
							placeholder = "Subject"
							class="form-control"
						    data-validation="[L>=2, L<=200]"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Message</label>
				<div class="col-sm-10">
					<textarea 	id="message"
							name="message"
							type="text"
							placeholder = "Name"
							class="form-control"  >{{ $data->message }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" ></label>
				<div class="col-sm-10">
					Created by {{ $data->creator->name }} on {{ $data->created_at }}
					<br />
					@if( $data->is_sent == 0 )
						<p id="unsent">This record has not been sent to subscribers yet. Click <a id="send" href="#">HERE</a> to send!</p>
						<progress  id="progress" class="progress progress-striped active" value="0" max="{{ count($accounts) }}" style="display:none"></progress>
						<b id="sending" style="display:none"></b>
					@else
						<p id="unsent">The record has been sent on {{ $data->sent_at }}</p>
					@endif
				</div>
			</div>
			

			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					@if( $data->is_sent == 0 )
					<button type="submit" id="btn-update" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
					@endif
					<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', $data->id) }}', '{{ route($route.'.list') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
				</div>
			</div>
		</form>
	</div>

@endsection
