@extends($route.'.main')
@section ('section-title', 'New Record')
@section ('section-css')
	
@endsection

@section ('imageuploadjs')
  
@endsection

@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
		}); 
	</script>
@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('user.layouts.error')

		@php ($group_id = 0)
		@php ($list_id = 0)
		@php ($subject = '')
		@php ($message = '')

       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))

			@php ($group_id = $invalidData['group_id'])
			@php ($list_id = $invalidData['list_id'])
			@php ($subject = $invalidData['subject'])
			@php ($message = $invalidData['message'])

       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			
			<div class="form-group row">
				<label for="action-id" class="col-sm-2 form-control-label">Account Group</label>
				<div class="col-sm-10">
					<select id="group_id" name="group_id" class="form-control">
						@if($group_id != 0)
							@php( $lable = DB::table('groups')->find($group_id) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->name }} ({{ count($lable->mails) }})</option>
							@endif
						@endif
						<option value="0" >Select Group</option>
						@foreach( $groups as $row)
							@if($row->id != $group_id)
								<option value="{{ $row->id }}" >{{ $row->name }} ({{ count($row->mails) }})</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="action-id" class="col-sm-2 form-control-label">Property List</label>
				<div class="col-sm-10">
					<select id="list_id" name="list_id" class="form-control">
						@if($list_id != 0)
							@php( $lable = DB::table('lists')->find($list_id) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->name }}<</option>
							@endif
						@endif
						<option value="0" >Select List</option>
						@foreach( $lists as $row)
							@if($row->id != $list_id)
								<option value="{{ $row->id }}" >{{ $row->name }}  ({{ count($row->properties) }})</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Subject</label>
				<div class="col-sm-10">
					<input 	id="subject"
							name="subject"
							value = "{{ $subject }}"
							type="text"
							placeholder = "Subject"
							class="form-control"
						    data-validation="[L>=2, L<=200]"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Message</label>
				<div class="col-sm-10">
					<textarea 	id="message"
							name="message"
							type="text"
							placeholder = "Name"
							class="form-control"  >{{ $message }}</textarea>
				</div>
			</div>
			

			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection