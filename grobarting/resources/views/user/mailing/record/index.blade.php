@extends($route.'.main')
@section ('section-title', 'All')
@section ('display-btn-add-new', 'display:none')
@section ('section-css')

@endsection
@section ('section-js')
<script type="text/javascript">	
	$(document).ready(function(){
		$("#btn-search").click(function(){
			search();
		})
		$("#limit").change(function(){
			search();
		})
		
	})

    function search(){
		
		key 		= $('#key').val();
		creator 		= $('#creator').val();
		d_from 		= $('#from').val();
		d_till 		= $('#till').val();
		limit 		= $('#limit').val();

		url="?limit="+limit;
		
		if(key != ""){
			url += '&key='+key;
		}
		if(creator != 0){
			url += '&creator='+creator;
		}
		if(isDate(d_from)){
			if(isDate(d_till)){
				url+='&from='+d_from+'&till='+d_till;
			}
		}
		
		$(location).attr('href', '{{ route($route.'.index') }}'+url);
	}





</script>
@endsection

@section ('section-content')
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<input id="key" type='text' class="form-control" value="{{ isset($appends['key'])?$appends['key']:'' }}" placeholder="Subject" />
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<select id="creator" class="form-control">
					@php( $creator = isset($appends['creator'])?$appends['creator']:0 )
					@if($creator != 0)
						@php( $lable = DB::table('users')->find($creator) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->name }}</option>
						@endif
					@endif
					<option value="0" >Creator</option>
					@foreach( $creators as $row)
						@if($row->id != $creator)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
				
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<div id="till-cnt" class='input-group date' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-1">
			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
		</div>
	</div><!--.row-->

	@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Subject</th>
					<th>Account Groups</th>
					<th>Property List</th>
					<th>Status</th>
					<th>Created By</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $row->subject }}</td>
						<td>{{ $row->group->name }} ({{ count($row->group->mails) }})</td>
						<td>{{ $row->list->name }} ({{ count($row->list->properties) }})</td>
						<td>
							<div class="checkbox-toggle">
						        <input  type="checkbox" id="status-{{ $row->id }}" @if ($row->is_sent == 1) checked data-value="1" @else data-value="0" @endif >
						        <label for="status-xx"></label>
					        </div>
						</td>
						<td>{{ $row->created_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<a href="{{ route($route.'.edit', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
	                           		<a href="#" onclick="deleteConfirm('{{ route($route.'.trash', $row->id) }}', '{{ route($route.'.index') }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="fa fa-trash"></span></a>
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	<span>No Data</span>
	@endif
	<div class="row">
		<div class="col-xs-1" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:100%">
				@if(isset($appends['limit']))
				<option>{{ $appends['limit'] }}</option>
				@endif
				<option>10</option>
				<option>20</option>
				<option>30</option>
				<option>40</option>
				<option>50</option>
				<option>60</option>
				<option>70</option>
				<option>80</option>
				<option>90</option>
				<option>100</option>
			</select>
		</div>
		<div class="col-xs-1">
			
		</div>
		<div class="col-xs-10">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
	
@endsection