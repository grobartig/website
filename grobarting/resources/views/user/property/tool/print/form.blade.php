<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Print Form</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Kanit&subset=thai,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset ('public/user/css/print.css') }}">

</head>

<body class="">
    <div class="container">
        <div class="row margin10">
            <div class="col-xs-12 header_property">
                <div class="col-xs-12 ">
                    <div class=" col-xs-12  wrap_header">
                        <div class="col-xs-2">
                           <img src="{{ asset ($data->agent->picture) }}" class="img img-responsive" />
                        </div>
                        <div class="col-xs-7 info_header">
                            <h4>{{ $data->agent->name }}</h4>
                            <div class="right_detail float_left">
                                <ul>
                                    <li>ID: {{ $data->agent->id }}</li>
                                    <li>Phnone: {{ $data->agent->phone }}</li>
                                    <li>Email: {{ $data->agent->email }}</li>
                                    <li>Language: {{ $data->agent->language }}</li>
                                </ul>
                            </div>

                            <div class="right_detail float_right">
                                <ul>
                                    <li>Phnone:(+855) 77 813 111 / (+855) 98 288 966</li>
                                    <li>Email:info@yaorealty.com</li>
                                    <li>Address: #331, Street 271, Sangkat Toul Tom Poung II</li>
                                    <li>Khan Chamkamon, Phnom Penh (4th Floors, Suite #403)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="ol-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="my_padding5">
                                <img src="{{ asset ('public/user/img/print/logo.png') }}" class="img img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-12">
                <div class="col-lg-12 my_padding10">
                    <h4>MLS@No. {{ $data->listing_code }}</h4></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <img src="{{ asset ('public/user/img/print/property.jpg') }}" class="img img-responsive" />
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <img src="{{ asset ('public/user/img/print/map.png') }}" class="img img-responsive" />
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 my_table_short">
                    <div class="right_detail">
                        <h4>$188,704 + GST/QST</h3>
          <h5>Lachine (Montreal)</h5>
          <h5>H854C9</h5>
          </div>
      <div class="table-responsive">
            <table class="table table-striped">

              <tbody>
                <tr>
                  <td>Region</td>
                  <td>Montreal</td>

                </tr>
                <tr>
                  <td>Neighbourhood</td>
                  <td>East</td>

                </tr>
                <tr>
                  <td>Body of water</td>
                  <td></td>

                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>

                </tr>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>   
  <div class="row">        
    <div class="col-xs-12"> 
    <div class="col-xs-12">
    <div class="table-responsive my_table">
            <table class="table table-striped">

              <tbody>
                <tr>

                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                </tr>
                <tr>

                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>

                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>

                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>

                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>

                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>

                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>

                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>

                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>

                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>

                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>

                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>

                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>

                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>

                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>

                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
    </div>
  </div>
    <div class="row">        
    <div class="col-xs-12"> 
    <div class="col-xs-12">
    <div class="table-responsive my_table">
            <table class="table table-striped">

              <tbody>
                <thead>
                 <tr>
                   <th>Municipal Assesment</th>
                   <th></th>
                   <th>Taxes(annual)</th>
                   <th></th> 
                   <th>Expenses/Energy(annual)</th> 
                   <th></th> 
                  </tr>
                 </thead>
                <tr>

                  <td>Year</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>
               <tr>

                  <td>Year</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>
                <tr>

                  <td>Year</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>
                <tr>

                  <td>Year</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>
                <tr>

                  <td>Year</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>
                <tr>

                  <td>Year</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>
                <tr>

                  <td>Total</td>
                  <td>not issued</td>
                  <td>Municipal</td>
                  <td>not issued</td>
                  <td>condo fees($95/month)</td>
                  <td>$1,152</td>

                </tr>

              </tbody>
            </table>
        </div>
    </div>
    </div>
  </div>
    <div class="row">        
    <div class="col-xs-12"> 
    <div class="col-xs-12 ">
    <div class="table-responsive my_table">
            <table class="table table-striped">
            <h3>Room(s) and Outdoor Feature(s)</h3>
             <thead>
                 <tr>
                   <th>No.of Rooms 5</th>
                   <th></th>
                   <th>No.of Bedrooms 2+0</th>
                   <th></th>
                   <th>No.of Bathrooms and Powder Rooms 1+0</th>
                 </tr>
              </thead>
             <thead>
                 <tr>
                   <th>Level</th>
                   <th>Room</th>
                   <th>Size</th>
                   <th>Floor Covering</th> 
                   <th>Additional Information</th> 

                  </tr>
                 </thead>
              <tbody>
                <tr>

                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                   <td>Lorem</td>
                </tr>
                <tr>

                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                   <td>Lorem</td>
                </tr>
                <tr>

                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                   <td>Lorem</td>
                </tr>
                <tr>

                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                   <td>Lorem</td>
                </tr>
                <tr>

                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                   <td>Lorem</td>
                </tr>

              </tbody>
            </table>
        </div>
    </div>
    </div>
  </div>
    <div class="row">        
    <div class="col-xs-12"> 
    <div class="col-xs-12">
    <div class="table-responsive my_table">
            <table class="table table-striped">
            <h3>Features</h3>
              <tbody>
                <tr>
                  <td>Sewage System</td>
                  <td>Municiplity</td>
                  <td>Renied Equip.(monthly)</td>
                </tr>

              </tbody>
            </table>
        </div>
    </div>
    </div>
  </div>
  <div class="row ">        
    <div class="col-xs-12">
      <div class="col-xs-12 footer"> 
        <div style="float:left;font-size:17px;padding-top:15px;font-weight:700;">
              <div>Prepared by:.........................</div>
            </div>
           <div style="float:right;">
              <div style="float:right;" >MLS@No 10321 page 1 of 3</div><br>
              <div style="float:right;">Centrey@resoursce</div>
            </div>
          </div>
    </div>
  </div>

</body>
</html>