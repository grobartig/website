@extends('user.property.tool.tab')
@section ('section-title', 'Printing Form')
@section ('tool-active-print', 'active')
@section ('tab-css')
	
@endsection


  

@section ('tab-js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
	<script type="text/JavaScript">
		$(document).ready(function(){
			
		})

		function findHTML(){
			source = $('#fromHTMLtestdiv').contents().find("html").html();
			console.log(source);
		}
		function demoFromHTML() {
			var pdf = new jsPDF('p', 'pt', 'letter')

			// source can be HTML-formatted string, or a reference
			// to an actual DOM element from which the text will be scraped.
			, source = $('#fromHTMLtestdiv').contents().find("html").html()

			// we support special element handlers. Register them with jQuery-style 
			// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
			// There is no support for any other type of selectors 
			// (class, of compound) at this time.
			, specialElementHandlers = {
				// element with id of "bypass" - jQuery style selector
				'#bypassme': function(element, renderer){
					// true = "handled elsewhere, bypass text extraction"
					return true
				}
			}

			margins = {
		      top: 80,
		      bottom: 60,
		      left: 40,
		      width: 522
		    };
		    // all coords and widths are in jsPDF instance's declared units
		    // 'inches' in this case
		    pdf.fromHTML(
		    	source // HTML string or DOM elem ref.
		    	, margins.left // x coord
		    	, margins.top // y coord
		    	, {
		    		'width': margins.width // max width of content on PDF
		    		, 'elementHandlers': specialElementHandlers
		    	},
		    	function (dispose) {
		    	  // dispose: object with X, Y of the last line add to the PDF 
		    	  //          this allow the insertion of new lines after html
		          pdf.save('Test.pdf');
		        },
		    	margins
		    )
		}
	</script>
@endsection

@section ('tool')
	<br />
	<div class="row">
		
		
		<div class="col-md-4">
			<select class="form-control">
				<option>Khmer</option>
				<option>English</option>
				<option>Chinese</option>
			</select>
		</div>
		<div class="col-md-4">
			<button   class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-print"></span></button>
			<button   class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-file-pdf-o"></span></button>
			<a href="{{ route($route.'.form', $id) }}" target="_blank"   class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-link"></span></a>
		</div>
	</div><!--.row-->
	<div  width=100% style="width:100%; margin-top:8px">
		<iframe id="fromHTMLtestdiv" width=100% height=500px src="{{ route($route.'.form', $id) }}"></iframe>
	</div>
@endsection