@extends($route.'.toolTab')
@section ('section-title', 'List')
@section ('tool-active-mailing', 'active')
@section ('tab-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}
	</style>
@endsection

@section ('tab-js')
	<script type="text/javaScript">
		$(document).ready(function(){
			
			$("#btn-search").click(function(){
				search();
			})
			$("#btn-add").click(function(){
				addPopUp();
			})
			$("#limit").change(function(){
				search();
			})
				
		})

		function search(){
			key 		= $('#key').val();
			d_from 		= $('#from').val();
			d_till 		= $('#till').val();
			limit 		= $('#limit').val();

			url="?type=mailing&limit="+limit;
			
			if(key != ""){
				url += '&key='+key;
			}
			if(isDate(d_from)){
				if(isDate(d_till)){
					url+='&from='+d_from+'&till='+d_till;
				}
			}
			
			$(location).attr('href', '{{ route($route.'.tool', $id) }}'+url);
		}
		function addPopUp(){
			$('#modal').modal('show')
		}
	</script>
@endsection

@section ('tool')
	<br />
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<input id="key" type='text' class="form-control" value="{{ isset($appends['key'])?$appends['key']:'' }}" placeholder="code" />
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="till-cnt" class='input-group date' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
			<button id="btn-add" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></button>
			<button id="btn-refresh" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-refresh"></span></button>
		</div>
	</div><!--.row-->
	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Email</th>
					<th>Sent By</th>
					<th>Sent Date&Time</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $row->mail->name }}</td>
						<td>{{ $row->mail->email }}</td>
						<td>{{ $row->sender->name }}</td>
						<td>{{ $row->created_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<button type="button" onclick="window.location.href='{{ route('user.mailing.account.edit', $row->mail->id) }}'" class="tabledit-edit-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-eye"></span></button>
	                           	</div>
                       		</div>
                       	</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	<div class="row">
		<div class="col-xs-2" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:60%">
				@if(isset($appends['limit']))
				<option value="{{ $appends['limit'] }}">{{ $appends['limit'] }}</option>
				@endif
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
				<option value="40">40</option>
				<option value="50">50</option>
				<option value="60">60</option>
				<option value="70">70</option>
				<option value="80">80</option>
				<option value="90">90</option>
				<option value="100">100</option>
			</select>
		</div>
		
		<div class="col-xs-10">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
	<br />
	@else
	This property has never been sent to any account.
	@endif
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Send Mail</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		Content
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection
