@extends('user.property.tool.tab')
@section ('section-title', 'Printing Form')
@section ('tool-active-status', 'active')
@section ('tab-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection



@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
	
		});

	
	</script>

@endsection

@section ('tool')
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update', $id) }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="property_id" value="{{ $id }}">
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Contract Type</label>
				<div class="col-sm-3">
					<div class="radio  margin-top-10">
						<input type="radio" name="contract_type_id" id="contract-type-1" value="1" @if($data->contract_type_id == 1 ) checked @endif >
						<label for="contract-type-1">Non exclusive</label>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="radio  margin-top-10">
						<input type="radio" name="contract_type_id" id="contract-type-2" value="2" @if($data->contract_type_id == 2 ) checked @endif >
						<label for="contract-type-2">Exclusive</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" ></label>
				<div class="col-sm-3 exculive-date-cnt" >
					<div class="form-group">
						<div id="from-cnt" class='input-group date'>
							<input id="from" name="exclusive_from" type='text' class="form-control" value="{{ $data->exclusive_from }}" placeholder="From"  />
						<span class="input-group-addon">
							<i class="font-icon font-icon-calend"></i>
						</span>
						</div>
					</div>
				</div>
				<div class="col-sm-3 exculive-date-cnt">
					<div class="form-group">
						<div id="till-cnt" class='input-group date'>
							<input id="till" name="exclusive_till" type='text' class="form-control" value="{{ $data->exclusive_till }}" placeholder="Till" />
						<span class="input-group-addon">
							<i class="font-icon font-icon-calend"></i>
						</span>
						</div>
					</div>
				</div>
			</div>

			
			<div class="form-group row">
				@php( $is_published 				= $data->is_published )
				@php( $is_featured 					= $data->is_featured )
				@php( $is_slided 					= $data->is_slided )
				@php( $is_best_dealed 				= $data->is_best_dealed )
				@php( $is_project_developmented 	= $data->is_project_developmented )
				@php( $feature_image 				= $data->media->feature )
				@php( $slide_image 					= $data->media->slide )

				<label class="col-sm-2 form-control-label" for="kh_content">Publish</label>
				<div class="col-sm-1">
					<div class="checkbox-toggle">
						<input  id="is_published-status" type="checkbox"  @if($is_published ==1 ) checked @endif @if( $feature_image == "") disabled @endif>
						<label @if( $feature_image != "") onclick="booleanForm('is_published')"  for="is_published-status"  @endif></label>
					</div>
					<input type="hidden" name="is_published" id="is_published" value="{{ $is_published }}">
				</div>
				<div class="col-sm-9">
					@if( $feature_image == "")<p>Property cannot be published because it does not have feature image.</p>@endif
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Feature</label>
				<div class="col-sm-1">
					<div class="checkbox-toggle">
						<input id="is_featured-status" type="checkbox"  @if($is_featured ==1 ) checked @endif @if( $is_published == 0) disabled @endif  >
						<label onclick="booleanForm('is_featured')" for="is_featured-status"></label>
					</div>
					<input type="hidden" name="is_featured" id="is_featured" value="{{ $data->is_featured }}">
				</div>
				<div class="col-sm-9">
					@if( $feature_image == "")<p></p>@endif
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Slide</label>
				<div class="col-sm-1">
					<div class="checkbox-toggle">
						<input id="is_slided-status" type="checkbox"  @if($is_slided ==1 & $slide_image!="" ) checked @endif @if( $is_published == 0) disabled @endif @if( $slide_image == "") disabled @endif >
						<label onclick="booleanForm('is_slided')" for="is_slided-status"></label>
					</div>
					<input type="hidden" name="is_slided" id="is_slided" value="{{ $is_slided }}">
				</div>
				<div class="col-sm-9">
					@if( $is_slided == "")<p></p>@endif
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Best Deal</label>
				<div class="col-sm-1">
					<div class="checkbox-toggle">
						<input id="is_best_dealed-status" type="checkbox"  @if($is_best_dealed ==1 ) checked @endif>
						<label onclick="booleanForm('is_best_dealed')" for="is_best_dealed-status"></label>
					</div>
					<input type="hidden" name="is_best_dealed" id="is_best_dealed" value="{{ $is_best_dealed }}">
				</div>
				<div class="col-sm-9">
					@if( $is_best_dealed == "")<p></p>@endif
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Project Development</label>
				<div class="col-sm-1">
					<div class="checkbox-toggle">
						<input id="is_project_developmented-status" type="checkbox"  @if($is_project_developmented ==1 ) checked @endif>
						<label onclick="booleanForm('is_project_developmented')" for="is_project_developmented-status"></label>
					</div>
					<input type="hidden" name="is_project_developmented" id="is_project_developmented" value="{{ $is_project_developmented }}">
				</div>
				<div class="col-sm-9">
					@if( $is_project_developmented == "")<p></p>@endif
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					@if(checkRole($id, 'update-overview'))
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
					@endif
					
				</div>
			</div>
	</form>
	
@endsection