@extends('user.property.tabForm')
@section ('tab-active-tool', 'active')

@section ('tab-content')
	
	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				@if(checkRole($id, 'view-status'))<li><a href="{{ route('user.property.tool.status.index', $id) }}" class="@yield ('tool-active-status')">Status</a></li>@endif
				<!-- @if(checkRole($id, 'view-print'))<li><a href="{{ route('user.property.tool.print.index', $id) }}" class="@yield ('tool-active-print')">Print Form</a></li>@endif
				@if(checkRole($id, 'view-listing'))<li><a href="{{ route('user.property.tool.listing.index', $id) }}" class="@yield ('tool-active-list')">List</a></li>@endif
				@if(checkRole($id, 'view-mailing'))<li><a href="#" class="@yield ('tool-active-mailing')">Mailing</a></li>@endif
				@if(checkRole($id, 'view-facebook'))<li><a href="#" class="@yield ('tool-active-facebook')">Facebook Post</a></li>@endif -->
			</ul>
		</nav><!--.files-manager-side-->

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('tool')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection