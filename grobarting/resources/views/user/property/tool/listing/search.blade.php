@if(count($data)>0)
<table id="pop-table" class="table pop">
	<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{ $row->code }}</td><td>{{ $row->name }} ({{count($row->properties)}})</td><td width=8%><i @if( $id != 0) onclick="addListToProperty({{$row->id}})" @else onclick="addPropertiesToList({{$row->id}})" @endif class="fa fa-plus"></i></td>
			</tr>
		@endforeach
		
	</tbody>
</table>
@else
	No Data Avaiable
@endif
