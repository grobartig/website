<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Property Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Hanuman" rel="stylesheet">
    <style type="text/css">
        html,
        body {
            font-family: Hanuman;
        }
    </style>

</head>
<div class="container">
    <div class="row text-center">
        <h2> Y.A.O Realty Co., Ltd. </h2>
    </div>
    <!--
    <div class="row">
        <div class="col-xs-1"> Report </div>
        <div class="col-xs-11"> Property Listing</div>
    </div>
    <div class="row">
        <div class="col-xs-1"> Date </div>
        <div class="col-xs-11"> Unset </div>
    </div>
    <div class="row">
        <div class="col-xs-1"> Type </div>
        <div class="col-xs-11"> Regisgered and Unregistered</div>
    </div>
    -->
    <br />
    <div class="row">
        <table class="table table-bordered ">
            <thead class="text-center">
                <tr>
                    <th>#</th>
                    <th width=9%>Listing Code</th>
                    <th width=9%>Tel. Number</th>
                    <th width=9%>Action</th>
                    <th width=9%>Price($)</th>
                    <th>Address</th>
                    <th>Size</th>
                    <th width=9%>Cadaster Type</th>
                    <th>Zip Code (Google map)</th>
                </tr>
            </thead>
            <tbody>
                @php ($i = 1)
                @foreach($data as $row)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>
                            @php( $l = '')
                            @if($row->is_published == 0)
                                @php( $l = 'L')
                            @endif
                            @php( $display_listing_code = $l.$row->province->abbre.'-'.$row->type->abbre.$row->listing_code )
                            {{$display_listing_code}}
                        </td>
                        <td>
                            @if( ! is_null($row->owner) )
                                {{ $row->owner->phone }}
                            @endif
                        </td>
                        <td> {{$row->action->en_name}} </td>
                        <td>
                            @if($row->action_id == 1 )
                                {{$row->price->selling_price}} @if( ! is_null($row->price->selling_price_type) ) ({{$row->price->selling_price_type->name}}) @endif
                            @elseif( $row->action_id == 2 )
                                {{$row->price->rental_price}} @if( ! is_null($row->price->rental_price_type) ) ({{$row->price->rental_price_type->name}}) @endif
                            @else
                                Sell: {{$row->price->selling_price}} @if( ! is_null($row->price->selling_price_type) ) ({{$row->price->selling_price_type->name}}) @endif<br />
                                Rent: {{$row->price->rental_price}} @if( ! is_null($row->price->rental_price_type) ) ({{$row->price->rental_price_type->name}}) @endif
                            @endif
                        </td>
                        <td>{{ $row->location->address }}</td>
                        <td></td>
                        <td>
                            @if($row->cadaster_id == 1 )
                                Soft
                            @else
                                Hard
                            @endif
                        </td>
                        <td>{{ $row->location->zip_code }}</td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>

</body>

</html>