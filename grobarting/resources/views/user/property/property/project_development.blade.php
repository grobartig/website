@extends($route.'.main')
@section ('section-title', 'Project Development Properties')
@section ('section-css')

@endsection
@section ('section-js')
<script type="text/javascript">	
	var source;
	function isbefore(a, b) {
	    if (a.parentNode == b.parentNode) {
	        for (var cur = a; cur; cur = cur.previousSibling) {
	            if (cur === b) {
	                return true;
	            }
	        }
	    }
	    return false;
	}

	function dragenter(e) {
		
	    var targetelem = e.target;
	    //console.log(e);
	    if (targetelem.nodeName == "TD") {
	       targetelem = targetelem.parentNode;   
	    }  
	    
	    if (isbefore(source, targetelem)) {
	        targetelem.parentNode.insertBefore(source, targetelem);
	        //console.log('moved :'+order);
	    } else {
	        targetelem.parentNode.insertBefore(source, targetelem.nextSibling);

	    }
	}

	function dragstart(e) {
	    source = e.target;
	    e.dataTransfer.effectAllowed = 'move';

	}
	function dragend(e){
		//console.log(e.target);
		elements = $(".moveable");
		//console.log(elements);
		data = [];
		for(i=0; i<elements.length; i++){
			var obj = new Object();
			obj.id = $('#'+elements[i].id).attr('data-id');
			obj.order = i+1;
			data[i] = obj;
		}
		
		var string = JSON.stringify(data);
		console.log(string);
		$.ajax({
		        url: "{{ route('user.property.property.re-order-project-development') }}",
		        type: 'POST',
		        data: {string:string},
		        success: function( response ) {
		         	console.log(response);
				    
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
					
		});
	}
	function updateStatus(id){
     	
     	$.ajax({
	        url: "{{ route($route.'.property.remove-project-development') }}",
	        method: 'POST',
	        data: {id:id },
	        success: function( response ) {
	            if ( response.status === 'success' ) {
	            	
	            	window.location.replace("{{ route($route.'.property.project_development') }}");
	            	
	            }else{
	            	swal("Error!", "Sorry there is an error happens. " ,"error");
	            }
	        },
	        error: function( response ) {
	           swal("Error!", "Sorry there is an error happens. " ,"error");
	        }
		});
	}

</script>
@endsection

@section ('section-content')
	
	@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Code</th>
					<th>Action</th>
					<th>Type</th>
					<th>Title</th>
					<th>Feature</th>
					<th></th>
				</tr>
			</thead>
			<tbody ondrop="alert('ddd')">

				@php ($i = 1)
				@foreach ($data as $row)
					<tr id="element-{{ $row->id }}" data-id="{{ $row->id }}" class="moveable" feature-order="{{ $row->featured_order }}" draggable="true" ondragenter="dragenter(event)" ondragstart="dragstart(event)" ondragend="dragend(event)">
						<td>{{ $i++ }}</td>
						<td>{{ $row->listing_code }}</td>
						<td>{{ $row->action->en_name }}</td>
						<td>{{ $row->type->en_name }}</td>
						<td>{{ $row->en_name }}</td>
						<td>
							<div class="checkbox-toggle">
						        <input onclick="updateStatus({{ $row->id }})" type="checkbox" id="status-{{ $row->id }}" @if ($row->is_featured == 1) checked data-value="1" @else data-value="0" @endif >
						        <label for="status-{{ $row->id }}"></label>
					        </div>
						</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<a href="{{ route($route.'.property.edit', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	<span>No Data</span>
	@endif


@endsection