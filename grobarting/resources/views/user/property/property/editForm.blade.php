@extends($route.'.tabForm')
@section ('section-title', 'Overview')
@section ('tab-active-edit', 'active')
@section ('tab-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection


@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
	
		});

	
	</script>

@endsection

@section ('tab-content')
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.property.update') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
			<input type="hidden" name="listing_code" value="{{ $data->listing_code }}">
			<div class="form-group row">
				<label for="action-id" class="col-sm-2 form-control-label">Action</label>
				<div class="col-sm-10">
					<select id="action-id" name="action-id" class="form-control">
						<option value="{{ $data->action_id }}" >{{ $data->action->en_name }}</option>
						@foreach( $actions as $row)
							@if($row->id != $data->action_id)
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="type-id" class="col-sm-2 form-control-label">Type</label>
				<div class="col-sm-10">
					<select id="type-id" name="type-id" class="form-control">
						<option value="{{ $data->type_id }}" >{{ $data->type->en_name }} ({{ $data->type->abbre }})</option>
						@foreach( $types as $row)
							@if($row->id != $data->type_id)
								<option value="{{ $row->id }}" >{{ $row->name }} ({{ $row->abbre }})</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Cadaster Type</label>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="cadaster_id" id="radio-1" value="1" @if($data->cadaster_id == 1 ) checked @endif >
						<label for="radio-1">Soft</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="cadaster_id" id="radio-2" value="2" @if($data->cadaster_id == 2 ) checked @endif >
						<label for="radio-2">Hard</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="cadaster_id" id="radio-3" value="3" @if($data->cadaster_id == 3 ) checked @endif >
						<label for="radio-3">N/A</label>
					</div>
				</div>
				
			</div>
		

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name (Kh)</label>
				<div class="col-sm-10">
					<input 	id="kh-name"
							name="kh-name"
							value = "{{ $data->kh_name }}"
							type="text"
							placeholder = "Name of property in Khmer"
							class="form-control"
						    data-validation="[L>=2, L<=200]"  />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name (En)</label>
				<div class="col-sm-10">
					<input 	id="en-name"
							name="en-name"
							value = "{{ $data->en_name }}"
							type="text"
							placeholder = "Name of property in English"
						   	class="form-control"
						   	data-validation="[L>=2, L<=200]" />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name (Cn)</label>
				<div class="col-sm-10">
					<input 	id="cn-name"
							name="cn-name"
							value = "{{ $data->cn_name }}"
							type="text"
							placeholder = "Name of property in Chinese"
						   	class="form-control"
						   	data-validation="[L>=2, L<=200]" />
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Excerpt (Kh)</label>
				<div class="col-sm-10">
					<textarea id="kh-excerpt"
							name="kh-excerpt"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="">{{ $data->kh_excerpt }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Excerpt (En)</label>
				<div class="col-sm-10">
					<textarea id="en-excerpt"
							name="en-excerpt"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="">{{ $data->en_excerpt }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Excerpt (Cn)</label>
				<div class="col-sm-10">
					<textarea id="cn-excerpt"
							name="cn-excerpt"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="">{{ $data->cn_excerpt }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Description (Kh)</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="kh-description" name="kh-description" class="form-control summernote ">{{ $data->kh_description }}</textarea>
					</div>	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Description (En)</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="en-description" name="en-description" class="form-control summernote ">{{ $data->en_description }}</textarea>
					</div>	
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Description (Cn)</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="cn-description" name="cn-description" class="form-control summernote ">{{ $data->cn_description }}</textarea>
					</div>	
				</div>
			</div>
	
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					@if(checkRole($id, 'update-overview'))
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
					@endif
					@if(checkRole($id, 'delete-overview'))
					<button type="button" onclick="deleteConfirm('{{ route($route.'.property.trash', $data->id) }}', '{{ route($route.'.property.index') }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>
					@endif
				</div>
			</div>
	</form>
	
@endsection