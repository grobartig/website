@extends('user.property.main')
@section ('section-title', 'Search Properties (Map)')
@section ('display-btn-add-new', 'display:none')
@section ('section-css')
	<style type="text/css">
		#map{
			width:100%;
			height:700px;
			background:red;
		}
	</style>
@endsection
@section ('section-js')
<script type="text/javascript" src="{{ asset ('public/user/js/lib/blockUI/jquery.blockUI.js') }}"></script>
<script type="text/javascript">	
	$(document).ready(function(){
		$("#btn-search").click(function(){
			search();
		})
		$("#limit").change(function(){
			search();
		})
		$("#province-id").change(function(){
			getDistricts($(this).val());
		})
		$('#district-id').change(function(){
    		getCommunes($(this).val());
    	})

    	//============check
    	$("#check-all").click(function(){
    		checkAll();
    	})
    	$(".check-list-item").click(function(){
    		checkItem($(this).attr('data-id'));
    	})
    	$("#btn-list").click(function(){
    		createList();
    	})

    	$("#search").keypress(function(e) {
			    if(e.which == 13) {
			       displayAvaiableLists();
			    }
			});

    	//==========>> Add To List
    	$("#btn-add-to-list").click(function(){
    		$('#modal').modal('show');
    		displaySelectedProperties();
    		displayAvaiableLists();
    	})
	})

    function search(print=0){
		action 		= $('#action-id').val();
		type 		= $('#type-id').val();
		province 	= $('#province-id').val();
		district 	= $('#district-id').val();
		commune 	= $('#commune-id').val();
		road 		= $('#road').val();
		listing_code = $('#listing_code').val();
		min 		= $('#min').val();
		max 		= $('#max').val();

		d_from 		= $('#from').val();
		d_till 		= $('#till').val();
		limit 		= $('#limit').val();

		url="?print="+print+"&limit="+limit;
		if(action != 0){
			url += '&action='+action;
		}
		if(type != 0){
			url += '&type='+type;
		}

		if(listing_code != ""){
			url += '&listing_code='+listing_code;
		}
		if(min != max && min < max){
			if(min > 0){
				url += '&min='+min;
			}
			if(max > 0){
				url += '&max='+max;
			}
		}

		if(province != 0){
			url += '&province='+province;
			if(district != 0){
				url += '&district='+district;
				if(commune != 0){
					url += '&commune='+commune;
				}
			}
		}
		if(road != ""){
			url += '&road='+road;
		}

		
		

		if(isDate(d_from)){
			if(isDate(d_till)){
				url+='&from='+d_from+'&till='+d_till;
			}
		}
		if(print == 1){
			window.open('{{ route($route.'.property.index') }}'+url, '_blank');
		}else{
			$(location).attr('href', '{{ route($route.'.property.index') }}'+url);
		}
		
	}

	function getDistricts(province_id){
			//Empty the district
			$("#district-id").html('<option value="0" >District</option>');
			//Empty the communes
			$("#commune-id").html('<option value="0" >Commune</option>');
			
			//Get Districts
			if(province_id != 0){
				$.ajax({
				        url: "{{ route('user.property.districts') }}?province_id="+province_id,
				        type: 'GET',
				        data: {},
				        success: function( response ) {
				           var districts = '';
				            var i;
						    var length = response.length;
						    for (i = 0; i < length; i++) {
						        districts += '<option value="'+response[i].id+'" >'+response[i].name+'</option>';
						    }
						    if(districts != ""){
						    	$('#district-id').append(districts);
						    	
						    }
						    
				        },
				        error: function( response ) {
				           swal("Error!", "Sorry there is an error happens. " ,"error");
				        }
							
				});
			}
	}

	function getCommunes(district_id){
		//Empty the communes
		$("#commune-id").html('<option value="0" >Commune</option>');

		if(district_id != 0){
			$.ajax({
			        url: "{{ route('user.property.communes') }}?district_id="+district_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var communes = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        communes += '<option value="'+response[i].id+'" >'+response[i].name+'</option>';
					    }
					    if(communes != ""){
					    	$('#commune-id').append(communes);
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
	}
	
	
</script>
@endsection

@section ('section-content')
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<select id="action-id" class="form-control">
					@php( $action = isset($appends['action'])?$appends['action']:0 )
					@if($action != 0)
						@php( $lable = DB::table('actions')->find($action) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
						@endif
					@endif
					<option value="0" >Action</option>
					@foreach( $actions as $row)
						@if($row->id != $action)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
				
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<select id="type-id" class="form-control">
					@php( $type = isset($appends['type'])?$appends['type']:0 )
					@if($type != 0)
						@php( $lable = DB::table('types')->find($type) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
						@endif
					@endif
					<option value="0" >Type</option>
					@foreach( $types as $row)
						@if($row->id != $type)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<select id="province-id" class="form-control">
					@php( $province = isset($appends['province'])?$appends['province']:0 )
					@if($province != 0)
						@php( $lable = DB::table('provinces')->find($province) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
						@endif
					@endif
					<option value="0" >Province</option>
					@foreach( $provinces as $row)
						@if($row->id != $province)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<select id="district-id" class="form-control">
					@php( $district = isset($appends['district'])?$appends['district']:0 )
					@if($province != 0)
						@php( $districts = DB::table('districts')->where('province_id', $province)->get() )
						@if($district != 0)
							@php( $lable = DB::table('districts')->find($district) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
							@endif
							<option value="0" >District</option>
							@foreach( $districts as $row)
								@if($row->id != $district)
									<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
								@endif
							@endforeach
						@else
							<option value="0" >District</option>
							@foreach( $districts as $row)
								<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
							@endforeach
						@endif
					@else
						<option value="0" >District</option>
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<select id="commune-id" class="form-control">
					@php( $commune = isset($appends['commune'])?$appends['commune']:0 )
					@if($province != 0)
						@if($district != 0)
							@php( $communes = DB::table('communes')->where('district_id', $district)->get() )
							@if($commune != 0)
								@php( $lable = DB::table('communes')->find($commune) )
								@if( sizeof($lable) == 1 )
									<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
								@endif
								<option value="0" >Commune</option>
								@foreach( $communes as $row)
									@if($row->id != $commune)
										<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
									@endif
								@endforeach
							@else
								<option value="0" >Commune</option>
								@foreach( $communes as $row)
									<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
								@endforeach
							@endif
						@else
							<option value="0" >Commune</option>
						@endif
					@else
						<option value="0" >Commune</option>
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input id="road" type='text' class="form-control" value="{{ isset($appends['road'])?$appends['road']:'' }}" placeholder="Road" />
			</div>
		</div>
	</div><!--.row-->
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<input id="listing_code" type='text' class="form-control" value="{{ isset($appends['listing_code'])?$appends['listing_code']:'' }}" placeholder="Listing Code" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input id="min" type='number' min=0 class="form-control" value="{{ isset($appends['min'])?$appends['min']:'' }}" placeholder="Min Price" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input id="max" type='number' min=0 class="form-control" value="{{ isset($appends['max'])?$appends['max']:'' }}" placeholder="Max Price" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<div id="till-cnt" class='input-group date ' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
			@if(checkPermision('user.property.property.print'))
			<button onclick="search(1)"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-print"></span></button>
			@endif
		</div>
	</div><!--.row-->

	@if(sizeof($data) > 0)
		<div id="map" class="map"></div>
	@else
	<span>No Data</span>
	@endif

	<div class="row">
		<div class="col-xs-1" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:100%">
				@if(isset($appends['limit']))
				<option>{{ $appends['limit'] }}</option>
				@endif
				<option>10</option>
				<option>20</option>
				<option>30</option>
				<option>40</option>
				<option>50</option>
				<option>60</option>
				<option>70</option>
				<option>80</option>
				<option>90</option>
				<option>100</option>
			</select>
		</div>
	
		<div class="col-xs-9">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
@endsection
