@extends($route.'.main')
@section ('section-title', 'Create New Property')
@section ('section-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection



@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 

			
			$("#province-id").change(function(){
				codeGenerator()
			})
			
		}); 

	
		
	</script>


@endsection

@section ('section-content')
	<div class="container-fluid">
		
		@include('user.layouts.error')
		@if(isset($_GET['listing_code']))
			<div class="alert alert-aquamarine alert-no-border alert-close alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<strong>Hey!</strong><br>
				You have just created a property which has listing code :<b>{{ $_GET['listing_code'] }}</b>
			</div>
		@endif

		@php ($action_id = 0)
		@php ($type_id = 0)
		
		@php ($province_id = 0)
		@php ($cadaster_id = 1)

		@php ($kh_name = "")
		@php ($en_name = "")
		@php ($cn_name = "")

		@php ($kh_excerpt = "")
		@php ($en_excerpt = "")
		@php ($cn_excerpt = "")

		@php ($kh_description = "")
		@php ($en_description = "")
		@php ($cn_description = "")

       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))

			@php ($action_id = $invalidData['action_id'])
			@php ($type_id = $invalidData['type_id'])
			@php ($province_id = $invalidData['province_id'])
			
			@php ($cadaster_id = $invalidData['cadaster_id'])

		
            @php ($listing_code = $invalidData['listing_code'])
            @php ($kh_name = $invalidData['kh_name'])
            @php ($en_name = $invalidData['en_name'])
            @php ($cn_name = $invalidData['cn_name'])

            @php ($kh_nexcerpt = $invalidData['kh_excerpt'])
            @php ($en_excerpt = $invalidData['en_excerpt'])
            @php ($cn_excerpt = $invalidData['cn_excerpt'])

            @php ($kh_description = $invalidData['kh_description'])
            @php ($en_description = $invalidData['en_description'])
            @php ($cn_description = $invalidData['cn_description'])

       	@endif
		<form id="form" action="{{ route('user.property.property.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}

			
			<div class="form-group row">
				<label for="action-id" class="col-sm-2 form-control-label">Action</label>
				<div class="col-sm-10">
					<select id="action-id" name="action-id" class="form-control">
						@if($action_id != 0)
							@php( $lable = DB::table('actions')->find($action_id) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
							@endif
						@endif
						<option value="0" >Select Action</option>
						@foreach( $actions as $row)
							@if($row->id != $action_id)
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="type-id" class="col-sm-2 form-control-label">Type</label>
				<div class="col-sm-10">
					<select id="type-id" name="type-id" class="form-control">
						@if($type_id != 0)
							@php( $lable = DB::table('types')->find($type_id) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->en_name }} ({{ $lable->abbre }})</option>
							@endif
						@endif
						<option value="0" >Select Type</option>
						@foreach( $types as $row)
							@if($row->id != $type_id)
								<option value="{{ $row->id }}" >{{ $row->name }} ({{ $row->abbre }})</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group row">
				<label for="province-id" class="col-sm-2 form-control-label">Province</label>
				<div class="col-sm-10">
					<select id="province-id" name="province-id" class="form-control">
						@if($province_id != 0)
							@php( $lable = DB::table('provinces')->find($province_id) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->en_name }} ({{ $lable->abbre }})</option>
							@endif
						@endif
						<option value="0" >Select Province</option>
						@foreach( $provinces as $row)
							@if($row->id != $province_id)
								<option value="{{ $row->id }}" >{{ $row->name }} ({{ $row->abbre }})</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Cadaster Type</label>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="cadaster_id" id="radio-1" value="1" @if($cadaster_id == 1 ) checked @endif >
						<label for="radio-1">Soft</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="cadaster_id" id="radio-2" value="2" @if($cadaster_id == 2 ) checked @endif >
						<label for="radio-2">Hard</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="cadaster_id" id="radio-3" value="3" @if($cadaster_id == 3 ) checked @endif >
						<label for="radio-3">N/A</label>
					</div>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name (Kh)</label>
				<div class="col-sm-10">
					<input 	id="kh-name"
							name="kh-name"
							value = "{{ $kh_name }}"
							type="text"
							placeholder = "Name of property in Khmer"
							class="form-control" />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name (En)</label>
				<div class="col-sm-10">
					<input 	id="en-name"
							name="en-name"
							value = "{{ $en_name }}"
							type="text"
							placeholder = "Name of property in English"
						   	class="form-control"
						   	data-validation="[L>=5, L<=200]" />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Name (Cn)</label>
				<div class="col-sm-10">
					<input 	id="cn-name"
							name="cn-name"
							value = "{{ $cn_name }}"
							type="text"
							placeholder = "Name of property in Chinese"
						   	class="form-control" />
				</div>
			</div>
			@if(checkPermision('user.property.staff.add'))
			<div class="form-group row">
				<label for="staff-id" class="col-sm-2 form-control-label">Staff In Charge</label>
				<div class="col-sm-5">
					<select id="staff-id" name="staff-id" class="form-control">
						<option value="0" >Select Staff</option>
						@foreach( $staffs as $row)
								<option value="{{ $row->id }}" >{{ $row->name }} - {{ $row->position }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-5">
					<select id="role-id" name="role-id" class="form-control">
						<option value="0" >Select Role</option>
						@foreach( $roles as $row)
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
			@endif
		
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection