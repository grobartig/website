@extends('user.property.main')
@section ('section-title', 'Search Properties')
@section ('display-btn-add-new', 'display:none')
@section ('section-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}
		#map{
			width:100%;
			height:700px;
		}
		.list-group-item:first-child{
			border-top-left-radius: 0px; 
    		border-top-right-radius: 0px;
		}
		.list-group-item:last-child{
			border-top-left-radius: 0px; 
    		border-top-right-radius: 0px;
		}
		.list-group-item-success{
		    color: #ffffff;
		    background-color: #3fc8f4;
		}
		.list-group-item-success a{
			color:white;
		}
	</style>
@endsection
@section ('section-js')
<script type="text/javascript" src="{{ asset ('public/user/js/lib/blockUI/jquery.blockUI.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8S3pD12gYeCiM2vUf5MuObhznkGbWNCk"> </script>
<script type="text/javascript">	
	$(document).ready(function(){
		$("#btn-search").click(function(){
			search();
		})
		$("#limit").change(function(){
			search();
		})
		$("#province-id").change(function(){
			getDistricts($(this).val());
		})
		$('#district-id').change(function(){
    		getCommunes($(this).val());
    	})

    	//============check
    	$("#check-all").click(function(){
    		checkAll();
    	})
    	$(".check-list-item").click(function(){
    		checkItem($(this).attr('data-id'));
    	})
    	$("#btn-list").click(function(){
    		createList();
    	})

    	$("#search").keypress(function(e) {
			    if(e.which == 13) {
			       displayAvaiableLists();
			    }
			});

    	//==========>> Add To List
    	$("#btn-add-to-list").click(function(){
    		$('#modal').modal('show');
    		displaySelectedProperties();
    		displayAvaiableLists();
    	})
	})

    function search(print=0){
		action 		= $('#action-id').val();
		type 		= $('#type-id').val();
		province 	= $('#province-id').val();
		district 	= $('#district-id').val();
		commune 	= $('#commune-id').val();
		road 		= $('#road').val();
		listing_code = $('#listing_code').val();
		min 		= $('#min').val();
		max 		= $('#max').val();

		d_from 		= $('#from').val();
		d_till 		= $('#till').val();
		limit 		= $('#limit').val();

		url="?print="+print+"&limit="+limit+"&map="+isMap;
		if(action != 0){
			url += '&action='+action;
		}
		if(type != 0){
			url += '&type='+type;
		}

		if(listing_code != ""){
			url += '&listing_code='+listing_code;
		}
		if(min != max && min < max){
			if(min > 0){
				url += '&min='+min;
			}
			if(max > 0){
				url += '&max='+max;
			}
		}

		if(province != 0){
			url += '&province='+province;
			if(district != 0){
				url += '&district='+district;
				if(commune != 0){
					url += '&commune='+commune;
				}
			}
		}
		if(road != ""){
			url += '&road='+road;
		}

		
		

		if(isDate(d_from)){
			if(isDate(d_till)){
				url+='&from='+d_from+'&till='+d_till;
			}
		}
		if(print == 1){
			window.open('{{ route($route.'.property.index') }}'+url, '_blank');
		}else{
			$(location).attr('href', '{{ route($route.'.property.index') }}'+url);
		}
		
	}
	//=============================================>> Make Multi Map
	@if($appends['map'] == 1 )
		var isMap = 1;
		var map = new google.maps.Map(document.getElementById('map'), {
	          zoom: 10,
	          center: {lat: 11.544873, lng: 104.892167}
	        });
		var rents = [];
      	var sells = [];
      	var rs = [];

      	@php($i = 1)
      	@foreach($data as $row)
      		@if($row->action_id == 1)
      			sells.push(['{{$row->en_name}}', {{$row->location->lat}}, {{$row->location->lng}}, {{$i++}}, '{{makeListingCodeByID($row->id)}}', '{{$row->action->en_name}}', '{{$row->price->selling_price}} @if( ! is_null($row->price->selling_price_type) ) ({{$row->price->selling_price_type->name}}) @endif', '{{$row->location->address}}', '{{ route('user.property.check-role', $row->id) }}']);
      		@elseif($row->action_id == 2)
      			rents.push(['{{$row->en_name}}', {{$row->location->lat}}, {{$row->location->lng}}, {{$i++}}, '{{makeListingCodeByID($row->id)}}', '{{$row->action->en_name}}', '{{$row->price->rental_price}} @if( ! is_null($row->price->rental_price_type) ) ({{$row->price->rental_price_type->name}}) @endif', '{{$row->location->address}}', '{{ route('user.property.check-role', $row->id) }}']);
      		@elseif($row->action_id == 3)
      			rs.push(['{{$row->en_name}}', {{$row->location->lat}}, {{$row->location->lng}}, {{$i++}}, '{{makeListingCodeByID($row->id)}}', '{{$row->action->en_name}}', 'Sell: {{$row->price->selling_price}} @if( ! is_null($row->price->selling_price_type) ) ({{$row->price->selling_price_type->name}}) @endif <br /> Rental: {{$row->price->rental_price}} @if( ! is_null($row->price->rental_price_type) ) ({{$row->price->rental_price_type->name}}) @endif', '{{$row->location->address}}', '{{ route('user.property.check-role', $row->id) }}']);
      		@endif
	     @endforeach

		$(document).ready(function(){
	        setMarkers(sells, '{{asset('public/user/img/marker/s.png')}}');
	        setMarkers(rents, '{{asset('public/user/img/marker/r.png')}}');
	        setMarkers(rs, '{{asset('public/user/img/marker/rs.png')}}');
		})

		function setMarkers(data, image_url) {

		    var marker, i; 
		    var image = {
	          url: image_url,
	          size: new google.maps.Size(27, 35),
	          origin: new google.maps.Point(0, 0),
	          anchor: new google.maps.Point(0, 32)
	        };

			for (i = 0; i < data.length; i++){  

				var title = data[i][0];
				var lat = data[i][1];
				var long = data[i][2];
				var id =  data[i][3];

				var code =  data[i][4];
				var action =  data[i][5];
				var price =  data[i][6];
				var address =  data[i][7];
				var url =  data[i][8];

				latlngset = new google.maps.LatLng(lat, long);

				var marker = new google.maps.Marker({  
				    map: map, 
				    icon:image,
				    title: title , 
				    position: latlngset  
				});

				map.setCenter(marker.getPosition())
				var content = '<ul class="list-group">'+
								'<li class="list-group-item list-group-item-success"><a href="'+url+'">Listing Code: <b>'+code+'</b></a></li>'+
								'<li class="list-group-item">Action: '+action+'</li>'+
								'<li class="list-group-item">Price: '+price+'</li>'+
								'<li class="list-group-item">Location: '+address+'</li>'+
							'</ul>';

				var infowindow = new google.maps.InfoWindow(); 
				google.maps.event.addListener(marker,'click', (function(marker, content, infowindow){ 
				        return function() {
				           infowindow.setContent(content);
				           infowindow.open(map, marker);
				        };
				    })(marker,content,infowindow)); 

			}
		}
	@else 
		var isMap = 0;
	@endif

	function isMakeMap(){
		if(isMap ==1 ){
			isMap = 0;
		}else{
			isMap = 1;
		}
	}

	//=============================================>> Location
	function getDistricts(province_id){
			//Empty the district
			$("#district-id").html('<option value="0" >District</option>');
			//Empty the communes
			$("#commune-id").html('<option value="0" >Commune</option>');
			
			//Get Districts
			if(province_id != 0){
				$.ajax({
				        url: "{{ route('user.property.districts') }}?province_id="+province_id,
				        type: 'GET',
				        data: {},
				        success: function( response ) {
				           var districts = '';
				            var i;
						    var length = response.length;
						    for (i = 0; i < length; i++) {
						        districts += '<option value="'+response[i].id+'" >'+response[i].name+'</option>';
						    }
						    if(districts != ""){
						    	$('#district-id').append(districts);
						    	
						    }
						    
				        },
				        error: function( response ) {
				           swal("Error!", "Sorry there is an error happens. " ,"error");
				        }
							
				});
			}
	}

	function getCommunes(district_id){
		//Empty the communes
		$("#commune-id").html('<option value="0" >Commune</option>');

		if(district_id != 0){
			$.ajax({
			        url: "{{ route('user.property.communes') }}?district_id="+district_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var communes = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        communes += '<option value="'+response[i].id+'" >'+response[i].name+'</option>';
					    }
					    if(communes != ""){
					    	$('#commune-id').append(communes);
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
	}
	@php( $is_able_create_list = checkPermision('user.property.tool.listing.index') )
	@if($is_able_create_list)
	//=============================================>> Create Listing
	var check = 'check-all';
	function checkAll(){
		//console.log(check);
		items = $(".check-list-item");
		if( check == 'check-all' ){
			items.removeClass('fa-square-o');
			items.addClass('fa-check-square-o');
			check = '';
		}else{
			items.removeClass('fa-check-square-o');
			items.addClass('fa-square-o');
			check = 'check-all';
		}
	}
	function checkItem(id){
		
		item = $("#item-"+id);
		if(item.hasClass('fa-square-o')){
			item.removeClass('fa-square-o');
			item.addClass('fa-check-square-o');
		}else{
			item.removeClass('fa-check-square-o');
			item.addClass('fa-square-o');
		}
	}
	
	function createList(){
		items = $(".check-list-item");
		//console.log(items);
		checkedItems = []; 
		for(i=0; i<items.length; i++){
			item = items[i];
			if(item.classList.contains('fa-check-square-o')){
				checkedItems.push(item.getAttribute('data-id'));
			}
		}
		//console.log(checkedItems);
		if( checkedItems.length != 0 ){
			swal({
				title: "Property List for Mailing",
				text: "Please name this list:",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: "Write something"
			}, function (inputValue) {
				if (inputValue === false) return false;
				if (inputValue === "") {
					swal.showInputError("You need to write something!");
					return false
				}
				
				//swal("Nice!", "You wrote: " + inputValue, "success");
				$.ajax({
			        url: "{{ route($route.'.tool.listing.create-list') }}",
			        method: 'PUT',
			        data: { name:inputValue, list:JSON.stringify(checkedItems) },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});

			});
		}else{
			swal("Please select at least a property.");
			
		}
	}

	function displaySelectedProperties(){
		items = $(".check-list-item");
		$("#selected-cnt").html('');
		for(i=0; i<items.length; i++){
			item = items[i];
			if(item.classList.contains('fa-check-square-o')){
				if(item.getAttribute('data-id')!=0){
					$("#selected-cnt").append('<tr><td>'+item.getAttribute('data-code')+'</td><td>'+item.getAttribute('data-name')+'</td></tr>');
				}
			}
		}
	}

    function displayAvaiableLists(){
    	key 		= $('#search').val();
		$.ajax({
		        url: "{{ route($route.'.tool.listing.search-list') }}?id=0&key="+key,
		        type: 'GET',
		        data: {},
		        success: function( response ) {
		          $("#result").html(response);

		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
					
		});
    }

	function addPropertiesToList(list_id){
		items = $(".check-list-item");
		//console.log(items);
		checkedItems = []; 
		for(i=0; i<items.length; i++){
			item = items[i];
			if(item.classList.contains('fa-check-square-o')){
				checkedItems.push(item.getAttribute('data-id'));
			}
		}
		//console.log(checkedItems);
		if( checkedItems.length != 0 ){
		

			$.ajax({
		        url: "{{ route($route.'.tool.listing.add-properties-to-list') }}",
		        method: 'PUT',
		        data: { list_id:list_id, list:JSON.stringify(checkedItems) },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	swal("Nice!", response.msg ,"success");
		            	displayAvaiableLists();
		            	$('.modal-content').unblock();
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
			});


		}else{
			swal("Please select at least an account.");
			
		}
	}
	@endif
</script>
@endsection

@section ('section-content')
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<select id="action-id" class="form-control">
					@php( $action = isset($appends['action'])?$appends['action']:0 )
					@if($action != 0)
						@php( $lable = DB::table('actions')->find($action) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
						@endif
					@endif
					<option value="0" >Action</option>
					@foreach( $actions as $row)
						@if($row->id != $action)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
				
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<select id="type-id" class="form-control">
					@php( $type = isset($appends['type'])?$appends['type']:0 )
					@if($type != 0)
						@php( $lable = DB::table('types')->find($type) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
						@endif
					@endif
					<option value="0" >Type</option>
					@foreach( $types as $row)
						@if($row->id != $type)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<select id="province-id" class="form-control">
					@php( $province = isset($appends['province'])?$appends['province']:0 )
					@if($province != 0)
						@php( $lable = DB::table('provinces')->find($province) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
						@endif
					@endif
					<option value="0" >Province</option>
					@foreach( $provinces as $row)
						@if($row->id != $province)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<select id="district-id" class="form-control">
					@php( $district = isset($appends['district'])?$appends['district']:0 )
					@if($province != 0)
						@php( $districts = DB::table('districts')->where('province_id', $province)->get() )
						@if($district != 0)
							@php( $lable = DB::table('districts')->find($district) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
							@endif
							<option value="0" >District</option>
							@foreach( $districts as $row)
								@if($row->id != $district)
									<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
								@endif
							@endforeach
						@else
							<option value="0" >District</option>
							@foreach( $districts as $row)
								<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
							@endforeach
						@endif
					@else
						<option value="0" >District</option>
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<select id="commune-id" class="form-control">
					@php( $commune = isset($appends['commune'])?$appends['commune']:0 )
					@if($province != 0)
						@if($district != 0)
							@php( $communes = DB::table('communes')->where('district_id', $district)->get() )
							@if($commune != 0)
								@php( $lable = DB::table('communes')->find($commune) )
								@if( sizeof($lable) == 1 )
									<option value="{{ $lable->id }}" >{{ $lable->en_name }}</option>
								@endif
								<option value="0" >Commune</option>
								@foreach( $communes as $row)
									@if($row->id != $commune)
										<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
									@endif
								@endforeach
							@else
								<option value="0" >Commune</option>
								@foreach( $communes as $row)
									<option value="{{ $row->id }}" >{{ $row->en_name }}</option>
								@endforeach
							@endif
						@else
							<option value="0" >Commune</option>
						@endif
					@else
						<option value="0" >Commune</option>
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input id="road" type='text' class="form-control" value="{{ isset($appends['road'])?$appends['road']:'' }}" placeholder="Road" />
			</div>
		</div>
	</div><!--.row-->
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<input id="listing_code" type='text' class="form-control" value="{{ isset($appends['listing_code'])?$appends['listing_code']:'' }}" placeholder="Listing Code" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input id="min" type='number' min=0 class="form-control" value="{{ isset($appends['min'])?$appends['min']:'' }}" placeholder="Min Price" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input id="max" type='number' min=0 class="form-control" value="{{ isset($appends['max'])?$appends['max']:'' }}" placeholder="Max Price" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<div id="till-cnt" class='input-group date ' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-1" style="padding-left: 0px; padding-top: 10px;">
			Map:<div class="checkbox-toggle" style="display: inline;    ">
			        <input  type="checkbox" id="map-check"  @if($appends['map'] == 1 ) checked @endif >
			        <label onclick="isMakeMap()" for="map-check"></label>
		        </div>
		</div>
		<div class="col-md-1">

			<button id="btn-search" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
			@if(checkPermision('user.property.property.print'))
			<button onclick="search(1)"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-print"></span></button>
			@endif
		</div>
	</div><!--.row-->

	@if(sizeof($data) > 0)
		@if($appends['map'] == 1)
			<div id="map" class="map"></div>
			
		@else

		<div class="table-responsive">
			<table id="table-edit" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						@if($is_able_create_list)
						<th><i id="check-all" data-id="0" class="fa fa-square-o check-list-item check-list"></i></th>
						@endif
						<th>Listing Code</th>
						<th>Action</th>
						<th>Title</th>
						<th>Price</th>
						<th>Road</th>
						<th>Updated Date</th>
						<th>Agent</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

					@php ($i = 1)
					@foreach ($data as $row)
						<tr>
							<td>{{ $i++ }}</td>
							@if($is_able_create_list)
							<td><i id="item-{{ $row->id }}" data-id="{{ $row->id }}" data-code="{{ $row->listing_code }}" data-name="{{ $row->en_name }}" class="fa fa-square-o  @if ($row->is_published == 1) check-list-item check-list  @else uncheckable @endif  "></i></td>
							@endif
							<td>
								@php( $l = '')
								@if($row->is_published == 0)
									@php( $l = 'L')
								@endif
								@php( $display_listing_code = $l.$row->province->abbre.'-'.$row->type->abbre.$row->listing_code )
								{{$display_listing_code}}
							</td>
							<td>{{ $row->action->en_name }}</td>
							<td>{{ $row->en_name }}</td>
							
							<td>
								 @if($row->action_id == 1 )
                                    {{$row->price->selling_price}} @if( ! is_null($row->price->selling_price_type) ) ({{$row->price->selling_price_type->name}}) @endif
                                @elseif( $row->action_id == 2 )
                                    {{$row->price->rental_price}} @if( ! is_null($row->price->rental_price_type) ) ({{$row->price->rental_price_type->name}}) @endif
                                @else
                                    Sell: {{$row->price->selling_price}} @if( ! is_null($row->price->selling_price_type) ) ({{$row->price->selling_price_type->name}}) @endif<br />
                                    Rent: {{$row->price->rental_price}} @if( ! is_null($row->price->rental_price_type) ) ({{$row->price->rental_price_type->name}}) @endif
                                @endif
							</td>
							<td>{{$row->location->road}}</td>
							<td>{{ $row->updated_at }}</td>
							<td></td>
							<td style="white-space: nowrap; width: 1%;">
								<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
		                           	<div class="btn-group btn-group-sm" style="float: none;">
		                           		@if(checkIfHasRole($row->id))
		                           		<a href="{{ route('user.property.check-role', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
		                           		@endif
		                           	</div>
		                       </div>
		                    </td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div >
		@endif

	@else
	<span>No Data</span>
	@endif

	<div class="row">
		<div class="col-xs-1" style="padding-right: 0px;">
			<select id="limit" class="form-control" style="margin-top: 15px;width:100%">
				@if(isset($appends['limit']))
				<option>{{ $appends['limit'] }}</option>
				@endif
				<option>10</option>
				<option>20</option>
				<option>30</option>
				<option>40</option>
				<option>50</option>
				<option>60</option>
				<option>70</option>
				<option>80</option>
				<option>90</option>
				<option>100</option>
			</select>
		</div>
		@if($appends['map'] == 0)
		<div class="col-xs-2">
			@if($is_able_create_list)
			<button style="margin-top:15px" id="btn-list" class="tabledit-delete-button btn btn-sm btn-primary" style="float: left;"><span class="fa fa-list"></span></button>
			<button style="margin-top:15px" id="btn-add-to-list" class="tabledit-delete-button btn btn-sm btn-primary" style="float: left;"><span class="fa fa-clipboard"></span></button>
			@endif
		</div>
		@else
			<div class="col-xs-3" style = 'padding-top: 12px;' >
				<img src="{{asset('public/user/img/marker/r.png')}}" /> Rent &nbsp;
				<img src="{{asset('public/user/img/marker/s.png')}}" /> Sell &nbsp;
				<img src="{{asset('public/user/img/marker/rs.png')}}" /> Rent or Sell
			</div>
		@endif
		<div class="col-xs-8">
			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add To List</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div id="block-ui" class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-6">
                                       				<div class="chat-area-header">
														<div class="clean">Add Properties To List</div>
													</div>
													<div id="selected">
														<table id="seleted-table" class="table pop">
															<thead>
																<th>Code.</th><th>Title.</th><th width=8%>.</th>
															</thead>
															<tbody id="selected-cnt">
																
															</tbody>
														</table>
                                       				</div>
                                       			</div>
                                       			<div class="col-xs-6">
                                       				<div class="chat-list-search">
														<input type="text" id="search" class="form-control form-control-rounded" placeholder="Name of List">
													</div>
                                       				<div id="result">
                                       					
                                       				</div>
                                       			</div>
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection