@extends('user.property.tabForm')
@section ('section-title', 'Enquiries')
@section ('tab-active-enquiries', 'active')

@section ('section-js')
<script type="text/javascript">	
	$(document).ready(function(event){

		$("#btn-property-code").click(function(){
    		create();
    	})
		get();
	});

	function create(){
		
			swal({
				title: "Property Inquiries",
				text: "Please type the customer Phone number or e-mail. In case having no existing customers, new one will be stored using phone number as primary.",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: "012876709 or username@domain.com"
			}, function (inputValue) {
				if (inputValue === false) return false;
				if (inputValue === "") {
					swal.showInputError("You need to write something!");
					return false
				}
				
				//swal("Nice!", "You wrote: " + inputValue, "success");
				$.ajax({
			        url: "{{ route($route.'.enquiries-store') }}?property_id={{$id}}",
			        method: 'POST',
			        data: { key:inputValue},
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	
			            	location.reload();
			            	
			            }else{
			            	alert(response);
			            }
			        },
			        error: function( response ) {
			            //console.log(response.responseJSON);
			            errors = response.responseJSON;
			            console.log(errors.phone1[0]);
			            if(errors.phone1[0]=='The phone1 format is invalid.'){
			            	alert("Please type a valide phone number");
			            }
			        }
				});

			});
		
		}

	function get(){
    	$.ajax({
					        url: "{{ route($route.'.enquiries-data',$id) }}",
					        type: 'GET',
					        data: {},
					        success: function( response ) {
					           $("#data-cnt").html(response);
					        },
					        error: function( response ) {
					           swal("Error!", "Sorry there is an error happens. " ,"error");
					        }
				})
    }

    function update(id) {
    	message = $("#message-"+id).val();
    		if(message != ""){
	    			$.ajax({
						        url: "{{ route($route.'.update-enquiries') }}",
						        type: 'POST',
						        data: {id:id, message:message },
						        success: function( response ) {
						            if ( response.status === 'success' ) {
						            	swal("Nice!", response.msg ,"success");
						            	//get_routes();
						            }else{
						            	swal("Error!", "Sorry there is an error happens. " ,"error");
						            }
						        },
						        error: function( response ) {
						           swal("Error!", "Sorry there is an error happens. " ,"error");
						        }
						
					});
			}else{
				swal("Error!", "Sorry! Message cannot be empty " ,"error");
	    		$("#message-"+id).addClass("error");
	    		$("#message-"+id).click(function(){
	    			$("#message-"+id).removeClass("error");
	    		})
			}
    }

</script>
@endsection

@section ('tab-content')
	<div>
		<div class="col-md-12">
			<a style="float: right;margin-bottom: 10px;margin-top: -10px;" id="btn-property-code" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>	
		</div>
	</div><!--.row-->
	<div id="data-cnt">
								
	</div >
@endsection