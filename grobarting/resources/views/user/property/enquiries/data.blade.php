@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>ID</th>
					<th>Phone</th>
					<th>Message</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>CPE-{{ $row->customer->cus_id }}</td>
						<td>{{ $row->customer->phone1 }}</td>
						<td><textarea id="message-{{$row->id}}" name="message" class="form-control  "> {{ $row->message }} </textarea></td>
						<td>{{ $row->created_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<button onclick="update({{$row->id}})" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-save"></span></button>
	                           		<button onclick="deleteConfirm('{{ route($route.'.trash', ['inquiry_id'=>$row->id, 'id'=>$id]) }}', '{{ route($route.'.index', ['id'=>$id]) }}')" class="tabledit-edit-button btn btn-sm btn-danger" style="float: none;"><span class="fa fa-trash"></span></button>
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
		<span>No Data</span>
	@endif