@extends('user.property.main')
@section ('section-css')
	@yield ('tab-css')
@endsection

@section ('section-js')
	@yield ('tab-js')
@endsection

@section ('section-content')
	@php($listing_code = makeListingCodeByID($id) )
	<h6>Listing Code: {{ $listing_code }}</h6>
	<section class="tabs-section">
		<div class="tabs-section-nav tabs-section-nav-icons">
			<div class="tbl">
				<ul class="nav" role="tablist">
					@if(checkRole($id, 'view-overview'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-edit')" onclick="window.location.href='{{ route('user.property.property.edit', $id) }}'" href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-building"></i> Overview
							</span>
						</a>
					</li>
					@endif
					@if(checkRole($id, 'view-about'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-about')" onclick="window.location.href='{{ route('user.property.about.redirect-url', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-image"></i> About
							</span>
						</a>
					</li>
					@endif
					@if(checkRole($id, 'view-confidentail'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-confidentail')" onclick="window.location.href='{{ route('user.property.confidentail.redirect-url', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-usd"></i> Confidentail
							</span>
						</a>
					</li>
					@endif
					<!-- @if(checkRole($id, 'view-inquiry'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-enquiries')" onclick="window.location.href='{{ route('user.property.enquiries.index', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-comment-o"></i> Inquiries
							</span>
						</a>
					</li>
					@endif -->
					@if(checkRole($id, 'view-tool'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-tool')" onclick="window.location.href='{{ route('user.property.tool.redirect-url', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-file-pdf-o"></i> Tool
							</span>
						</a>
					</li>
					@endif
					@if(checkRole($id, 'view-staff'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-staff')" onclick="window.location.href='{{ route('user.property.staff.index', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-users"></i> Staffs
							</span>
						</a>
					</li>
					@endif
					
				</ul>
			</div>
		</div><!--.tabs-section-nav-->

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active">
				<div id="tab-content-cnt" class="container-fluid">
					@yield ('tab-content')
				</div>
			</div>
		</div><!--.tab-content-->
	</section><!--.tabs-section-->
@endsection