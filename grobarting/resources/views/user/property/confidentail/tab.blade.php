@extends('user.property.tabForm')
@section ('tab-active-confidentail', 'active')

@section ('tab-content')
	
	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				@if(checkRole($id, 'view-owner'))<li><a href="{{ route('user.property.confidentail.owner.index', $id) }}" class="@yield ('confidentail-active-owner')">Owner</a></li>@endif
				@if(checkRole($id, 'view-customer'))<li><a href="{{ route('user.property.confidentail.customer.index', $id) }}" class="@yield ('confidentail-active-customer')">Customer</a></li>@endif
			</ul>
		</nav><!--.files-manager-side-->

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('confidentail')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection