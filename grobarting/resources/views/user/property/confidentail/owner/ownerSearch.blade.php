@if(count($data)>0)
<table id="pop-table" class="table pop">
	<thead>
		<th>Name</th><th>E-mail</th><th>Phone</th><th></th>
	</thead>
	<tbody>
		@foreach($data as $row)
			
			<tr>
				<td>{{ $row->name }}</td><td>{{ $row->email }} <td>{{$row->phone1}}<br />{{ $row->phone2 }}</td></td><td width=8%><i onclick="addToProperty({{$row->id}})" class="fa fa-plus"></i></td>
			</tr>
		@endforeach
		
	</tbody>
</table>
@else
	No Data Avaiable
@endif
