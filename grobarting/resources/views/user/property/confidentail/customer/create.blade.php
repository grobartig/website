@extends('user.property.confidentail.tab')
@section ('section-title', 'Confidentail Information')
@section ('confidentail-active-customer', 'active')


@section ('tab-js')
	<script type="text/JavaScript">
		// $(document).ready(function(event){
		
		// 	$('#form').validate({
		// 		modules : 'file',
		// 		submit: {
		// 			settings: {
		// 				inputContainer: '.form-group',
		// 				errorListClass: 'form-tooltip-error'
		// 			}
		// 		}
		// 	}); 
			
		// });

	
	</script>

@endsection

@section ('confidentail')
	<div class="container-fluid">
		@include('user.layouts.error')

		@php ($title_id = 1)
		@php ($first_name = "")
		@php ($last_name = "")
		@php ($email = "")
		@php ($phone = "")
		@php ($address = "")
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($title_id = $invalidData['title_id'])
            @php ($first_name = $invalidData['first_name'])
            @php ($last_name = $invalidData['last_name'])
            @php ($phone = $invalidData['phone'])
            @php ($email = $invalidData['email'])
            @php ($address = $invalidData['address'])
            
       	@endif
		<form id="form" action="{{ route($route.'.store', $id) }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Title</label>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-1" value="1" @if($title_id == 1 ) checked @endif >
						<label for="radio-1">Mr.</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-2" value="2" @if($title_id == 2 ) checked @endif >
						<label for="radio-2">Mrs.</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-3" value="3" @if($title_id == 3 ) checked @endif >
						<label for="radio-3">Miss.</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="first_name">First Name</label>
				<div class="col-sm-10">
					<input 	id="first_name"
							name="first_name"
						   	value = "{{$first_name}}"
						   	type="text"
						   	placeholder = "Eg. Jhon Son"
						   	class="form-control"
						   	data-validation="[L>=1, L<=100, MIXED]"
							data-validation-message="$ must be between 2 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="first_name">Last Name</label>
				<div class="col-sm-10">
					<input 	id="last_name"
							name="last_name"
						   	value = "{{$last_name}}"
						   	type="text"
						   	placeholder = "Eg. Jhon Son"
						   	class="form-control"
						   	data-validation="[L>=1, L<=100, MIXED]"
							data-validation-message="$ must be between 1 and 100 characters. No special characters allowed." />
							
				</div>
			</div>
			

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone</label>
				<div class="col-sm-10">
					<input 	id="phone"
							name="phone"
						   	value = "{{$phone}}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control"
						   	data-validation="[L>=9, L<=10, numeric]"
							data-validation-message="$ is not correct." 
							data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
							data-validation-regex-message="$ must start with 0 and has 9 or 10 digits" />
							
				</div>
			</div>
			
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">E-mail</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{$email}}"
							type="text"
							placeholder = "Eg. username@example.com"
						   	class="form-control">
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Address</label>
				<div class="col-sm-10">
					<input 	id="address"
							name="address"
							value = "{{$address}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
		
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>
@endsection