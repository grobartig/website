@extends('user.property.confidentail.tab')
@section ('section-title', 'Customer')
@section ('confidentail-active-customer', 'active')


@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			// $('#form').validate({
			// 	modules : 'file',
			// 	submit: {
			// 		settings: {
			// 			inputContainer: '.form-group',
			// 			errorListClass: 'form-tooltip-error'
			// 		}
			// 	}
			// }); 
			$("#search").keypress(function(e) {
			    if(e.which == 13) {
			       changeCustomer();
			    }
			});
		});

		function changeCustomer(){
			$("#modal").modal("show");
			searchCustomers();
		}

		function searchCustomers(){
			key 		= $('#search').val();
			$.ajax({
			        url: "{{ route($route.'.search') }}?id={{$id}}&key="+key,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#result").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		function addToProperty(customer_id){
			$.ajax({
			        url: "{{ route($route.'.add-to-property') }}?id={{$id}}&customer_id="+customer_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          if(response =="added"){
			          	window.location.href="{{ route($route.".index", $id) }}"
			          }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
	</script>
@endsection

@section ('confidentail')
	
	@if(count($data) == 1)
		@include('user.layouts.error')
		<form id="form" action="{{ route($route.'.update', $id) }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >ID</label>
				<div class="col-sm-10">
					
					<p class="form-control-static"><input type="text" readonly="" class="form-control"   value="CPE-{{ $data->cus_id }}" ></p>
					
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Title</label>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-1" value="1" @if($data->title_id == 1 ) checked @endif >
						<label for="radio-1">Mr.</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-2" value="2" @if($data->title_id == 2 ) checked @endif >
						<label for="radio-2">Mrs.</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-3" value="3" @if($data->title_id == 3 ) checked @endif >
						<label for="radio-3">Miss.</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="first_name">First Name</label>
				<div class="col-sm-10">
					<input 	id="first_name"
							name="first_name"
						   	value = "{{$data->first_name}}"
						   	type="text"
						   	placeholder = "Eg. Jhon Son"
						   	class="form-control"
						   	data-validation="[L>=1, L<=100, MIXED]"
							data-validation-message="$ must be between 2 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="first_name">Last Name</label>
				<div class="col-sm-10">
					<input 	id="last_name"
							name="last_name"
						   	value = "{{$data->last_name}}"
						   	type="text"
						   	placeholder = "Eg. Jhon Son"
						   	class="form-control"
						   	data-validation="[L>=1, L<=100, MIXED]"
							data-validation-message="$ must be between 1 and 100 characters. No special characters allowed." />
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone</label>
				<div class="col-sm-10">
					<input 	id="phone"
							name="phone"
						   	value = "{{ $data->phone }}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control"
						   	data-validation="[L>=9, L<=10, numeric]"
							data-validation-message="$ is not correct." 
							data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
							data-validation-regex-message="$ must start with 0 and has 9 or 10 digits" />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">E-mail</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{ $data->email }}"
							type="text"
							placeholder = "Eg. you@example.com"
						   	class="form-control">
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Address</label>
				<div class="col-sm-10">
					<input 	id="address"
							name="address"
							value = "{{ $data->address_line_1 }}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
			
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					@if(checkRole($id, 'view-customer'))<button type="button" onclick="window.location.href='{{ route('user.customer.edit', $data->id) }}'" class="btn btn-success"> <fa class="fa fa-eye"></i> View</button>@endif
					@if(checkRole($id, 'update-customer'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
					@if(checkRole($id, 'change-customer'))<button type="button" onclick="changeCustomer()" class="btn btn-success"> <fa class="fa fa-cogs"></i> Change</button>@endif
				</div>
			</div>
		</form>
	@else
		<br />
		<div class="row">
			<div class="col-md-12">
				This property has not had customer yet. @if(checkRole($id, 'create-customer'))  Click <a href="{{ route($route.'.create', $id) }}" >Here</a> to add new. @endif  @if(checkRole($id, 'change-customer'))<a onclick="changeCustomer()" href="#" >Here</a> to add existing.@endif
			</div>
		</div><!--.row-->
	@endif
	
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Owner</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-12">
                                       				<div class="chat-list-search">
														<input type="text" id="search" class="form-control form-control-rounded" placeholder="Type Name, E-mail, or Phone">
													</div>
                                       				<div id="result">
                                       					
                                       				</div>
                                       			</div>
                                       			
                                       			
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection


