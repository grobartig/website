
<table id="seleted-table" class="table pop">
	<thead>
		<th>Name.</th><th>Role</th><th width=8%>.</th>
	</thead>
	<tbody>
		@php( $i=1 )
		@foreach($data as $row)
		<tr>
			<td>@if($row->staff) {{ $row->staff->en_name }} @endif</td>
			<td>@if($row->role) {{ $row->role->name }} @endif</td>
			<td><i onclick="remove({{ $row->staff_id }})" class="fa fa-times"></i></td>
		</tr>
		@endforeach
	</tbody>
</table>
