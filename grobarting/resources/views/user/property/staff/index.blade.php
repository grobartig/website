@extends('user.property.tabForm')
@section ('section-title', 'Overview')
@section ('tab-active-staff', 'active')
@section ('tab-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}
	</style>
@endsection

@section ('tab-js')
	<script type="text/javaScript">
		$(document).ready(function(){
			$("#btn-add-more-data").click(function(){
				selected(); 
				search();
			})

			$("#search").keypress(function(e) {
			    if(e.which == 13) {
			       search();
			    }
			});
		})

		function search(){
			key 		= $('#search').val();
			$.ajax({
			        url: "{{ route($route.'.search') }}?id={{$id}}&key="+key,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#result").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
		function selected(){
			$.ajax({
			        url: "{{ route($route.'.selected') }}?id="+{{ $id }},
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function remove(staff_id){
			$.ajax({
			        url: "{{ route($route.'.remove') }}?id={{ $id }}&staff_id="+staff_id,
			        type: 'DELETE',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          search();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function add(staff_id){
			role_id = $("#role-"+staff_id).val();
			$.ajax({
			        url: "{{ route($route.'.add') }}?id={{ $id }}&staff_id="+staff_id+"&role_id="+role_id,
			        type: 'PUT',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          search();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function is_primary(primary_id){
	     	$.ajax({
		        url: "{{ route($route.'.primary') }}",
		        method: 'POST',
		        data: {id:{{$id}}, primary_id:primary_id  },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	window.location.replace("{{ route($route.'.index', $id) }}");
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
			});
		}
		function access(role_id){
	     	$("#access").modal("show");
	     	$.ajax({
		        url: "{{ route($route.'.role-access') }}?role_id="+role_id,
		        method: 'GET',
		        data: { },
		        success: function( response ) {
		           $("#role-cnt").html(response);
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
			});
		}
	</script>
@endsection

@section ('tab-content')
	<br />
	<div>
		<div class="col-md-12">
			<button id="btn-add-refresh" onclick="window.location.href='{{ route($route.'.index', $id) }}'"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;margin-left: 5px;"><span class="fa fa-refresh"></span></button>
			@if(checkRole($id, 'change-staff'))<button id="btn-add-more-data" data-toggle="modal" data-target="#modal" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;"><span class="fa fa-plus"></span></button>@endif
		</div>
	</div><!--.row-->

	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th style="width:25%">Name</th>
					<th>Role</th>
					<th>Primary</th>
					<th>Added Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@php ($user_id = Auth::id())
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>
							@if($row->staff)
								{{ $row->staff->en_name }}
							@endif
						</td>
						<td>
							{{ $row->role->name }}
						</td>
						<td>
							<div class="checkbox-toggle">
						        <input  type="checkbox" @if ($row->is_primary == 1) checked data-value="1" @else data-value="0" @endif >
						        <label @if ($row->is_primary == 0) onclick="is_primary({{$row->id}})" @endif  ></label>
					        </div>
						</td>
						<td>{{ $row->created_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<button type="button" onclick="access({{$row->role->id}})" class="tabledit-edit-button btn btn-sm btn-default" style="float: none;"><span class="fa fa-list-ul"></span></button>
	                           	</div>
                       		</div>
                       	</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	This property is not found in any lists.
	@endif
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Staff In Charges</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-5">
                                       				<div class="chat-list-search">
														<input type="text" id="search" class="form-control form-control-rounded" placeholder="Name of User">
													</div>
                                       				<div id="result">
                                       				</div>
                                       			</div>
                                       			<div class="col-xs-7">
                                       				<div class="chat-area-header">
														<div class="clean">Existing properties in this list</div>
													</div>
													<div id="selected">
                                       					
                                       				</div>
                                       			</div>
                                       			
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
<div class="modal fade" id="access" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Property Access</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		<div class="row">
                                       			<div id="role-cnt" class="col-xs-12">
                                       			</div>
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection