@if(count($data)>0)
<table id="pop-table" class="table pop">
	<thead>
		<th>Name</th>
		<th>Position</th>
		<th>Role</th>
		<th></th>
	</thead>
	<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{ $row->name }}</td>
				<td>{{ $row->position }} </td>
				<td>
					<select id="role-{{$row->id}}">
					@foreach($roles as $role)
						<option value="{{$role->id}}" >{{ $role->name }}</option>
					@endforeach
					</select>
				</td>
				<td width=8%><i onclick="add({{$row->id}})" class="fa fa-plus"></i></td>
			</tr>
		@endforeach
	</tbody>
</table>
@else
	No Data Avaiable
@endif
