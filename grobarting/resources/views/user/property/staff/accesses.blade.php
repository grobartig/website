<br />
<h4>Role:{{$role->name}}</h4>
<div class="row m-t-lg">
	@foreach( $accesses as $access )
		@php( $check = "" )
        @foreach($data as $row)
            @if($row->access_id == $access->id)
                @php( $check = "checked" )
            @endif
        @endforeach
		<div class="col-sm-6 col-sm-4 col-md-4 col-lg-4">
			<div class="checkbox-bird">
				<input type="checkbox"  {{ $check }}>
				<label class="item" >{{ ucwords(str_replace('-', ' ',$access->name)) }}</label>
			</div>
		</div>
	@endforeach
</div>