
@extends('user.property.about.tab')
@section ('section-title', 'Price')
@section ('about-active-price', 'active')
@section ('tab-css')
	
@endsection

@section ('imageuploadjs')
   
@endsection


@section ('tab-js')

@endsection

@section ('about')
	
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
			<input type="hidden" name="property_id" value="{{ $data->property_id }}">
			@php( $action_id = $data->property->action_id )
			@if($action_id == 1 || $action_id == 3)
			<div class="form-group row">
				<label class="col-sm-4 form-control-label" >Asking Price (USD)</label>
				<div class="col-sm-5">
					<input 	id="selling_price"
							name="selling_price"
							value = "{{ $data->selling_price }}"
							type="number"
							placeholder = ""
							step="0.01"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
				<div class="col-sm-3">
					<select id="selling_price_type_id" name="selling_price_type_id" class="form-control">
						@if( $data->selling_price_type_id != 0 )
							<option value="{{ $data->selling_price_type_id }}"  >{{ $data->selling_price_type->name}}</option>
						@else
							<option value="0" >Select Unit </option>
						@endif
						
						@foreach( $selling_price_types as $row)
							@if($row->id != $data->selling_price_type_id )
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 form-control-label" >Selling Price (USD)</label>
				<div class="col-sm-5">
					<input 	id="actual_selling_price"
							name="actual_selling_price"
							value = "{{ $data->actual_selling_price }}"
							type="number"
							placeholder = ""
							step="0.01"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
				<div class="col-sm-3">
					<select id="actual_selling_price_type_id" name="actual_selling_price_type_id" class="form-control">
						@if( $data->actual_selling_price_type_id != 0 )
							<option value="{{ $data->actual_selling_price_type_id }}"  >{{ $data->selling_price_type->name}}</option>
						@else
							<option value="0" >Select Unit </option>
						@endif
						
						@foreach( $selling_price_types as $row)
							@if($row->id != $data->actual_selling_price_type_id )
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			@endif
			@if($action_id == 2 || $action_id == 3)
			<div class="form-group row">
				<label class="col-sm-4 form-control-label" >Rental Price (USD)</label>
				<div class="col-sm-5">
					<input 	id="rental_price"
							name="rental_price"
							value = "{{ $data->rental_price }}"
							type="number"
							placeholder = ""
							step="0.01"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
				<div class="col-sm-3">
					<select id="rental_price_type_id" name="rental_price_type_id" class="form-control">
						@if( ! is_null($data->rental_price_type_id) )
							<option value="{{ $data->rental_price_type_id }}"  >{{ $data->rental_price_type->name}}</option>
						@else
							<option value="0" >Select Unit </option>
						@endif
						
						@foreach( $rental_price_types as $row)
							@if($row->id != $data->rental_price_type_id )
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 form-control-label" >Actual Rental Price (USD)</label>
				<div class="col-sm-5">
					<input 	id="actual_rental_price"
							name="actual_rental_price"
							value = "{{ $data->actual_rental_price }}"
							type="number"
							placeholder = ""
							step="0.001"
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
				<div class="col-sm-3">
					<select id="actual_rental_price_type_id" name="actual_rental_price_type_id" class="form-control">
						@if( ! is_null($data->rental_price_type_id) )
							<option value="{{ $data->actual_rental_price_type_id }}"  >{{ $data->rental_price_type->name}}</option>
						@else
							<option value="0" >Select Unit </option>
						@endif
						
						@foreach( $rental_price_types as $row)
							@if($row->id != $data->actual_rental_price_type_id )
								<option value="{{ $row->id }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			@endif
			<div class="form-group row">
				<label class="col-sm-4 form-control-label" >Deposit</label>
				<div class="col-sm-8">
					<input 	id="deposit"
							name="deposit"
							value = "{{ $data->deposit }}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
			@if($data->updater_id != '')
			<div class="form-group row">
					<label class="col-sm-4 form-control-label" >Update By</label>
					<div class="col-sm-8">
						<input 	id="deposit"
								disabled="" 
								name="deposit"
								value = ""
								type="text"
								placeholder = ""
							   	class="form-control"
							   	data-validation="[L>=1, L<=200]"/>
					</div>
				</div>
			@endif
			@if(checkRole($id, 'update-price'))
			<div class="form-group row">
				<label class="col-sm-4 form-control-label"></label>
				<div class="col-sm-8">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				</div>
			</div>
			@endif
	</form>
	
@endsection