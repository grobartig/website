@extends('user.property.about.tab')
@section ('section-title', 'Picture')
@section ('about-active-picture', 'active')
@php ($feature = "https://dummyimage.com/600x400/d6d2d6/9999a1&text=Free+Size")
@php ($slide = "http://via.placeholder.com/1900x630")
@php ($video_image = "https://dummyimage.com/600x400/d6d2d6/9999a1&text=Free+Size")
@if( $data->feature !="" )
	@php ($feature = asset('public/uploads/property/feature/'.$data->feature))
@endif
@if( $data->slide !="" )
	@php ($slide = asset($data->slide))
@endif
@if( $data->video_image !="" )
	@php ($video_image = asset('public/uploads/property/video_image/'.$data->video_image))
@endif
@section ('tab-css')
	<link href="{{ asset ('public/user/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/user/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 500px;
		}
	</style>
@endsection
@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection
@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			
		}); 
		
	</script>
	<script type="text/JavaScript">
		
		var btnCust = ''; 
		$("#feature").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="{{ $feature }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image free dimesion with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif"]
		});
		$("#video_image").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="{{ $video_image }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image free dimesion with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif"]
		});
		$("#slide").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="{{ $slide }}" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 1900x630 with .jpg or .png type</i></span>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif"]
		});
	</script>
@endsection

@section ('about')
	
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $data->id }}">
		<input type="hidden" name="property_id" value="{{ $data->property_id }}">

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">Feature</label>
			<div class="col-sm-10">
				<div class="kv-avatar center-block">
			        <input id="feature" name="feature" type="file" class="file-loading">
			    </div>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">Video Image</label>
			<div class="col-sm-10">
				<div class="kv-avatar center-block">
			        <input id="video_image" name="video_image" type="file" class="file-loading">
			    </div>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="email">Slide</label>
			<div class="col-sm-10">
				<div class="kv-avatar center-block">
			        <input id="slide" name="slide" type="file" class="file-loading">
			    </div>
			</div>
		</div>
		
		@if($data->updater_id != '')
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Update By</label>
				<div class="col-sm-10">
					<input 	id="deposit"
							disabled="" 
							name="deposit"
							value = ""
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
		@endif
		@if(checkRole($id, 'update-picture'))
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				
			</div>
		</div>
		@endif
	</form>
	
@endsection