@extends('user.property.about.tab')
@section ('section-title', 'Amenities')
@section ('about-active-amenity', 'active')

@section ('tab-js')
<script type="text/javascript">
	$(document).ready(function(){
		$('.item').click(function(){
			check_id = $(this).attr('for');
			feature_id = $("#"+check_id).attr('feature-id');
			features(feature_id);
		})
	})
	function features(feature_id){
		
		$.ajax({
		        url: "{{ route($route.'.check') }}?property_id={{ $id }}&feature_id="+feature_id,
		        type: 'GET',
		        data: { },
		        success: function( response ) {
		            if ( response.status === 'success' ) {
		            	toastr.success(response.msg);
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
		});
	}
</script>
@endsection

@section ('about')
	@php($able_to_check=checkRole($id, 'check-amenity'))
	@if(sizeof($features) > 1)
		<div class="row m-t-lg">
			@foreach($features as $feature)
				@php( $check = "" )
		        @foreach($dataFeature as $row)
		            @if($row->feature_id == $feature->id)
		                @php( $check = "checked" )
		            @endif
		        @endforeach
				<div class="col-sm-6 col-sm-4 col-md-4">
					<div class="checkbox-bird">
						<input type="checkbox" feature-id="{{ $feature->id }}" id="feature-{{ $feature->id }}" {{ $check }} >
						<label @if($able_to_check) class="item"  for="feature-{{ $feature->id }}" @endif>{{ $feature->name }}</label>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<br />
		This type of property has no feature.
	@endif
@endsection