@extends('user.property.about.tab')
@section ('section-title', 'Location')
@section ('about-active-location', 'active')

@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			
			$("#province-id").change(function(){
				getDistricts($(this).val());
				$("#district-id").html('<option id="0" >Select District</option>');
				$("#commune-id").html('<option id="0" >Select Commune</option>');
				lat = $("#province-id option:selected").attr('lat');
				lng = $("#province-id option:selected").attr('lng');
				//console.log(lat+' '+lng);
				makeMap(lat, lng, 10);
				codeGenerator();
			})
			$('#district-id').change(function(){
	    		getCommunes($(this).val());
	    		$("#commune-id").html('<option id="0" >Select Commune</option>');
	    		lat = $("#district-id option:selected").attr('lat');
				lng = $("#district-id option:selected").attr('lng');
				console.log(lat+' '+lng);
				makeMap(lat, lng, 13);
	    	})

	    	$('#commune-id').change(function(){
	    		//getCommunes($(this).val());
	    		lat = $("#commune-id option:selected").attr('lat');
				lng = $("#commune-id option:selected").attr('lng');
				//console.log(lat+' '+lng);
				makeMap(lat, lng, 18);
	    	})

		});

		function getDistricts(province_id){
			
			//Get Districts
			$.ajax({
			        url: "{{ route('user.property.districts') }}?province_id="+province_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var districts = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        districts += '<option value="'+response[i].id+'" lat="'+response[i].lat+'" lng="'+response[i].lng+'"  >'+response[i].name+'</option>';
					    }
					    if(districts != ""){
					    	$('#district-id').append(districts);
					    	
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		function getCommunes(district_id){
			//Empty the communes
			$("#commune-id").html('<option id="0" >Select Commune</option>');

			$.ajax({
			        url: "{{ route('user.property.communes') }}?district_id="+district_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var communes = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        communes += '<option value="'+response[i].id+'" lat="'+response[i].lat+'" lng="'+response[i].lng+'"  >'+response[i].name+'</option>';
					    }
					    if(communes != ""){
					    	$('#commune-id').append(communes);
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		

		
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyA8S3pD12gYeCiM2vUf5MuObhznkGbWNCk"></script>
	<script type="text/JavaScript">
		$(document).ready(function(){
			
			@if($data->lat != 0 && $data->lng !=0 )
				makeMap({{$data->lat}}, {{$data->lng}}, 20);
			@else 
				makeMap(11.537886, 104.910652, 10);// Map of phnom penh
			@endif
		})
		//var marker ="";
		function makeMap(lat, lng, zoom = 10){
			var latlng = new google.maps.LatLng(lat, lng);
			var map = new google.maps.Map(document.getElementById('map'), {
			    center: latlng,
			    zoom: zoom,
			    mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var marker = new google.maps.Marker({
			    position: latlng,
			    map: map,
			    title: '',
			    draggable: true
			});
			google.maps.event.addListener(marker, 'dragend', function (event) {
			    $("#lat").val(this.getPosition().lat());
			    $("#lng").val(this.getPosition().lng());
			});
		}

		function enLargeMap(){

		}
	</script>
@endsection

@section ('about')
	
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('POST') }}
			<input type="hidden" name="id" value="{{ $data->id }}">
			<input type="hidden" name="property_id" value="{{ $data->property_id }}">
			<input type="hidden" name="listing_code" value="{{ $data->listing_code }}">

			<div class="form-group row">
				<label for="province-id" class="col-sm-2 form-control-label">Province</label>
				<div class="col-sm-10">
					<select id="province-id" name="province-id" class="form-control">
						<option value="{{ $data->property->province_id }}" lat= "{{ $data->property->province->lat}}" lng= "{{ $data->property->province->lng}}" >{{ $data->property->province->en_name }}</option>
						@foreach( $provinces as $row )
							@if($row->id != $data->property->province_id)
								<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}" >{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="district-id" class="col-sm-2 form-control-label">District</label>
				<div class="col-sm-10">
					<select id="district-id" name="district-id" class="form-control">
						
						@if( ! is_null($data->property->district_id) )
							<option value="{{ $data->property->district_id }}" lat= "{{ $data->property->district->lat}}" lng= "{{ $data->property->district->lng}}" >{{ $data->property->district->en_name}}</option>
						@else
							<option value="0" lat= "0" lng= "" >Select District</option>
						@endif
						
						@foreach( $districts as $row)
							@if($row->id != $data->property->district_id)
								<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}">{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="commune-id" class="col-sm-2 form-control-label">Commune</label>
				<div class="col-sm-10">
					<select id="commune-id" name="commune-id" class="form-control">
						@if( ! is_null($data->property->commune_id) )
							<option value="{{ $data->property->commune_id }}" lat= "{{ $data->property->commune->lat}}" lng= "{{ $data->property->commune->lng}}" >{{ $data->property->commune->en_name }}</option>
						@else
							<option value="0" lat= "0" lng= "" >Select Commune</option>
						@endif

						@foreach( $communes as $row)
							@if($row->id != $data->property->commune_id)
								<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}">{{ $row->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Road</label>
				<div class="col-sm-10">
					<input 	id="road"
							name="road"
							value = "{{ $data->road }}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Address</label>
				<div class="col-sm-10">
					<input 	id="address"
							name="address"
							value = "{{ $data->address }}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
			@if($data->updater_id != '')
				<div class="form-group row">
					<label class="col-sm-2 form-control-label" >Update By</label>
					<div class="col-sm-10">
						<input 	id="deposit"
								disabled="" 
								name="deposit"
								value = ""
								type="text"
								placeholder = ""
							   	class="form-control"
							   	data-validation="[L>=1, L<=200]"/>
					</div>
				</div>
			@endif
			<!--
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Zip Code</label>
				<div class="col-sm-10">
					<input 	id="zip_code"
							name="zip_code"
							value = "{{ $data->zip_code }}"
							type="text"
							placeholder = "12345"
						   	class="form-control"
						   	data-validation="[L>=5, L<=15, MIXED]"
						   	data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
				</div>
			</div>
			-->
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Latitude </label>
				<div class="col-sm-10">
					<input 	id="lat"
							name="lat"
							value = "{{ $data->lat }}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Longitude</label>
				<div class="col-sm-10">
					<input 	id="lng"
							name="lng"
							value = "{{ $data->lng }}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Display Map</label>
				<div class="col-sm-10">
					<div class="radio">
						<input type="radio" name="is-actual-map" id="radio-1" value="1" @if($data->is_actual_map == 1 ) checked @endif>
						<label for="radio-1">Actual</label>
					</div>
					<div class="radio">
						<input type="radio" name="is-actual-map" id="radio-2" value="0" @if($data->is_actual_map == 0 ) checked @endif>
						<label for="radio-2">Surounded</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Map</label>
				<div id="map-cnt" class="col-sm-10">
					<div id="map" style="height:400px;border: 1px solid gray;"></div>
				</div>
			</div>

			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>
				</div>
			</div>
	</form>
	
@endsection