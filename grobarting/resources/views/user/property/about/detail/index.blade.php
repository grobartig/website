@extends('user.property.about.tab')
@section ('section-title', 'Detail Information')
@section ('about-active-detail', 'active')
@section ('tab-css')
	<style type="text/css">
		.pop>tbody>tr>td, .pop>thead>tr>th{
			padding:4px !important;
			height: 0px;
			font-size: 13px;
		}
		.pop>tbody>tr>td>i{
			cursor:pointer;
		}
	</style>
@endsection


@section ('tab-js')
<script type="text/javascript">
	$(document).ready(function(){
			$("#btn-add-more-data").click(function(){
				selected(); 
				search();
			})

			$("#search").keypress(function(e) {
			    if(e.which == 13) {
			       search();
			    }
			});
	})

	var source;
	function isbefore(a, b) {
	    if (a.parentNode == b.parentNode) {
	        for (var cur = a; cur; cur = cur.previousSibling) {
	            if (cur === b) {
	                return true;
	            }
	        }
	    }
	    return false;
	}

	function dragenter(e) {
		
	    var targetelem = e.target;
	    //console.log(e);
	    if (targetelem.nodeName == "TD") {
	       targetelem = targetelem.parentNode;   
	    }  
	    
	    if (isbefore(source, targetelem)) {
	        targetelem.parentNode.insertBefore(source, targetelem);
	        //console.log('moved :'+order);
	    } else {
	        targetelem.parentNode.insertBefore(source, targetelem.nextSibling);

	    }
	}

	function dragstart(e) {
	    source = e.target;
	    e.dataTransfer.effectAllowed = 'move';

	}
	function dragend(e){
		//console.log(e.target);
		elements = $(".moveable");
		//console.log(elements);
		data = [];
		for(i=0; i<elements.length; i++){
			var obj = new Object();
			obj.id = $('#'+elements[i].id).attr('data-id');
			obj.order = i+1;
			data[i] = obj;
		}
		
		var string = JSON.stringify(data);
		console.log(string);
		$.ajax({
		        url: "{{ route($route.'.order') }}?id={{$id}}",
		        type: 'POST',
		        data: {string:string},
		        success: function( response ) {
		         	if ( response.status === 'success' ) {
	            		swal("Nice!", response.msg ,"success");
	            	
		            }else{
		            	swal("Error!", "Sorry there is an error happens. " ,"error");
		            }
				    
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
					
		});
	}


	function save(id){
		value = $("#value-"+id).val();
		$.ajax({
		        url: "{{ route($route.'.save') }}?id={{$id}}&property_detail_id="+id+"&value="+value,
		        type: 'GET',
		        data: {},
		        success: function( response ) {
		         	toastr.success(response.msg);
		        },
		        error: function( response ) {
		           swal("Error!", "Sorry there is an error happens. " ,"error");
		        }
					
		});
	}
	function removeDetail(id){
	swal({
			title: "Are you sure you wan to delete?",
			text: "",
			type: "warning",
			showCancelButton: true,
			cancelButtonClass: "btn-default",
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		},
		function(){
			
			window.location.replace('{{ route($route.'.trash') }}?id={{$id}}&property_detail_id='+id);
		});
	}

	function search(){
			key 		= $('#search').val();
			$.ajax({
			        url: "{{ route($route.'.search') }}?id={{$id}}&key="+key,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#result").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}
		function selected(){
			$.ajax({
			        url: "{{ route($route.'.selected') }}?id="+{{ $id }},
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function remove(detail_id){
			$.ajax({
			        url: "{{ route($route.'.remove') }}?id={{ $id }}&detail_id="+detail_id,
			        type: 'DELETE',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          search();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}
		function add(detail_id){
			$.ajax({
			        url: "{{ route($route.'.add') }}?id={{ $id }}&detail_id="+detail_id,
			        type: 'PUT',
			        data: {},
			        success: function( response ) {
			          $("#selected").html(response);
			          search();
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }	
			});
		}

		function saveAll(){
			values = $(".values");
			items = []; 
			for(i=0; i<values.length; i++){
				value = values[i];
				item = [value.getAttribute('data-id'), value.value];
				items.push(item);
			}
			console.log(items);

			if( items.length != 0 ){
				$.ajax({
			         url: "{{ route($route.'.save-all') }}?id={{$id}}",
			        method: 'POST',
			        data: { items:JSON.stringify(items) },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	displayAvaiableLists();
			            	$('.modal-content').unblock();
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});


			}else{
				swal("Please select at least an account.");
				
			}

		}

		function updateStatus(id){
		     	thestatus = $('#status-'+id);
		     	active = thestatus.attr('data-value');

		     	if(active == 1){
		     		active = 0;
		     		thestatus.attr('data-value', 1);
		     	}else{
		     		active = 1;
		     		thestatus.attr('data-value', 0);
		     	}

		     	$.ajax({
			        url: "{{ route($route.'.update-status') }}",
			        method: 'POST',
			        data: {id:id, status:active },
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});
			}
	
</script>
@endsection

@section ('about')
	<br />
	<div>
		<div class="col-md-12">
			<button id="btn-add-refresh" onclick="window.location.href='{{ route($route.'.index', $id) }}'"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;margin-left: 5px;"><span class="fa fa-refresh"></span></button>
			@if(checkRole($id, 'create-detail'))<button id="btn-add-more-data" data-toggle="modal" data-target="#modal" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;margin-bottom: 5px;"><span class="fa fa-plus"></span></button>@endif
		</div>
	</div><!--.row-->
	@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Detail</th>
					<th>Showed</th>
					<th>Value</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr id="element-{{ $row->id }}" data-id="{{ $row->id }}" class="moveable" feature-order="{{ $row->featured_order }}" draggable="true" ondragenter="dragenter(event)" ondragstart="dragstart(event)" ondragend="dragend(event)">
						<td>{{ $i++ }}</td>
						<td>{{ $row->detail->en_name }} ({{ $row->detail->unit }})</td>
						<td>
							<div class="checkbox-toggle">
							        <input onclick="updateStatus({{ $row->id }})" type="checkbox" id="status-{{ $row->id }}" @if ($row->is_showed == 1) checked data-value="1" @else data-value="0" @endif >
							        <label for="status-{{ $row->id }}"></label>
						        </div>
							</td>
						<td><input 	id="value-{{$row->id}}" value = "{{$row->value}}" data-id="{{$row->id}}" type="text" placeholder = "Detail Value" class="form-control values" /></td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		@if(checkRole($id, 'update-detail'))<button onclick="save({{$row->id}})" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-save"></span></button>@endif
	                           		@if(checkRole($id, 'delete-detail'))<button onclick="removeDetail({{$row->id}})" class="tabledit-edit-button btn btn-sm btn-danger" style="float: none;"><span class="fa fa-trash"></span></button>@endif
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>

		</table>
		<br/>
		<button onclick="saveAll()" class="tabledit-edit-button btn btn btn-success" style="float: right;"><span class="fa fa-save"> Save </span></button>
	</div >
	@else
	<span>No Detail information for this property.</span>
	@endif
@endsection
@section ('modal')
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="font-icon-close-2"></i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Detail information item to property</h4>
                </div>
                <div class="modal-upload menu-bottom">
                    <div class="modal-upload-cont">
                        <div class="modal-upload-cont-in">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab-upload-2">
                                    <div class="modal-upload-body scrollable-block">
                                       	<div class="container-fluid">
                                       		<div class="row">
                                       			<div class="col-xs-5">
                                       				<div class="chat-list-search">
														<input type="text" id="search" class="form-control form-control-rounded" placeholder="Name of List">
													
													</div>
                                       				<div id="result">
                                       					
                                       				</div>
                                       			</div>
                                       			<div class="col-xs-7">
                                       				<div class="chat-area-header">
														<div class="clean">Existing Detail Information</div>
													</div>
													<div id="selected">
                                       					
                                       				</div>
                                       			</div>
                                       			
                                       		</div>
                                       	</div> 
                                    </div><!--.modal-upload-body-->
                                    <div class="modal-upload-bottom">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-rounded btn-default">Close</button>
                                    </div><!--.modal-upload-bottom-->
                                </div><!--.tab-pane-->
                              
                            </div><!--.tab-content-->
                        </div><!--.modal-upload-cont-in-->
                    </div><!--.modal-upload-cont-->
                   
                </div>
            </div>
        </div>
</div><!--.modal-->
@endsection