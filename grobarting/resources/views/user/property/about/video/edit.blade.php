@extends('user.property.about.tab')
@section ('section-title', 'Edit Video')
@section ('about-active-video', 'active')
@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 

			$("#youtube-id").blur(function(){
				youtube_id= $(this).val();
				if(youtube_id != ""){
					$("#iframe").attr("src", "//www.youtube.com/embed/"+youtube_id);
				}
			})
			
		}); 
		
	</script>
@endsection



@section ('about')
	<br />
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route($route.'.create', $id) }}" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;   "><span class="fa fa-plus"></span></a> &nbsp;
			<a href="{{ route($route.'.index', $id) }}" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right; margin-right: 5px;"><span class="fa fa-arrow-left"></span></a>
		</div>
	</div><!--.row-->
	
	
	@include('user.layouts.error')


	<form id="form" action="{{ route($route.'.update') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="property_id" value="{{ $id}}">
		<input type="hidden" name="video_id" value="{{ $data->id}}">

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Youtube ID</label>
			<div class="col-sm-10">
				<input 	id="youtube-id"
						name="youtube-id"
						value = "{{ $data->youtube_id }}"
						type="text"
						placeholder = "Youtube ID"
					   	class="form-control"
					   	data-validation="[L>=6, L<=18]"
					   	/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Video</label>
			<div class="col-sm-10">
				<div class="embed-responsive embed-responsive-4by3">
					<iframe id="iframe" class="embed-responsive-item" src="//www.youtube.com/embed/{{ $data->youtube_id }}"></iframe>
				</div>
			</div>
		</div>
		@if($data->updater_id != '')
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Update By</label>
				<div class="col-sm-10">
					<input 	id="deposit"
							disabled="" 
							name="deposit"
							value = "{{ $data->user->en_name }} On {{$data->updated_at}}"
							type="text"
							placeholder = ""
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"/>
				</div>
			</div>
		@endif
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				@if(checkRole($id, 'update-video'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
				@if(checkRole($id, 'delete-video'))<button type="button" onclick="deleteConfirm('{{ route($route.'.trash', ['id'=>$id, 'video_id'=>$data->id]) }}', '{{ route($route.'.index', $id) }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>@endif
			</div>
		</div>
	</form>
	
@endsection