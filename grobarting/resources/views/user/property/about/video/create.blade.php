@extends('user.property.about.tab')
@section ('section-title', 'Add Video')
@section ('about-active-video', 'active')
@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			$("#video-cnt").hide();
			$("#youtube-id").blur(function(){
				youtube_id= $(this).val();
				if(youtube_id != ""){
					$("#video-cnt").show();
					$("#iframe").attr("src", "//www.youtube.com/embed/"+youtube_id);
				}
			})
			
		}); 
		
	</script>
@endsection



@section ('about')
	<br />
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route($route.'.create', $id) }}" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;"><span class="fa fa-arrow-left"></span></a>
		</div>
	</div><!--.row-->
	@include('user.layouts.error')


	@php ($youtube_id = "")

   	@if (Session::has('invalidData'))
        @php ($invalidData = Session::get('invalidData'))
        @php ($youtube_id = $invalidData['youtube_id'])

   	@endif
	<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<input type="hidden" name="property_id" value="{{ $id}}">
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Youtube ID</label>
			<div class="col-sm-10">
				<input 	id="youtube-id"
						name="youtube-id"
						value = "{{ $youtube_id }}"
						type="text"
						placeholder = "Youtube ID"
					   	class="form-control"
					   	data-validation="[L>=6, L<=18]" />
			</div>
		</div>
		<div id="video-cnt" class="form-group row">
			<label class="col-sm-2 form-control-label" >Video</label>
			<div class="col-sm-10">
				<div class="embed-responsive embed-responsive-4by3">
					<iframe id="iframe" class="embed-responsive-item" src=""></iframe>
				</div>
			</div>
		</div>
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				
			</div>
		</div>
	</form>
	
@endsection