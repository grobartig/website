@extends('user.property.about.tab')
@section ('section-title', 'Videos')
@section ('about-active-video', 'active')

@section ('about')
	<br />
	<div class="row">
		<div class="col-md-12">
			@if(checkRole($id, 'create-video'))
			<a href="{{ route($route.'.create', ['id'=>$id]) }}" class="tabledit-delete-button btn btn-sm btn-primary" style="float: right;"><span class="fa fa-plus"></span></a>
			@endif
		</div>
	</div><!--.row-->
	<br />
	@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Video</th>
					<th>Updated Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $row->en_title }}</td>
						<td width=50%>
							<div class="embed-responsive embed-responsive-4by3">
							  <iframe class="embed-responsive-item" src="//www.youtube.com/embed/{{ $row->youtube_id }}"></iframe>
							</div>
						</td>
						<td>{{ $row->updated_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		@if(checkRole($id, 'view-video'))
	                           		<a href="{{ route($route.'.edit', ['id'=>$id, 'video_id'=>$row->id]) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
	                           		@endif
	                           		@if(checkRole($id, 'delete-video'))
	                           		<a href="#" onclick="deleteConfirm('{{ route($route.'.trash', ['id'=>$id, 'video_id'=>$row->id]) }}', '{{ route($route.'.index', ['id'=>$id]) }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></a>
	                           		@endif
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	<span>No Videos</span>
	@endif
@endsection