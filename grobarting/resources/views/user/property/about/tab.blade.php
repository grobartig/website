@extends('user.property.tabForm')
@section ('tab-active-about', 'active')

@section ('tab-content')
	
	<section class="box-typical files-manager">
		<nav class="files-manager-side" style="height: auto;">
			<ul class="files-manager-side-list">
				@if(checkRole($id, 'view-price'))<li><a href="{{ route('user.property.about.price.index', $id) }}" class="@yield ('about-active-price')">Price</a></li>@endif
				@if(checkRole($id, 'view-location'))<li><a href="{{ route('user.property.about.location.index', $id) }}" class="@yield ('about-active-location')">Location</a></li>@endif
				@if(checkRole($id, 'view-detail'))<li><a href="{{ route('user.property.about.detail.index', $id) }}" class="@yield ('about-active-detail')">Details</a></li>@endif
				@if(checkRole($id, 'view-amenity'))<li><a href="{{ route('user.property.about.amenity.index', $id) }}" class="@yield ('about-active-amenity')">Amenities</a></li>@endif
				@if(checkRole($id, 'view-picture'))<li><a href="{{ route('user.property.about.picture.index', $id) }}" class="@yield ('about-active-picture')">Feature & Slide</a></li>@endif
				@if(checkRole($id, 'view-photo'))<li><a href="{{ route('user.property.about.photo.index', $id) }}" class="@yield ('about-active-photo')">Photos</a></li>@endif
				@if(checkRole($id, 'view-image'))<li><a href="{{ route('user.property.about.image.index', $id) }}" class="@yield ('about-active-image')">Images</a></li>@endif
				@if(checkRole($id, 'view-video'))<li><a href="{{ route('user.property.about.video.index', $id) }}" class="@yield ('about-active-video')">Videos</a></li>@endif
			</ul>
		</nav><!--.files-manager-side-->

		<div class="files-manager-panel">
			<div class="files-manager-panel-in">
				<div class="container-fluid">
					@yield ('about')
				</div>
			</div><!--.files-manager-panel-in-->
		</div><!--.files-manager-panels-->
	</section>
	
@endsection