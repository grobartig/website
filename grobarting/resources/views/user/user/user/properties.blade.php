@extends($route.'.tabForm')
@section ('section-title', "User's logged records")
@section ('tab-active-properties', 'active')
@section ('tab-css')

@endsection

@section ('tab-js')
<script type="text/javascript">
	function search(){
		d_from 	= $('#from').val();
		d_till 	= $('#till').val();
		limit 	= $('#limit').val();
		role 		= $('#role-id').val();

		url="?limit="+limit;
		if(role != 0){
			url += '&role='+role;
		}
		if(isDate(d_from)){
			if(isDate(d_till)){
				url+='&from='+d_from+'&till='+d_till;
			}
		}
		
		$(location).attr('href', '{{ route($route.'.properties', $id) }}'+url);
	}
</script>

@endsection

@section ('tab-content')
	
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<div id="from-cnt" class='input-group date'>
					<input id="from" type='text' class="form-control" value="{{ isset($appends['from'])?$appends['from']:'' }}" placeholder="From" />
				<span class="input-group-addon">
					<i class="font-icon font-icon-calend"></i>
				</span>
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group">
				<div id="till-cnt" class='input-group date' >
					<input id="till" type='text' class="form-control" value="{{ isset($appends['till'])?$appends['till']:''  }}" placeholder="Till" />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<select id="role-id" class="form-control">
					@php( $role = isset($appends['role'])?$appends['role']:0 )
					@if($role != 0)
						@php( $lable = DB::table('roles')->find($role) )
						@if( sizeof($lable) == 1 )
							<option value="{{ $lable->id }}" >{{ $lable->name }}</option>
						@endif
					@endif
					<option value="0" >Role</option>
					@foreach( $roles as $row)
						@if($row->id != $role)
							<option value="{{ $row->id }}" >{{ $row->name }}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<button onclick="search()"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-search"></span></button>
		</div>
	</div><!--.row-->
				
		
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Role</th>
					<th>Listing Code</th>
					<th>Action</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					@php($property = $row->property)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $row->role->name }}</td>
						<td>{{makeListingCode($property)}}</td>
						<td>{{$property->action->en_name}}</td>
						<td>{{ $row->created_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		<a href="{{ route('user.property.check-role', $row->property_id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
				
			</tbody>
		</table>

	</div >
	
	<div class="row">
		<div class="col-xs-2">
			<select id="limit" onchange="search()" class="form-control" style="margin-top: 15px;width:50%">
				@if(isset($appends['limit']))
				<option>{{ $appends['limit'] }}</option>
				@endif
				<option>10</option>
				<option>20</option>
				<option>30</option>
				<option>40</option>
				<option>50</option>
				<option>60</option>
				<option>70</option>
				<option>80</option>
				<option>90</option>
				<option>100</option>
			</select>
		</div>
		<div class="col-xs-10">

			{{ $data->appends($appends)->links('vendor.pagination.custom-html') }}
		</div>
	</div>
		

@endsection