@extends($route.'.main') @section ('section-title', '') @section ('section-css')
<link href="{{ asset ('public/user/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset ('public/user/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css" />
<!-- some CSS styling changes and overrides -->
<style>
    .kv-avatar .file-preview-frame,
    .kv-avatar .file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }

    
    .kv-avatar .file-input {
        display: table-cell;
        max-width: 220px;
    }
</style>
@endsection @section ('imageuploadjs')
<script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset ('public/user/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection @section ('section-js')
<script>
    var btnCust = '';

    var btnCust = '';
    $("#image").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="http://via.placeholder.com/66X66" alt="Missing Image" class="img img-responsive"><span class="text-muted">Click to select <br /><i style="font-size:12px">Image dimesion must be 66X66 px with .jpg or .png type</i></span>',
        layoutTemplates: {
            main2: '{preview} ' + btnCust + ' {remove} {browse}'
        },
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
</script>
@endsection @section ('section-content')
<div class="container-fluid">
    @include('user.layouts.error') @php ($en_name = "") @php ($kh_name = "") @php ($cn_name = "") @php ($en_title = "") @php ($kh_title = "") @php ($cn_title = "") @php ($en_description = "") @php ($kh_description = "") @php ($cn_description = "") @if (Session::has('invalidData')) @php ($invalidData = Session::get('invalidData')) @php ($en_name = $invalidData['en_name']) @php ($kh_name = $invalidData['kh_name']) @php ($cn_name = $invalidData['cn_name']) @php ($en_title = $invalidData['en_title']) @php ($kh_title = $invalidData['kh_title']) @php ($cn_title = $invalidData['cn_title']) @php ($en_description = $invalidData['en_description']) @php ($kh_description = $invalidData['kh_description']) @php ($cn_description = $invalidData['cn_description']) @endif

    <form id="form" action="{{ route($route.'.store') }}" name="form" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }} {{ method_field('PUT') }}

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (EN) </label>
            <div class="col-sm-10">
                <input id="en_name" name="en_name" value="{{$en_name}}" type="text" placeholder="Please enter name" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (Kh) </label>
            <div class="col-sm-10">
                <input id="kh_name" name="kh_name" value="{{$kh_name}}" type="text" placeholder="Please enter kh name" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>

        <!-- <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="name">Name (CN) </label>
            <div class="col-sm-10">
                <input id="cn_name" name="cn_name" value="{{$cn_name}}" type="text" placeholder="Please enter cn name" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="title">Title (EN)</label>
            <div class="col-sm-10">
                <input id="en_title" name="en_title" value="{{$en_title}}" type="text" placeholder="Please enter title" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="title">Title (KH)</label>
            <div class="col-sm-10">
                <input id="kh_title" name="kh_title" value="{{$kh_title}}" type="text" placeholder="Please enter kh title" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="title">Title (CN)</label>
            <div class="col-sm-10">
                <input id="cn_title" name="cn_title" value="{{$cn_title}}" type="text" placeholder="Please enter cn title" class="form-control" data-validation="[L>=1, L<=200]" />

            </div>
        </div> -->

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="description">Description (EN)</label>
            <div class="col-sm-10">
                <div class="summernote-theme-2">
                    <textarea id="en_description" name="en_description" class="form-control ">{{$en_description}} </textarea>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="kh_description">Description (KH)</label>
            <div class="col-sm-10">
                <div class="summernote-theme-2">
                    <textarea id="kh_description" name="kh_description" class="form-control ">{{$kh_description}} </textarea>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="cn_description">Description (CN)</label>
            <div class="col-sm-10">
                <div class="summernote-theme-2">
                    <textarea id="cn_description" name="cn_description" class="form-control ">{{$cn_description}} </textarea>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label" for="email">Image</label>
            <div class="col-sm-10">
                <div class="kv-avatar center-block">
                    <input id="image" name="image" type="file" class="file-loading">
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 form-control-label"></label>
            <div class="col-sm-10">

                <button type="submit" class="btn btn-success">
                    <fa class="fa fa-plus">
                        </i> Create</button>
            </div>
        </div>
    </form>
</div>

@endsection