@extends ($route.'.main')
@section ('section-title', 'Create New Member')
@section ('section-css')
	<link href="{{ asset ('public/cp/css/plugin/fileinput/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ('public/cp/css/plugin/fileinput/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
	<!-- some CSS styling changes and overrides -->
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    max-width: 220px;
		}
	</style>
@endsection

@section ('imageuploadjs')
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/fileinput.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset ('public/cp/js/plugin/fileinput/theme.js') }}" type="text/javascript"></script>
@endsection

@section ('section-js')
	
@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('cp.layouts.error')

		@php ($first_name = "")
		@php ($last_name = "")
		@php ($email = "")
		@php ($subject = "")
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($first_name = $invalidData['first_name'])
            @php ($last_name = $invalidData['last_name'])
            @php ($email = $invalidData['email'])
            @php ($subject = $invalidData['subject'])
            
       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="first_name">First_Name</label>
				<div class="col-sm-10">
					<input 	id="first_name"
							name="first_name"
						   	value = "{{$first_name}}"
						   	type="text"
						   	placeholder = "Enter first_name."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 6 and 18 characters. No special characters allowed." />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="last_name">Last_Name</label>
				<div class="col-sm-10">
					<input 	id="last_name"
							name="last_name"
						   	value = "{{$last_name}}"
						   	type="text"
						   	placeholder = "Enter last_name."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 1 and 200 characters. No special characters allowed." />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Email</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{ $email }}"
							type="text"
							placeholder = "Eg. you@example.com"
						   	class="form-control"
						   	data-validation="[EMAIL]">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="subject">Subject</label>
				<div class="col-sm-10">
					<input 	id="subject"
							name="subject"
							value = "{{ $subject }}"
							type="text"
							placeholder = "Eg. you@example.com"
						   	class="form-control"
						   	data-validation="[EMAIL]">
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="message">Message</label>
				<div class="col-sm-10">
					<input 	id="message"
							name="message"
						   	value = ""
						   	type="text"
						   	placeholder = "Enter message."
						   	class="form-control"
						   	data-validation="[L>=1, L<=200]"
							data-validation-message="$ must be between 1 and 200 characters. No special characters allowed." />
							
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection