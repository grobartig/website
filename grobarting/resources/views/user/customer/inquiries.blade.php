@extends($route.'.tab')
@section ('section-title', 'inquiries')
@section ('tab-active-enquiry', 'active')

@section ('section-js')
<script type="text/javascript">	
	$(document).ready(function(event){

		$("#btn-property-code").click(function(){
    		create();
    	})
		get();
	});

	@if(checkPermision($route.'.create-inquiry'))
	function create(){
		
			swal({
				title: "Property Inquiries",
				text: "Please type a listing code:",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: "Write something"
			}, function (inputValue) {
				if (inputValue === false) return false;
				if (inputValue === "") {
					swal.showInputError("You need to write a listing code!");
					return false
				}
				
				//swal("Nice!", "You wrote: " + inputValue, "success");
				$.ajax({
			        url: "{{ route($route.'.inquiries-store') }}?cutomer_id={{$id}}",
			        method: 'POST',
			        data: { listing_code:inputValue},
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	location.reload();
			            }else if( response.status === 'erorr' ){
			            	swal("Sorry!", response.msg );
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});

			});
	}
	@endif

	function get(){
    	$.ajax({
					        url: "{{ route($route.'.inquiries-data',$id) }}",
					        type: 'GET',
					        data: {},
					        success: function( response ) {
					           $("#data-cnt").html(response);
					        },
					        error: function( response ) {
					           swal("Error!", "Sorry there is an error happens. " ,"error");
					        }
				})
    }

    function update(id) {
    	message = $("#message-"+id).val();
    		if(message != ""){
	    			$.ajax({
						        url: "{{ route($route.'.update-inquiries') }}",
						        type: 'POST',
						        data: {id:id, message:message },
						        success: function( response ) {
						            if ( response.status === 'success' ) {
						            	swal("Nice!", response.msg ,"success");
						            	//get_routes();
						            }else{
						            	swal("Error!", "Sorry there is an error happens. " ,"error");
						            }
						        },
						        error: function( response ) {
						           swal("Error!", "Sorry there is an error happens. " ,"error");
						        }
						
					});
			}else{
				swal("Error!", "Sorry! Message cannot be empty " ,"error");
	    		$("#message-"+id).addClass("error");
	    		$("#message-"+id).click(function(){
	    			$("#message-"+id).removeClass("error");
	    		})
			}
    }

</script>
@endsection

@section ('tab-content')
	<div>
		<div class="col-md-12">
			@if(checkPermision($route.'.create-inquiry'))<a style="float: right;margin-bottom: 10px;margin-top: -10px;" id="btn-property-code" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>@endif	
		</div>
	</div><!--.row-->
	<div id="data-cnt">
								
	</div >
@endsection