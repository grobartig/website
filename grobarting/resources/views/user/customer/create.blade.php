@extends($route.'.main')
@section ('section-title', 'Create New')

@section ('section-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			// $('#form').validate({
			// 	modules : 'file',
			// 	submit: {
			// 		settings: {
			// 			inputContainer: '.form-group',
			// 			errorListClass: 'form-tooltip-error'
			// 		}
			// 	}
			// }); 
			

		}); 
		
	</script>

@endsection

@section ('section-content')
	<div class="container-fluid">
		@include('user.layouts.error')

		@php ($title_id = 1)
		@php ($apply = "Buy")
		@php ($reason = "")
		@php ($first_name = "")
		@php ($last_name = "")
		@php ($company = "")
		@php ($department = "")
		@php ($email = "")
		@php ($phone = "")
		@php ($address_line_1 = "")
		@php ($address_line_2 = "")
		<!-- @php ($address_line_3 = "") -->
		@php ($country = "")
		@php ($postal_code = "")
       
       	@if (Session::has('invalidData'))
            @php ($invalidData = Session::get('invalidData'))
            @php ($title_id = $invalidData['title_id'])
            @php ($apply = $invalidData['apply'])
            @php ($reason = $invalidData['reason'])
            @php ($first_name = $invalidData['first_name'])
            @php ($last_name = $invalidData['last_name'])
            @php ($company = $invalidData['company'])
            @php ($department = $invalidData['department'])
            @php ($email = $invalidData['email'])
            @php ($phone = $invalidData['phone'])
            @php ($address_line_1 = $invalidData['address_line_1'])
            @php ($address_line_2 = $invalidData['address_line_2'])
            <!-- @php ($address_line_3 = $invalidData['address_line_3']) -->
            @php ($country = $invalidData['country'])
            @php ($postal_code = $invalidData['postal_code'])
       	@endif
		<form id="form" action="{{ route($route.'.store') }}" name="form" method="POST"  enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PUT') }}

			<!-- <div class="form-group row">
				<label class="col-sm-2 form-control-label" for="apply">Active</label>
				<div class="col-sm-10">
					<input 	id="apply"
							name="apply"
						   	value = "{{$apply}}"
						   	type="text"
						   	placeholder = "Eg. Enter Buy Sell or Rent "
						   	class="form-control"
							/>
							
				</div>
		</div> -->
		<!-- <div class="form-group row">
			<label class="col-sm-2 form-control-label" for="reason">Reason</label>
			<div class="col-sm-10">
				<input 	id="reason"
						name="reason"
					   	value = "{{$reason}}"
					   	type="text"
					   	placeholder = "Eg. Enter Your Reason "
					   	class="form-control"
						/>
						
			</div>
		</div> -->
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="apply">Active</label>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="apply" id="radio-1" value="Buy" @if($apply == "Buy" ) checked @endif >
						<label for="radio-1">Buy</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="apply" id="radio-2" value="Sell" @if($apply == "Sell" ) checked @endif >
						<label for="radio-2">Sell</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="apply" id="radio-3" value="Rent" @if($apply == "Rent" ) checked @endif >
						<label for="radio-3">Rent</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="apply" id="radio-4" value="Business Investment Consultation" @if($apply == "Business Investment Consultation" ) checked @endif >
						<label for="radio-4"> Business Investment Consultation </label>
					</div>
				</div>
		</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="first_name">First Name</label>
				<div class="col-sm-10">
					<input 	id="first_name"
							name="first_name"
						   	value = "{{$first_name}}"
						   	type="text"
						   	placeholder = "Eg. Jhon "
						   	class="form-control"
							/>
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="last_name">Last Name</label>
				<div class="col-sm-10">
					<input 	id="last_name"
							name="last_name"
						   	value = "{{$last_name}}"
						   	type="text"
						   	placeholder = "Eg. son "
						   	class="form-control"
							/>
							
				</div>
			</div>
			<!-- <div class="form-group row">
				<label class="col-sm-2 form-control-label" for="kh_content">Title</label>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-1" value="1" @if($title_id == 1 ) checked @endif >
						<label for="radio-1">Mr.</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-2" value="2" @if($title_id == 2 ) checked @endif >
						<label for="radio-2">Mrs.</label>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="radio  margin-top-10">
						<input type="radio" name="title_id" id="radio-3" value="3" @if($title_id == 3 ) checked @endif >
						<label for="radio-3">Miss.</label>
					</div>
				</div>
			</div> -->
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="company">Company</label>
				<div class="col-sm-10">
					<input 	id="company"
							name="company"
						   	value = "{{$company}}"
						   	type="text"
						   	placeholder = "Eg. Your Company Name "
						   	class="form-control"
							/>
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="department">Department</label>
				<div class="col-sm-10">
					<input 	id="department"
							name="department"
						   	value = "{{$department}}"
						   	type="text"
						   	placeholder = "Eg. Your Department Name "
						   	class="form-control"
							/>
							
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="phone">Phone </label>
				<div class="col-sm-10">
					<input 	id="phone"
							name="phone"
						   	value = "{{$phone}}"
						   	type="text" 
						   	placeholder = "Eg. 093123457"
						   	class="form-control"
						   	data-validation="[L>=9, L<=10, numeric]"
							data-validation-message="$ is not correct." 
							data-validation-regex="/(^[00-9].{8}$)|(^[00-9].{9}$)/"
							data-validation-regex-message="$ must start with 0 and has 9 or 10 digits" />
							
				</div>
			</div>
			
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">E-mail</label>
				<div class="col-sm-10">
					<input 	id="email"
							name="email"
							value = "{{$email}}"
							type="text"
							placeholder = "Eg. username@example.com"
						   	class="form-control">
				</div>
			</div>
			
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Address Line 1</label>
				<div class="col-sm-10">
					<input 	id="address_line_1"
							name="address_line_1"
							value = "{{$address_line_1}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Address Line 2</label>
				<div class="col-sm-10">
					<input 	id="address_line_2"
							name="address_line_2"
							value = "{{$address_line_2}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
			<!-- <div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Address Line 3</label>
				<div class="col-sm-10">
					<input 	id="address_line_3"
							name="address_line_3"
							value = "{{$address_line_3}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div> -->
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Country</label>
				<div class="col-sm-10">
					<input 	id="country"
							name="country"
							value = "{{$country}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Postal Code</label>
				<div class="col-sm-10">
					<input 	id="postal_code"
							name="postal_code"
							value = "{{$postal_code}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="email">Postal Code</label>
				<div class="col-sm-10">
					<input 	id="postal_code"
							name="postal_code"
							value = "{{$postal_code}}"
							type="text"
							placeholder = ""
						   	class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label" for="inquiry">Inquiry</label>
				<div class="col-sm-10">
					<textarea class="form-control" id="inquiry" name="inquiry"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 form-control-label"></label>
				<div class="col-sm-10">
					
					<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>
				</div>
			</div>
		</form>
	</div>

@endsection