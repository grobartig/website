@extends($route.'.tab')
@section ('section-title', 'Properties')
@section ('tab-active-property', 'active')

@section ('section-js')
<script type="text/javascript">	
	$(document).ready(function(event){

		$("#btn-add").click(function(){
    		add();
    	})
	});

	@if(checkPermision($route.'.add-property'))
	function add(){
		
			swal({
				title: "Property",
				text: "Please type a listing code:",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				inputPlaceholder: "Write something"
			}, function (inputValue) {
				if (inputValue === false) return false;
				if (inputValue === "") {
					swal.showInputError("You need to write a listing code!");
					return false
				}
				
				//swal("Nice!", "You wrote: " + inputValue, "success");
				$.ajax({
			        url: "{{ route($route.'.properties-add') }}?customer_id={{$id}}",
			        method: 'POST',
			        data: { listing_code:inputValue},
			        success: function( response ) {
			            if ( response.status === 'success' ) {
			            	swal("Nice!", response.msg ,"success");
			            	location.reload();
			            }else if( response.status === 'erorr' ){
			            	swal("Sorry!", response.msg );
			            }else{
			            	swal("Error!", "Sorry there is an error happens. " ,"error");
			            }
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
				});

			});
	}
	@endif

	

</script>
@endsection

@section ('tab-content')
	<div>
		<div class="col-md-12">
			@if(checkPermision($route.'.add-property'))<a style="float: right;margin-bottom: 10px;margin-top: -10px;" id="btn-add" class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>@endif	
		</div>
	</div><!--.row-->
	@if(sizeof($data) > 0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th width=15%>Staff In Charge</th>
					<th>Listing Code</th>
					<th>Action</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td class="table-photo"  >
							@php($charge = staffInCharge($row) )
							@if(!empty($charge))
								
								<img src="{{ asset ($charge->staff->picture) }}" alt="" data-toggle="tooltip" data-placement="bottom" title="{{ $charge->staff->en_name }}" style="display:inline"> <b>{{ $charge->staff->en_name }}</b>
							@endif

						</td>
						<td>{{makeListingCode($row)}}</td>
						<td>{{ $row->action->en_name }}</td>
						<td>{{ $row->purchase_date }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		@if(checkIfHasRole($row->id))
	                           		<a href="{{ route('user.property.check-role', $row->id) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>
	                           		@endif
	                           		@if(checkPermision($route.'.remove-property'))<button onclick="deleteConfirm('{{ route($route.'.properties-remove', $row->id) }}', '{{ route($route.'.properties',$id) }}')" class="tabledit-edit-button btn btn-sm btn-danger" style="float: none;"><span class="fa fa-trash"></span></button>@endif
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
		<span>No Data</span>
	@endif
@endsection