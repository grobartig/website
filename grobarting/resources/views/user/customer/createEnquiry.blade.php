@extends($route.'.tab')
@section ('section-title', 'Create Need')
@section ('tab-active-need', 'active')
@section ('tab-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection

@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			// $('#form').validate({
			// 	modules : 'file',
			// 	submit: {
			// 		settings: {
			// 			inputContainer: '.form-group',
			// 			errorListClass: 'form-tooltip-error'
			// 		}
			// 	}
			// }); 
			
			//==================================== Amenties
			getAmenties();
			$("#type-id").change(function(){
				getAmenties();
			})

			//==================================== Province
			$("#province-id-1").change(function(){
				getDistricts($(this).val(), 1);
				$("#district-id-1").html('<option id="0" >Select District</option>');
				$("#commune-id-1").html('<option id="0" >Select Commune</option>');
				
			})
			$('#district-id-1').change(function(){
	    		getCommunes($(this).val(), 1);
	    		$("#commune-id-1").html('<option id="0" >Select Commune</option>');
	    		
	    	})

	    	

	    	$("#province-id-2").change(function(){
				getDistricts($(this).val(),2);
				$("#district-id-2").html('<option id="0" >Select District</option>');
				$("#commune-id-2").html('<option id="0" >Select Commune</option>');
				
				
			})
			$('#district-id-2').change(function(){
	    		getCommunes($(this).val(),2);
	    		$("#commune-id-2").html('<option id="0" >Select Commune</option>');
	    	})


		});
		
		function getAmenties(){
			type_id = $('#type-id').val();
			//Get Districts
			$.ajax({
			        url: "{{ route($route.'.amenties') }}?type_id="+type_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $('#amenties-cnt').html(response);
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		function getDistricts(province_id, target){
			
			//Get Districts
			$.ajax({
			        url: "{{ route($route.'.districts') }}?province_id="+province_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var districts = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        districts += '<option value="'+response[i].id+'" lat="'+response[i].lat+'" lng="'+response[i].lng+'"  >'+response[i].name+'</option>';
					    }
					    if(districts != ""){
					    	$('#district-id-'+target).append(districts);
					    	
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		function getCommunes(district_id, target){
			//Empty the communes
			$("#commune-id-"+target).html('<option id="0" >Select Commune</option>');

			$.ajax({
			        url: "{{ route($route.'.communes') }}?district_id="+district_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var communes = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        communes += '<option value="'+response[i].id+'" lat="'+response[i].lat+'" lng="'+response[i].lng+'"  >'+response[i].name+'</option>';
					    }
					    if(communes != ""){
					    	$('#commune-id-'+target).append(communes);
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}


		
	</script>

@endsection

@section ('tab-content')
	@include('user.layouts.error')

	@php ($action_id = 1)
	@php ($type_id = 0)
	@php ($min_price = "")
	@php ($max_price = "")
	@php ($purpose = "")
	@php ($other = "")

   	@if (Session::has('invalidData'))
        @php ($invalidData = Session::get('invalidData'))
        @php ($action_id = $invalidData['action_id'])
		@php ($type_id = $invalidData['type_id'])
		@php ($min_price = $invalidData['min_price'])
		@php ($max_price = $invalidData['max_price'])
		@php ($purpose = $invalidData['purpose'])
		@php ($other = $invalidData['other'])

	@endif
	<form id="form" action="{{ route($route.'.store-enquiry') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<input type="hidden" name="id" value="{{ $id }}">
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="kh_content">Action</label>
			<div class="col-sm-1">
				<div class="radio  margin-top-10">
					<input type="radio" name="action_id" id="radio-1" value="1" @if($action_id == 1 ) checked @endif >
					<label for="radio-1">Buy</label>
				</div>
			</div>
			<div class="col-sm-1">
				<div class="radio  margin-top-10">
					<input type="radio" name="action_id" id="radio-2" value="2" @if($action_id == 2 ) checked @endif >
					<label for="radio-2">Rent</label>
				</div>
			</div>
		</div>

		<div class="form-group row">
				<label for="type-id" class="col-sm-2 form-control-label">Type</label>
				<div class="col-sm-10">
					<select id="type-id" name="type-id" class="form-control">
						@if($type_id != 0)
							@php( $lable = DB::table('types')->find($type_id) )
							@if( sizeof($lable) == 1 )
								<option value="{{ $lable->id }}" >{{ $lable->en_name }} ({{ $lable->abbre }})</option>
							@endif
						@endif
						<option value="0" >Select Type</option>
						@foreach( $types as $row)
							@if($row->id != $type_id)
								<option value="{{ $row->id }}" >{{ $row->en_name }} ({{ $row->abbre }})</option>
							@endif
						@endforeach
					</select>
				</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Minimum Price</label>
			<div class="col-sm-10">
				<input 	id="min_price"
						name="min_price"
						value = "{{ $min_price }}"
						type="number"
						data-validation="[L>=2, L<=18, MIXED]"
						placeholder = "Type Your minimum price."
						class="form-control" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Maximum Price</label>
			<div class="col-sm-10">
				<input 	id="max_price"
						name="max_price"
						value = "{{ $max_price }}"
						type="number"
						data-validation="[L>=2, L<=18, MIXED]"
						placeholder = "Type Your maximum price."
						class="form-control" />
			</div>
		</div>

		<div class="form-group row">
				<label for="province-id" class="col-sm-2 form-control-label">Location 1</label>
				<div class="col-sm-10">
					<div class="col-sm-4">
						<select id="province-id-1" name="province-id-1" class="form-control">
							
							<option value="0" >Select Province</option>
							@foreach( $provinces as $row)
								<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}" >{{ $row->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-4">
						<select id="district-id-1" name="district-id-1" class="form-control">
							<option value="0" >Select District</option>
						</select>
					</div>
					<div class="col-sm-4">
						<select id="commune-id-1" name="commune-id-1" class="form-control">
							<option value="0" >Select Commune</option>
						</select>
					</div>
				</div>
		</div>

		<div class="form-group row">
				<label for="province-id" class="col-sm-2 form-control-label">Location 2</label>
				<div class="col-sm-10">
					<div class="col-sm-4">
						<select id="province-id-2" name="province-id-2" class="form-control">
							
							<option value="0" >Select Province</option>
							@foreach( $provinces as $row)
								<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}" >{{ $row->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-4">
						<select id="district-id-2" name="district-id-2" class="form-control">
							<option value="0" >Select District</option>
						</select>
					</div>
					<div class="col-sm-4">
						<select id="commune-id-2" name="commune-id-2" class="form-control">
							<option value="0" >Select Commune</option>
						</select>
					</div>
				</div>
		</div>

		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Purpose</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="purpose" name="purpose" class="form-control summernote "> {{$purpose}} </textarea>
					</div>	
				</div>
		</div>
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Amenties</label>
				<div id="amenties-cnt" class="col-sm-10">

				</div>
		</div>
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Other</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="other" name="other" class="form-control summernote "> {{$other}} </textarea>
					</div>	
				</div>
		</div>

	
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>	
			</div>
		</div>
	</form>
@endsection