@extends($route.'.main')
@section ('section-css')
	@yield ('tab-css')
@endsection

@section ('section-js')
	@yield ('tab-js')
@endsection

@section ('section-content')
	
			
	<section class="tabs-section">
		<div class="tabs-section-nav tabs-section-nav-icons">
			<div class="tbl">
				<ul class="nav" role="tablist">
					@if(checkPermision($route.'.edit'))
					<li class="nav-item">
						
						<a class="nav-link @yield ('tab-active-edit')" onclick="window.location.href='{{ route($route.'.edit', $id) }}'" href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-user"></i> Overview
							</span>
						</a>
					</li>
					@endif
					@if(checkPermision($route.'.enquiries'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-need')" onclick="window.location.href='{{ route($route.'.enquiries', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-dot-circle-o"></i> Property Enquiries
							</span>
						</a>
					</li>
					@endif
					
					@if(checkPermision($route.'.inquiries'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-enquiry')" onclick="window.location.href='{{ route($route.'.inquiries', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-comment"></i> Property Inquiries
							</span>
						</a>
					</li>
					@endif
					@if(checkPermision($route.'.notes'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-note')" onclick="window.location.href='{{ route($route.'.notes', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-sticky-note-o"></i> Notes
							</span>
						</a>
					</li>
					@endif
					@if(checkPermision($route.'.properties'))
					<li class="nav-item">
						<a class="nav-link @yield ('tab-active-property')" onclick="window.location.href='{{ route($route.'.properties', $id) }}' " href="#" role="tab" data-toggle="tab">
							<span class="nav-link-in">
								<i class="fa fa-building"></i> Properties
							</span>
						</a>
					</li>
					@endif
					
				</ul>
			</div>
		</div><!--.tabs-section-nav-->

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active">
				<div id="tab-content-cnt" class="container-fluid">
					@yield ('tab-content')
				</div>
			</div>
		</div><!--.tab-content-->
	</section><!--.tabs-section-->
				
	


@endsection