@extends($route.'.tab')
@section ('section-title', 'Needs')
@section ('tab-active-need', 'active')
@section ('tab-css')
	
@endsection

@section ('imageuploadjs')
   
@endsection

@section ('tab-js')
	
@endsection

@section ('tab-content')
	<div>
		<div class="col-md-12">
			@if(checkPermision($route.'.create-enquiry'))<a style="float: right;margin-bottom: 10px;margin-top: -10px;" href="{{ route($route.'.create-enquiry',$id) }}"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>	@endif
		</div>
	</div><!--.row-->
	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Action</th>
					<th>Type</th>
					<th>Minimum Price</th>
					<th>Maximum Price</th>
					<th>Location</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>@if(!empty($row->action->en_name)) {{ $row->action->en_name }} @endif</td>
						<td> @if(!empty($row->type->en_name)) {{ $row->type->en_name }} @endif</td>
						<td>{{ $row->min_price }}</td>
						<td>{{ $row->max_price }}</td>
						<td> @if(!empty($row->customerNeedLocation1->province->en_name)) {{ $row->customerNeedLocation1->province->en_name }} @endif</td>
						<td>{{ $row->updated_at }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		@if(checkPermision($route.'.edit-enquiry'))<a href="{{ route($route.'.edit-enquiry', ['id'=>$id,'note_id'=>$row->id]) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>@endif
	                           		@if(checkPermision($route.'.trash-enquiry'))<a href="#" onclick="deleteConfirm('{{ route($route.'.trash-enquiry', $row->id) }}', '{{ route($route.'.enquiries',$id) }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></a>@endif
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	No Data
	@endif
@endsection