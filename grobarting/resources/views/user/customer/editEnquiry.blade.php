@extends($route.'.tab')
@section ('section-title', 'Overview')
@section ('tab-active-need', 'active')
@section ('tab-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection


@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			});

			//==================================== Amenties
			getAmenties();
			$("#type-id").change(function(){
				getAmenties();
			})
			//=========================== District 
			getDistricts(1);
			getDistricts(2);

			$("#province-id-1").change(function(){
				getDistricts(1);
				$("#district-id-1").html('<option id="0" >Select District</option>');
				$("#commune-id-1").html('<option id="0" >Select Commune</option>');
			})
			$('#district-id-1').change(function(){
	    		getCommunes(1);
	    		$("#commune-id-1").html('<option id="0" >Select Commune</option>');
	    		
	    	})

	 

	    	$("#province-id-2").change(function(){
				getDistricts(2);
				$("#district-id-2").html('<option id="0" >Select District</option>');
				$("#commune-id-2").html('<option id="0" >Select Commune</option>');
			
			})
			$('#district-id-2').change(function(){
	    		getCommunes(2);
	    		$("#commune-id-2").html('<option id="0" >Select Commune</option>');
	    		
	    	})


		});
		
		function getAmenties(){
			type_id = $('#type-id').val();

			//Get Districts
			$.ajax({
			        url: "{{ route($route.'.edit-amenties') }}?customer_need_id={{$need_id}}&type_id="+type_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			          $('#amenties-cnt-edit').html(response);
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		function getDistricts(target){
			province_id = $('#province-id-'+target).val();
			//Get Districts
			$.ajax({
			        url: "{{ route($route.'.districts') }}?province_id="+province_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var districts = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        districts += '<option value="'+response[i].id+'" lat="'+response[i].lat+'" lng="'+response[i].lng+'"  >'+response[i].name+'</option>';
					    }
					    if(districts != ""){
					    	$('#district-id-'+target).append(districts);
					    	
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}

		function getCommunes(target){
			//Empty the communes
			district_id = $('#district-id-'+target).val();

			$("#commune-id-"+target).html('<option id="0" >Select Commune</option>');

			$.ajax({
			        url: "{{ route($route.'.communes') }}?district_id="+district_id,
			        type: 'GET',
			        data: {},
			        success: function( response ) {
			           var communes = '';
			            var i;
					    var length = response.length;
					    for (i = 0; i < length; i++) {
					        communes += '<option value="'+response[i].id+'" lat="'+response[i].lat+'" lng="'+response[i].lng+'"  >'+response[i].name+'</option>';
					    }
					    if(communes != ""){
					    	$('#commune-id-'+target).append(communes);
					    }
					    
			        },
			        error: function( response ) {
			           swal("Error!", "Sorry there is an error happens. " ,"error");
			        }
						
			});
		}


		
	</script>

@endsection

@section ('tab-content')
	<br />
	<div>
		<div class="col-md-12">
			<a style="float: right;margin-bottom: 10px;margin-top: -10px;" href="{{ route($route.'.create-enquiry',$id) }}"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>	
		</div>
	</div><!--.row-->
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update-enquiry') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $id }}">
		<input type="hidden" name="need_id" value="{{ $need_id }}">
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="kh_content">Action</label>
			<div class="col-sm-1">
				<div class="radio  margin-top-10">
					<input type="radio" name="action_id" id="radio-1" value="1" @if(!empty($data->action_id)) @if($data->action_id == 1 ) checked @endif @endif >
					<label for="radio-1">Buy</label>
				</div>
			</div>
			<div class="col-sm-1">
				<div class="radio  margin-top-10">
					<input type="radio" name="action_id" id="radio-2" value="2" @if(!empty($data->action_id))  @if($data->action_id == 2 ) checked @endif @endif >
					<label for="radio-2">Rent</label>
				</div>
			</div>
		</div>
		<div class="form-group row">
				<label for="type-id" class="col-sm-2 form-control-label">Type</label>
				<div class="col-sm-10">
					<select id="type-id" name="type-id" class="form-control">
						
						@foreach ($types as $row)
                          <option value="{{ $row->id }}" @if(!empty($data->type_id)) <?php if($data->type_id==$row->id){ echo "selected"; }else{ echo""; } ?> @endif > {{ $row->en_name }} ({{ $row->abbre }})</option>
                        @endforeach
                        <option value="0">Select Type</option>
					</select>
				</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Minimum Price</label>
			<div class="col-sm-10">
				<input 	id="min_price"
						name="min_price"
						value = "{{ $data->min_price }}"
						type="number"
						placeholder = "Type Your minimum price."
						class="form-control" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Maximum Price</label>
			<div class="col-sm-10">
				<input 	id="max_price"
						name="max_price"
						value = "{{ $data->max_price }}"
						type="number"
						placeholder = "Type Your maximum price."
						class="form-control" />
			</div>
		</div>
		
		<input type="hidden" name="customer_need_location1" value="{{ $data->customerNeedLocation1->id }}">
		<div class="form-group row">
				<label for="province-id" class="col-sm-2 form-control-label">Location 1</label>
				<div class="col-sm-10">
					<div class="col-sm-4">
						<select id="province-id-1" name="province-id-1" class="form-control">
							@php($location1 = $data->customerNeedLocation1)
							<option value="{{ $location1->province_id }}" >{{ $location1->province->en_name }}</option>
							
							@foreach( $provinces as $row)
								@if($row->id != $location1->province_id)
									<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}" >{{ $row->name }}</option>
								@endif
							@endforeach

						</select>
					</div>

					<div class="col-sm-4">
						<select id="district-id-1" name="district-id-1" class="form-control">
							@if( ! is_null($data->customerNeedLocation1->district_id) )
								<option value="{{ $data->customerNeedLocation1->district_id }}" lat= "{{ $data->customerNeedLocation1->district->lat}}" lng= "{{ $data->customerNeedLocation1->district->lng}}" >{{ $data->customerNeedLocation1->district->en_name}}</option>
							@else
								<option value="0" lat= "0" lng= "" >Select District</option>
							@endif
							
						</select>
					</div>
					<div class="col-sm-4">
						<select id="commune-id-1" name="commune-id-1" class="form-control">
							@if( ! is_null($data->customerNeedLocation1->commune_id) )
								<option value="{{ $data->customerNeedLocation1->commune_id }}" lat= "{{ $data->customerNeedLocation1->commune->lat}}" lng= "{{ $data->customerNeedLocation1->commune->lng}}" >{{ $data->customerNeedLocation1->commune->en_name }}</option>
							@else
								<option value="0" lat= "0" lng= "" >Select Commune</option>
							@endif

							
						</select>
					</div>
					
				</div>


		</div>
		<input type="hidden" name="customer_need_location2" value="{{ $data->customerNeedLocation2->id }}">
		<div class="form-group row">
				<label for="province-id" class="col-sm-2 form-control-label">Location 2</label>
				<div class="col-sm-10">
					<div class="col-sm-4">
						<select id="province-id-2" name="province-id-2" class="form-control">
							@php($location2 = $data->customerNeedLocation2)
							@if(!is_null($location2->province_id))
								<option value="{{ $location2->province_id }}" >{{ $location2->province->en_name }}</option>
							@else
								<option value="0" >Select Province</option>
							@endif
							
							
							@foreach( $provinces as $row)
								@if($row->id != $location2->province_id)
									<option value="{{ $row->id }}" lat= "{{ $row->lat }}" lng= "{{ $row->lng }}" >{{ $row->name }}</option>
								@endif
							@endforeach

						</select>
					</div>

					<div class="col-sm-4">
						<select id="district-id-2" name="district-id-2" class="form-control">
							@if( ! is_null($data->customerNeedLocation2->district_id) )
								<option value="{{ $data->customerNeedLocation2->district_id }}" lat= "{{ $data->customerNeedLocation2->district->lat}}" lng= "{{ $data->customerNeedLocation2->district->lng}}" >{{ $data->customerNeedLocation2->district->en_name}}</option>
							@else
								<option value="0" lat= "0" lng= "" >Select District</option>
							@endif
							
							
						</select>
					</div>
					<div class="col-sm-4">
						<select id="commune-id-2" name="commune-id-2" class="form-control">
							@if( ! is_null($data->customerNeedLocation2->commune_id) )
								<option value="{{ $data->customerNeedLocation2->commune_id }}" lat= "{{ $data->customerNeedLocation2->commune->lat}}" lng= "{{ $data->customerNeedLocation2->commune->lng}}" >{{ $data->customerNeedLocation2->commune->en_name }}</option>
							@else
								<option value="0" lat= "0" lng= "" >Select Commune</option>
							@endif

							
						</select>
					</div>
					
				</div>

				
		</div>

		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Purpose</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="purpose" name="purpose" class="form-control summernote ">{{ $data->purpose }} </textarea>
					</div>	
				</div>
		</div>
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Amenties</label>
				<div id="amenties-cnt-edit" class="col-sm-10">

				</div>
		</div>
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Other</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="other" name="other" class="form-control summernote ">{{ $data->other }} </textarea>
					</div>	
				</div>
		</div>
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				@if(checkPermision($route.'.update-enquiry'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
				@if(checkPermision($route.'.trash-enquiry'))<button type="button" onclick="deleteConfirm('{{ route($route.'.trash-enquiry', $data->id) }}', '{{ route($route.'.enquiries',$id) }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>@endif
			</div>
		</div>
	</form>
@endsection