@extends($route.'.tab')
@section ('section-title', 'Overview')
@section ('tab-active-note', 'active')
@section ('tab-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection


@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			

		}); 

		function is_notified(){
			val 	= $('#is_notified').val();
			if(val == 0){
				$('#is_notified').val(1);
				$(".date-cnt").show();
			}else{
				$('#is_notified').val(0);
				$(".date-cnt").hide();
			}
		}
		
	</script>

@endsection

@section ('tab-content')
	<br />
	<div>
		<div class="col-md-12">
			@if(checkPermision($route.'.create-note'))<a style="float: right;margin-bottom: 10px;margin-top: -10px;" href="{{ route($route.'.create-note',$id) }}"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>	@endif
		</div>
	</div><!--.row-->
	@include('user.layouts.error')
	<form id="form" action="{{ route($route.'.update-note', $id) }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('POST') }}
		<input type="hidden" name="id" value="{{ $id }}">
		<input type="hidden" name="note_id" value="{{ $note_id }}">
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" >Note</label>
			<div class="col-sm-10">
				<div class="summernote-theme-1">
					<textarea id="note" name="note" class="form-control summernote ">{{$data->note}} </textarea>
				</div>	
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="">Notify</label>
			<div class="col-sm-1">
				<div class="checkbox-toggle margin-top-10">
					<input id="is_notified-status" type="checkbox"  @if($data->is_notified ==1 ) checked @endif>
					<label onclick="is_notified()" for="is_notified-status" ></label>
				</div>
				<input type="hidden" name="is_notified" id="is_notified" value="{{ $data->is_notified }}">
			</div>
			<div class="col-sm-3 date-cnt" @if($data->is_notified == 0 ) style="display:none" @endif >
				<div class="form-group">
					<div id="from-cnt" class='input-group date'>
						<input id="date" name="date" type='text' class="form-control" value="{{ $data->date }}" placeholder="Date"  />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				@if(checkPermision($route.'.update-note'))<button type="submit" class="btn btn-success"> <fa class="fa fa-cog"></i> Update</button>@endif
				@if(checkPermision($route.'.trash-note'))<button type="button" onclick="deleteConfirm('{{ route($route.'.trash-note', $data->id) }}', '{{ route($route.'.notes',$id) }}')" class="btn btn-danger"> <fa class="fa fa-trash"></i> Delete</button>@endif
			</div>
		</div>
	</form>
@endsection