@if(count($data)>0)
<table id="pop-table" class="table pop">
	<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{ $row->code }}</td><td>{{ $row->name }}</td><td width=8%><i onclick="addToList({{$row->id}})" class="fa fa-plus"></i></td>
			</tr>
		@endforeach
		
	</tbody>
</table>
@else
	No Data Avaiable
@endif
