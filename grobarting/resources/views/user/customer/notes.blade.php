@extends($route.'.tab')
@section ('section-title', 'Note')
@section ('tab-active-note', 'active')
@section ('tab-css')
	
@endsection

@section ('imageuploadjs')
   
@endsection

@section ('tab-js')
	
@endsection

@section ('tab-content')
	
	<div>
		<div class="col-md-12">
			@if(checkPermision($route.'.create-note'))<a style="float: right;margin-bottom: 10px;margin-top: -10px;" href="{{ route($route.'.create-note',$id) }}"  class="tabledit-delete-button btn btn-sm btn-primary" style="float: none;"><span class="fa fa-plus"></span></a>	@endif
		</div>
	</div><!--.row-->
	@if(count($data)>0)
	<div class="table-responsive">
		<table id="table-edit" class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Note</th>
					<th>Notifiy</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				@php ($i = 1)
				@foreach ($data as $row)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $row->note }}</td>
						<td>
								<div class="checkbox-toggle">
							        <input  type="checkbox" id="status-{{ $row->id }}" @if ($row->is_notified == 1) checked data-value="1" @else data-value="0" @endif >
							        <label for="status-xx"></label>
						        </div>
							</td>
						<td>{{ $row->date }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
	                           	<div class="btn-group btn-group-sm" style="float: none;">
	                           		@if(checkPermision($route.'.edit-note'))<a href="{{ route($route.'.edit-note', ['id'=>$id,'note_id'=>$row->id]) }}" class="tabledit-edit-button btn btn-sm btn-success" style="float: none;"><span class="fa fa-eye"></span></a>@endif
	                           		@if(checkPermision($route.'.trash-note'))<a href="#" onclick="deleteConfirm('{{ route($route.'.trash-note', $row->id) }}', '{{ route($route.'.notes',$id) }}')" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><span class="glyphicon glyphicon-trash"></span></a>@endif
	                           	</div>
	                       </div>
	                    </td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div >
	@else
	No Notes
	@endif
@endsection