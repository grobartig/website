@extends($route.'.tab')
@section ('section-title', 'Create Note')
@section ('tab-active-note', 'active')
@section ('tab-css')
	<style type="text/css">
		.margin-top-10{
			margin-top:10px;
		}
		
	</style>
@endsection

@section ('tab-js')
	<script type="text/JavaScript">
		$(document).ready(function(event){
		
			$('#form').validate({
				modules : 'file',
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			}); 
			
		}); 

		function is_notified(){
			val 	= $('#is_notified').val();
			if(val == 0){
				$('#is_notified').val(1);
				$(".date-cnt").show();
			}else{
				$('#is_notified').val(0);
				$(".date-cnt").hide();
			}
		}
		
	</script>

@endsection

@section ('tab-content')
	@include('user.layouts.error')

	@php ($note = "")
	@php ($is_notified = 0)
	@php ($date = "")

   	
	<form id="form" action="{{ route($route.'.store-note') }}" name="form" method="POST"  enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<input type="hidden" name="id" value="{{ $id }}">
		
		<div class="form-group row">
				<label class="col-sm-2 form-control-label" >Note</label>
				<div class="col-sm-10">
					<div class="summernote-theme-1">
						<textarea id="note" name="note" class="form-control summernote "> </textarea>
					</div>	
				</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label" for="kh_content">Notify</label>
			<div class="col-sm-1">
				<div class="checkbox-toggle margin-top-10">
					<input id="is_notified-status" type="checkbox"  >
					<label onclick="is_notified()" for="is_notified-status"></label>
				</div>
				<input type="hidden" name="is_notified" id="is_notified" value="0">
			</div>

			<div class="col-sm-3 date-cnt" @if($is_notified == 0 ) style="display:none" @endif >
				<div class="form-group">
					<div id="from-cnt" class='input-group date'>
						<input id="date" name="date" type='text' class="form-control" value="{{ $date }}" placeholder="Date"  />
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 form-control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-success"> <fa class="fa fa-plus"></i> Create</button>	
			</div>
		</div>
	</form>
@endsection