@extends($route.'.main')

@section ('display-btn-add-new', 'display:none')
@section ('section-css')
	<link rel="stylesheet" href="{{ asset ('public/user/css/lib/lobipanel/lobipanel.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('public/user/css/lib/jqueryui/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('public/user/css/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('public/user/css/main.css')}}">


@endsection
@section ('section-js')

	<script type="text/javascript" src="{{ asset ('public/user/js/lib/jqueryui/jquery-ui.min.js')}}"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<script type="text/javascript" src="{{ asset ('public/user/js/lib/d3/d3.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset ('public/user/js/lib/charts-c3js/c3.min.js')}}"></script>
	

	<script>
		$(document).ready(function() {
			$('.panel').lobiPanel({
				sortable: true
			});

			 var donutChart = c3.generate({
		        bindto: '#donut-chart',
		        data: {
		            columns: [
		                @foreach($types as $row)
		                ['{{$row->name}}', {{$row->num_of_properties_count}}],
		                @endforeach
		                
		            ],
		            type : 'donut'
		        },
		        donut: {
		            title: "Properties by Type"
		        }
		    });

			$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
				$('.dahsboard-column').matchHeight();
			});
		});
	</script>
@endsection

@section ('section-content')

	<div class="container-fluid">
	        <div class="row">
	           
	            <div class="col-xl-6">
	                <div class="row">
	                    <div class="col-sm-6">
	                        <article class="statistic-box red">
	                            <div>
	                                <div class="number">{{count($properties)}}</div>
	                                <div class="caption"><div>Total Properties</div></div>
	                                <div class="percent">
	                                   
	                                </div>
	                            </div>
	                        </article>
	                    </div><!--.col-->
	                    <div class="col-sm-6">
	                        <article class="statistic-box purple">
	                            <div>
	                                <div class="number">{{count($properties_sell)}}</div>
	                                <div class="caption"><div>Sell</div></div>
	                                <div class="percent">
	                                   
	                                </div>
	                            </div>
	                        </article>
	                    </div><!--.col-->
	                    <div class="col-sm-6">
	                        <article class="statistic-box yellow">
	                            <div>
	                                <div class="number">{{count($properties_rent)}}</div>
	                                <div class="caption"><div>Rent</div></div>
	                                <div class="percent">
	                                   
	                                </div>
	                            </div>
	                        </article>
	                    </div><!--.col-->
	                    <div class="col-sm-6">
	                        <article class="statistic-box green">
	                            <div>
	                                <div class="number">{{count($properties_rent_sell)}}</div>
	                                <div class="caption"><div>Sell or Rent</div></div>
	                                <div class="percent">
	                                   
	                                </div>
	                            </div>
	                        </article>
	                    </div><!--.col-->
	                </div><!--.row-->
	            </div><!--.col-->

	            <div class="col-xl-6">
	               
	                    <div class="card-block">
	                        <div id="donut-chart"></div>
	                    </div>
	               
	            </div><!--.col-->
	        </div><!--.row-->
	
	        <!-- <div class="row">
	            <div class="col-xl-6 dahsboard-column">
	                <section class="box-typical box-typical-dashboard panel panel-default">
	                    <header class="box-typical-header panel-heading">
	                        <h3 class="panel-title">Properties by Provinces</h3>
	                    </header>
	                    <div class="box-typical-body panel-body">
	                        <table class="tbl-typical">
	                            <tr>
	                                <th align="center"><div>N</div></th>
	                                <th align="center"><div>Province</div></th>
	                                <th align="center"><div>Num Of Properties</div></th>
	                            </tr>
	                            @php($i = 0)
	                            @foreach($provinces as $row)
	                            <tr>
	                            	<td align="center">{{$i++}}</td>
	                                <td align="center">{{$row->en_name}}</td>
	                                <td align="center">{{$row->num_of_properties_count}}</td>
	                            </tr>
	                            @endforeach
	                        </table>
	                    </div>
	                </section>
	                
	            </div>
	            <div class="col-xl-6 dahsboard-column">
	                <section class="box-typical box-typical-dashboard panel panel-default">
	                    <header class="box-typical-header panel-heading">
	                        <h3 class="panel-title">Properties by Agents</h3>
	                    </header>
	                    <div class="box-typical-body panel-body">
	                        <div class="contact-row-list">
	                        	@foreach($users as $row)
	                            <article class="contact-row">
	                                <div class="user-card-row">
	                                    <div class="tbl-row">
	                                        <div class="tbl-cell tbl-cell-photo">
	                                            <a href="index.html#">
	                                                <img src="{{asset($row->picture)}}" alt="">
	                                            </a>
	                                        </div>
	                                        <div class="tbl-cell">
	                                            <p class="user-card-row-name">{{$row->en_name}}</p>
	                                            <p class="user-card-row-mail">{{$row->email}}</p>
	                                        </div>
	                                        <div class="tbl-cell tbl-cell-status">{{$row->num_of_properties_count}}</div>
	                                    </div>
	                                </div>
	                            </article>
	                            @endforeach
	                        </div>
	                    </div>
	                </section>
	            </div>
	        </div> -->
	    </div>
@endsection